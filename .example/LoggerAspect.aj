aspect LoggerAspect{

  public void log(String message){
    System.out.println("[LOG] "+message);
  }

  pointcut hello() :
    execution(* *.hello(..)) ;

  pointcut bye() :
    execution(* *.bye(..)) ;    

  before() : hello() {
    log("Entering hello method!");
  }

  after() : hello() {
    log("Exiting hello method!");
  }
  
  void around() : bye() {
  	log("Entering bye method!");
  	proceed();
  	log("Exiting bye method!");
  }
}