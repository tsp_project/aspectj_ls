package it.unimi.di.langserver;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.eclipse.lsp4j.CompletionItem;
import org.junit.jupiter.api.Test;

import it.unimi.di.autocompletion.AspectJParserIdentifierListener;
import it.unimi.di.autocompletion.AspectJParserVariableListener;


public class ParserProxyTests {

    @Test
    public void findVariablesTest(){

        Object variables[] = {"variable1", "variable2", "variable3"};

        String aspect = String.format(
            "aspect MyAspect { String %s = 5; int %s = 5; AnotherType %s = null }",
            variables);


        ParserProxy parserProxy = ParserProxy.getInstance();

        AspectJParserVariableListener variableListener = new AspectJParserVariableListener();
        List<ParseTreeListener> listeners = new ArrayList<>();
        listeners.add(variableListener);

        parserProxy.parse(aspect, listeners);

        List<String> foundVariables = new ArrayList<>();
        for(CompletionItem ci : variableListener.getCompletionItems()){
            foundVariables.add( ci.getInsertText() );
        }
        assertEquals(foundVariables.size(), variables.length);
        for(Object var : variables){
            assertTrue(foundVariables.contains(var)); 
        } 
    }


    @Test
    public void findIdentifiersTest(){

        String aspect = "aspect MyAspect { String var1 = \"hello\"; Point var3 = new Point(); int var2 = 5;  }";

        ParserProxy parserProxy = ParserProxy.getInstance();

        List<ParseTreeListener> listeners = new ArrayList<>();

        AspectJParserIdentifierListener identifierListener = new AspectJParserIdentifierListener();
        listeners.add(identifierListener);

        parserProxy.parse(aspect, listeners);

        List<String> foundIdentifiers = new ArrayList<>();
        for(CompletionItem ci : identifierListener.getCompletionItems()){
            foundIdentifiers.add( ci.getInsertText() );
        }
        assertEquals(foundIdentifiers.size(), 6);
        assertTrue(foundIdentifiers.contains("MyAspect"));
        assertTrue(foundIdentifiers.contains("String"));
        assertTrue(foundIdentifiers.contains("var1"));
        assertTrue(foundIdentifiers.contains("Point"));
        assertTrue(foundIdentifiers.contains("var3"));
        assertTrue(foundIdentifiers.contains("var2"));
    }
}
