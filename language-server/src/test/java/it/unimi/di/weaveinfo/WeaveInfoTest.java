package it.unimi.di.weaveinfo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.lsp4j.TextDocumentItem;
import org.junit.jupiter.api.Test;

public class WeaveInfoTest {

    @Test
    public void simpleAdviceWeaveInfoTest() throws IOException{
        String helloLoggerAspectString =
            "aspect HelloLoggerAspect{                          \n"+
            "  pointcut hello() :                               \n"+
            "    execution(* *.hello(..)) ;                     \n"+
            "                                                   \n"+
            "  before() : hello() {                             \n"+
            "    System.out.println(\"Entering hello method\"); \n"+
            "  }                                                \n"+
            "                                                   \n"+
            "  after() : hello() {                              \n"+
            "    System.out.println(\"Exiting hello method\");  \n"+
            "  }                                                \n"+
            "}                                                  \n";
        TextDocumentItem helloLoggerAspect = new TextDocumentItem( "file:///home/project/src/HelloLoggerAspect.aj", "aj", 0, helloLoggerAspectString);

        String simpleClassString =
            "public class SimpleClass{           \n"+
            "  public void hello(){              \n"+
            "    System.out.println(\"Hello!\"); \n"+
            "  }                                 \n"+
            "  public void bye(){                \n"+
            "    System.out.println(\"Bye!\");   \n"+
            "  }                                 \n"+
            "}                                   \n";
        TextDocumentItem simpleClass = new TextDocumentItem( "file:///home/project/src/SimpleClass.java", "java", 0, simpleClassString);

        List<TextDocumentItem> documents = new ArrayList<>();
        documents.add(helloLoggerAspect);
        documents.add(simpleClass);

        List<AdviceWeaveInfo> expected = new ArrayList<>();
        expected.add( new AdviceWeaveInfo("method-execution(void SimpleClass.hello())", "before", "file:///home/project/src/HelloLoggerAspect.aj", 5, "file:///home/project/src/SimpleClass.java", 2) );
        expected.add( new AdviceWeaveInfo("method-execution(void SimpleClass.hello())", "after",  "file:///home/project/src/HelloLoggerAspect.aj", 9, "file:///home/project/src/SimpleClass.java", 2) );

        int collectedWeaveInfos = WeaveInfoCalculator.calculate(documents);
        Set<AdviceWeaveInfo> weaveInfos = AdviceWeaveInfoCollector.getInstance().getAllAdviceWeaveInfos();

        assertEquals(expected.size(), collectedWeaveInfos);
        assertEquals(expected.size(), weaveInfos.size());
        assertTrue(weaveInfos.containsAll(expected));
    }

    @Test
    public void isAboutAdviceTest(){
        WeaveInfo wi = new AdviceWeaveInfo("", "", "", 0, "", 0);
        assertTrue(wi.isAboutAdvice());
    }
}
