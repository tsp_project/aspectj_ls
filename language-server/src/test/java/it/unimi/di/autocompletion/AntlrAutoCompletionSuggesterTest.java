package it.unimi.di.autocompletion;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Set;

import org.eclipse.lsp4j.Position;

import org.junit.jupiter.api.Test;

public class AntlrAutoCompletionSuggesterTest {
    
    @Test
    public void voidSuggestionsTest(){
        String program = "";
        AntlrAutoCompletionSuggester suggester = new AntlrAutoCompletionSuggester("AspectJLanguageSupport");

        Set<String> suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'aspect'"));
        assertTrue(suggestions.contains("'public'"));
        assertTrue(suggestions.contains("'private'"));
    }

    @Test void fieldDeclarationTest(){
        String program ;
        AntlrAutoCompletionSuggester suggester = new AntlrAutoCompletionSuggester("AspectJLanguageSupport");
        Set<String> suggestions;
        
        program = "aspect MyApsect { int var = ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("IntegerLiteral"));
        assertTrue(suggestions.contains("StringLiteral"));

        program = "aspect MyApsect { private int var = ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("IntegerLiteral"));
        assertTrue(suggestions.contains("StringLiteral"));
    }

    @Test
    public void aspectSuggestionsTest(){
        String program = "";
        AntlrAutoCompletionSuggester suggester = new AntlrAutoCompletionSuggester("AspectJLanguageSupport");
        Set<String> suggestions;
        
        program += "aspect ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("Identifier"));

        program += "AspectName ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'{'"));
        assertTrue(suggestions.contains("'implements'"));
        assertTrue(suggestions.contains("'extends'"));

        program += "{ ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'{'"));
        assertTrue(suggestions.contains("'pointcut'"));
        assertTrue(suggestions.contains("'boolean'"));
        assertTrue(suggestions.contains("'int'"));
        assertTrue(suggestions.contains("'final'"));

        program += "private ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'int'"));

        program += "int ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("Identifier"));
        
        program += "variable ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'='"));
        
        program += "= ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("IntegerLiteral"));

        program += "5 ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("';'"));
        
        program += "; ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'}'"));

        program += "String ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("Identifier"));
    }

    @Test
    public void bracketsSuggestionsTest(){
        String program;
        AntlrAutoCompletionSuggester suggester = new AntlrAutoCompletionSuggester("AspectJLanguageSupport");
        Set<String> suggestions;


        program = "aspect AspectName { ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'}'"));

        program = "aspect AspectName { {";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'}'"));

        program = "aspect AspectName { int var = array[5";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("']'"));
        
        program = "aspect AspectName { int var = 5+(4";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("')'"));
    }
    
    @Test
    public void brackets2SuggestionsTest(){
        String program;
        AntlrAutoCompletionSuggester suggester = new AntlrAutoCompletionSuggester("AspectJLanguageSupport");
        Set<String> suggestions;


        program = "aspect AspectName { {";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'}'"));

        program = "aspect AspectName { int var = array[5+3";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("']'"));
        
        /*program = "aspect AspectName { int var = 5+(4*3";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("')'"));*/
    }

    @Test
    public void truncateTextAtCaretTest(){
        String text = "abc \nabcdef \n123";

        assertEquals(
            "",
            AntlrAutoCompletionSuggester.truncateTextAtCaret(text, new Position(0, 0))
        );
        assertEquals(
            "abc",
            AntlrAutoCompletionSuggester.truncateTextAtCaret(text, new Position(0, 3))
        );
        assertEquals(
            "abc \nabc",
            AntlrAutoCompletionSuggester.truncateTextAtCaret(text, new Position(1, 3))
        );
        assertEquals(
            "abc \nabcdef \n12",
            AntlrAutoCompletionSuggester.truncateTextAtCaret(text, new Position(2, 2))
        );
        assertEquals(
            "abc \nabcdef \n123",
            AntlrAutoCompletionSuggester.truncateTextAtCaret(text, new Position(2, 3))
        );
    }

    @Test
    public void methodDeclarationTest(){
        String program;
        AntlrAutoCompletionSuggester suggester = new AntlrAutoCompletionSuggester("AspectJLanguageSupport");
        Set<String> suggestions;

        program  = "aspect MyAspect { private int var = 5;\n";
        program += "public ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'static'"));
        assertTrue(suggestions.contains("'void'"));

        program += "static ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'void'"));
        assertTrue(suggestions.contains("'int'"));

        program += "void ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("Identifier"));

        program += "meth0d ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'('"));

        program += "( ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'int'"));
        assertTrue(suggestions.contains("')'"));

        program += "int ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("Identifier"));

        program += "param ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("')'"));

        program += ") ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'{'"));

        program += "{\n ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("Identifier"));

        program += "doSomething() ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("';'"));

        program += "; ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("'}'"));

        program += "; ";
        suggestions = suggester.suggestions(program);
        assertTrue(suggestions.contains("Identifier"));
    }
}
