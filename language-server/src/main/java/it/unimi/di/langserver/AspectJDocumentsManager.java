package it.unimi.di.langserver;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.eclipse.lsp4j.DidChangeTextDocumentParams;
import org.eclipse.lsp4j.DidCloseTextDocumentParams;
import org.eclipse.lsp4j.DidOpenTextDocumentParams;
import org.eclipse.lsp4j.TextDocumentContentChangeEvent;
import org.eclipse.lsp4j.TextDocumentItem;

import it.unimi.di.validator.AspectJValidator;

public class AspectJDocumentsManager {

    private final HashMap<String, TextDocumentItem> documents;
    private AspectJValidator validator;
    private AspectJLanguageServer languageServer;

    public AspectJDocumentsManager(AspectJLanguageServer languageServer){
        this.documents = new HashMap<>();
        this.languageServer = languageServer;
        this.validator = new AspectJValidator();
    }

    public void onOpen(DidOpenTextDocumentParams params){
        TextDocumentItem docItem = params.getTextDocument();
        String docUri = docItem.getUri();

        this.documents.put(docUri, docItem);

        // Validate only if it is an AspectJ file
        if(docUri.endsWith(".aj"))
            this.validator.validate(docUri, docItem.getText(), this.languageServer.getClient());
    }

    public void onChange(DidChangeTextDocumentParams params){
        List<TextDocumentContentChangeEvent> changes = params.getContentChanges();
        TextDocumentContentChangeEvent lastChange = changes.get(changes.size()-1);
        TextDocumentItem document = this.documents.get(params.getTextDocument().getUri());
        String text = lastChange.getText();

        document.setVersion(params.getTextDocument().getVersion());
        document.setText(text);

        // Validate only if it is an AspectJ file
        if(document.getUri().endsWith(".aj"))
            this.validator.validate(document.getUri(), text, this.languageServer.getClient());
    }
    
    public void onClose(DidCloseTextDocumentParams params){
        this.documents.remove(params.getTextDocument().getUri());
    }

    public TextDocumentItem get(String uri){
        return this.documents.get(uri);
    }

    public Collection<TextDocumentItem> getAll(){
        return this.documents.values();
    }
}
