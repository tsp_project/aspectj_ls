package it.unimi.di.langserver;

import org.eclipse.lsp4j.*;
import org.eclipse.lsp4j.jsonrpc.messages.Either;
import org.eclipse.lsp4j.services.TextDocumentService;

import it.unimi.di.autocompletion.AntlrAutoCompletionSuggester;
import it.unimi.di.autocompletion.AspectJParserCompletionListener;
import it.unimi.di.autocompletion.AspectJParserIdentifierListener;
import it.unimi.di.autocompletion.AspectJSnippetsService;
import it.unimi.di.autocompletion.CompletionItemProvider;
import it.unimi.di.weaveinfo.CodeLine;
import it.unimi.di.weaveinfo.AdviceWeaveInfoCollector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

public class AspectJTextDocumentService implements TextDocumentService {

    private AspectJSnippetsService snippetsService;
    private AspectJDocumentsManager documentsManager;
    private ParserProxy parserProxy;

    public AspectJTextDocumentService(AspectJDocumentsManager documentsManager){
        this.snippetsService = AspectJSnippetsService.getInstance();
        this.documentsManager = documentsManager;
        this.parserProxy = ParserProxy.getInstance();
    }

    private Map<String, AspectJParserCompletionListener> createParseListeners(){
        Map<String, AspectJParserCompletionListener> listeners = new HashMap<>();

        listeners.put("identifier", new AspectJParserIdentifierListener());

        return listeners;
    }

    private boolean isLiteralToken(String suggestion){
        return suggestion.charAt(0)=='\'' && suggestion.charAt(suggestion.length()-1)=='\'';
    }

    private String trimLiteralToken(String suggestion){
        return suggestion.substring(1, suggestion.length()-1);
    }

    private CompletionItem createLiteralCompletionItem(String suggestion){
        CompletionItem completionItem = new CompletionItem();
        String keyword = trimLiteralToken(suggestion);

        completionItem.setInsertText(keyword);
        completionItem.setLabel(keyword);
        completionItem.setKind(CompletionItemKind.Keyword);
        completionItem.setDetail(keyword);

        return completionItem;
    }


    @Override
    public CompletableFuture<Either<List<CompletionItem>, CompletionList>> completion(CompletionParams completionParams) {

        // Java completion not implemented for the moment
        if(completionParams.getTextDocument().getUri().endsWith(".java"))
            return null;

        List<CompletionItem> completions = new ArrayList<>();

        String docUri = completionParams.getTextDocument().getUri();
        String docText = this.documentsManager.get(docUri).getText();
        Position caretPosition = completionParams.getPosition();

        AntlrAutoCompletionSuggester completionSuggester = new AntlrAutoCompletionSuggester("AspectJLanguageSupport");
        Set<String> completionSuggestions = completionSuggester.suggestions(docText, caretPosition);

        for(String suggestion: completionSuggestions){
            if(isLiteralToken(suggestion)){
                completions.add( createLiteralCompletionItem(suggestion) );
            }
        }

        for(String keyword: snippetsService.getSnippetsKeywords()){
            if(completionSuggestions.contains(addSingleQuotes(keyword))){
                completions.addAll(snippetsService.getSnippetsCompletionItems(keyword));
            }
        }
        
        Map<String, AspectJParserCompletionListener> parseListeners = createParseListeners();

        parserProxy.parse(docText, parseListeners.values());
        
        if(completionSuggestions.contains("Identifier")){
            CompletionItemProvider identifierListener = parseListeners.get("identifier");
            completions.addAll(identifierListener.getCompletionItems());
        }

        return CompletableFuture.supplyAsync(() -> Either.forLeft(completions));
    }

    private String addSingleQuotes(String keyword) {
        return "'"+keyword+"'";
    }

    @Override
    public CompletableFuture<CompletionItem> resolveCompletionItem(CompletionItem completionItem) {
        return null;
    }

    @Override
    public CompletableFuture<Hover> hover(HoverParams params) {

        CodeLine hoverCodeLine = CodeLine.of(params.getTextDocument().getUri(), params.getPosition().getLine()+1);
        AdviceWeaveInfoCollector weaveInfoCollector = AdviceWeaveInfoCollector.getInstance();

        if(weaveInfoCollector.mapsAreSet()){
            if(weaveInfoCollector.adviceMapContainsKey(hoverCodeLine)){
                Hover response = new Hover(weaveInfoCollector.getAdviceTargets(hoverCodeLine));
                return CompletableFuture.supplyAsync(() -> response);
            }
            if(weaveInfoCollector.joinPointMapContainsKey(hoverCodeLine)){
                Hover response = new Hover(weaveInfoCollector.getJoinPointAffections(hoverCodeLine));
                return CompletableFuture.supplyAsync(() -> response);
            }
            return null;
        }else{
            Hover response = new Hover(new MarkupContent("markdown", "Press `Calculate Weave Info` button (`Ctrl+W`)."));
            return CompletableFuture.supplyAsync(() -> response);
        }
    }

    @Override
    public CompletableFuture<SignatureHelp> signatureHelp(SignatureHelpParams params) {
        // Auto-generated method stub
        return TextDocumentService.super.signatureHelp(params);
    }

    @Override
    public CompletableFuture<Either<List<? extends Location>, List<? extends LocationLink>>> definition(
            DefinitionParams params) {
        // Auto-generated method stub
        return TextDocumentService.super.definition(params);
    }

    @Override
    public CompletableFuture<List<? extends Location>> references(ReferenceParams referenceParams) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends DocumentHighlight>> documentHighlight(DocumentHighlightParams params) {
        // Auto-generated method stub
        return TextDocumentService.super.documentHighlight(params);
    }

    @Override
    public CompletableFuture<List<Either<SymbolInformation, DocumentSymbol>>> documentSymbol(
            DocumentSymbolParams params) {
        // Auto-generated method stub
        return TextDocumentService.super.documentSymbol(params);
    }

    @Override
    public CompletableFuture<List<Either<Command, CodeAction>>> codeAction(CodeActionParams params) {
        // Auto-generated method stub
        return TextDocumentService.super.codeAction(params);
    }

    @Override
    public CompletableFuture<List<? extends CodeLens>> codeLens(CodeLensParams codeLensParams) {
        return null;
    }

    @Override
    public CompletableFuture<CodeLens> resolveCodeLens(CodeLens codeLens) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> formatting(DocumentFormattingParams documentFormattingParams) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> rangeFormatting(DocumentRangeFormattingParams documentRangeFormattingParams) {
        return null;
    }

    @Override
    public CompletableFuture<List<? extends TextEdit>> onTypeFormatting(DocumentOnTypeFormattingParams documentOnTypeFormattingParams) {
        return null;
    }

    @Override
    public CompletableFuture<WorkspaceEdit> rename(RenameParams renameParams) {
        return null;
    }

    @Override
    public void didOpen(DidOpenTextDocumentParams didOpenTextDocumentParams) {
        this.documentsManager.onOpen(didOpenTextDocumentParams);
        AdviceWeaveInfoCollector.getInstance().unsetMaps();
    }

    @Override
    public void didChange(DidChangeTextDocumentParams didChangeTextDocumentParams) {
        this.documentsManager.onChange(didChangeTextDocumentParams);
        AdviceWeaveInfoCollector.getInstance().unsetMaps();
    }

    @Override
    public void didClose(DidCloseTextDocumentParams didCloseTextDocumentParams) {
        this.documentsManager.onClose(didCloseTextDocumentParams);
        AdviceWeaveInfoCollector.getInstance().unsetMaps();
    }

    @Override
    public void didSave(DidSaveTextDocumentParams didSaveTextDocumentParams) {
        // Do Nothing
    }
}
