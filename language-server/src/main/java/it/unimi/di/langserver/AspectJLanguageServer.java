package it.unimi.di.langserver;

import org.eclipse.lsp4j.CompletionOptions;
import org.eclipse.lsp4j.InitializeParams;
import org.eclipse.lsp4j.InitializeResult;
import org.eclipse.lsp4j.MessageParams;
import org.eclipse.lsp4j.MessageType;
import org.eclipse.lsp4j.ServerCapabilities;
import org.eclipse.lsp4j.TextDocumentSyncKind;
import org.eclipse.lsp4j.jsonrpc.services.JsonRequest;
import org.eclipse.lsp4j.services.LanguageClient;
import org.eclipse.lsp4j.services.LanguageClientAware;
import org.eclipse.lsp4j.services.LanguageServer;
import org.eclipse.lsp4j.services.TextDocumentService;
import org.eclipse.lsp4j.services.WorkspaceService;

import it.unimi.di.weaveinfo.WeaveInfoCalculator;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public class AspectJLanguageServer implements LanguageServer, LanguageClientAware {
    private TextDocumentService textDocumentService;
    private WorkspaceService workspaceService;
    private LanguageClient client;
    private AspectJDocumentsManager documentsManager;
    private int errorCode = 1;

    public AspectJLanguageServer() {
        this.documentsManager = new AspectJDocumentsManager(this);
        this.textDocumentService = new AspectJTextDocumentService(documentsManager);
        this.workspaceService = new AspectJWorkspaceService();
    }

    @Override
    public CompletableFuture<InitializeResult> initialize(InitializeParams initializeParams) {
        // Initialize the InitializeResult for this LS.
        final InitializeResult initializeResult = new InitializeResult(new ServerCapabilities());

        // Set the capabilities of the LS to inform the client.
        initializeResult.getCapabilities().setTextDocumentSync(TextDocumentSyncKind.Full);
        CompletionOptions completionOptions = new CompletionOptions();
        initializeResult.getCapabilities().setCompletionProvider(completionOptions);
        initializeResult.getCapabilities().setHoverProvider(true);
        return CompletableFuture.supplyAsync(()->initializeResult);
    }

    @JsonRequest
    public CompletableFuture<Void> calculateWeaveInfo() {
        try {
            int collectedWeaveInfos = WeaveInfoCalculator.calculate( documentsManager.getAll() );

            if(collectedWeaveInfos==0){
                this.client.showMessage(new MessageParams(MessageType.Warning, "No weave info found!"));
            }else{
                this.client.showMessage(new MessageParams(MessageType.Info, "Weave info calculated! "+collectedWeaveInfos+" infos found!"));
            }
        } catch (IOException e) {
            this.client.showMessage(new MessageParams(MessageType.Error, "Error during weave info calculation.\n"+e.getMessage()));
        }

        return null;
    }

    @Override
    public CompletableFuture<Object> shutdown() {
        // If shutdown request comes from client, set the error code to 0.
        errorCode = 0;
        return null;
    }

    @Override
    public void exit() {
        // Kill the LS on exit request from client.
        System.exit(errorCode);
    }

    @Override
    public TextDocumentService getTextDocumentService() {
        // Return the endpoint for language features.
        return this.textDocumentService;
    }

    @Override
    public WorkspaceService getWorkspaceService() {
        // Return the endpoint for workspace functionality.
        return this.workspaceService;
    }

    @Override
    public void connect(LanguageClient languageClient) {
        // Get the client which started this LS.
        this.client = languageClient;
    }

    public LanguageClient getClient(){
        return this.client;
    }
}
