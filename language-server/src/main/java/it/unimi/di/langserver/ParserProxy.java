package it.unimi.di.langserver;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTreeListener;

import java.util.Collection;

import it.unimi.di.parser.AspectJLexer;
import it.unimi.di.parser.AspectJParser;


public class ParserProxy {

    private static ParserProxy instance = null;

    private ParserProxy(){}

    // Double-checking Singleton
    public static ParserProxy getInstance(){
        if(instance == null){
            synchronized (ParserProxy.class) {
                if(instance == null){
                    instance = new ParserProxy();
                }
            }
        }
        return instance;
    }

    private class ParsingObjects{
        private AspectJLexer lexer;
        private TokenStream tokenStream;
        private AspectJParser parser;

        ParsingObjects(String program){
            this.lexer = new AspectJLexer(CharStreams.fromString(program));
            this.tokenStream = new CommonTokenStream(lexer);
            this.parser = new AspectJParser(tokenStream);
        }

        private AspectJLexer getLexer() {
            return this.lexer;
        }

        private TokenStream getTokenStream() {
            return this.tokenStream;
        }

        private AspectJParser getParser() {
            return this.parser;
        }
    }

    public void parse(String program, Collection<? extends ParseTreeListener> listeners){
        AspectJParser parser = new ParsingObjects(program).getParser();

        // Add listeners
        for(ParseTreeListener listener: listeners){
            parser.addParseListener(listener);
        }

        parser.aspectDeclaration();
    }

    public void parseErrors(String program, Collection<? extends ANTLRErrorListener> listeners){
        ParsingObjects parsingObjects = new ParsingObjects(program);
        AspectJLexer lexer = parsingObjects.getLexer();
        AspectJParser parser = parsingObjects.getParser();

        // Add error-listeners
        for(ANTLRErrorListener listener: listeners){
            lexer.addErrorListener(listener);
            parser.addErrorListener(listener);
        }

        parser.aspectDeclaration();
    }
}
