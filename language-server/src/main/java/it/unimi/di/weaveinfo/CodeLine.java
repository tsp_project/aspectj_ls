package it.unimi.di.weaveinfo;

public class CodeLine{

    private String fileUri;
    private Integer lineNumber;

    private CodeLine(String fileUri, Integer lineNumber) {
        this.fileUri = fileUri;
        this.lineNumber = lineNumber;
    }

    public static CodeLine of(String fileUri, Integer lineNumber){
        return new CodeLine(fileUri, lineNumber);
    }

    public String toString(){
        return String.format("%s:%d", fileUri, lineNumber);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fileUri == null) ? 0 : fileUri.hashCode());
        result = prime * result + ((lineNumber == null) ? 0 : lineNumber.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CodeLine other = (CodeLine) obj;
        if (fileUri == null) {
            if (other.fileUri != null)
                return false;
        } else if (!fileUri.equals(other.fileUri))
            return false;
        if (lineNumber == null) {
            if (other.lineNumber != null)
                return false;
        } else if (!lineNumber.equals(other.lineNumber))
            return false;
        return true;
    }
}
