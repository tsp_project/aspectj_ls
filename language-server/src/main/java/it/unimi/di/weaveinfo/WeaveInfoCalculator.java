package it.unimi.di.weaveinfo;

import java.io.IOException;
import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.aspectj.bridge.IMessage;
import org.aspectj.bridge.MessageHandler;
import org.aspectj.bridge.WeaveMessage;
import org.aspectj.tools.ajc.Main;
import org.eclipse.lsp4j.TextDocumentItem;

public class WeaveInfoCalculator {

    private static final String AJC_DIR_PREFIX = "langserver_ajc_temp_";
    private static final String ADVISES_WEAVE_MESSAGE_FORMAT = "Join point '%1' in Type '%2' (%3) advised by %4 advice from '%5' (%6)%7";

    private WeaveInfoCalculator(){};

    public static int calculate(Collection<TextDocumentItem> documents)  throws IOException{
        Path localDir = saveDocumentsLocally(documents);
        Collection<WeaveInfo> weaveInfos = compileAndGetWeaveInfos(localDir, documents);
        collectWeaveInfos(weaveInfos);
        deleteDirectoryRecursively(localDir);
        return weaveInfos.size();
    }

    private static void collectWeaveInfos(Collection<WeaveInfo> weaveInfos) {
        Map<CodeLine, List<AdviceWeaveInfo>> advicesFiles   = new HashMap<>();
        Map<CodeLine, List<AdviceWeaveInfo>> joinPointsFiles = new HashMap<>();

        for (WeaveInfo wi : weaveInfos) {
            if (wi.isAboutAdvice()) {
                AdviceWeaveInfo awi = (AdviceWeaveInfo) wi;

                if (!advicesFiles.containsKey(awi.getAdviceCodeLine()))
                    advicesFiles.put(awi.getAdviceCodeLine(), new ArrayList<>());
                if (!joinPointsFiles.containsKey(awi.getJoinPointCodeLine()))
                    joinPointsFiles.put(awi.getJoinPointCodeLine(), new ArrayList<>());

                advicesFiles.get(awi.getAdviceCodeLine()).add(awi);
                joinPointsFiles.get(awi.getJoinPointCodeLine()).add(awi);
            }
        }

        AdviceWeaveInfoCollector.getInstance().setMaps(advicesFiles, joinPointsFiles);
    }

    private static Collection<WeaveInfo> compileAndGetWeaveInfos(Path localDir, Collection<TextDocumentItem> documents) {
        Collection<WeaveInfo> result = new ArrayList<>();

        String[] arguments = new String[] {
            "-14",
            "-showWeaveInfo",
            "-sourceroots",
            localDir.toString()
        };

        Main compiler = new Main();
        MessageHandler messageHandler = new MessageHandler();
        compiler.run(arguments, messageHandler);
        IMessage[] messages = messageHandler.getMessages(IMessage.WEAVEINFO, true);

        for(IMessage mess : messages){
            if(mess instanceof WeaveMessage){
                WeaveMessage weaveMessage = (WeaveMessage) mess;
                String weaveMessageString = weaveMessage.getMessage();

                if(isAdvisesWeaveMessage(weaveMessageString)){
                    List<String> aWinfos = extractAdvisesWeaveInfos(weaveMessageString);

                    String joinPoint = aWinfos.get(0);
                    String adviceName = aWinfos.get(3);
                    String adviceFileURI = getUriFromFileName(aWinfos.get(5).split(":")[0], documents);
                    int adviceLinePosition = Integer.parseInt(aWinfos.get(5).split(":")[1]);
                    String affectedFileURI = getUriFromFileName(aWinfos.get(2).split(":")[0], documents);
                    int affectedLinePosition = Integer.parseInt(aWinfos.get(2).split(":")[1]);

                    result.add( new AdviceWeaveInfo(joinPoint,
                                        adviceName, adviceFileURI, adviceLinePosition,
                                        affectedFileURI, affectedLinePosition
                    ));
                }
            }
        }
        return result;
    }

    private static String getUriFromFileName(String fileName, Collection<TextDocumentItem> documents){
        for(TextDocumentItem doc : documents){
            if(doc.getUri().endsWith(fileName))
                return doc.getUri();
        }
        return "'URI not found'";
    }

    private static List<String> extractAdvisesWeaveInfos(String message) {
        assert(ADVISES_WEAVE_MESSAGE_FORMAT.equals(WeaveMessage.WEAVEMESSAGE_ADVISES.getMessage()));

        final String[] chunks = ADVISES_WEAVE_MESSAGE_FORMAT.replaceAll("\\(", "\\\\(").replaceAll("\\)", "\\\\)").split("%\\d");
        final String sep = "XXXXX";
        String[] split;
        List<String> result = new ArrayList<>();

        message = message.replaceFirst(chunks[0], "");
        for(int i=1; i<chunks.length; i++){
            message = message.replaceFirst(chunks[i], sep);
            split = message.split(sep);
            result.add(split[0]);
            if(split.length>1)
                message = split[1];
        }

        return result;
    }

    private static boolean isAdvisesWeaveMessage(String weaveMessageString) {
        return weaveMessageString.startsWith("Join point '");
    }

    private static Path saveDocumentsLocally(Collection<TextDocumentItem> documents) throws IOException {
        Path ajcDir = Files.createTempDirectory(AJC_DIR_PREFIX);

        for(TextDocumentItem doc : documents){
            Path originalPath = Path.of(URI.create(doc.getUri()));
            Path localPath = Path.of(ajcDir.toString(), originalPath.toString());
            Files.createDirectories(localPath.getParent());
            Files.writeString(localPath, doc.getText());
        }
        return ajcDir;
    }

    private static void deleteDirectoryRecursively(Path dirPath) {
        if(Files.exists(dirPath)){
            try {
                try (Stream<Path> walk = Files.walk(dirPath)) {
                    walk.sorted(Comparator.reverseOrder())
                        .map(Path::toFile)
                        //.peek(System.out::println)
                        .forEach(File::delete);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}