package it.unimi.di.weaveinfo;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.lsp4j.MarkupContent;

public class AdviceWeaveInfoCollector {
    private static AdviceWeaveInfoCollector instance = null;
    private Map<CodeLine, List<AdviceWeaveInfo>> adviceMap;
    private Map<CodeLine, List<AdviceWeaveInfo>> joinPointMap;

    private AdviceWeaveInfoCollector(){}

    // Double-checking Singleton
    public static AdviceWeaveInfoCollector getInstance(){
        if(instance == null){
            synchronized (AdviceWeaveInfoCollector.class) {
                if(instance == null){
                    instance = new AdviceWeaveInfoCollector();
                }
            }
        }
        return instance;
    }

    public void setMaps(Map<CodeLine, List<AdviceWeaveInfo>> adviceMap, Map<CodeLine, List<AdviceWeaveInfo>> joinPointMap){
        this.adviceMap = adviceMap;
        this.joinPointMap = joinPointMap;
    }

    public void unsetMaps(){
        this.adviceMap = null;
        this.joinPointMap = null;
    }

    public boolean mapsAreSet(){
        return adviceMap != null && joinPointMap != null;
    }

    public boolean adviceMapContainsKey(CodeLine adviceCodeLine){
        return adviceMap.containsKey(adviceCodeLine);
    }

    public boolean joinPointMapContainsKey(CodeLine joinPointCodeLine){
        return joinPointMap.containsKey(joinPointCodeLine);
    }

    /**Only for testing.
     * re-collect all the AdviceWeaveInfo(s) in a set.
     */
    Set<AdviceWeaveInfo> getAllAdviceWeaveInfos(){
        Set<AdviceWeaveInfo> result = new HashSet<>();
        for(CodeLine key : adviceMap.keySet())
            result.addAll(adviceMap.get(key));
        for(CodeLine key : joinPointMap.keySet())
            result.addAll(joinPointMap.get(key));
        return result;
    }

    private String uriToName(String uri){
        return uri.substring(uri.lastIndexOf("/")+1);
    }

    public MarkupContent getAdviceTargets(CodeLine adviceCodeLine){
        String result = "This advice affects:\n";
        for(AdviceWeaveInfo awi : adviceMap.get(adviceCodeLine))
            result += String.format("- JoinPoint `%s`, in [%s at line %d](%s#%d);%n",
                                    awi.getJoinPoint(),
                                    uriToName(awi.getJoinPointFileURI()), awi.getJoinPointLineNumber(),
                                    awi.getJoinPointFileURI(), awi.getJoinPointLineNumber());
        return new MarkupContent( "markdown", result);
    }

    public MarkupContent getJoinPointAffections(CodeLine joinPointCodeLine){
        String result = "This JoinPoint is affected by:\n";
        for(AdviceWeaveInfo awi : joinPointMap.get(joinPointCodeLine))
            result += String.format("- Advice `%s`, in [%s at line %d](%s#%d);%n",
                                    awi.getAdviceName(),
                                    uriToName(awi.getAdviceFileURI()), awi.getAdviceLineNumber(),
                                    awi.getAdviceFileURI(), awi.getAdviceLineNumber());
        return new MarkupContent( "markdown", result);
    }
}
