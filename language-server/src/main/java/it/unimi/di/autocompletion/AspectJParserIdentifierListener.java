package it.unimi.di.autocompletion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.eclipse.lsp4j.CompletionItem;

import it.unimi.di.parser.AspectJParser;

public class AspectJParserIdentifierListener extends AspectJParserCompletionListener{

    private Collection<String> identifiers;

    public AspectJParserIdentifierListener(){
        this.identifiers = new HashSet<>();
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        Vocabulary vocabulary = AspectJParser.VOCABULARY;
        int tokenType = node.getSymbol().getType();
        if( vocabulary.getDisplayName(tokenType).equals("Identifier") )
            identifiers.add(node.getText());
    }

    private CompletionItem createCompletionItem(String insertText){
        CompletionItem completionItem = new CompletionItem();
        
        completionItem.setInsertText(insertText);
        completionItem.setLabel(insertText);
        completionItem.setKind(null);
        completionItem.setDetail("Identifier");

        return completionItem;
    }    

    @Override
    public Collection<CompletionItem> getCompletionItems() {
        List<CompletionItem> result = new ArrayList<>();
        for(String identifier : identifiers)
            result.add(createCompletionItem(identifier));
        return result;
    }
    
}
