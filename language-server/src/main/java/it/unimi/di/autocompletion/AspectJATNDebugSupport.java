package it.unimi.di.autocompletion;

import java.util.List;

import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.atn.*;

import it.unimi.di.parser.AspectJParser;

public class AspectJATNDebugSupport {

    private AspectJATNDebugSupport(){}

    public static String stateName(int stateType){
        return ATNState.serializationNames.get(stateType);
    }

    public static String ruleName(int ruleIndex){
        return AspectJParser.ruleNames[ruleIndex];
    }

    public static String stateDescription(ATNState state){
        return String.format("(%d:%s:%s)", state.stateNumber, stateName(state.getStateType()), ruleName(state.ruleIndex));
    }


    public static void describeStateAndParserStack(ATNState state, ParserStack parserStack){
        /*Use to describe current ATN state and parser stack when they are not compatible.*/
        String s = "";
        s += String.format("State %s (%s) ", state.stateNumber, stateName(state.getStateType()));
        if(state.getStateType()==ATNState.BLOCK_END)
            s+= String.format("(blockStartState=%d, blockStartStateType=%s) ", ((BlockEndState)state).startState.stateNumber, stateName(((BlockEndState)state).startState.getStateType()) );
        s += String.format("was not compatible with stack %s, ", parserStack.statesStack());
        if(!parserStack.isEmpty()){
            s += String.format("where top-stack-state is: %d (%s), ", parserStack.last().stateNumber, stateName(parserStack.last().getStateType()));
            if(parserStack.last().getStateType()==ATNState.RULE_START)
                s += String.format("which corresponding rule-stop state is %d", ((RuleStartState) parserStack.last()).stopState.stateNumber);
            else if(parserStack.last().getStateType()==ATNState.BLOCK_START)
                s += String.format("which corresponding block-end state is %d", ((BasicBlockStartState) parserStack.last()).endState.stateNumber);
        }
        System.out.println(s);
    }

    public static void describeLabelIfMonitored(int label, ATNState currentState, ATNState transitionTargetState, ParserStack parserStack, List<String> labelsToMonitor ){
        /** Use to monitor a label you suspect will be excluded from suggestions. */
        Vocabulary vocabulary = AspectJParser.VOCABULARY;
        String labelName =  vocabulary.getDisplayName(label);

        if( labelsToMonitor.contains(labelName) ){
            String s = "";
            s += "EXCLUDING: "+labelName;
            s += "State: "+currentState+", type: "+ stateName( currentState.getStateType() ) +", rule: "+ruleName(currentState.ruleIndex);
            s += "target State: "+transitionTargetState+", type: "+ stateName( transitionTargetState.getStateType() ) +", rule: "+ruleName(transitionTargetState.ruleIndex);
            s += "last state on stack: "+parserStack.last()+", type: "+ stateName( parserStack.last().getStateType() ) + ", rule: " + ruleName( parserStack.last().ruleIndex );
            s += "RuleStack: " + parserStack.rulesStack();
            s += "StateStack: " + parserStack.statesStack();
            s += "----------------------------------------";
            System.out.println(s);
        }
    }
}
