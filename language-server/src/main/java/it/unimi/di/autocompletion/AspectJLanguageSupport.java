package it.unimi.di.autocompletion;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import it.unimi.di.parser.AspectJLexer;
import it.unimi.di.parser.AspectJParser;

public class AspectJLanguageSupport extends LanguageSupport{

    public AspectJLanguageSupport(String program){
        this.lexer  = new AspectJLexer(CharStreams.fromString(program));
        this.parser = new AspectJParser(new CommonTokenStream(this.lexer));
    }    
}
