package it.unimi.di.autocompletion;

import it.unimi.di.parser.AspectJParserBaseListener;

public abstract class AspectJParserCompletionListener extends AspectJParserBaseListener implements CompletionItemProvider{}
