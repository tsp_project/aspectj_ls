package it.unimi.di.autocompletion;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.antlr.v4.runtime.atn.ATNState;
import org.antlr.v4.runtime.atn.BasicBlockStartState;
import org.antlr.v4.runtime.atn.BasicState;
import org.antlr.v4.runtime.atn.BlockEndState;
import org.antlr.v4.runtime.atn.LoopEndState;
import org.antlr.v4.runtime.atn.PlusBlockStartState;
import org.antlr.v4.runtime.atn.PlusLoopbackState;
import org.antlr.v4.runtime.atn.RuleStartState;
import org.antlr.v4.runtime.atn.RuleStopState;
import org.antlr.v4.runtime.atn.StarBlockStartState;
import org.antlr.v4.runtime.atn.StarLoopEntryState;
import org.antlr.v4.runtime.atn.StarLoopbackState;
import org.antlr.v4.runtime.misc.Pair;

import static it.unimi.di.autocompletion.AspectJATNDebugSupport.*;


public class ParserStack {

    private Stack<ATNState> stack;

    public ParserStack(){
        this.stack = new Stack<>();
    }

    private ParserStack copy(){
        ParserStack result = new ParserStack();
        for(ATNState state: this.stack)
            result.stack.push(state);
        return result;
    }

    private ParserStack plus(ATNState state){
        ParserStack result = this.copy();
        result.stack.push(state);
        return result;
    }

    private ParserStack minusLast(){
        ParserStack result = this.copy();
        result.stack.pop();
        return result;
    }

    protected ATNState last(){
        return this.stack.peek();
    }

    protected boolean isEmpty(){
        return this.stack.isEmpty();
    }

    public Pair<Boolean, ParserStack> process(ATNState state) {
        if( state instanceof RuleStartState ||
            state instanceof StarBlockStartState ||
            state instanceof BasicBlockStartState ||
            state instanceof PlusBlockStartState ||
            state instanceof StarLoopEntryState
        ){
            return new Pair<>(true, this.plus(state));
        }else if(state instanceof BlockEndState){
            if(this.last().equals( ((BlockEndState) state).startState ) ){
                return new Pair<>(true, this.minusLast());
            }else{
                return new Pair<>(false, null);
            }
        }else if(state instanceof LoopEndState){
            boolean cont =  this.last() instanceof StarLoopEntryState && 
                            ((StarLoopEntryState) this.last()).loopBackState.equals( ((LoopEndState) state).loopBackState );
            if(cont){
                return new Pair<>(true, this.minusLast());
            }else{
                return new Pair<>(false, null);
            }
        }else if(state instanceof RuleStopState){
            boolean cont =  this.last() instanceof RuleStartState &&
                            ((RuleStartState) this.last()).stopState.equals( ((RuleStopState) state) );
            if(cont){
                return new Pair<>(true, this.minusLast());
            }else{
                return new Pair<>(false, null);
            }
        }else if(state instanceof StarLoopbackState){
            boolean cont =  this.last() instanceof StarLoopEntryState &&
                            ((StarLoopEntryState) this.last()).loopBackState.equals( ((StarLoopbackState) state) );
            if(cont){
                return new Pair<>(true, this.minusLast());
            }else{
                return new Pair<>(false, null);
            }
        }else if(   state instanceof BasicState ||
                    state instanceof PlusLoopbackState){
            return new Pair<>(true, this.copy());
        }else{
            throw new UnsupportedOperationException(state.getClass().getName()); 
        }
    }

    public List<Integer> rulesWeAreIn() {
        List<Integer> result = new ArrayList<>();
        for(ATNState state: this.stack){
            if(state instanceof RuleStartState){
                result.add( state.ruleIndex );
            }
        }
        return result;
    }

    protected List<String> rulesStack(){
        List<String> rules = new ArrayList<>();
        for(ATNState state : this.stack){
            if(state instanceof RuleStartState){
                rules.add( ruleName(state.ruleIndex) );
            }
        }
        return rules;
    }

    protected List<String> statesStack(){
        List<String> result = new ArrayList<>();
        for(ATNState state : this.stack){
            result.add( stateName(state.getStateType()) );
        }
        return result;
    }
    
}
