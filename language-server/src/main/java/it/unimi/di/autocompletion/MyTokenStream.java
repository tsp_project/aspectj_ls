package it.unimi.di.autocompletion;

import java.util.List;

import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Token;

import static it.unimi.di.autocompletion.AntlrAutoCompletionSuggester.CARET_TOKEN_TYPE;

public class MyTokenStream {

    private List<Token> tokensList;
    private int index;

    public MyTokenStream(List<Token> tokensList){
        this.tokensList = tokensList;
        this.index = 0;
    }

    public MyTokenStream(List<Token> tokensList, int index){
        this.tokensList = tokensList;
        this.index = index;
    }

    public boolean atCaret(){
        return this.next().getType() < 0;
    }

    public Token next(){
        return this.index < this.tokensList.size() ? tokensList.get(this.index) : new CommonToken(CARET_TOKEN_TYPE);
    }

    public MyTokenStream move(){
        return new MyTokenStream(tokensList, this.index+1);
    }
}
