package it.unimi.di.autocompletion;

import java.util.Collection;

import org.eclipse.lsp4j.CompletionItem;

public interface CompletionItemProvider{
    public Collection<CompletionItem> getCompletionItems(); 
}
