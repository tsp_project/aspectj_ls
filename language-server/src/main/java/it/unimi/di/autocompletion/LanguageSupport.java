package it.unimi.di.autocompletion;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.atn.ATNState;

public abstract class LanguageSupport {

    protected Lexer lexer;

    protected Parser parser;

    public Lexer getLexer(){
        return this.lexer;
    }

    public Parser getParser(){
        return this.parser;
    }

    public Vocabulary getVocabulary(){
        return this.parser.getVocabulary();
    }

    public ATNState getInitialState(){
        return this.parser.getATN().states.get(0);
    }
}
