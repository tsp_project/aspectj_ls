package it.unimi.di.autocompletion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;

public class AspectJSnippetsService {
    private static AspectJSnippetsService instance = null;
    private HashMap<String, List<CompletionItem>> snippetsMap = null;

    private AspectJSnippetsService(){
        snippetsMap = createSnippetItemsMap();
    }

    // Double-checking Singleton
    public static AspectJSnippetsService getInstance(){
        if(instance == null){
            synchronized (AspectJSnippetsService.class) {
                if(instance == null){
                    instance = new AspectJSnippetsService();
                }
            }
        }
        return instance;
    }
    

    private HashMap<String, List<CompletionItem>> createSnippetItemsMap(){
        snippetsMap = new HashMap<>();

        snippetsMap.put("aspect", new ArrayList<>());
        snippetsMap.put("pointcut", new ArrayList<>());

        

        snippetsMap.get("aspect").add(
            createSnippetItem(
                "aspect AspectName{\n\t\n}",
                "aspect (snippet)",
                "basic aspect snippet")
        );
        snippetsMap.get("aspect").add(
            createSnippetItem(
                "privileged aspect AspectName{\n\t\n}",
                "privileged aspect (snippet)",
                "privileged aspect snippet")
        );
        snippetsMap.get("pointcut").add(
            createSnippetItem(
                "abstract pointcut pointcutName();",
                "abstract pointcut (snippet)",
                "abstract pointcut snippet")
        );
        snippetsMap.get("pointcut").add(
            createSnippetItem(
                "pointcut pointcutName() :\n\t;",
                "pointcut (snippet)",
                "basic pointcut snippet")
        );
        
        return snippetsMap;
    }

    
    private CompletionItem createSnippetItem(String insertText, String label, String detail){
        CompletionItem completionItem = new CompletionItem();
        
        completionItem.setInsertText(insertText);
        completionItem.setLabel(label);
        completionItem.setKind(CompletionItemKind.Snippet);
        completionItem.setDetail(detail);

        return completionItem;
    }


    public Set<String> getSnippetsKeywords(){
        return snippetsMap.keySet();
    }


    public Collection<CompletionItem> getSnippetsCompletionItems(String keyword){
        if(snippetsMap.containsKey(keyword)){
            return snippetsMap.get(keyword);
        }else{
            throw new IllegalArgumentException("No snippets exist for keyword: "+keyword);
        }
    } 
    
    
}
