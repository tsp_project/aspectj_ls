package it.unimi.di.autocompletion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.lsp4j.CompletionItem;
import org.eclipse.lsp4j.CompletionItemKind;

import it.unimi.di.parser.AspectJParser;

public class AspectJParserVariableListener extends AspectJParserCompletionListener{

    private List<String> variables;

    public AspectJParserVariableListener(){
        this.variables = new ArrayList<>();
    }

    @Override
    public void exitVariableDeclaratorId(AspectJParser.VariableDeclaratorIdContext ctx){
        variables.add(ctx.getText());
    }

    private CompletionItem createCompletionItem(String insertText, String detail){
        CompletionItem completionItem = new CompletionItem();
        
        completionItem.setInsertText(insertText);
        completionItem.setLabel(insertText);
        completionItem.setKind(CompletionItemKind.Variable);
        completionItem.setDetail(detail);

        return completionItem;
    }

    @Override
    public Collection<CompletionItem> getCompletionItems() {
        Set<CompletionItem> result = new HashSet<>();
        for(String var: this.variables){
            result.add(createCompletionItem(var, var));
        }
        return result;
    }
}
