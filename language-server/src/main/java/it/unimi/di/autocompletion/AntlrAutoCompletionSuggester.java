package it.unimi.di.autocompletion;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.atn.ATNState;
import org.antlr.v4.runtime.atn.AtomTransition;
import org.antlr.v4.runtime.atn.RuleStartState;
import org.antlr.v4.runtime.atn.RuleStopState;
import org.antlr.v4.runtime.atn.SetTransition;
import org.antlr.v4.runtime.atn.Transition;
import org.antlr.v4.runtime.misc.Pair;
import org.eclipse.lsp4j.Position;

public class AntlrAutoCompletionSuggester {

    public static final int CARET_TOKEN_TYPE = -10;
    private static final String PATH = "it.unimi.di.autocompletion.";

    private String lsClass;

    public AntlrAutoCompletionSuggester(String lsClass){
        this.lsClass = lsClass;
    }


    protected static String truncateTextAtCaret(String text, Position caretPosition){
        int lineNumber = caretPosition.getLine();
        int columnNumber = caretPosition.getCharacter();

        String result = "";
        String[] lines = text.split("\n");

        for(int i=0; i<lineNumber; i++)
            result += lines[i] + "\n";

        for(int i=0; i<columnNumber; i++)
            result += lines[lineNumber].charAt(i);

        return result;
    }


    public Set<String> suggestions(String program, Position caretPosition){
        return suggestions(truncateTextAtCaret(program, caretPosition));
    }


    public Set<String> suggestions(String program){

        Set<Integer> collector = new HashSet<>();
        Set<String> result = new HashSet<>();
        List<String> history = new ArrayList<>();

        try {
            Object languageSupportGeneral = Class.forName(PATH+lsClass).getConstructor(String.class).newInstance(program);
            assert(languageSupportGeneral instanceof LanguageSupport);
            LanguageSupport languageSupport = (LanguageSupport) languageSupportGeneral;

            process(languageSupport.getParser().getRuleNames() , languageSupport.getVocabulary(), languageSupport.getInitialState(),
                new MyTokenStream(getPreceedingTokens(languageSupport.getLexer())),
                collector, new ParserStack(), new HashSet<>(), history);
            
            for(Integer tokenType: collector){
                result.add( languageSupport.getVocabulary().getDisplayName(tokenType) );
            }

        } catch ( InstantiationException   | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException| NoSuchMethodException  | SecurityException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        

        return result;
    }

    private void process(   String[] ruleNames,
                            Vocabulary vocabulary,
                            ATNState state,
                            MyTokenStream tokens,
                            Set<Integer> collector,
                            ParserStack parserStack,
                            Set<Integer> alreadyPassed,
                            List<String> history){
        
        boolean atCaret = tokens.atCaret();
        Pair<Boolean, ParserStack> stackRes = parserStack.process(state);
        
        if(!stackRes.a)
            return;
        
        for(Transition transition : state.getTransitions()){
            String desc = describe(ruleNames, vocabulary, state, transition);
            if(transition.isEpsilon()){
                if(!alreadyPassed.contains(transition.target.stateNumber)){
                    if(canGoThere(parserStack, state, transition.target)){
                        Set<Integer> newAlreadyPassed = new HashSet<>(alreadyPassed);
                        newAlreadyPassed.add(transition.target.stateNumber);
                        history.add(desc);
                        process(ruleNames, vocabulary, transition.target, tokens, collector, stackRes.b, newAlreadyPassed, history);
                    }
                }
            }else if(transition instanceof AtomTransition){
                int nextTokenType = tokens.next().getType();
                if(canGoThere(parserStack, state, transition.target)){
                    if(atCaret){
                        if(isCompatibleWithStack(transition.target, parserStack)){
                            collector.add( ((AtomTransition) transition).label );
                        }
                    }else{
                        if(nextTokenType == ((AtomTransition) transition).label){
                            history.add(desc);
                            process(ruleNames, vocabulary, transition.target, tokens.move(), collector, stackRes.b, new HashSet<>(), history);
                        }
                    }
                }
            }else if(transition instanceof SetTransition){
                int nextTokenType = tokens.next().getType();
                for(Integer sym : transition.label().toList()){
                    if(canGoThere(parserStack, state, transition.target)){
                        if(atCaret){
                            if(isCompatibleWithStack(transition.target, parserStack)){
                                collector.add(sym);
                            }
                        }else{
                            if(nextTokenType == sym){
                                history.add(desc);
                                process(ruleNames, vocabulary, transition.target, tokens.move(), collector, stackRes.b, new HashSet<>(), history);
                            }
                        }
                    }
                }
            }else{
                throw new UnsupportedOperationException(transition.getClass().getName());
            }
        }
    }


    private List<Token> getPreceedingTokens(Lexer lexer){
        List<Token> result = new ArrayList<>();

        Token token;
        do{
            token = lexer.nextToken();
            if(token.getChannel()==0)
                result.add(token);              
        }while(token.getType() >= 0);

        result.set(result.size()-1, new CommonToken(CARET_TOKEN_TYPE));

        return result;
    }


    private boolean isCompatibleWithStack(ATNState state, ParserStack parserStack){
        Pair<Boolean, ParserStack> res = parserStack.process(state);
        if(!res.a){
            return false;
        }
        if(state.onlyHasEpsilonTransitions()){
            for(Transition transition : state.getTransitions()){
                if(isCompatibleWithStack(transition.target, res.b))
                    return true;
            }
            return false;
        }else{
            return true;
        }
    }

    private boolean canGoThere(ParserStack parserStack, ATNState currentState, ATNState targetState){
        int currentRule;
        if(currentState instanceof RuleStopState){
            List<Integer> rulesWeAreIn = parserStack.rulesWeAreIn();
            if(rulesWeAreIn.size() < 2)
                throw new IllegalStateException();
            currentRule = rulesWeAreIn.get(rulesWeAreIn.size()-2);
        }else{
            currentRule = currentState.ruleIndex;
        }
        if(targetState instanceof RuleStartState){
            return true;
        }else{
            return targetState.ruleIndex == currentRule;
        }

    }

    private String describe(String[] ruleNames, Vocabulary vocabulary, ATNState state, Transition transition){
        return "";
    }

}
