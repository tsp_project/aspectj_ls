package it.unimi.di.validator;

import java.util.ArrayList;
import java.util.Collection;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.eclipse.lsp4j.PublishDiagnosticsParams;
import org.eclipse.lsp4j.services.LanguageClient;

import it.unimi.di.langserver.ParserProxy;

public class AspectJValidator {

    private ParserProxy parserProxy;

    public AspectJValidator(){
        this.parserProxy = ParserProxy.getInstance();
    }

    /**Validate the text and send possible errors to the client. */
    public void validate(String docUri, String text, LanguageClient client) {
        Collection<ANTLRErrorListener> errorListeners = new ArrayList<>();
        SyntaxErrorListener syntaxErrorListener = new SyntaxErrorListener();
        errorListeners.add( syntaxErrorListener );

        this.parserProxy.parseErrors(text, errorListeners);

        if(syntaxErrorListener.getErrors().isPresent()){
            client.publishDiagnostics(new PublishDiagnosticsParams(docUri, syntaxErrorListener.getErrors().get()));
        }

    }
}
