package it.unimi.di.validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.eclipse.lsp4j.Diagnostic;
import org.eclipse.lsp4j.Position;
import org.eclipse.lsp4j.Range;

public class SyntaxErrorListener extends BaseErrorListener {

    private List<Diagnostic> errors;

    public SyntaxErrorListener(){
        this.errors = new ArrayList<>();
    }

    @Override
    public void syntaxError(Recognizer<?,?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e){
        Range errorRange = new Range(new Position(line-1, charPositionInLine), new Position(line-1, charPositionInLine+1));
        this.errors.add( new Diagnostic(errorRange, msg) );
    }

    public Optional<List<Diagnostic>> getErrors(){
        return Optional.of(this.errors);
    }
}
