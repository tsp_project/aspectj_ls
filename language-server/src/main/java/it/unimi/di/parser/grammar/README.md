# Grammars' Info

Grammars' files were downloaded from [this repo](https://github.com/antlr/grammars-v4) at commit [`bf5f5825b822f6777ea00cd9ecf6cd036cb31187`](https://github.com/antlr/grammars-v4/tree/bf5f5825b822f6777ea00cd9ecf6cd036cb31187).

Specifically:
- [`AspectJParser.g4`](https://raw.githubusercontent.com/antlr/grammars-v4/bf5f5825b822f6777ea00cd9ecf6cd036cb31187/aspectj/AspectJParser.g4)
- [`AspectJLexer.g4`](https://raw.githubusercontent.com/antlr/grammars-v4/bf5f5825b822f6777ea00cd9ecf6cd036cb31187/aspectj/AspectJLexer.g4)
- [`Java.g4`](https://raw.githubusercontent.com/antlr/grammars-v4/bf5f5825b822f6777ea00cd9ecf6cd036cb31187/java/Java.g4)

Then `Java.g4` was splitted into `JavaLexer.g4` and `JavaParser.g4`.

In `AspectJParser.g4` the `typeDeclaration` rule was removed ; moreover the `'static' aspectDeclaration` option in the `classBodyDeclaration` rule was removed too.

AspectJ Parser and Lexer were generated through commands:
```
$ antlr4 language-server/src/main/java/it/unimi/di/parser/grammar/AspectJLexer.g4 -package it.unimi.di.parser
$ antlr4 language-server/src/main/java/it/unimi/di/parser/grammar/AspectJParser.g4 -package it.unimi.di.parser
```

`antlr4` was at version `4.7.2`.

Then files `AspectJLexer.java`, `AspectJParser.java`, `AspectJParserListener.java` and `AspectJParserBaseListener.java` were moved outside grammar directory (in parser directory).