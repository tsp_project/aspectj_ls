// Generated from language-server/src/main/java/it/unimi/di/parser/grammar/AspectJParser.g4 by ANTLR 4.7.2
package it.unimi.di.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AspectJParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		DOTDOT=1, DQUOTE=2, ADVICEEXECUTION=3, ANNOTATION=4, ARGS=5, AFTER=6, 
		AROUND=7, ASPECT=8, BEFORE=9, CALL=10, CFLOW=11, CFLOWBELOW=12, DECLARE=13, 
		ERROR=14, EXECUTION=15, GET=16, HANDLER=17, INITIALIZATION=18, ISSINGLETON=19, 
		PARENTS=20, PERCFLOW=21, PERCFLOWBELOW=22, PERTARGET=23, PERTHIS=24, PERTYPEWITHIN=25, 
		POINTCUT=26, PRECEDENCE=27, PREINITIALIZATION=28, PRIVILEGED=29, RETURNING=30, 
		SET=31, SOFT=32, STATICINITIALIZATION=33, TARGET=34, THROWING=35, WARNING=36, 
		WITHIN=37, WITHINCODE=38, ANNOTATION_AFTER=39, ANNOTATION_AFTERRETURNING=40, 
		ANNOTATION_AFTERTHROWING=41, ANNOTATION_AROUND=42, ANNOTATION_ASPECT=43, 
		ANNOTATION_BEFORE=44, ANNOTATION_DECLAREPARENTS=45, ANNOTATION_DECLAREMIXIN=46, 
		ANNOTATION_DECLAREWARNING=47, ANNOTATION_DECLAREERROR=48, ANNOTATION_DECLAREPRECEDENCE=49, 
		ANNOTATION_POINTCUT=50, ANNOTATION_CONSTRUCTOR=51, ANNOTATION_DEFAULTIMPL=52, 
		ANNOTATION_FIELD=53, ANNOTATION_INTERFACES=54, ANNOTATION_TYPE=55, ANNOTATION_METHOD=56, 
		ANNOTATION_VALUE=57, AT=58, ABSTRACT=59, ASSERT=60, BOOLEAN=61, BREAK=62, 
		BYTE=63, CASE=64, CATCH=65, CHAR=66, CLASS=67, CONST=68, CONTINUE=69, 
		DEFAULT=70, DO=71, DOUBLE=72, ELSE=73, ENUM=74, EXTENDS=75, FINAL=76, 
		FINALLY=77, FLOAT=78, FOR=79, IF=80, GOTO=81, IMPLEMENTS=82, IMPORT=83, 
		INSTANCEOF=84, INT=85, INTERFACE=86, LONG=87, NATIVE=88, NEW=89, PACKAGE=90, 
		PRIVATE=91, PROTECTED=92, PUBLIC=93, RETURN=94, SHORT=95, STATIC=96, STRICTFP=97, 
		SUPER=98, SWITCH=99, SYNCHRONIZED=100, THIS=101, THROW=102, THROWS=103, 
		TRANSIENT=104, TRY=105, VOID=106, VOLATILE=107, WHILE=108, IntegerLiteral=109, 
		FloatingPointLiteral=110, BooleanLiteral=111, CharacterLiteral=112, StringLiteral=113, 
		NullLiteral=114, LPAREN=115, RPAREN=116, LBRACE=117, RBRACE=118, LBRACK=119, 
		RBRACK=120, SEMI=121, COMMA=122, DOT=123, ASSIGN=124, GT=125, LT=126, 
		BANG=127, TILDE=128, QUESTION=129, COLON=130, EQUAL=131, LE=132, GE=133, 
		NOTEQUAL=134, AND=135, OR=136, INC=137, DEC=138, ADD=139, SUB=140, MUL=141, 
		DIV=142, BITAND=143, BITOR=144, CARET=145, MOD=146, ADD_ASSIGN=147, SUB_ASSIGN=148, 
		MUL_ASSIGN=149, DIV_ASSIGN=150, AND_ASSIGN=151, OR_ASSIGN=152, XOR_ASSIGN=153, 
		MOD_ASSIGN=154, LSHIFT_ASSIGN=155, RSHIFT_ASSIGN=156, URSHIFT_ASSIGN=157, 
		Identifier=158, ELLIPSIS=159, WS=160, COMMENT=161, LINE_COMMENT=162, WS1=163, 
		COMMENT1=164, LINE_COMMENT1=165, INVALID1=166, WS2=167, COMMENT2=168, 
		LINE_COMMENT2=169, WS3=170, COMMENT3=171, LINE_COMMENT3=172, INVALID3=173, 
		WS4=174, COMMENT4=175, LINE_COMMENT4=176, INVALID4=177;
	public static final int
		RULE_aspectDeclaration = 0, RULE_aspectBody = 1, RULE_aspectBodyDeclaration = 2, 
		RULE_classBodyDeclaration = 3, RULE_memberDeclaration = 4, RULE_annotation = 5, 
		RULE_classPattern = 6, RULE_classPatternList = 7, RULE_advice = 8, RULE_adviceSpec = 9, 
		RULE_perClause = 10, RULE_pointcutDeclaration = 11, RULE_pointcutExpression = 12, 
		RULE_pointcutPrimitive = 13, RULE_referencePointcut = 14, RULE_interTypeMemberDeclaration = 15, 
		RULE_interTypeDeclaration = 16, RULE_typePattern = 17, RULE_simpleTypePattern = 18, 
		RULE_dottedNamePattern = 19, RULE_optionalParensTypePattern = 20, RULE_fieldPattern = 21, 
		RULE_fieldModifiersPattern = 22, RULE_fieldModifier = 23, RULE_dotOrDotDot = 24, 
		RULE_simpleNamePattern = 25, RULE_methodOrConstructorPattern = 26, RULE_methodPattern = 27, 
		RULE_methodModifiersPattern = 28, RULE_methodModifier = 29, RULE_formalsPattern = 30, 
		RULE_formalsPatternAfterDotDot = 31, RULE_throwsPattern = 32, RULE_typePatternList = 33, 
		RULE_constructorPattern = 34, RULE_constructorModifiersPattern = 35, RULE_constructorModifier = 36, 
		RULE_annotationPattern = 37, RULE_annotationTypePattern = 38, RULE_formalParametersPattern = 39, 
		RULE_typeOrIdentifier = 40, RULE_annotationOrIdentifer = 41, RULE_annotationsOrIdentifiersPattern = 42, 
		RULE_annotationsOrIdentifiersPatternAfterDotDot = 43, RULE_argsPattern = 44, 
		RULE_argsPatternList = 45, RULE_id = 46, RULE_classDeclaration = 47, RULE_typeParameter = 48, 
		RULE_enumDeclaration = 49, RULE_enumConstant = 50, RULE_interfaceDeclaration = 51, 
		RULE_methodDeclaration = 52, RULE_constructorDeclaration = 53, RULE_constantDeclarator = 54, 
		RULE_interfaceMethodDeclaration = 55, RULE_variableDeclaratorId = 56, 
		RULE_enumConstantName = 57, RULE_classOrInterfaceType = 58, RULE_qualifiedName = 59, 
		RULE_elementValuePair = 60, RULE_annotationTypeDeclaration = 61, RULE_annotationMethodRest = 62, 
		RULE_statement = 63, RULE_catchClause = 64, RULE_expression = 65, RULE_primary = 66, 
		RULE_createdName = 67, RULE_innerCreator = 68, RULE_superSuffix = 69, 
		RULE_explicitGenericInvocationSuffix = 70, RULE_compilationUnit = 71, 
		RULE_packageDeclaration = 72, RULE_importDeclaration = 73, RULE_typeDeclaration = 74, 
		RULE_modifier = 75, RULE_classOrInterfaceModifier = 76, RULE_variableModifier = 77, 
		RULE_typeParameters = 78, RULE_typeBound = 79, RULE_enumConstants = 80, 
		RULE_enumBodyDeclarations = 81, RULE_typeList = 82, RULE_classBody = 83, 
		RULE_interfaceBody = 84, RULE_genericMethodDeclaration = 85, RULE_genericConstructorDeclaration = 86, 
		RULE_fieldDeclaration = 87, RULE_interfaceBodyDeclaration = 88, RULE_interfaceMemberDeclaration = 89, 
		RULE_constDeclaration = 90, RULE_genericInterfaceMethodDeclaration = 91, 
		RULE_variableDeclarators = 92, RULE_variableDeclarator = 93, RULE_variableInitializer = 94, 
		RULE_arrayInitializer = 95, RULE_type = 96, RULE_primitiveType = 97, RULE_typeArguments = 98, 
		RULE_typeArgument = 99, RULE_qualifiedNameList = 100, RULE_formalParameters = 101, 
		RULE_formalParameterList = 102, RULE_formalParameter = 103, RULE_lastFormalParameter = 104, 
		RULE_methodBody = 105, RULE_constructorBody = 106, RULE_literal = 107, 
		RULE_annotationName = 108, RULE_elementValuePairs = 109, RULE_elementValue = 110, 
		RULE_elementValueArrayInitializer = 111, RULE_annotationTypeBody = 112, 
		RULE_annotationTypeElementDeclaration = 113, RULE_annotationTypeElementRest = 114, 
		RULE_annotationMethodOrConstantRest = 115, RULE_annotationConstantRest = 116, 
		RULE_defaultValue = 117, RULE_block = 118, RULE_blockStatement = 119, 
		RULE_localVariableDeclarationStatement = 120, RULE_localVariableDeclaration = 121, 
		RULE_catchType = 122, RULE_finallyBlock = 123, RULE_resourceSpecification = 124, 
		RULE_resources = 125, RULE_resource = 126, RULE_switchBlockStatementGroup = 127, 
		RULE_switchLabel = 128, RULE_forControl = 129, RULE_forInit = 130, RULE_enhancedForControl = 131, 
		RULE_forUpdate = 132, RULE_parExpression = 133, RULE_expressionList = 134, 
		RULE_statementExpression = 135, RULE_constantExpression = 136, RULE_creator = 137, 
		RULE_arrayCreatorRest = 138, RULE_classCreatorRest = 139, RULE_explicitGenericInvocation = 140, 
		RULE_nonWildcardTypeArguments = 141, RULE_typeArgumentsOrDiamond = 142, 
		RULE_nonWildcardTypeArgumentsOrDiamond = 143, RULE_arguments = 144;
	private static String[] makeRuleNames() {
		return new String[] {
			"aspectDeclaration", "aspectBody", "aspectBodyDeclaration", "classBodyDeclaration", 
			"memberDeclaration", "annotation", "classPattern", "classPatternList", 
			"advice", "adviceSpec", "perClause", "pointcutDeclaration", "pointcutExpression", 
			"pointcutPrimitive", "referencePointcut", "interTypeMemberDeclaration", 
			"interTypeDeclaration", "typePattern", "simpleTypePattern", "dottedNamePattern", 
			"optionalParensTypePattern", "fieldPattern", "fieldModifiersPattern", 
			"fieldModifier", "dotOrDotDot", "simpleNamePattern", "methodOrConstructorPattern", 
			"methodPattern", "methodModifiersPattern", "methodModifier", "formalsPattern", 
			"formalsPatternAfterDotDot", "throwsPattern", "typePatternList", "constructorPattern", 
			"constructorModifiersPattern", "constructorModifier", "annotationPattern", 
			"annotationTypePattern", "formalParametersPattern", "typeOrIdentifier", 
			"annotationOrIdentifer", "annotationsOrIdentifiersPattern", "annotationsOrIdentifiersPatternAfterDotDot", 
			"argsPattern", "argsPatternList", "id", "classDeclaration", "typeParameter", 
			"enumDeclaration", "enumConstant", "interfaceDeclaration", "methodDeclaration", 
			"constructorDeclaration", "constantDeclarator", "interfaceMethodDeclaration", 
			"variableDeclaratorId", "enumConstantName", "classOrInterfaceType", "qualifiedName", 
			"elementValuePair", "annotationTypeDeclaration", "annotationMethodRest", 
			"statement", "catchClause", "expression", "primary", "createdName", "innerCreator", 
			"superSuffix", "explicitGenericInvocationSuffix", "compilationUnit", 
			"packageDeclaration", "importDeclaration", "typeDeclaration", "modifier", 
			"classOrInterfaceModifier", "variableModifier", "typeParameters", "typeBound", 
			"enumConstants", "enumBodyDeclarations", "typeList", "classBody", "interfaceBody", 
			"genericMethodDeclaration", "genericConstructorDeclaration", "fieldDeclaration", 
			"interfaceBodyDeclaration", "interfaceMemberDeclaration", "constDeclaration", 
			"genericInterfaceMethodDeclaration", "variableDeclarators", "variableDeclarator", 
			"variableInitializer", "arrayInitializer", "type", "primitiveType", "typeArguments", 
			"typeArgument", "qualifiedNameList", "formalParameters", "formalParameterList", 
			"formalParameter", "lastFormalParameter", "methodBody", "constructorBody", 
			"literal", "annotationName", "elementValuePairs", "elementValue", "elementValueArrayInitializer", 
			"annotationTypeBody", "annotationTypeElementDeclaration", "annotationTypeElementRest", 
			"annotationMethodOrConstantRest", "annotationConstantRest", "defaultValue", 
			"block", "blockStatement", "localVariableDeclarationStatement", "localVariableDeclaration", 
			"catchType", "finallyBlock", "resourceSpecification", "resources", "resource", 
			"switchBlockStatementGroup", "switchLabel", "forControl", "forInit", 
			"enhancedForControl", "forUpdate", "parExpression", "expressionList", 
			"statementExpression", "constantExpression", "creator", "arrayCreatorRest", 
			"classCreatorRest", "explicitGenericInvocation", "nonWildcardTypeArguments", 
			"typeArgumentsOrDiamond", "nonWildcardTypeArgumentsOrDiamond", "arguments"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'..'", "'\"'", "'adviceexecution'", "'annotation'", "'args'", 
			"'after'", "'around'", "'aspect'", "'before'", "'call'", "'cflow'", "'cflowbelow'", 
			"'declare'", "'error'", "'execution'", "'get'", "'handler'", "'initialization'", 
			"'issingleton'", "'parents'", "'percflow'", "'percflowbelow'", "'pertarget'", 
			"'perthis'", "'pertypewithin'", "'pointcut'", "'precedence'", "'preinitialization'", 
			"'privileged'", "'returning'", "'set'", "'soft'", "'staticinitialization'", 
			"'target'", "'throwing'", "'warning'", "'within'", "'withincode'", "'After'", 
			"'AfterReturning'", "'AfterThrowing'", "'Around'", "'Aspect'", "'Before'", 
			"'DeclareParents'", "'DeclareMixin'", "'DeclareWarning'", "'DeclareError'", 
			"'DeclarePrecedence'", "'Pointcut'", "'constructor'", "'defaultImpl'", 
			"'field'", "'interfaces'", "'type'", "'method'", "'value'", "'@'", "'abstract'", 
			"'assert'", "'boolean'", "'break'", "'byte'", "'case'", "'catch'", "'char'", 
			"'class'", "'const'", "'continue'", "'default'", "'do'", "'double'", 
			"'else'", "'enum'", "'extends'", "'final'", "'finally'", "'float'", "'for'", 
			"'if'", "'goto'", "'implements'", "'import'", "'instanceof'", "'int'", 
			"'interface'", "'long'", "'native'", "'new'", "'package'", "'private'", 
			"'protected'", "'public'", "'return'", "'short'", "'static'", "'strictfp'", 
			"'super'", "'switch'", "'synchronized'", "'this'", "'throw'", "'throws'", 
			"'transient'", "'try'", "'void'", "'volatile'", "'while'", null, null, 
			null, null, null, "'null'", "'('", "')'", "'{'", "'}'", "'['", "']'", 
			"';'", "','", "'.'", "'='", "'>'", "'<'", "'!'", "'~'", "'?'", "':'", 
			"'=='", "'<='", "'>='", "'!='", "'&&'", "'||'", "'++'", "'--'", "'+'", 
			"'-'", "'*'", "'/'", "'&'", "'|'", "'^'", "'%'", "'+='", "'-='", "'*='", 
			"'/='", "'&='", "'|='", "'^='", "'%='", "'<<='", "'>>='", "'>>>='", null, 
			"'...'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "DOTDOT", "DQUOTE", "ADVICEEXECUTION", "ANNOTATION", "ARGS", "AFTER", 
			"AROUND", "ASPECT", "BEFORE", "CALL", "CFLOW", "CFLOWBELOW", "DECLARE", 
			"ERROR", "EXECUTION", "GET", "HANDLER", "INITIALIZATION", "ISSINGLETON", 
			"PARENTS", "PERCFLOW", "PERCFLOWBELOW", "PERTARGET", "PERTHIS", "PERTYPEWITHIN", 
			"POINTCUT", "PRECEDENCE", "PREINITIALIZATION", "PRIVILEGED", "RETURNING", 
			"SET", "SOFT", "STATICINITIALIZATION", "TARGET", "THROWING", "WARNING", 
			"WITHIN", "WITHINCODE", "ANNOTATION_AFTER", "ANNOTATION_AFTERRETURNING", 
			"ANNOTATION_AFTERTHROWING", "ANNOTATION_AROUND", "ANNOTATION_ASPECT", 
			"ANNOTATION_BEFORE", "ANNOTATION_DECLAREPARENTS", "ANNOTATION_DECLAREMIXIN", 
			"ANNOTATION_DECLAREWARNING", "ANNOTATION_DECLAREERROR", "ANNOTATION_DECLAREPRECEDENCE", 
			"ANNOTATION_POINTCUT", "ANNOTATION_CONSTRUCTOR", "ANNOTATION_DEFAULTIMPL", 
			"ANNOTATION_FIELD", "ANNOTATION_INTERFACES", "ANNOTATION_TYPE", "ANNOTATION_METHOD", 
			"ANNOTATION_VALUE", "AT", "ABSTRACT", "ASSERT", "BOOLEAN", "BREAK", "BYTE", 
			"CASE", "CATCH", "CHAR", "CLASS", "CONST", "CONTINUE", "DEFAULT", "DO", 
			"DOUBLE", "ELSE", "ENUM", "EXTENDS", "FINAL", "FINALLY", "FLOAT", "FOR", 
			"IF", "GOTO", "IMPLEMENTS", "IMPORT", "INSTANCEOF", "INT", "INTERFACE", 
			"LONG", "NATIVE", "NEW", "PACKAGE", "PRIVATE", "PROTECTED", "PUBLIC", 
			"RETURN", "SHORT", "STATIC", "STRICTFP", "SUPER", "SWITCH", "SYNCHRONIZED", 
			"THIS", "THROW", "THROWS", "TRANSIENT", "TRY", "VOID", "VOLATILE", "WHILE", 
			"IntegerLiteral", "FloatingPointLiteral", "BooleanLiteral", "CharacterLiteral", 
			"StringLiteral", "NullLiteral", "LPAREN", "RPAREN", "LBRACE", "RBRACE", 
			"LBRACK", "RBRACK", "SEMI", "COMMA", "DOT", "ASSIGN", "GT", "LT", "BANG", 
			"TILDE", "QUESTION", "COLON", "EQUAL", "LE", "GE", "NOTEQUAL", "AND", 
			"OR", "INC", "DEC", "ADD", "SUB", "MUL", "DIV", "BITAND", "BITOR", "CARET", 
			"MOD", "ADD_ASSIGN", "SUB_ASSIGN", "MUL_ASSIGN", "DIV_ASSIGN", "AND_ASSIGN", 
			"OR_ASSIGN", "XOR_ASSIGN", "MOD_ASSIGN", "LSHIFT_ASSIGN", "RSHIFT_ASSIGN", 
			"URSHIFT_ASSIGN", "Identifier", "ELLIPSIS", "WS", "COMMENT", "LINE_COMMENT", 
			"WS1", "COMMENT1", "LINE_COMMENT1", "INVALID1", "WS2", "COMMENT2", "LINE_COMMENT2", 
			"WS3", "COMMENT3", "LINE_COMMENT3", "INVALID3", "WS4", "COMMENT4", "LINE_COMMENT4", 
			"INVALID4"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "AspectJParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AspectJParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class AspectDeclarationContext extends ParserRuleContext {
		public TerminalNode ASPECT() { return getToken(AspectJParser.ASPECT, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public AspectBodyContext aspectBody() {
			return getRuleContext(AspectBodyContext.class,0);
		}
		public TerminalNode PRIVILEGED() { return getToken(AspectJParser.PRIVILEGED, 0); }
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public TerminalNode EXTENDS() { return getToken(AspectJParser.EXTENDS, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IMPLEMENTS() { return getToken(AspectJParser.IMPLEMENTS, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public PerClauseContext perClause() {
			return getRuleContext(PerClauseContext.class,0);
		}
		public AspectDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aspectDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAspectDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAspectDeclaration(this);
		}
	}

	public final AspectDeclarationContext aspectDeclaration() throws RecognitionException {
		AspectDeclarationContext _localctx = new AspectDeclarationContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_aspectDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(291);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==PRIVILEGED) {
				{
				setState(290);
				match(PRIVILEGED);
				}
			}

			setState(296);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (NATIVE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SYNCHRONIZED - 58)) | (1L << (TRANSIENT - 58)) | (1L << (VOLATILE - 58)))) != 0)) {
				{
				{
				setState(293);
				modifier();
				}
				}
				setState(298);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(299);
			match(ASPECT);
			setState(300);
			id();
			setState(303);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EXTENDS) {
				{
				setState(301);
				match(EXTENDS);
				setState(302);
				type();
				}
			}

			setState(307);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IMPLEMENTS) {
				{
				setState(305);
				match(IMPLEMENTS);
				setState(306);
				typeList();
				}
			}

			setState(310);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ISSINGLETON) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN))) != 0)) {
				{
				setState(309);
				perClause();
				}
			}

			setState(312);
			aspectBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AspectBodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<AspectBodyDeclarationContext> aspectBodyDeclaration() {
			return getRuleContexts(AspectBodyDeclarationContext.class);
		}
		public AspectBodyDeclarationContext aspectBodyDeclaration(int i) {
			return getRuleContext(AspectBodyDeclarationContext.class,i);
		}
		public AspectBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aspectBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAspectBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAspectBody(this);
		}
	}

	public final AspectBodyContext aspectBody() throws RecognitionException {
		AspectBodyContext _localctx = new AspectBodyContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_aspectBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(314);
			match(LBRACE);
			setState(318);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << ABSTRACT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (CLASS - 66)) | (1L << (DOUBLE - 66)) | (1L << (ENUM - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (INTERFACE - 66)) | (1L << (LONG - 66)) | (1L << (NATIVE - 66)) | (1L << (PRIVATE - 66)) | (1L << (PROTECTED - 66)) | (1L << (PUBLIC - 66)) | (1L << (SHORT - 66)) | (1L << (STATIC - 66)) | (1L << (STRICTFP - 66)) | (1L << (SYNCHRONIZED - 66)) | (1L << (TRANSIENT - 66)) | (1L << (VOID - 66)) | (1L << (VOLATILE - 66)) | (1L << (LBRACE - 66)) | (1L << (SEMI - 66)) | (1L << (LT - 66)))) != 0) || _la==Identifier) {
				{
				{
				setState(315);
				aspectBodyDeclaration();
				}
				}
				setState(320);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(321);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AspectBodyDeclarationContext extends ParserRuleContext {
		public ClassBodyDeclarationContext classBodyDeclaration() {
			return getRuleContext(ClassBodyDeclarationContext.class,0);
		}
		public AdviceContext advice() {
			return getRuleContext(AdviceContext.class,0);
		}
		public InterTypeMemberDeclarationContext interTypeMemberDeclaration() {
			return getRuleContext(InterTypeMemberDeclarationContext.class,0);
		}
		public InterTypeDeclarationContext interTypeDeclaration() {
			return getRuleContext(InterTypeDeclarationContext.class,0);
		}
		public AspectBodyDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aspectBodyDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAspectBodyDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAspectBodyDeclaration(this);
		}
	}

	public final AspectBodyDeclarationContext aspectBodyDeclaration() throws RecognitionException {
		AspectBodyDeclarationContext _localctx = new AspectBodyDeclarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_aspectBodyDeclaration);
		try {
			setState(327);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(323);
				classBodyDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(324);
				advice();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(325);
				interTypeMemberDeclaration();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(326);
				interTypeDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassBodyDeclarationContext extends ParserRuleContext {
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode STATIC() { return getToken(AspectJParser.STATIC, 0); }
		public MemberDeclarationContext memberDeclaration() {
			return getRuleContext(MemberDeclarationContext.class,0);
		}
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public ClassBodyDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classBodyDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassBodyDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassBodyDeclaration(this);
		}
	}

	public final ClassBodyDeclarationContext classBodyDeclaration() throws RecognitionException {
		ClassBodyDeclarationContext _localctx = new ClassBodyDeclarationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_classBodyDeclaration);
		int _la;
		try {
			int _alt;
			setState(341);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(329);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(331);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==STATIC) {
					{
					setState(330);
					match(STATIC);
					}
				}

				setState(333);
				block();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(337);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(334);
						modifier();
						}
						} 
					}
					setState(339);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
				}
				setState(340);
				memberDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MemberDeclarationContext extends ParserRuleContext {
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public GenericMethodDeclarationContext genericMethodDeclaration() {
			return getRuleContext(GenericMethodDeclarationContext.class,0);
		}
		public FieldDeclarationContext fieldDeclaration() {
			return getRuleContext(FieldDeclarationContext.class,0);
		}
		public ConstructorDeclarationContext constructorDeclaration() {
			return getRuleContext(ConstructorDeclarationContext.class,0);
		}
		public GenericConstructorDeclarationContext genericConstructorDeclaration() {
			return getRuleContext(GenericConstructorDeclarationContext.class,0);
		}
		public InterfaceDeclarationContext interfaceDeclaration() {
			return getRuleContext(InterfaceDeclarationContext.class,0);
		}
		public AnnotationTypeDeclarationContext annotationTypeDeclaration() {
			return getRuleContext(AnnotationTypeDeclarationContext.class,0);
		}
		public ClassDeclarationContext classDeclaration() {
			return getRuleContext(ClassDeclarationContext.class,0);
		}
		public EnumDeclarationContext enumDeclaration() {
			return getRuleContext(EnumDeclarationContext.class,0);
		}
		public PointcutDeclarationContext pointcutDeclaration() {
			return getRuleContext(PointcutDeclarationContext.class,0);
		}
		public MemberDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_memberDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterMemberDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitMemberDeclaration(this);
		}
	}

	public final MemberDeclarationContext memberDeclaration() throws RecognitionException {
		MemberDeclarationContext _localctx = new MemberDeclarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_memberDeclaration);
		try {
			setState(353);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(343);
				methodDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(344);
				genericMethodDeclaration();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(345);
				fieldDeclaration();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(346);
				constructorDeclaration();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(347);
				genericConstructorDeclaration();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(348);
				interfaceDeclaration();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(349);
				annotationTypeDeclaration();
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(350);
				classDeclaration();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(351);
				enumDeclaration();
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(352);
				pointcutDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationContext extends ParserRuleContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public AnnotationNameContext annotationName() {
			return getRuleContext(AnnotationNameContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public ElementValuePairsContext elementValuePairs() {
			return getRuleContext(ElementValuePairsContext.class,0);
		}
		public ElementValueContext elementValue() {
			return getRuleContext(ElementValueContext.class,0);
		}
		public TerminalNode ANNOTATION_AFTER() { return getToken(AspectJParser.ANNOTATION_AFTER, 0); }
		public List<TerminalNode> DQUOTE() { return getTokens(AspectJParser.DQUOTE); }
		public TerminalNode DQUOTE(int i) {
			return getToken(AspectJParser.DQUOTE, i);
		}
		public PointcutExpressionContext pointcutExpression() {
			return getRuleContext(PointcutExpressionContext.class,0);
		}
		public TerminalNode ANNOTATION_AFTERRETURNING() { return getToken(AspectJParser.ANNOTATION_AFTERRETURNING, 0); }
		public TerminalNode POINTCUT() { return getToken(AspectJParser.POINTCUT, 0); }
		public List<TerminalNode> ASSIGN() { return getTokens(AspectJParser.ASSIGN); }
		public TerminalNode ASSIGN(int i) {
			return getToken(AspectJParser.ASSIGN, i);
		}
		public TerminalNode COMMA() { return getToken(AspectJParser.COMMA, 0); }
		public TerminalNode RETURNING() { return getToken(AspectJParser.RETURNING, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode ANNOTATION_AFTERTHROWING() { return getToken(AspectJParser.ANNOTATION_AFTERTHROWING, 0); }
		public TerminalNode ANNOTATION_AROUND() { return getToken(AspectJParser.ANNOTATION_AROUND, 0); }
		public TerminalNode ANNOTATION_ASPECT() { return getToken(AspectJParser.ANNOTATION_ASPECT, 0); }
		public PerClauseContext perClause() {
			return getRuleContext(PerClauseContext.class,0);
		}
		public TerminalNode ANNOTATION_BEFORE() { return getToken(AspectJParser.ANNOTATION_BEFORE, 0); }
		public TerminalNode ANNOTATION_DECLAREERROR() { return getToken(AspectJParser.ANNOTATION_DECLAREERROR, 0); }
		public TerminalNode ANNOTATION_DECLAREMIXIN() { return getToken(AspectJParser.ANNOTATION_DECLAREMIXIN, 0); }
		public TerminalNode ANNOTATION_VALUE() { return getToken(AspectJParser.ANNOTATION_VALUE, 0); }
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode ANNOTATION_INTERFACES() { return getToken(AspectJParser.ANNOTATION_INTERFACES, 0); }
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public ClassPatternListContext classPatternList() {
			return getRuleContext(ClassPatternListContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public TerminalNode ANNOTATION_DECLAREPARENTS() { return getToken(AspectJParser.ANNOTATION_DECLAREPARENTS, 0); }
		public TerminalNode ANNOTATION_DEFAULTIMPL() { return getToken(AspectJParser.ANNOTATION_DEFAULTIMPL, 0); }
		public ClassPatternContext classPattern() {
			return getRuleContext(ClassPatternContext.class,0);
		}
		public TerminalNode ANNOTATION_DECLAREPRECEDENCE() { return getToken(AspectJParser.ANNOTATION_DECLAREPRECEDENCE, 0); }
		public TypePatternListContext typePatternList() {
			return getRuleContext(TypePatternListContext.class,0);
		}
		public TerminalNode ANNOTATION_DECLAREWARNING() { return getToken(AspectJParser.ANNOTATION_DECLAREWARNING, 0); }
		public TerminalNode ANNOTATION_POINTCUT() { return getToken(AspectJParser.ANNOTATION_POINTCUT, 0); }
		public AnnotationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotation(this);
		}
	}

	public final AnnotationContext annotation() throws RecognitionException {
		AnnotationContext _localctx = new AnnotationContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_annotation);
		int _la;
		try {
			setState(502);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(355);
				match(AT);
				setState(356);
				annotationName();
				setState(363);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LPAREN) {
					{
					setState(357);
					match(LPAREN);
					setState(360);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
					case 1:
						{
						setState(358);
						elementValuePairs();
						}
						break;
					case 2:
						{
						setState(359);
						elementValue();
						}
						break;
					}
					setState(362);
					match(RPAREN);
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(365);
				match(AT);
				setState(366);
				match(ANNOTATION_AFTER);
				setState(367);
				match(LPAREN);
				setState(368);
				match(DQUOTE);
				setState(369);
				pointcutExpression(0);
				setState(370);
				match(DQUOTE);
				setState(371);
				match(RPAREN);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(373);
				match(AT);
				setState(374);
				match(ANNOTATION_AFTERRETURNING);
				setState(375);
				match(LPAREN);
				setState(376);
				match(DQUOTE);
				setState(377);
				pointcutExpression(0);
				setState(378);
				match(DQUOTE);
				setState(379);
				match(RPAREN);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(381);
				match(AT);
				setState(382);
				match(ANNOTATION_AFTERRETURNING);
				setState(383);
				match(LPAREN);
				setState(384);
				match(POINTCUT);
				setState(385);
				match(ASSIGN);
				setState(386);
				match(DQUOTE);
				setState(387);
				pointcutExpression(0);
				setState(388);
				match(DQUOTE);
				setState(389);
				match(COMMA);
				setState(390);
				match(RETURNING);
				setState(391);
				match(ASSIGN);
				setState(392);
				match(DQUOTE);
				setState(393);
				id();
				setState(394);
				match(DQUOTE);
				setState(395);
				match(RPAREN);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(397);
				match(AT);
				setState(398);
				match(ANNOTATION_AFTERTHROWING);
				setState(399);
				match(LPAREN);
				setState(400);
				match(DQUOTE);
				setState(401);
				pointcutExpression(0);
				setState(402);
				match(DQUOTE);
				setState(403);
				match(RPAREN);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(405);
				match(AT);
				setState(406);
				match(ANNOTATION_AROUND);
				setState(407);
				match(LPAREN);
				setState(408);
				match(DQUOTE);
				setState(409);
				pointcutExpression(0);
				setState(410);
				match(DQUOTE);
				setState(411);
				match(RPAREN);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(413);
				match(AT);
				setState(414);
				match(ANNOTATION_ASPECT);
				setState(421);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LPAREN) {
					{
					setState(415);
					match(LPAREN);
					setState(416);
					match(DQUOTE);
					setState(417);
					perClause();
					setState(418);
					match(DQUOTE);
					setState(419);
					match(RPAREN);
					}
				}

				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(423);
				match(AT);
				setState(424);
				match(ANNOTATION_BEFORE);
				setState(425);
				match(LPAREN);
				setState(426);
				match(DQUOTE);
				setState(427);
				pointcutExpression(0);
				setState(428);
				match(DQUOTE);
				setState(429);
				match(RPAREN);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(431);
				match(AT);
				setState(432);
				match(ANNOTATION_DECLAREERROR);
				setState(433);
				match(LPAREN);
				setState(434);
				match(DQUOTE);
				setState(435);
				pointcutExpression(0);
				setState(436);
				match(DQUOTE);
				setState(437);
				match(RPAREN);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(439);
				match(AT);
				setState(440);
				match(ANNOTATION_DECLAREMIXIN);
				setState(441);
				match(LPAREN);
				setState(442);
				match(ANNOTATION_VALUE);
				setState(443);
				match(ASSIGN);
				setState(444);
				match(DQUOTE);
				setState(445);
				typePattern(0);
				setState(446);
				match(DQUOTE);
				setState(447);
				match(COMMA);
				setState(448);
				match(ANNOTATION_INTERFACES);
				setState(449);
				match(ASSIGN);
				setState(450);
				match(LBRACE);
				setState(451);
				classPatternList();
				setState(452);
				match(RBRACE);
				setState(453);
				match(RPAREN);
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(455);
				match(AT);
				setState(456);
				match(ANNOTATION_DECLAREPARENTS);
				setState(457);
				match(LPAREN);
				setState(458);
				match(DQUOTE);
				setState(459);
				typePattern(0);
				setState(460);
				match(DQUOTE);
				setState(461);
				match(RPAREN);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(463);
				match(AT);
				setState(464);
				match(ANNOTATION_DECLAREPARENTS);
				setState(465);
				match(LPAREN);
				setState(466);
				match(ANNOTATION_VALUE);
				setState(467);
				match(ASSIGN);
				setState(468);
				match(DQUOTE);
				setState(469);
				typePattern(0);
				setState(470);
				match(DQUOTE);
				setState(471);
				match(COMMA);
				setState(472);
				match(ANNOTATION_DEFAULTIMPL);
				setState(473);
				match(ASSIGN);
				setState(474);
				classPattern();
				setState(475);
				match(RPAREN);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(477);
				match(AT);
				setState(478);
				match(ANNOTATION_DECLAREPRECEDENCE);
				setState(479);
				match(LPAREN);
				setState(480);
				match(DQUOTE);
				setState(481);
				typePatternList();
				setState(482);
				match(DQUOTE);
				setState(483);
				match(RPAREN);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(485);
				match(AT);
				setState(486);
				match(ANNOTATION_DECLAREWARNING);
				setState(487);
				match(LPAREN);
				setState(488);
				match(DQUOTE);
				setState(489);
				pointcutExpression(0);
				setState(490);
				match(DQUOTE);
				setState(491);
				match(RPAREN);
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(493);
				match(AT);
				setState(494);
				match(ANNOTATION_POINTCUT);
				setState(495);
				match(LPAREN);
				setState(496);
				match(DQUOTE);
				setState(498);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DOTDOT) | (1L << ADVICEEXECUTION) | (1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (IF - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (SHORT - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (LPAREN - 66)) | (1L << (DOT - 66)) | (1L << (BANG - 66)))) != 0) || _la==MUL || _la==Identifier) {
					{
					setState(497);
					pointcutExpression(0);
					}
				}

				setState(500);
				match(DQUOTE);
				setState(501);
				match(RPAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassPatternContext extends ParserRuleContext {
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public List<TerminalNode> DOT() { return getTokens(AspectJParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(AspectJParser.DOT, i);
		}
		public TerminalNode CLASS() { return getToken(AspectJParser.CLASS, 0); }
		public ClassPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassPattern(this);
		}
	}

	public final ClassPatternContext classPattern() throws RecognitionException {
		ClassPatternContext _localctx = new ClassPatternContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_classPattern);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(504);
			id();
			setState(509);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(505);
					match(DOT);
					setState(506);
					id();
					}
					} 
				}
				setState(511);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			setState(512);
			match(DOT);
			setState(513);
			match(CLASS);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassPatternListContext extends ParserRuleContext {
		public List<ClassPatternContext> classPattern() {
			return getRuleContexts(ClassPatternContext.class);
		}
		public ClassPatternContext classPattern(int i) {
			return getRuleContext(ClassPatternContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public ClassPatternListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classPatternList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassPatternList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassPatternList(this);
		}
	}

	public final ClassPatternListContext classPatternList() throws RecognitionException {
		ClassPatternListContext _localctx = new ClassPatternListContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_classPatternList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(515);
			classPattern();
			setState(520);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(516);
				match(COMMA);
				setState(517);
				classPattern();
				}
				}
				setState(522);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AdviceContext extends ParserRuleContext {
		public AdviceSpecContext adviceSpec() {
			return getRuleContext(AdviceSpecContext.class,0);
		}
		public TerminalNode COLON() { return getToken(AspectJParser.COLON, 0); }
		public PointcutExpressionContext pointcutExpression() {
			return getRuleContext(PointcutExpressionContext.class,0);
		}
		public MethodBodyContext methodBody() {
			return getRuleContext(MethodBodyContext.class,0);
		}
		public TerminalNode STRICTFP() { return getToken(AspectJParser.STRICTFP, 0); }
		public TerminalNode THROWS() { return getToken(AspectJParser.THROWS, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public AdviceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_advice; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAdvice(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAdvice(this);
		}
	}

	public final AdviceContext advice() throws RecognitionException {
		AdviceContext _localctx = new AdviceContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_advice);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(524);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STRICTFP) {
				{
				setState(523);
				match(STRICTFP);
				}
			}

			setState(526);
			adviceSpec();
			setState(529);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==THROWS) {
				{
				setState(527);
				match(THROWS);
				setState(528);
				typeList();
				}
			}

			setState(531);
			match(COLON);
			setState(532);
			pointcutExpression(0);
			setState(533);
			methodBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AdviceSpecContext extends ParserRuleContext {
		public TerminalNode BEFORE() { return getToken(AspectJParser.BEFORE, 0); }
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public TerminalNode AFTER() { return getToken(AspectJParser.AFTER, 0); }
		public TerminalNode RETURNING() { return getToken(AspectJParser.RETURNING, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public FormalParameterContext formalParameter() {
			return getRuleContext(FormalParameterContext.class,0);
		}
		public TerminalNode THROWING() { return getToken(AspectJParser.THROWING, 0); }
		public TerminalNode AROUND() { return getToken(AspectJParser.AROUND, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode VOID() { return getToken(AspectJParser.VOID, 0); }
		public AdviceSpecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_adviceSpec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAdviceSpec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAdviceSpec(this);
		}
	}

	public final AdviceSpecContext adviceSpec() throws RecognitionException {
		AdviceSpecContext _localctx = new AdviceSpecContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_adviceSpec);
		int _la;
		try {
			setState(565);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(535);
				match(BEFORE);
				setState(536);
				formalParameters();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(537);
				match(AFTER);
				setState(538);
				formalParameters();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(539);
				match(AFTER);
				setState(540);
				formalParameters();
				setState(541);
				match(RETURNING);
				setState(547);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LPAREN) {
					{
					setState(542);
					match(LPAREN);
					setState(544);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (SHORT - 66)))) != 0) || _la==Identifier) {
						{
						setState(543);
						formalParameter();
						}
					}

					setState(546);
					match(RPAREN);
					}
				}

				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(549);
				match(AFTER);
				setState(550);
				formalParameters();
				setState(551);
				match(THROWING);
				setState(557);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LPAREN) {
					{
					setState(552);
					match(LPAREN);
					setState(554);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (SHORT - 66)))) != 0) || _la==Identifier) {
						{
						setState(553);
						formalParameter();
						}
					}

					setState(556);
					match(RPAREN);
					}
				}

				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(561);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ARGS:
				case AFTER:
				case AROUND:
				case ASPECT:
				case BEFORE:
				case CALL:
				case CFLOW:
				case CFLOWBELOW:
				case DECLARE:
				case ERROR:
				case EXECUTION:
				case GET:
				case HANDLER:
				case INITIALIZATION:
				case ISSINGLETON:
				case PARENTS:
				case PERCFLOW:
				case PERCFLOWBELOW:
				case PERTARGET:
				case PERTHIS:
				case PERTYPEWITHIN:
				case POINTCUT:
				case PRECEDENCE:
				case PREINITIALIZATION:
				case PRIVILEGED:
				case RETURNING:
				case SET:
				case SOFT:
				case STATICINITIALIZATION:
				case TARGET:
				case THROWING:
				case WARNING:
				case WITHIN:
				case WITHINCODE:
				case BOOLEAN:
				case BYTE:
				case CHAR:
				case DOUBLE:
				case FLOAT:
				case INT:
				case LONG:
				case SHORT:
				case Identifier:
					{
					setState(559);
					type();
					}
					break;
				case VOID:
					{
					setState(560);
					match(VOID);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(563);
				match(AROUND);
				setState(564);
				formalParameters();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PerClauseContext extends ParserRuleContext {
		public TerminalNode PERTARGET() { return getToken(AspectJParser.PERTARGET, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public PointcutExpressionContext pointcutExpression() {
			return getRuleContext(PointcutExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public TerminalNode PERTHIS() { return getToken(AspectJParser.PERTHIS, 0); }
		public TerminalNode PERCFLOW() { return getToken(AspectJParser.PERCFLOW, 0); }
		public TerminalNode PERCFLOWBELOW() { return getToken(AspectJParser.PERCFLOWBELOW, 0); }
		public TerminalNode PERTYPEWITHIN() { return getToken(AspectJParser.PERTYPEWITHIN, 0); }
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode ISSINGLETON() { return getToken(AspectJParser.ISSINGLETON, 0); }
		public PerClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_perClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterPerClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitPerClause(this);
		}
	}

	public final PerClauseContext perClause() throws RecognitionException {
		PerClauseContext _localctx = new PerClauseContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_perClause);
		try {
			setState(595);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PERTARGET:
				enterOuterAlt(_localctx, 1);
				{
				setState(567);
				match(PERTARGET);
				setState(568);
				match(LPAREN);
				setState(569);
				pointcutExpression(0);
				setState(570);
				match(RPAREN);
				}
				break;
			case PERTHIS:
				enterOuterAlt(_localctx, 2);
				{
				setState(572);
				match(PERTHIS);
				setState(573);
				match(LPAREN);
				setState(574);
				pointcutExpression(0);
				setState(575);
				match(RPAREN);
				}
				break;
			case PERCFLOW:
				enterOuterAlt(_localctx, 3);
				{
				setState(577);
				match(PERCFLOW);
				setState(578);
				match(LPAREN);
				setState(579);
				pointcutExpression(0);
				setState(580);
				match(RPAREN);
				}
				break;
			case PERCFLOWBELOW:
				enterOuterAlt(_localctx, 4);
				{
				setState(582);
				match(PERCFLOWBELOW);
				setState(583);
				match(LPAREN);
				setState(584);
				pointcutExpression(0);
				setState(585);
				match(RPAREN);
				}
				break;
			case PERTYPEWITHIN:
				enterOuterAlt(_localctx, 5);
				{
				setState(587);
				match(PERTYPEWITHIN);
				setState(588);
				match(LPAREN);
				setState(589);
				typePattern(0);
				setState(590);
				match(RPAREN);
				}
				break;
			case ISSINGLETON:
				enterOuterAlt(_localctx, 6);
				{
				setState(592);
				match(ISSINGLETON);
				setState(593);
				match(LPAREN);
				setState(594);
				match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PointcutDeclarationContext extends ParserRuleContext {
		public TerminalNode ABSTRACT() { return getToken(AspectJParser.ABSTRACT, 0); }
		public TerminalNode POINTCUT() { return getToken(AspectJParser.POINTCUT, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public TerminalNode COLON() { return getToken(AspectJParser.COLON, 0); }
		public PointcutExpressionContext pointcutExpression() {
			return getRuleContext(PointcutExpressionContext.class,0);
		}
		public PointcutDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pointcutDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterPointcutDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitPointcutDeclaration(this);
		}
	}

	public final PointcutDeclarationContext pointcutDeclaration() throws RecognitionException {
		PointcutDeclarationContext _localctx = new PointcutDeclarationContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_pointcutDeclaration);
		int _la;
		try {
			setState(622);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(597);
				match(ABSTRACT);
				setState(601);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (NATIVE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SYNCHRONIZED - 58)) | (1L << (TRANSIENT - 58)) | (1L << (VOLATILE - 58)))) != 0)) {
					{
					{
					setState(598);
					modifier();
					}
					}
					setState(603);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(604);
				match(POINTCUT);
				setState(605);
				id();
				setState(606);
				formalParameters();
				setState(607);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(612);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (NATIVE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SYNCHRONIZED - 58)) | (1L << (TRANSIENT - 58)) | (1L << (VOLATILE - 58)))) != 0)) {
					{
					{
					setState(609);
					modifier();
					}
					}
					setState(614);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(615);
				match(POINTCUT);
				setState(616);
				id();
				setState(617);
				formalParameters();
				setState(618);
				match(COLON);
				setState(619);
				pointcutExpression(0);
				setState(620);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PointcutExpressionContext extends ParserRuleContext {
		public PointcutPrimitiveContext pointcutPrimitive() {
			return getRuleContext(PointcutPrimitiveContext.class,0);
		}
		public ReferencePointcutContext referencePointcut() {
			return getRuleContext(ReferencePointcutContext.class,0);
		}
		public TerminalNode BANG() { return getToken(AspectJParser.BANG, 0); }
		public List<PointcutExpressionContext> pointcutExpression() {
			return getRuleContexts(PointcutExpressionContext.class);
		}
		public PointcutExpressionContext pointcutExpression(int i) {
			return getRuleContext(PointcutExpressionContext.class,i);
		}
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public TerminalNode AND() { return getToken(AspectJParser.AND, 0); }
		public TerminalNode OR() { return getToken(AspectJParser.OR, 0); }
		public PointcutExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pointcutExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterPointcutExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitPointcutExpression(this);
		}
	}

	public final PointcutExpressionContext pointcutExpression() throws RecognitionException {
		return pointcutExpression(0);
	}

	private PointcutExpressionContext pointcutExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PointcutExpressionContext _localctx = new PointcutExpressionContext(_ctx, _parentState);
		PointcutExpressionContext _prevctx = _localctx;
		int _startState = 24;
		enterRecursionRule(_localctx, 24, RULE_pointcutExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(635);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				{
				setState(627);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,30,_ctx) ) {
				case 1:
					{
					setState(625);
					pointcutPrimitive();
					}
					break;
				case 2:
					{
					setState(626);
					referencePointcut();
					}
					break;
				}
				}
				break;
			case 2:
				{
				setState(629);
				match(BANG);
				setState(630);
				pointcutExpression(4);
				}
				break;
			case 3:
				{
				setState(631);
				match(LPAREN);
				setState(632);
				pointcutExpression(0);
				setState(633);
				match(RPAREN);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(645);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(643);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
					case 1:
						{
						_localctx = new PointcutExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_pointcutExpression);
						setState(637);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(638);
						match(AND);
						setState(639);
						pointcutExpression(3);
						}
						break;
					case 2:
						{
						_localctx = new PointcutExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_pointcutExpression);
						setState(640);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(641);
						match(OR);
						setState(642);
						pointcutExpression(2);
						}
						break;
					}
					} 
				}
				setState(647);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PointcutPrimitiveContext extends ParserRuleContext {
		public PointcutPrimitiveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pointcutPrimitive; }
	 
		public PointcutPrimitiveContext() { }
		public void copyFrom(PointcutPrimitiveContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class InitializationPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode INITIALIZATION() { return getToken(AspectJParser.INITIALIZATION, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public ConstructorPatternContext constructorPattern() {
			return getRuleContext(ConstructorPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public InitializationPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInitializationPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInitializationPointcut(this);
		}
	}
	public static class StaticInitializationPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode STATICINITIALIZATION() { return getToken(AspectJParser.STATICINITIALIZATION, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public OptionalParensTypePatternContext optionalParensTypePattern() {
			return getRuleContext(OptionalParensTypePatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public StaticInitializationPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterStaticInitializationPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitStaticInitializationPointcut(this);
		}
	}
	public static class CFlowPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode CFLOW() { return getToken(AspectJParser.CFLOW, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public PointcutExpressionContext pointcutExpression() {
			return getRuleContext(PointcutExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public CFlowPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCFlowPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCFlowPointcut(this);
		}
	}
	public static class AnnotationArgsPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode ARGS() { return getToken(AspectJParser.ARGS, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public AnnotationsOrIdentifiersPatternContext annotationsOrIdentifiersPattern() {
			return getRuleContext(AnnotationsOrIdentifiersPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationArgsPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationArgsPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationArgsPointcut(this);
		}
	}
	public static class GetPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode GET() { return getToken(AspectJParser.GET, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public FieldPatternContext fieldPattern() {
			return getRuleContext(FieldPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public GetPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterGetPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitGetPointcut(this);
		}
	}
	public static class ExecutionPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode EXECUTION() { return getToken(AspectJParser.EXECUTION, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public MethodOrConstructorPatternContext methodOrConstructorPattern() {
			return getRuleContext(MethodOrConstructorPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public ExecutionPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterExecutionPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitExecutionPointcut(this);
		}
	}
	public static class TargetPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode TARGET() { return getToken(AspectJParser.TARGET, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TypeOrIdentifierContext typeOrIdentifier() {
			return getRuleContext(TypeOrIdentifierContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public TargetPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTargetPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTargetPointcut(this);
		}
	}
	public static class AdviceExecutionPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode ADVICEEXECUTION() { return getToken(AspectJParser.ADVICEEXECUTION, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AdviceExecutionPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAdviceExecutionPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAdviceExecutionPointcut(this);
		}
	}
	public static class AnnotationPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode ANNOTATION() { return getToken(AspectJParser.ANNOTATION, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public AnnotationOrIdentiferContext annotationOrIdentifer() {
			return getRuleContext(AnnotationOrIdentiferContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationPointcut(this);
		}
	}
	public static class AnnotationTargetPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode TARGET() { return getToken(AspectJParser.TARGET, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public AnnotationOrIdentiferContext annotationOrIdentifer() {
			return getRuleContext(AnnotationOrIdentiferContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationTargetPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationTargetPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationTargetPointcut(this);
		}
	}
	public static class AnnotationThisPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode THIS() { return getToken(AspectJParser.THIS, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public AnnotationOrIdentiferContext annotationOrIdentifer() {
			return getRuleContext(AnnotationOrIdentiferContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationThisPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationThisPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationThisPointcut(this);
		}
	}
	public static class SetPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode SET() { return getToken(AspectJParser.SET, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public FieldPatternContext fieldPattern() {
			return getRuleContext(FieldPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public SetPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterSetPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitSetPointcut(this);
		}
	}
	public static class WithinCodePointcutContext extends PointcutPrimitiveContext {
		public TerminalNode WITHINCODE() { return getToken(AspectJParser.WITHINCODE, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public MethodOrConstructorPatternContext methodOrConstructorPattern() {
			return getRuleContext(MethodOrConstructorPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public WithinCodePointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterWithinCodePointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitWithinCodePointcut(this);
		}
	}
	public static class ArgsPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode ARGS() { return getToken(AspectJParser.ARGS, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public ArgsPatternListContext argsPatternList() {
			return getRuleContext(ArgsPatternListContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public ArgsPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterArgsPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitArgsPointcut(this);
		}
	}
	public static class AnnotationWithinPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode WITHIN() { return getToken(AspectJParser.WITHIN, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public AnnotationOrIdentiferContext annotationOrIdentifer() {
			return getRuleContext(AnnotationOrIdentiferContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationWithinPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationWithinPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationWithinPointcut(this);
		}
	}
	public static class CallPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode CALL() { return getToken(AspectJParser.CALL, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public MethodOrConstructorPatternContext methodOrConstructorPattern() {
			return getRuleContext(MethodOrConstructorPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public CallPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCallPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCallPointcut(this);
		}
	}
	public static class WithinPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode WITHIN() { return getToken(AspectJParser.WITHIN, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public OptionalParensTypePatternContext optionalParensTypePattern() {
			return getRuleContext(OptionalParensTypePatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public WithinPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterWithinPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitWithinPointcut(this);
		}
	}
	public static class AnnotationWithinCodePointcutContext extends PointcutPrimitiveContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode WITHINCODE() { return getToken(AspectJParser.WITHINCODE, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public AnnotationOrIdentiferContext annotationOrIdentifer() {
			return getRuleContext(AnnotationOrIdentiferContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationWithinCodePointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationWithinCodePointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationWithinCodePointcut(this);
		}
	}
	public static class IfPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode IF() { return getToken(AspectJParser.IF, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public IfPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterIfPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitIfPointcut(this);
		}
	}
	public static class PreInitializationPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode PREINITIALIZATION() { return getToken(AspectJParser.PREINITIALIZATION, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public ConstructorPatternContext constructorPattern() {
			return getRuleContext(ConstructorPatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public PreInitializationPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterPreInitializationPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitPreInitializationPointcut(this);
		}
	}
	public static class CFlowBelowPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode CFLOWBELOW() { return getToken(AspectJParser.CFLOWBELOW, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public PointcutExpressionContext pointcutExpression() {
			return getRuleContext(PointcutExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public CFlowBelowPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCFlowBelowPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCFlowBelowPointcut(this);
		}
	}
	public static class ThisPointcutPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode THIS() { return getToken(AspectJParser.THIS, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TypeOrIdentifierContext typeOrIdentifier() {
			return getRuleContext(TypeOrIdentifierContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public ThisPointcutPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterThisPointcutPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitThisPointcutPointcut(this);
		}
	}
	public static class HandlerPointcutContext extends PointcutPrimitiveContext {
		public TerminalNode HANDLER() { return getToken(AspectJParser.HANDLER, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public OptionalParensTypePatternContext optionalParensTypePattern() {
			return getRuleContext(OptionalParensTypePatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public HandlerPointcutContext(PointcutPrimitiveContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterHandlerPointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitHandlerPointcut(this);
		}
	}

	public final PointcutPrimitiveContext pointcutPrimitive() throws RecognitionException {
		PointcutPrimitiveContext _localctx = new PointcutPrimitiveContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_pointcutPrimitive);
		int _la;
		try {
			setState(768);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				_localctx = new CallPointcutContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(648);
				match(CALL);
				setState(649);
				match(LPAREN);
				setState(650);
				methodOrConstructorPattern();
				setState(651);
				match(RPAREN);
				}
				break;
			case 2:
				_localctx = new ExecutionPointcutContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(653);
				match(EXECUTION);
				setState(654);
				match(LPAREN);
				setState(655);
				methodOrConstructorPattern();
				setState(656);
				match(RPAREN);
				}
				break;
			case 3:
				_localctx = new InitializationPointcutContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(658);
				match(INITIALIZATION);
				setState(659);
				match(LPAREN);
				setState(660);
				constructorPattern();
				setState(661);
				match(RPAREN);
				}
				break;
			case 4:
				_localctx = new PreInitializationPointcutContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(663);
				match(PREINITIALIZATION);
				setState(664);
				match(LPAREN);
				setState(665);
				constructorPattern();
				setState(666);
				match(RPAREN);
				}
				break;
			case 5:
				_localctx = new StaticInitializationPointcutContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(668);
				match(STATICINITIALIZATION);
				setState(669);
				match(LPAREN);
				setState(670);
				optionalParensTypePattern();
				setState(671);
				match(RPAREN);
				}
				break;
			case 6:
				_localctx = new GetPointcutContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(673);
				match(GET);
				setState(674);
				match(LPAREN);
				setState(675);
				fieldPattern();
				setState(676);
				match(RPAREN);
				}
				break;
			case 7:
				_localctx = new SetPointcutContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(678);
				match(SET);
				setState(679);
				match(LPAREN);
				setState(680);
				fieldPattern();
				setState(681);
				match(RPAREN);
				}
				break;
			case 8:
				_localctx = new HandlerPointcutContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(683);
				match(HANDLER);
				setState(684);
				match(LPAREN);
				setState(685);
				optionalParensTypePattern();
				setState(686);
				match(RPAREN);
				}
				break;
			case 9:
				_localctx = new AdviceExecutionPointcutContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(688);
				match(ADVICEEXECUTION);
				setState(689);
				match(LPAREN);
				setState(690);
				match(RPAREN);
				}
				break;
			case 10:
				_localctx = new WithinPointcutContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(691);
				match(WITHIN);
				setState(692);
				match(LPAREN);
				setState(693);
				optionalParensTypePattern();
				setState(694);
				match(RPAREN);
				}
				break;
			case 11:
				_localctx = new WithinCodePointcutContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(696);
				match(WITHINCODE);
				setState(697);
				match(LPAREN);
				setState(698);
				methodOrConstructorPattern();
				setState(699);
				match(RPAREN);
				}
				break;
			case 12:
				_localctx = new CFlowPointcutContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(701);
				match(CFLOW);
				setState(702);
				match(LPAREN);
				setState(703);
				pointcutExpression(0);
				setState(704);
				match(RPAREN);
				}
				break;
			case 13:
				_localctx = new CFlowBelowPointcutContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(706);
				match(CFLOWBELOW);
				setState(707);
				match(LPAREN);
				setState(708);
				pointcutExpression(0);
				setState(709);
				match(RPAREN);
				}
				break;
			case 14:
				_localctx = new IfPointcutContext(_localctx);
				enterOuterAlt(_localctx, 14);
				{
				setState(711);
				match(IF);
				setState(712);
				match(LPAREN);
				setState(714);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
					{
					setState(713);
					expression(0);
					}
				}

				setState(716);
				match(RPAREN);
				}
				break;
			case 15:
				_localctx = new ThisPointcutPointcutContext(_localctx);
				enterOuterAlt(_localctx, 15);
				{
				setState(717);
				match(THIS);
				setState(718);
				match(LPAREN);
				setState(719);
				typeOrIdentifier();
				setState(720);
				match(RPAREN);
				}
				break;
			case 16:
				_localctx = new TargetPointcutContext(_localctx);
				enterOuterAlt(_localctx, 16);
				{
				setState(722);
				match(TARGET);
				setState(723);
				match(LPAREN);
				setState(724);
				typeOrIdentifier();
				setState(725);
				match(RPAREN);
				}
				break;
			case 17:
				_localctx = new ArgsPointcutContext(_localctx);
				enterOuterAlt(_localctx, 17);
				{
				setState(727);
				match(ARGS);
				setState(728);
				match(LPAREN);
				setState(729);
				argsPatternList();
				setState(730);
				match(RPAREN);
				}
				break;
			case 18:
				_localctx = new AnnotationThisPointcutContext(_localctx);
				enterOuterAlt(_localctx, 18);
				{
				setState(732);
				match(AT);
				setState(733);
				match(THIS);
				setState(734);
				match(LPAREN);
				setState(735);
				annotationOrIdentifer();
				setState(736);
				match(RPAREN);
				}
				break;
			case 19:
				_localctx = new AnnotationTargetPointcutContext(_localctx);
				enterOuterAlt(_localctx, 19);
				{
				setState(738);
				match(AT);
				setState(739);
				match(TARGET);
				setState(740);
				match(LPAREN);
				setState(741);
				annotationOrIdentifer();
				setState(742);
				match(RPAREN);
				}
				break;
			case 20:
				_localctx = new AnnotationArgsPointcutContext(_localctx);
				enterOuterAlt(_localctx, 20);
				{
				setState(744);
				match(AT);
				setState(745);
				match(ARGS);
				setState(746);
				match(LPAREN);
				setState(747);
				annotationsOrIdentifiersPattern();
				setState(748);
				match(RPAREN);
				}
				break;
			case 21:
				_localctx = new AnnotationWithinPointcutContext(_localctx);
				enterOuterAlt(_localctx, 21);
				{
				setState(750);
				match(AT);
				setState(751);
				match(WITHIN);
				setState(752);
				match(LPAREN);
				setState(753);
				annotationOrIdentifer();
				setState(754);
				match(RPAREN);
				}
				break;
			case 22:
				_localctx = new AnnotationWithinCodePointcutContext(_localctx);
				enterOuterAlt(_localctx, 22);
				{
				setState(756);
				match(AT);
				setState(757);
				match(WITHINCODE);
				setState(758);
				match(LPAREN);
				setState(759);
				annotationOrIdentifer();
				setState(760);
				match(RPAREN);
				}
				break;
			case 23:
				_localctx = new AnnotationPointcutContext(_localctx);
				enterOuterAlt(_localctx, 23);
				{
				setState(762);
				match(AT);
				setState(763);
				match(ANNOTATION);
				setState(764);
				match(LPAREN);
				setState(765);
				annotationOrIdentifer();
				setState(766);
				match(RPAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReferencePointcutContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public FormalParametersPatternContext formalParametersPattern() {
			return getRuleContext(FormalParametersPatternContext.class,0);
		}
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode DOT() { return getToken(AspectJParser.DOT, 0); }
		public ReferencePointcutContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_referencePointcut; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterReferencePointcut(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitReferencePointcut(this);
		}
	}

	public final ReferencePointcutContext referencePointcut() throws RecognitionException {
		ReferencePointcutContext _localctx = new ReferencePointcutContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_referencePointcut);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(773);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				{
				setState(770);
				typePattern(0);
				setState(771);
				match(DOT);
				}
				break;
			}
			setState(775);
			id();
			setState(776);
			formalParametersPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterTypeMemberDeclarationContext extends ParserRuleContext {
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public TerminalNode DOT() { return getToken(AspectJParser.DOT, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public MethodBodyContext methodBody() {
			return getRuleContext(MethodBodyContext.class,0);
		}
		public TerminalNode VOID() { return getToken(AspectJParser.VOID, 0); }
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public TerminalNode THROWS() { return getToken(AspectJParser.THROWS, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public TerminalNode ABSTRACT() { return getToken(AspectJParser.ABSTRACT, 0); }
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public TerminalNode NEW() { return getToken(AspectJParser.NEW, 0); }
		public TerminalNode ASSIGN() { return getToken(AspectJParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public InterTypeMemberDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interTypeMemberDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInterTypeMemberDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInterTypeMemberDeclaration(this);
		}
	}

	public final InterTypeMemberDeclarationContext interTypeMemberDeclaration() throws RecognitionException {
		InterTypeMemberDeclarationContext _localctx = new InterTypeMemberDeclarationContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_interTypeMemberDeclaration);
		int _la;
		try {
			int _alt;
			setState(860);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,49,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(781);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (NATIVE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SYNCHRONIZED - 58)) | (1L << (TRANSIENT - 58)) | (1L << (VOLATILE - 58)))) != 0)) {
					{
					{
					setState(778);
					modifier();
					}
					}
					setState(783);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(786);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ARGS:
				case AFTER:
				case AROUND:
				case ASPECT:
				case BEFORE:
				case CALL:
				case CFLOW:
				case CFLOWBELOW:
				case DECLARE:
				case ERROR:
				case EXECUTION:
				case GET:
				case HANDLER:
				case INITIALIZATION:
				case ISSINGLETON:
				case PARENTS:
				case PERCFLOW:
				case PERCFLOWBELOW:
				case PERTARGET:
				case PERTHIS:
				case PERTYPEWITHIN:
				case POINTCUT:
				case PRECEDENCE:
				case PREINITIALIZATION:
				case PRIVILEGED:
				case RETURNING:
				case SET:
				case SOFT:
				case STATICINITIALIZATION:
				case TARGET:
				case THROWING:
				case WARNING:
				case WITHIN:
				case WITHINCODE:
				case BOOLEAN:
				case BYTE:
				case CHAR:
				case DOUBLE:
				case FLOAT:
				case INT:
				case LONG:
				case SHORT:
				case Identifier:
					{
					setState(784);
					type();
					}
					break;
				case VOID:
					{
					setState(785);
					match(VOID);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(788);
				type();
				setState(789);
				match(DOT);
				setState(790);
				id();
				setState(791);
				formalParameters();
				setState(794);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==THROWS) {
					{
					setState(792);
					match(THROWS);
					setState(793);
					typeList();
					}
				}

				setState(796);
				methodBody();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(801);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(798);
						modifier();
						}
						} 
					}
					setState(803);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
				}
				setState(804);
				match(ABSTRACT);
				setState(808);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (NATIVE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SYNCHRONIZED - 58)) | (1L << (TRANSIENT - 58)) | (1L << (VOLATILE - 58)))) != 0)) {
					{
					{
					setState(805);
					modifier();
					}
					}
					setState(810);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(813);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ARGS:
				case AFTER:
				case AROUND:
				case ASPECT:
				case BEFORE:
				case CALL:
				case CFLOW:
				case CFLOWBELOW:
				case DECLARE:
				case ERROR:
				case EXECUTION:
				case GET:
				case HANDLER:
				case INITIALIZATION:
				case ISSINGLETON:
				case PARENTS:
				case PERCFLOW:
				case PERCFLOWBELOW:
				case PERTARGET:
				case PERTHIS:
				case PERTYPEWITHIN:
				case POINTCUT:
				case PRECEDENCE:
				case PREINITIALIZATION:
				case PRIVILEGED:
				case RETURNING:
				case SET:
				case SOFT:
				case STATICINITIALIZATION:
				case TARGET:
				case THROWING:
				case WARNING:
				case WITHIN:
				case WITHINCODE:
				case BOOLEAN:
				case BYTE:
				case CHAR:
				case DOUBLE:
				case FLOAT:
				case INT:
				case LONG:
				case SHORT:
				case Identifier:
					{
					setState(811);
					type();
					}
					break;
				case VOID:
					{
					setState(812);
					match(VOID);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(815);
				type();
				setState(816);
				match(DOT);
				setState(817);
				id();
				setState(818);
				formalParameters();
				setState(821);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==THROWS) {
					{
					setState(819);
					match(THROWS);
					setState(820);
					typeList();
					}
				}

				setState(823);
				match(SEMI);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(828);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (NATIVE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SYNCHRONIZED - 58)) | (1L << (TRANSIENT - 58)) | (1L << (VOLATILE - 58)))) != 0)) {
					{
					{
					setState(825);
					modifier();
					}
					}
					setState(830);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(831);
				type();
				setState(832);
				match(DOT);
				setState(833);
				match(NEW);
				setState(834);
				formalParameters();
				setState(837);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==THROWS) {
					{
					setState(835);
					match(THROWS);
					setState(836);
					typeList();
					}
				}

				setState(839);
				methodBody();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(844);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (NATIVE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SYNCHRONIZED - 58)) | (1L << (TRANSIENT - 58)) | (1L << (VOLATILE - 58)))) != 0)) {
					{
					{
					setState(841);
					modifier();
					}
					}
					setState(846);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(849);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ARGS:
				case AFTER:
				case AROUND:
				case ASPECT:
				case BEFORE:
				case CALL:
				case CFLOW:
				case CFLOWBELOW:
				case DECLARE:
				case ERROR:
				case EXECUTION:
				case GET:
				case HANDLER:
				case INITIALIZATION:
				case ISSINGLETON:
				case PARENTS:
				case PERCFLOW:
				case PERCFLOWBELOW:
				case PERTARGET:
				case PERTHIS:
				case PERTYPEWITHIN:
				case POINTCUT:
				case PRECEDENCE:
				case PREINITIALIZATION:
				case PRIVILEGED:
				case RETURNING:
				case SET:
				case SOFT:
				case STATICINITIALIZATION:
				case TARGET:
				case THROWING:
				case WARNING:
				case WITHIN:
				case WITHINCODE:
				case BOOLEAN:
				case BYTE:
				case CHAR:
				case DOUBLE:
				case FLOAT:
				case INT:
				case LONG:
				case SHORT:
				case Identifier:
					{
					setState(847);
					type();
					}
					break;
				case VOID:
					{
					setState(848);
					match(VOID);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(851);
				type();
				setState(852);
				match(DOT);
				setState(853);
				id();
				setState(856);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ASSIGN) {
					{
					setState(854);
					match(ASSIGN);
					setState(855);
					expression(0);
					}
				}

				setState(858);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterTypeDeclarationContext extends ParserRuleContext {
		public TerminalNode DECLARE() { return getToken(AspectJParser.DECLARE, 0); }
		public TerminalNode PARENTS() { return getToken(AspectJParser.PARENTS, 0); }
		public List<TerminalNode> COLON() { return getTokens(AspectJParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(AspectJParser.COLON, i);
		}
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode EXTENDS() { return getToken(AspectJParser.EXTENDS, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public TerminalNode IMPLEMENTS() { return getToken(AspectJParser.IMPLEMENTS, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public TerminalNode WARNING() { return getToken(AspectJParser.WARNING, 0); }
		public PointcutExpressionContext pointcutExpression() {
			return getRuleContext(PointcutExpressionContext.class,0);
		}
		public TerminalNode StringLiteral() { return getToken(AspectJParser.StringLiteral, 0); }
		public TerminalNode ERROR() { return getToken(AspectJParser.ERROR, 0); }
		public TerminalNode SOFT() { return getToken(AspectJParser.SOFT, 0); }
		public TerminalNode PRECEDENCE() { return getToken(AspectJParser.PRECEDENCE, 0); }
		public TypePatternListContext typePatternList() {
			return getRuleContext(TypePatternListContext.class,0);
		}
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode ANNOTATION_TYPE() { return getToken(AspectJParser.ANNOTATION_TYPE, 0); }
		public AnnotationContext annotation() {
			return getRuleContext(AnnotationContext.class,0);
		}
		public TerminalNode ANNOTATION_METHOD() { return getToken(AspectJParser.ANNOTATION_METHOD, 0); }
		public MethodPatternContext methodPattern() {
			return getRuleContext(MethodPatternContext.class,0);
		}
		public TerminalNode ANNOTATION_CONSTRUCTOR() { return getToken(AspectJParser.ANNOTATION_CONSTRUCTOR, 0); }
		public ConstructorPatternContext constructorPattern() {
			return getRuleContext(ConstructorPatternContext.class,0);
		}
		public TerminalNode ANNOTATION_FIELD() { return getToken(AspectJParser.ANNOTATION_FIELD, 0); }
		public FieldPatternContext fieldPattern() {
			return getRuleContext(FieldPatternContext.class,0);
		}
		public InterTypeDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interTypeDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInterTypeDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInterTypeDeclaration(this);
		}
	}

	public final InterTypeDeclarationContext interTypeDeclaration() throws RecognitionException {
		InterTypeDeclarationContext _localctx = new InterTypeDeclarationContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_interTypeDeclaration);
		try {
			setState(944);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(862);
				match(DECLARE);
				setState(863);
				match(PARENTS);
				setState(864);
				match(COLON);
				setState(865);
				typePattern(0);
				setState(866);
				match(EXTENDS);
				setState(867);
				type();
				setState(868);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(870);
				match(DECLARE);
				setState(871);
				match(PARENTS);
				setState(872);
				match(COLON);
				setState(873);
				typePattern(0);
				setState(874);
				match(IMPLEMENTS);
				setState(875);
				typeList();
				setState(876);
				match(SEMI);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(878);
				match(DECLARE);
				setState(879);
				match(WARNING);
				setState(880);
				match(COLON);
				setState(881);
				pointcutExpression(0);
				setState(882);
				match(COLON);
				setState(883);
				match(StringLiteral);
				setState(884);
				match(SEMI);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(886);
				match(DECLARE);
				setState(887);
				match(ERROR);
				setState(888);
				match(COLON);
				setState(889);
				pointcutExpression(0);
				setState(890);
				match(COLON);
				setState(891);
				match(StringLiteral);
				setState(892);
				match(SEMI);
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(894);
				match(DECLARE);
				setState(895);
				match(SOFT);
				setState(896);
				match(COLON);
				setState(897);
				type();
				setState(898);
				match(COLON);
				setState(899);
				pointcutExpression(0);
				setState(900);
				match(SEMI);
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(902);
				match(DECLARE);
				setState(903);
				match(PRECEDENCE);
				setState(904);
				match(COLON);
				setState(905);
				typePatternList();
				setState(906);
				match(SEMI);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(908);
				match(DECLARE);
				setState(909);
				match(AT);
				setState(910);
				match(ANNOTATION_TYPE);
				setState(911);
				match(COLON);
				setState(912);
				typePattern(0);
				setState(913);
				match(COLON);
				setState(914);
				annotation();
				setState(915);
				match(SEMI);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(917);
				match(DECLARE);
				setState(918);
				match(AT);
				setState(919);
				match(ANNOTATION_METHOD);
				setState(920);
				match(COLON);
				setState(921);
				methodPattern();
				setState(922);
				match(COLON);
				setState(923);
				annotation();
				setState(924);
				match(SEMI);
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(926);
				match(DECLARE);
				setState(927);
				match(AT);
				setState(928);
				match(ANNOTATION_CONSTRUCTOR);
				setState(929);
				match(COLON);
				setState(930);
				constructorPattern();
				setState(931);
				match(COLON);
				setState(932);
				annotation();
				setState(933);
				match(SEMI);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(935);
				match(DECLARE);
				setState(936);
				match(AT);
				setState(937);
				match(ANNOTATION_FIELD);
				setState(938);
				match(COLON);
				setState(939);
				fieldPattern();
				setState(940);
				match(COLON);
				setState(941);
				annotation();
				setState(942);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypePatternContext extends ParserRuleContext {
		public SimpleTypePatternContext simpleTypePattern() {
			return getRuleContext(SimpleTypePatternContext.class,0);
		}
		public TerminalNode BANG() { return getToken(AspectJParser.BANG, 0); }
		public List<TypePatternContext> typePattern() {
			return getRuleContexts(TypePatternContext.class);
		}
		public TypePatternContext typePattern(int i) {
			return getRuleContext(TypePatternContext.class,i);
		}
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationPatternContext annotationPattern() {
			return getRuleContext(AnnotationPatternContext.class,0);
		}
		public TerminalNode AND() { return getToken(AspectJParser.AND, 0); }
		public TerminalNode OR() { return getToken(AspectJParser.OR, 0); }
		public TypePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypePattern(this);
		}
	}

	public final TypePatternContext typePattern() throws RecognitionException {
		return typePattern(0);
	}

	private TypePatternContext typePattern(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		TypePatternContext _localctx = new TypePatternContext(_ctx, _parentState);
		TypePatternContext _prevctx = _localctx;
		int _startState = 34;
		enterRecursionRule(_localctx, 34, RULE_typePattern, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(957);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOTDOT:
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case VOID:
			case DOT:
			case MUL:
			case Identifier:
				{
				setState(947);
				simpleTypePattern();
				}
				break;
			case BANG:
				{
				setState(948);
				match(BANG);
				setState(949);
				typePattern(4);
				}
				break;
			case LPAREN:
				{
				setState(950);
				match(LPAREN);
				setState(952);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,51,_ctx) ) {
				case 1:
					{
					setState(951);
					annotationPattern();
					}
					break;
				}
				setState(954);
				typePattern(0);
				setState(955);
				match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(967);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(965);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
					case 1:
						{
						_localctx = new TypePatternContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_typePattern);
						setState(959);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(960);
						match(AND);
						setState(961);
						typePattern(3);
						}
						break;
					case 2:
						{
						_localctx = new TypePatternContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_typePattern);
						setState(962);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(963);
						match(OR);
						setState(964);
						typePattern(2);
						}
						break;
					}
					} 
				}
				setState(969);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SimpleTypePatternContext extends ParserRuleContext {
		public DottedNamePatternContext dottedNamePattern() {
			return getRuleContext(DottedNamePatternContext.class,0);
		}
		public TerminalNode ADD() { return getToken(AspectJParser.ADD, 0); }
		public List<TerminalNode> LBRACK() { return getTokens(AspectJParser.LBRACK); }
		public TerminalNode LBRACK(int i) {
			return getToken(AspectJParser.LBRACK, i);
		}
		public List<TerminalNode> RBRACK() { return getTokens(AspectJParser.RBRACK); }
		public TerminalNode RBRACK(int i) {
			return getToken(AspectJParser.RBRACK, i);
		}
		public SimpleTypePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleTypePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterSimpleTypePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitSimpleTypePattern(this);
		}
	}

	public final SimpleTypePatternContext simpleTypePattern() throws RecognitionException {
		SimpleTypePatternContext _localctx = new SimpleTypePatternContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_simpleTypePattern);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(970);
			dottedNamePattern();
			setState(972);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
			case 1:
				{
				setState(971);
				match(ADD);
				}
				break;
			}
			setState(978);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,56,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(974);
					match(LBRACK);
					setState(975);
					match(RBRACK);
					}
					} 
				}
				setState(980);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,56,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DottedNamePatternContext extends ParserRuleContext {
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public List<TerminalNode> MUL() { return getTokens(AspectJParser.MUL); }
		public TerminalNode MUL(int i) {
			return getToken(AspectJParser.MUL, i);
		}
		public List<TerminalNode> DOT() { return getTokens(AspectJParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(AspectJParser.DOT, i);
		}
		public List<TerminalNode> DOTDOT() { return getTokens(AspectJParser.DOTDOT); }
		public TerminalNode DOTDOT(int i) {
			return getToken(AspectJParser.DOTDOT, i);
		}
		public TerminalNode VOID() { return getToken(AspectJParser.VOID, 0); }
		public DottedNamePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dottedNamePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterDottedNamePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitDottedNamePattern(this);
		}
	}

	public final DottedNamePatternContext dottedNamePattern() throws RecognitionException {
		DottedNamePatternContext _localctx = new DottedNamePatternContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_dottedNamePattern);
		try {
			int _alt;
			setState(991);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOTDOT:
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case DOT:
			case MUL:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(986); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						setState(986);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,57,_ctx) ) {
						case 1:
							{
							setState(981);
							type();
							}
							break;
						case 2:
							{
							setState(982);
							id();
							}
							break;
						case 3:
							{
							setState(983);
							match(MUL);
							}
							break;
						case 4:
							{
							setState(984);
							match(DOT);
							}
							break;
						case 5:
							{
							setState(985);
							match(DOTDOT);
							}
							break;
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(988); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,58,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case VOID:
				enterOuterAlt(_localctx, 2);
				{
				setState(990);
				match(VOID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OptionalParensTypePatternContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationPatternContext annotationPattern() {
			return getRuleContext(AnnotationPatternContext.class,0);
		}
		public OptionalParensTypePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_optionalParensTypePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterOptionalParensTypePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitOptionalParensTypePattern(this);
		}
	}

	public final OptionalParensTypePatternContext optionalParensTypePattern() throws RecognitionException {
		OptionalParensTypePatternContext _localctx = new OptionalParensTypePatternContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_optionalParensTypePattern);
		try {
			setState(1004);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,62,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(993);
				match(LPAREN);
				setState(995);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
				case 1:
					{
					setState(994);
					annotationPattern();
					}
					break;
				}
				setState(997);
				typePattern(0);
				setState(998);
				match(RPAREN);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1001);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
				case 1:
					{
					setState(1000);
					annotationPattern();
					}
					break;
				}
				setState(1003);
				typePattern(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldPatternContext extends ParserRuleContext {
		public List<TypePatternContext> typePattern() {
			return getRuleContexts(TypePatternContext.class);
		}
		public TypePatternContext typePattern(int i) {
			return getRuleContext(TypePatternContext.class,i);
		}
		public SimpleNamePatternContext simpleNamePattern() {
			return getRuleContext(SimpleNamePatternContext.class,0);
		}
		public AnnotationPatternContext annotationPattern() {
			return getRuleContext(AnnotationPatternContext.class,0);
		}
		public FieldModifiersPatternContext fieldModifiersPattern() {
			return getRuleContext(FieldModifiersPatternContext.class,0);
		}
		public DotOrDotDotContext dotOrDotDot() {
			return getRuleContext(DotOrDotDotContext.class,0);
		}
		public FieldPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFieldPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFieldPattern(this);
		}
	}

	public final FieldPatternContext fieldPattern() throws RecognitionException {
		FieldPatternContext _localctx = new FieldPatternContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_fieldPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1007);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
			case 1:
				{
				setState(1006);
				annotationPattern();
				}
				break;
			}
			setState(1010);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,64,_ctx) ) {
			case 1:
				{
				setState(1009);
				fieldModifiersPattern();
				}
				break;
			}
			setState(1012);
			typePattern(0);
			setState(1016);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
			case 1:
				{
				setState(1013);
				typePattern(0);
				setState(1014);
				dotOrDotDot();
				}
				break;
			}
			setState(1018);
			simpleNamePattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldModifiersPatternContext extends ParserRuleContext {
		public FieldModifierContext fieldModifier() {
			return getRuleContext(FieldModifierContext.class,0);
		}
		public TerminalNode BANG() { return getToken(AspectJParser.BANG, 0); }
		public List<FieldModifiersPatternContext> fieldModifiersPattern() {
			return getRuleContexts(FieldModifiersPatternContext.class);
		}
		public FieldModifiersPatternContext fieldModifiersPattern(int i) {
			return getRuleContext(FieldModifiersPatternContext.class,i);
		}
		public FieldModifiersPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldModifiersPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFieldModifiersPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFieldModifiersPattern(this);
		}
	}

	public final FieldModifiersPatternContext fieldModifiersPattern() throws RecognitionException {
		FieldModifiersPatternContext _localctx = new FieldModifiersPatternContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_fieldModifiersPattern);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1021);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BANG) {
				{
				setState(1020);
				match(BANG);
				}
			}

			setState(1023);
			fieldModifier();
			setState(1027);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,67,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1024);
					fieldModifiersPattern();
					}
					} 
				}
				setState(1029);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,67,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldModifierContext extends ParserRuleContext {
		public TerminalNode PUBLIC() { return getToken(AspectJParser.PUBLIC, 0); }
		public TerminalNode PRIVATE() { return getToken(AspectJParser.PRIVATE, 0); }
		public TerminalNode PROTECTED() { return getToken(AspectJParser.PROTECTED, 0); }
		public TerminalNode STATIC() { return getToken(AspectJParser.STATIC, 0); }
		public TerminalNode TRANSIENT() { return getToken(AspectJParser.TRANSIENT, 0); }
		public TerminalNode FINAL() { return getToken(AspectJParser.FINAL, 0); }
		public FieldModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldModifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFieldModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFieldModifier(this);
		}
	}

	public final FieldModifierContext fieldModifier() throws RecognitionException {
		FieldModifierContext _localctx = new FieldModifierContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_fieldModifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1030);
			_la = _input.LA(1);
			if ( !(((((_la - 76)) & ~0x3f) == 0 && ((1L << (_la - 76)) & ((1L << (FINAL - 76)) | (1L << (PRIVATE - 76)) | (1L << (PROTECTED - 76)) | (1L << (PUBLIC - 76)) | (1L << (STATIC - 76)) | (1L << (TRANSIENT - 76)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DotOrDotDotContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(AspectJParser.DOT, 0); }
		public TerminalNode DOTDOT() { return getToken(AspectJParser.DOTDOT, 0); }
		public DotOrDotDotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dotOrDotDot; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterDotOrDotDot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitDotOrDotDot(this);
		}
	}

	public final DotOrDotDotContext dotOrDotDot() throws RecognitionException {
		DotOrDotDotContext _localctx = new DotOrDotDotContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_dotOrDotDot);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1032);
			_la = _input.LA(1);
			if ( !(_la==DOTDOT || _la==DOT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleNamePatternContext extends ParserRuleContext {
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public List<TerminalNode> MUL() { return getTokens(AspectJParser.MUL); }
		public TerminalNode MUL(int i) {
			return getToken(AspectJParser.MUL, i);
		}
		public SimpleNamePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleNamePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterSimpleNamePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitSimpleNamePattern(this);
		}
	}

	public final SimpleNamePatternContext simpleNamePattern() throws RecognitionException {
		SimpleNamePatternContext _localctx = new SimpleNamePatternContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_simpleNamePattern);
		int _la;
		try {
			int _alt;
			setState(1057);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1034);
				id();
				setState(1039);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1035);
						match(MUL);
						setState(1036);
						id();
						}
						} 
					}
					setState(1041);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,68,_ctx);
				}
				setState(1043);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==MUL) {
					{
					setState(1042);
					match(MUL);
					}
				}

				}
				break;
			case MUL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1045);
				match(MUL);
				setState(1051);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1046);
						id();
						setState(1047);
						match(MUL);
						}
						} 
					}
					setState(1053);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,70,_ctx);
				}
				setState(1055);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE))) != 0) || _la==Identifier) {
					{
					setState(1054);
					id();
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodOrConstructorPatternContext extends ParserRuleContext {
		public MethodPatternContext methodPattern() {
			return getRuleContext(MethodPatternContext.class,0);
		}
		public ConstructorPatternContext constructorPattern() {
			return getRuleContext(ConstructorPatternContext.class,0);
		}
		public MethodOrConstructorPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodOrConstructorPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterMethodOrConstructorPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitMethodOrConstructorPattern(this);
		}
	}

	public final MethodOrConstructorPatternContext methodOrConstructorPattern() throws RecognitionException {
		MethodOrConstructorPatternContext _localctx = new MethodOrConstructorPatternContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_methodOrConstructorPattern);
		try {
			setState(1061);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,73,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1059);
				methodPattern();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1060);
				constructorPattern();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodPatternContext extends ParserRuleContext {
		public List<TypePatternContext> typePattern() {
			return getRuleContexts(TypePatternContext.class);
		}
		public TypePatternContext typePattern(int i) {
			return getRuleContext(TypePatternContext.class,i);
		}
		public SimpleNamePatternContext simpleNamePattern() {
			return getRuleContext(SimpleNamePatternContext.class,0);
		}
		public FormalParametersPatternContext formalParametersPattern() {
			return getRuleContext(FormalParametersPatternContext.class,0);
		}
		public AnnotationPatternContext annotationPattern() {
			return getRuleContext(AnnotationPatternContext.class,0);
		}
		public MethodModifiersPatternContext methodModifiersPattern() {
			return getRuleContext(MethodModifiersPatternContext.class,0);
		}
		public DotOrDotDotContext dotOrDotDot() {
			return getRuleContext(DotOrDotDotContext.class,0);
		}
		public ThrowsPatternContext throwsPattern() {
			return getRuleContext(ThrowsPatternContext.class,0);
		}
		public MethodPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterMethodPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitMethodPattern(this);
		}
	}

	public final MethodPatternContext methodPattern() throws RecognitionException {
		MethodPatternContext _localctx = new MethodPatternContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_methodPattern);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1064);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,74,_ctx) ) {
			case 1:
				{
				setState(1063);
				annotationPattern();
				}
				break;
			}
			setState(1067);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,75,_ctx) ) {
			case 1:
				{
				setState(1066);
				methodModifiersPattern();
				}
				break;
			}
			setState(1069);
			typePattern(0);
			setState(1073);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,76,_ctx) ) {
			case 1:
				{
				setState(1070);
				typePattern(0);
				setState(1071);
				dotOrDotDot();
				}
				break;
			}
			setState(1075);
			simpleNamePattern();
			setState(1076);
			formalParametersPattern();
			setState(1078);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==THROWS) {
				{
				setState(1077);
				throwsPattern();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodModifiersPatternContext extends ParserRuleContext {
		public MethodModifierContext methodModifier() {
			return getRuleContext(MethodModifierContext.class,0);
		}
		public TerminalNode BANG() { return getToken(AspectJParser.BANG, 0); }
		public List<MethodModifiersPatternContext> methodModifiersPattern() {
			return getRuleContexts(MethodModifiersPatternContext.class);
		}
		public MethodModifiersPatternContext methodModifiersPattern(int i) {
			return getRuleContext(MethodModifiersPatternContext.class,i);
		}
		public MethodModifiersPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodModifiersPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterMethodModifiersPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitMethodModifiersPattern(this);
		}
	}

	public final MethodModifiersPatternContext methodModifiersPattern() throws RecognitionException {
		MethodModifiersPatternContext _localctx = new MethodModifiersPatternContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_methodModifiersPattern);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1081);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BANG) {
				{
				setState(1080);
				match(BANG);
				}
			}

			setState(1083);
			methodModifier();
			setState(1087);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,79,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1084);
					methodModifiersPattern();
					}
					} 
				}
				setState(1089);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,79,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodModifierContext extends ParserRuleContext {
		public TerminalNode PUBLIC() { return getToken(AspectJParser.PUBLIC, 0); }
		public TerminalNode PRIVATE() { return getToken(AspectJParser.PRIVATE, 0); }
		public TerminalNode PROTECTED() { return getToken(AspectJParser.PROTECTED, 0); }
		public TerminalNode STATIC() { return getToken(AspectJParser.STATIC, 0); }
		public TerminalNode SYNCHRONIZED() { return getToken(AspectJParser.SYNCHRONIZED, 0); }
		public TerminalNode FINAL() { return getToken(AspectJParser.FINAL, 0); }
		public MethodModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodModifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterMethodModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitMethodModifier(this);
		}
	}

	public final MethodModifierContext methodModifier() throws RecognitionException {
		MethodModifierContext _localctx = new MethodModifierContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_methodModifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1090);
			_la = _input.LA(1);
			if ( !(((((_la - 76)) & ~0x3f) == 0 && ((1L << (_la - 76)) & ((1L << (FINAL - 76)) | (1L << (PRIVATE - 76)) | (1L << (PROTECTED - 76)) | (1L << (PUBLIC - 76)) | (1L << (STATIC - 76)) | (1L << (SYNCHRONIZED - 76)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalsPatternContext extends ParserRuleContext {
		public TerminalNode DOTDOT() { return getToken(AspectJParser.DOTDOT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public List<FormalsPatternAfterDotDotContext> formalsPatternAfterDotDot() {
			return getRuleContexts(FormalsPatternAfterDotDotContext.class);
		}
		public FormalsPatternAfterDotDotContext formalsPatternAfterDotDot(int i) {
			return getRuleContext(FormalsPatternAfterDotDotContext.class,i);
		}
		public OptionalParensTypePatternContext optionalParensTypePattern() {
			return getRuleContext(OptionalParensTypePatternContext.class,0);
		}
		public List<FormalsPatternContext> formalsPattern() {
			return getRuleContexts(FormalsPatternContext.class);
		}
		public FormalsPatternContext formalsPattern(int i) {
			return getRuleContext(FormalsPatternContext.class,i);
		}
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode ELLIPSIS() { return getToken(AspectJParser.ELLIPSIS, 0); }
		public FormalsPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalsPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFormalsPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFormalsPattern(this);
		}
	}

	public final FormalsPatternContext formalsPattern() throws RecognitionException {
		FormalsPatternContext _localctx = new FormalsPatternContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_formalsPattern);
		try {
			int _alt;
			setState(1111);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,82,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1092);
				match(DOTDOT);
				setState(1097);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,80,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1093);
						match(COMMA);
						setState(1094);
						formalsPatternAfterDotDot();
						}
						} 
					}
					setState(1099);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,80,_ctx);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1100);
				optionalParensTypePattern();
				setState(1105);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,81,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1101);
						match(COMMA);
						setState(1102);
						formalsPattern();
						}
						} 
					}
					setState(1107);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,81,_ctx);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1108);
				typePattern(0);
				setState(1109);
				match(ELLIPSIS);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalsPatternAfterDotDotContext extends ParserRuleContext {
		public OptionalParensTypePatternContext optionalParensTypePattern() {
			return getRuleContext(OptionalParensTypePatternContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public List<FormalsPatternAfterDotDotContext> formalsPatternAfterDotDot() {
			return getRuleContexts(FormalsPatternAfterDotDotContext.class);
		}
		public FormalsPatternAfterDotDotContext formalsPatternAfterDotDot(int i) {
			return getRuleContext(FormalsPatternAfterDotDotContext.class,i);
		}
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode ELLIPSIS() { return getToken(AspectJParser.ELLIPSIS, 0); }
		public FormalsPatternAfterDotDotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalsPatternAfterDotDot; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFormalsPatternAfterDotDot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFormalsPatternAfterDotDot(this);
		}
	}

	public final FormalsPatternAfterDotDotContext formalsPatternAfterDotDot() throws RecognitionException {
		FormalsPatternAfterDotDotContext _localctx = new FormalsPatternAfterDotDotContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_formalsPatternAfterDotDot);
		try {
			int _alt;
			setState(1124);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,84,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1113);
				optionalParensTypePattern();
				setState(1118);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,83,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1114);
						match(COMMA);
						setState(1115);
						formalsPatternAfterDotDot();
						}
						} 
					}
					setState(1120);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,83,_ctx);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1121);
				typePattern(0);
				setState(1122);
				match(ELLIPSIS);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ThrowsPatternContext extends ParserRuleContext {
		public TerminalNode THROWS() { return getToken(AspectJParser.THROWS, 0); }
		public TypePatternListContext typePatternList() {
			return getRuleContext(TypePatternListContext.class,0);
		}
		public ThrowsPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_throwsPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterThrowsPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitThrowsPattern(this);
		}
	}

	public final ThrowsPatternContext throwsPattern() throws RecognitionException {
		ThrowsPatternContext _localctx = new ThrowsPatternContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_throwsPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1126);
			match(THROWS);
			setState(1127);
			typePatternList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypePatternListContext extends ParserRuleContext {
		public List<TypePatternContext> typePattern() {
			return getRuleContexts(TypePatternContext.class);
		}
		public TypePatternContext typePattern(int i) {
			return getRuleContext(TypePatternContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public TypePatternListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typePatternList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypePatternList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypePatternList(this);
		}
	}

	public final TypePatternListContext typePatternList() throws RecognitionException {
		TypePatternListContext _localctx = new TypePatternListContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_typePatternList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1129);
			typePattern(0);
			setState(1134);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1130);
				match(COMMA);
				setState(1131);
				typePattern(0);
				}
				}
				setState(1136);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorPatternContext extends ParserRuleContext {
		public TerminalNode NEW() { return getToken(AspectJParser.NEW, 0); }
		public FormalParametersPatternContext formalParametersPattern() {
			return getRuleContext(FormalParametersPatternContext.class,0);
		}
		public AnnotationPatternContext annotationPattern() {
			return getRuleContext(AnnotationPatternContext.class,0);
		}
		public ConstructorModifiersPatternContext constructorModifiersPattern() {
			return getRuleContext(ConstructorModifiersPatternContext.class,0);
		}
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public DotOrDotDotContext dotOrDotDot() {
			return getRuleContext(DotOrDotDotContext.class,0);
		}
		public ThrowsPatternContext throwsPattern() {
			return getRuleContext(ThrowsPatternContext.class,0);
		}
		public ConstructorPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstructorPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstructorPattern(this);
		}
	}

	public final ConstructorPatternContext constructorPattern() throws RecognitionException {
		ConstructorPatternContext _localctx = new ConstructorPatternContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_constructorPattern);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1138);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,86,_ctx) ) {
			case 1:
				{
				setState(1137);
				annotationPattern();
				}
				break;
			}
			setState(1141);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,87,_ctx) ) {
			case 1:
				{
				setState(1140);
				constructorModifiersPattern();
				}
				break;
			}
			setState(1146);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DOTDOT) | (1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (SHORT - 66)) | (1L << (VOID - 66)) | (1L << (LPAREN - 66)) | (1L << (DOT - 66)) | (1L << (BANG - 66)))) != 0) || _la==MUL || _la==Identifier) {
				{
				setState(1143);
				typePattern(0);
				setState(1144);
				dotOrDotDot();
				}
			}

			setState(1148);
			match(NEW);
			setState(1149);
			formalParametersPattern();
			setState(1151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==THROWS) {
				{
				setState(1150);
				throwsPattern();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorModifiersPatternContext extends ParserRuleContext {
		public ConstructorModifierContext constructorModifier() {
			return getRuleContext(ConstructorModifierContext.class,0);
		}
		public TerminalNode BANG() { return getToken(AspectJParser.BANG, 0); }
		public List<ConstructorModifiersPatternContext> constructorModifiersPattern() {
			return getRuleContexts(ConstructorModifiersPatternContext.class);
		}
		public ConstructorModifiersPatternContext constructorModifiersPattern(int i) {
			return getRuleContext(ConstructorModifiersPatternContext.class,i);
		}
		public ConstructorModifiersPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorModifiersPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstructorModifiersPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstructorModifiersPattern(this);
		}
	}

	public final ConstructorModifiersPatternContext constructorModifiersPattern() throws RecognitionException {
		ConstructorModifiersPatternContext _localctx = new ConstructorModifiersPatternContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_constructorModifiersPattern);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1154);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BANG) {
				{
				setState(1153);
				match(BANG);
				}
			}

			setState(1156);
			constructorModifier();
			setState(1160);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,91,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1157);
					constructorModifiersPattern();
					}
					} 
				}
				setState(1162);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,91,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorModifierContext extends ParserRuleContext {
		public TerminalNode PUBLIC() { return getToken(AspectJParser.PUBLIC, 0); }
		public TerminalNode PRIVATE() { return getToken(AspectJParser.PRIVATE, 0); }
		public TerminalNode PROTECTED() { return getToken(AspectJParser.PROTECTED, 0); }
		public ConstructorModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorModifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstructorModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstructorModifier(this);
		}
	}

	public final ConstructorModifierContext constructorModifier() throws RecognitionException {
		ConstructorModifierContext _localctx = new ConstructorModifierContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_constructorModifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1163);
			_la = _input.LA(1);
			if ( !(((((_la - 91)) & ~0x3f) == 0 && ((1L << (_la - 91)) & ((1L << (PRIVATE - 91)) | (1L << (PROTECTED - 91)) | (1L << (PUBLIC - 91)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationPatternContext extends ParserRuleContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public AnnotationTypePatternContext annotationTypePattern() {
			return getRuleContext(AnnotationTypePatternContext.class,0);
		}
		public TerminalNode BANG() { return getToken(AspectJParser.BANG, 0); }
		public List<AnnotationPatternContext> annotationPattern() {
			return getRuleContexts(AnnotationPatternContext.class);
		}
		public AnnotationPatternContext annotationPattern(int i) {
			return getRuleContext(AnnotationPatternContext.class,i);
		}
		public AnnotationPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationPattern(this);
		}
	}

	public final AnnotationPatternContext annotationPattern() throws RecognitionException {
		AnnotationPatternContext _localctx = new AnnotationPatternContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_annotationPattern);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1166);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==BANG) {
				{
				setState(1165);
				match(BANG);
				}
			}

			setState(1168);
			match(AT);
			setState(1169);
			annotationTypePattern();
			setState(1173);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,93,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1170);
					annotationPattern();
					}
					} 
				}
				setState(1175);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,93,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationTypePatternContext extends ParserRuleContext {
		public QualifiedNameContext qualifiedName() {
			return getRuleContext(QualifiedNameContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TypePatternContext typePattern() {
			return getRuleContext(TypePatternContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public AnnotationTypePatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationTypePattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationTypePattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationTypePattern(this);
		}
	}

	public final AnnotationTypePatternContext annotationTypePattern() throws RecognitionException {
		AnnotationTypePatternContext _localctx = new AnnotationTypePatternContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_annotationTypePattern);
		try {
			setState(1181);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1176);
				qualifiedName();
				}
				break;
			case LPAREN:
				enterOuterAlt(_localctx, 2);
				{
				setState(1177);
				match(LPAREN);
				setState(1178);
				typePattern(0);
				setState(1179);
				match(RPAREN);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParametersPatternContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public FormalsPatternContext formalsPattern() {
			return getRuleContext(FormalsPatternContext.class,0);
		}
		public FormalParametersPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParametersPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFormalParametersPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFormalParametersPattern(this);
		}
	}

	public final FormalParametersPatternContext formalParametersPattern() throws RecognitionException {
		FormalParametersPatternContext _localctx = new FormalParametersPatternContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_formalParametersPattern);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1183);
			match(LPAREN);
			setState(1185);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DOTDOT) | (1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (SHORT - 66)) | (1L << (VOID - 66)) | (1L << (LPAREN - 66)) | (1L << (DOT - 66)) | (1L << (BANG - 66)))) != 0) || _la==MUL || _la==Identifier) {
				{
				setState(1184);
				formalsPattern();
				}
			}

			setState(1187);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeOrIdentifierContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public TypeOrIdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeOrIdentifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeOrIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeOrIdentifier(this);
		}
	}

	public final TypeOrIdentifierContext typeOrIdentifier() throws RecognitionException {
		TypeOrIdentifierContext _localctx = new TypeOrIdentifierContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_typeOrIdentifier);
		try {
			setState(1191);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,96,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1189);
				type();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1190);
				variableDeclaratorId();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationOrIdentiferContext extends ParserRuleContext {
		public QualifiedNameContext qualifiedName() {
			return getRuleContext(QualifiedNameContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public AnnotationOrIdentiferContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationOrIdentifer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationOrIdentifer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationOrIdentifer(this);
		}
	}

	public final AnnotationOrIdentiferContext annotationOrIdentifer() throws RecognitionException {
		AnnotationOrIdentiferContext _localctx = new AnnotationOrIdentiferContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_annotationOrIdentifer);
		try {
			setState(1195);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,97,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1193);
				qualifiedName();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1194);
				id();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationsOrIdentifiersPatternContext extends ParserRuleContext {
		public TerminalNode DOTDOT() { return getToken(AspectJParser.DOTDOT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public AnnotationsOrIdentifiersPatternAfterDotDotContext annotationsOrIdentifiersPatternAfterDotDot() {
			return getRuleContext(AnnotationsOrIdentifiersPatternAfterDotDotContext.class,0);
		}
		public AnnotationOrIdentiferContext annotationOrIdentifer() {
			return getRuleContext(AnnotationOrIdentiferContext.class,0);
		}
		public List<AnnotationsOrIdentifiersPatternContext> annotationsOrIdentifiersPattern() {
			return getRuleContexts(AnnotationsOrIdentifiersPatternContext.class);
		}
		public AnnotationsOrIdentifiersPatternContext annotationsOrIdentifiersPattern(int i) {
			return getRuleContext(AnnotationsOrIdentifiersPatternContext.class,i);
		}
		public TerminalNode MUL() { return getToken(AspectJParser.MUL, 0); }
		public AnnotationsOrIdentifiersPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationsOrIdentifiersPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationsOrIdentifiersPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationsOrIdentifiersPattern(this);
		}
	}

	public final AnnotationsOrIdentifiersPatternContext annotationsOrIdentifiersPattern() throws RecognitionException {
		AnnotationsOrIdentifiersPatternContext _localctx = new AnnotationsOrIdentifiersPatternContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_annotationsOrIdentifiersPattern);
		try {
			int _alt;
			setState(1218);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOTDOT:
				enterOuterAlt(_localctx, 1);
				{
				setState(1197);
				match(DOTDOT);
				setState(1200);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,98,_ctx) ) {
				case 1:
					{
					setState(1198);
					match(COMMA);
					setState(1199);
					annotationsOrIdentifiersPatternAfterDotDot();
					}
					break;
				}
				}
				break;
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(1202);
				annotationOrIdentifer();
				setState(1207);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,99,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1203);
						match(COMMA);
						setState(1204);
						annotationsOrIdentifiersPattern();
						}
						} 
					}
					setState(1209);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,99,_ctx);
				}
				}
				break;
			case MUL:
				enterOuterAlt(_localctx, 3);
				{
				setState(1210);
				match(MUL);
				setState(1215);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1211);
						match(COMMA);
						setState(1212);
						annotationsOrIdentifiersPattern();
						}
						} 
					}
					setState(1217);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationsOrIdentifiersPatternAfterDotDotContext extends ParserRuleContext {
		public AnnotationOrIdentiferContext annotationOrIdentifer() {
			return getRuleContext(AnnotationOrIdentiferContext.class,0);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public List<AnnotationsOrIdentifiersPatternAfterDotDotContext> annotationsOrIdentifiersPatternAfterDotDot() {
			return getRuleContexts(AnnotationsOrIdentifiersPatternAfterDotDotContext.class);
		}
		public AnnotationsOrIdentifiersPatternAfterDotDotContext annotationsOrIdentifiersPatternAfterDotDot(int i) {
			return getRuleContext(AnnotationsOrIdentifiersPatternAfterDotDotContext.class,i);
		}
		public TerminalNode MUL() { return getToken(AspectJParser.MUL, 0); }
		public AnnotationsOrIdentifiersPatternAfterDotDotContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationsOrIdentifiersPatternAfterDotDot; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationsOrIdentifiersPatternAfterDotDot(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationsOrIdentifiersPatternAfterDotDot(this);
		}
	}

	public final AnnotationsOrIdentifiersPatternAfterDotDotContext annotationsOrIdentifiersPatternAfterDotDot() throws RecognitionException {
		AnnotationsOrIdentifiersPatternAfterDotDotContext _localctx = new AnnotationsOrIdentifiersPatternAfterDotDotContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_annotationsOrIdentifiersPatternAfterDotDot);
		try {
			int _alt;
			setState(1236);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1220);
				annotationOrIdentifer();
				setState(1225);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,102,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1221);
						match(COMMA);
						setState(1222);
						annotationsOrIdentifiersPatternAfterDotDot();
						}
						} 
					}
					setState(1227);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,102,_ctx);
				}
				}
				break;
			case MUL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1228);
				match(MUL);
				setState(1233);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,103,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1229);
						match(COMMA);
						setState(1230);
						annotationsOrIdentifiersPatternAfterDotDot();
						}
						} 
					}
					setState(1235);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,103,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsPatternContext extends ParserRuleContext {
		public TypeOrIdentifierContext typeOrIdentifier() {
			return getRuleContext(TypeOrIdentifierContext.class,0);
		}
		public TerminalNode MUL() { return getToken(AspectJParser.MUL, 0); }
		public TerminalNode DOTDOT() { return getToken(AspectJParser.DOTDOT, 0); }
		public ArgsPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argsPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterArgsPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitArgsPattern(this);
		}
	}

	public final ArgsPatternContext argsPattern() throws RecognitionException {
		ArgsPatternContext _localctx = new ArgsPatternContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_argsPattern);
		int _la;
		try {
			setState(1240);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1238);
				typeOrIdentifier();
				}
				break;
			case DOTDOT:
			case MUL:
				enterOuterAlt(_localctx, 2);
				{
				setState(1239);
				_la = _input.LA(1);
				if ( !(_la==DOTDOT || _la==MUL) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgsPatternListContext extends ParserRuleContext {
		public List<ArgsPatternContext> argsPattern() {
			return getRuleContexts(ArgsPatternContext.class);
		}
		public ArgsPatternContext argsPattern(int i) {
			return getRuleContext(ArgsPatternContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public ArgsPatternListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argsPatternList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterArgsPatternList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitArgsPatternList(this);
		}
	}

	public final ArgsPatternListContext argsPatternList() throws RecognitionException {
		ArgsPatternListContext _localctx = new ArgsPatternListContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_argsPatternList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1242);
			argsPattern();
			setState(1247);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1243);
				match(COMMA);
				setState(1244);
				argsPattern();
				}
				}
				setState(1249);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdContext extends ParserRuleContext {
		public TerminalNode ARGS() { return getToken(AspectJParser.ARGS, 0); }
		public TerminalNode AFTER() { return getToken(AspectJParser.AFTER, 0); }
		public TerminalNode AROUND() { return getToken(AspectJParser.AROUND, 0); }
		public TerminalNode ASPECT() { return getToken(AspectJParser.ASPECT, 0); }
		public TerminalNode BEFORE() { return getToken(AspectJParser.BEFORE, 0); }
		public TerminalNode CALL() { return getToken(AspectJParser.CALL, 0); }
		public TerminalNode CFLOW() { return getToken(AspectJParser.CFLOW, 0); }
		public TerminalNode CFLOWBELOW() { return getToken(AspectJParser.CFLOWBELOW, 0); }
		public TerminalNode DECLARE() { return getToken(AspectJParser.DECLARE, 0); }
		public TerminalNode ERROR() { return getToken(AspectJParser.ERROR, 0); }
		public TerminalNode EXECUTION() { return getToken(AspectJParser.EXECUTION, 0); }
		public TerminalNode GET() { return getToken(AspectJParser.GET, 0); }
		public TerminalNode HANDLER() { return getToken(AspectJParser.HANDLER, 0); }
		public TerminalNode INITIALIZATION() { return getToken(AspectJParser.INITIALIZATION, 0); }
		public TerminalNode ISSINGLETON() { return getToken(AspectJParser.ISSINGLETON, 0); }
		public TerminalNode PARENTS() { return getToken(AspectJParser.PARENTS, 0); }
		public TerminalNode PERCFLOW() { return getToken(AspectJParser.PERCFLOW, 0); }
		public TerminalNode PERCFLOWBELOW() { return getToken(AspectJParser.PERCFLOWBELOW, 0); }
		public TerminalNode PERTARGET() { return getToken(AspectJParser.PERTARGET, 0); }
		public TerminalNode PERTHIS() { return getToken(AspectJParser.PERTHIS, 0); }
		public TerminalNode PERTYPEWITHIN() { return getToken(AspectJParser.PERTYPEWITHIN, 0); }
		public TerminalNode POINTCUT() { return getToken(AspectJParser.POINTCUT, 0); }
		public TerminalNode PRECEDENCE() { return getToken(AspectJParser.PRECEDENCE, 0); }
		public TerminalNode PREINITIALIZATION() { return getToken(AspectJParser.PREINITIALIZATION, 0); }
		public TerminalNode PRIVILEGED() { return getToken(AspectJParser.PRIVILEGED, 0); }
		public TerminalNode RETURNING() { return getToken(AspectJParser.RETURNING, 0); }
		public TerminalNode SET() { return getToken(AspectJParser.SET, 0); }
		public TerminalNode SOFT() { return getToken(AspectJParser.SOFT, 0); }
		public TerminalNode STATICINITIALIZATION() { return getToken(AspectJParser.STATICINITIALIZATION, 0); }
		public TerminalNode TARGET() { return getToken(AspectJParser.TARGET, 0); }
		public TerminalNode THROWING() { return getToken(AspectJParser.THROWING, 0); }
		public TerminalNode WARNING() { return getToken(AspectJParser.WARNING, 0); }
		public TerminalNode WITHIN() { return getToken(AspectJParser.WITHIN, 0); }
		public TerminalNode WITHINCODE() { return getToken(AspectJParser.WITHINCODE, 0); }
		public TerminalNode Identifier() { return getToken(AspectJParser.Identifier, 0); }
		public IdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitId(this);
		}
	}

	public final IdContext id() throws RecognitionException {
		IdContext _localctx = new IdContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_id);
		int _la;
		try {
			setState(1252);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
				enterOuterAlt(_localctx, 1);
				{
				setState(1250);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(1251);
				match(Identifier);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDeclarationContext extends ParserRuleContext {
		public TerminalNode CLASS() { return getToken(AspectJParser.CLASS, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public ClassBodyContext classBody() {
			return getRuleContext(ClassBodyContext.class,0);
		}
		public TypeParametersContext typeParameters() {
			return getRuleContext(TypeParametersContext.class,0);
		}
		public TerminalNode EXTENDS() { return getToken(AspectJParser.EXTENDS, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode IMPLEMENTS() { return getToken(AspectJParser.IMPLEMENTS, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public ClassDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassDeclaration(this);
		}
	}

	public final ClassDeclarationContext classDeclaration() throws RecognitionException {
		ClassDeclarationContext _localctx = new ClassDeclarationContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_classDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1254);
			match(CLASS);
			setState(1255);
			id();
			setState(1257);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LT) {
				{
				setState(1256);
				typeParameters();
				}
			}

			setState(1261);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EXTENDS) {
				{
				setState(1259);
				match(EXTENDS);
				setState(1260);
				type();
				}
			}

			setState(1265);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IMPLEMENTS) {
				{
				setState(1263);
				match(IMPLEMENTS);
				setState(1264);
				typeList();
				}
			}

			setState(1267);
			classBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeParameterContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode EXTENDS() { return getToken(AspectJParser.EXTENDS, 0); }
		public TypeBoundContext typeBound() {
			return getRuleContext(TypeBoundContext.class,0);
		}
		public TypeParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeParameter(this);
		}
	}

	public final TypeParameterContext typeParameter() throws RecognitionException {
		TypeParameterContext _localctx = new TypeParameterContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_typeParameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1269);
			id();
			setState(1272);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EXTENDS) {
				{
				setState(1270);
				match(EXTENDS);
				setState(1271);
				typeBound();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnumDeclarationContext extends ParserRuleContext {
		public TerminalNode ENUM() { return getToken(AspectJParser.ENUM, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public TerminalNode IMPLEMENTS() { return getToken(AspectJParser.IMPLEMENTS, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public EnumConstantsContext enumConstants() {
			return getRuleContext(EnumConstantsContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(AspectJParser.COMMA, 0); }
		public EnumBodyDeclarationsContext enumBodyDeclarations() {
			return getRuleContext(EnumBodyDeclarationsContext.class,0);
		}
		public EnumDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterEnumDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitEnumDeclaration(this);
		}
	}

	public final EnumDeclarationContext enumDeclaration() throws RecognitionException {
		EnumDeclarationContext _localctx = new EnumDeclarationContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_enumDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1274);
			match(ENUM);
			setState(1275);
			id();
			setState(1278);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==IMPLEMENTS) {
				{
				setState(1276);
				match(IMPLEMENTS);
				setState(1277);
				typeList();
				}
			}

			setState(1280);
			match(LBRACE);
			setState(1282);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT))) != 0) || _la==Identifier) {
				{
				setState(1281);
				enumConstants();
				}
			}

			setState(1285);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(1284);
				match(COMMA);
				}
			}

			setState(1288);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEMI) {
				{
				setState(1287);
				enumBodyDeclarations();
				}
			}

			setState(1290);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnumConstantContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<AnnotationContext> annotation() {
			return getRuleContexts(AnnotationContext.class);
		}
		public AnnotationContext annotation(int i) {
			return getRuleContext(AnnotationContext.class,i);
		}
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public ClassBodyContext classBody() {
			return getRuleContext(ClassBodyContext.class,0);
		}
		public EnumConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumConstant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterEnumConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitEnumConstant(this);
		}
	}

	public final EnumConstantContext enumConstant() throws RecognitionException {
		EnumConstantContext _localctx = new EnumConstantContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_enumConstant);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1295);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT) {
				{
				{
				setState(1292);
				annotation();
				}
				}
				setState(1297);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1298);
			id();
			setState(1300);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LPAREN) {
				{
				setState(1299);
				arguments();
				}
			}

			setState(1303);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LBRACE) {
				{
				setState(1302);
				classBody();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterfaceDeclarationContext extends ParserRuleContext {
		public TerminalNode INTERFACE() { return getToken(AspectJParser.INTERFACE, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public InterfaceBodyContext interfaceBody() {
			return getRuleContext(InterfaceBodyContext.class,0);
		}
		public TypeParametersContext typeParameters() {
			return getRuleContext(TypeParametersContext.class,0);
		}
		public TerminalNode EXTENDS() { return getToken(AspectJParser.EXTENDS, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public InterfaceDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interfaceDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInterfaceDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInterfaceDeclaration(this);
		}
	}

	public final InterfaceDeclarationContext interfaceDeclaration() throws RecognitionException {
		InterfaceDeclarationContext _localctx = new InterfaceDeclarationContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_interfaceDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1305);
			match(INTERFACE);
			setState(1306);
			id();
			setState(1308);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LT) {
				{
				setState(1307);
				typeParameters();
				}
			}

			setState(1312);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==EXTENDS) {
				{
				setState(1310);
				match(EXTENDS);
				setState(1311);
				typeList();
				}
			}

			setState(1314);
			interfaceBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDeclarationContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode VOID() { return getToken(AspectJParser.VOID, 0); }
		public MethodBodyContext methodBody() {
			return getRuleContext(MethodBodyContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public List<TerminalNode> LBRACK() { return getTokens(AspectJParser.LBRACK); }
		public TerminalNode LBRACK(int i) {
			return getToken(AspectJParser.LBRACK, i);
		}
		public List<TerminalNode> RBRACK() { return getTokens(AspectJParser.RBRACK); }
		public TerminalNode RBRACK(int i) {
			return getToken(AspectJParser.RBRACK, i);
		}
		public TerminalNode THROWS() { return getToken(AspectJParser.THROWS, 0); }
		public QualifiedNameListContext qualifiedNameList() {
			return getRuleContext(QualifiedNameListContext.class,0);
		}
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitMethodDeclaration(this);
		}
	}

	public final MethodDeclarationContext methodDeclaration() throws RecognitionException {
		MethodDeclarationContext _localctx = new MethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_methodDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1318);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case Identifier:
				{
				setState(1316);
				type();
				}
				break;
			case VOID:
				{
				setState(1317);
				match(VOID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(1320);
			id();
			setState(1321);
			formalParameters();
			setState(1326);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LBRACK) {
				{
				{
				setState(1322);
				match(LBRACK);
				setState(1323);
				match(RBRACK);
				}
				}
				setState(1328);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1331);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==THROWS) {
				{
				setState(1329);
				match(THROWS);
				setState(1330);
				qualifiedNameList();
				}
			}

			setState(1335);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LBRACE:
				{
				setState(1333);
				methodBody();
				}
				break;
			case SEMI:
				{
				setState(1334);
				match(SEMI);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorDeclarationContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public ConstructorBodyContext constructorBody() {
			return getRuleContext(ConstructorBodyContext.class,0);
		}
		public TerminalNode THROWS() { return getToken(AspectJParser.THROWS, 0); }
		public QualifiedNameListContext qualifiedNameList() {
			return getRuleContext(QualifiedNameListContext.class,0);
		}
		public ConstructorDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstructorDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstructorDeclaration(this);
		}
	}

	public final ConstructorDeclarationContext constructorDeclaration() throws RecognitionException {
		ConstructorDeclarationContext _localctx = new ConstructorDeclarationContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_constructorDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1337);
			id();
			setState(1338);
			formalParameters();
			setState(1341);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==THROWS) {
				{
				setState(1339);
				match(THROWS);
				setState(1340);
				qualifiedNameList();
				}
			}

			setState(1343);
			constructorBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantDeclaratorContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(AspectJParser.ASSIGN, 0); }
		public VariableInitializerContext variableInitializer() {
			return getRuleContext(VariableInitializerContext.class,0);
		}
		public List<TerminalNode> LBRACK() { return getTokens(AspectJParser.LBRACK); }
		public TerminalNode LBRACK(int i) {
			return getToken(AspectJParser.LBRACK, i);
		}
		public List<TerminalNode> RBRACK() { return getTokens(AspectJParser.RBRACK); }
		public TerminalNode RBRACK(int i) {
			return getToken(AspectJParser.RBRACK, i);
		}
		public ConstantDeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constantDeclarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstantDeclarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstantDeclarator(this);
		}
	}

	public final ConstantDeclaratorContext constantDeclarator() throws RecognitionException {
		ConstantDeclaratorContext _localctx = new ConstantDeclaratorContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_constantDeclarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1345);
			id();
			setState(1350);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LBRACK) {
				{
				{
				setState(1346);
				match(LBRACK);
				setState(1347);
				match(RBRACK);
				}
				}
				setState(1352);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1353);
			match(ASSIGN);
			setState(1354);
			variableInitializer();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterfaceMethodDeclarationContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode VOID() { return getToken(AspectJParser.VOID, 0); }
		public List<TerminalNode> LBRACK() { return getTokens(AspectJParser.LBRACK); }
		public TerminalNode LBRACK(int i) {
			return getToken(AspectJParser.LBRACK, i);
		}
		public List<TerminalNode> RBRACK() { return getTokens(AspectJParser.RBRACK); }
		public TerminalNode RBRACK(int i) {
			return getToken(AspectJParser.RBRACK, i);
		}
		public TerminalNode THROWS() { return getToken(AspectJParser.THROWS, 0); }
		public QualifiedNameListContext qualifiedNameList() {
			return getRuleContext(QualifiedNameListContext.class,0);
		}
		public InterfaceMethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interfaceMethodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInterfaceMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInterfaceMethodDeclaration(this);
		}
	}

	public final InterfaceMethodDeclarationContext interfaceMethodDeclaration() throws RecognitionException {
		InterfaceMethodDeclarationContext _localctx = new InterfaceMethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_interfaceMethodDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1358);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case Identifier:
				{
				setState(1356);
				type();
				}
				break;
			case VOID:
				{
				setState(1357);
				match(VOID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(1360);
			id();
			setState(1361);
			formalParameters();
			setState(1366);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LBRACK) {
				{
				{
				setState(1362);
				match(LBRACK);
				setState(1363);
				match(RBRACK);
				}
				}
				setState(1368);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1371);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==THROWS) {
				{
				setState(1369);
				match(THROWS);
				setState(1370);
				qualifiedNameList();
				}
			}

			setState(1373);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclaratorIdContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<TerminalNode> LBRACK() { return getTokens(AspectJParser.LBRACK); }
		public TerminalNode LBRACK(int i) {
			return getToken(AspectJParser.LBRACK, i);
		}
		public List<TerminalNode> RBRACK() { return getTokens(AspectJParser.RBRACK); }
		public TerminalNode RBRACK(int i) {
			return getToken(AspectJParser.RBRACK, i);
		}
		public VariableDeclaratorIdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaratorId; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterVariableDeclaratorId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitVariableDeclaratorId(this);
		}
	}

	public final VariableDeclaratorIdContext variableDeclaratorId() throws RecognitionException {
		VariableDeclaratorIdContext _localctx = new VariableDeclaratorIdContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_variableDeclaratorId);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1375);
			id();
			setState(1380);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LBRACK) {
				{
				{
				setState(1376);
				match(LBRACK);
				setState(1377);
				match(RBRACK);
				}
				}
				setState(1382);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnumConstantNameContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public EnumConstantNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumConstantName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterEnumConstantName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitEnumConstantName(this);
		}
	}

	public final EnumConstantNameContext enumConstantName() throws RecognitionException {
		EnumConstantNameContext _localctx = new EnumConstantNameContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_enumConstantName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1383);
			id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassOrInterfaceTypeContext extends ParserRuleContext {
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public List<TypeArgumentsContext> typeArguments() {
			return getRuleContexts(TypeArgumentsContext.class);
		}
		public TypeArgumentsContext typeArguments(int i) {
			return getRuleContext(TypeArgumentsContext.class,i);
		}
		public List<TerminalNode> DOT() { return getTokens(AspectJParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(AspectJParser.DOT, i);
		}
		public ClassOrInterfaceTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classOrInterfaceType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassOrInterfaceType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassOrInterfaceType(this);
		}
	}

	public final ClassOrInterfaceTypeContext classOrInterfaceType() throws RecognitionException {
		ClassOrInterfaceTypeContext _localctx = new ClassOrInterfaceTypeContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_classOrInterfaceType);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1385);
			id();
			setState(1387);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,131,_ctx) ) {
			case 1:
				{
				setState(1386);
				typeArguments();
				}
				break;
			}
			setState(1396);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,133,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1389);
					match(DOT);
					setState(1390);
					id();
					setState(1392);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,132,_ctx) ) {
					case 1:
						{
						setState(1391);
						typeArguments();
						}
						break;
					}
					}
					} 
				}
				setState(1398);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,133,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualifiedNameContext extends ParserRuleContext {
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public List<TerminalNode> DOT() { return getTokens(AspectJParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(AspectJParser.DOT, i);
		}
		public QualifiedNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualifiedName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterQualifiedName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitQualifiedName(this);
		}
	}

	public final QualifiedNameContext qualifiedName() throws RecognitionException {
		QualifiedNameContext _localctx = new QualifiedNameContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_qualifiedName);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1399);
			id();
			setState(1404);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,134,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1400);
					match(DOT);
					setState(1401);
					id();
					}
					} 
				}
				setState(1406);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,134,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementValuePairContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(AspectJParser.ASSIGN, 0); }
		public ElementValueContext elementValue() {
			return getRuleContext(ElementValueContext.class,0);
		}
		public ElementValuePairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elementValuePair; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterElementValuePair(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitElementValuePair(this);
		}
	}

	public final ElementValuePairContext elementValuePair() throws RecognitionException {
		ElementValuePairContext _localctx = new ElementValuePairContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_elementValuePair);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1407);
			id();
			setState(1408);
			match(ASSIGN);
			setState(1409);
			elementValue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationTypeDeclarationContext extends ParserRuleContext {
		public TerminalNode AT() { return getToken(AspectJParser.AT, 0); }
		public TerminalNode INTERFACE() { return getToken(AspectJParser.INTERFACE, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public AnnotationTypeBodyContext annotationTypeBody() {
			return getRuleContext(AnnotationTypeBodyContext.class,0);
		}
		public AnnotationTypeDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationTypeDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationTypeDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationTypeDeclaration(this);
		}
	}

	public final AnnotationTypeDeclarationContext annotationTypeDeclaration() throws RecognitionException {
		AnnotationTypeDeclarationContext _localctx = new AnnotationTypeDeclarationContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_annotationTypeDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1411);
			match(AT);
			setState(1412);
			match(INTERFACE);
			setState(1413);
			id();
			setState(1414);
			annotationTypeBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationMethodRestContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public DefaultValueContext defaultValue() {
			return getRuleContext(DefaultValueContext.class,0);
		}
		public AnnotationMethodRestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationMethodRest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationMethodRest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationMethodRest(this);
		}
	}

	public final AnnotationMethodRestContext annotationMethodRest() throws RecognitionException {
		AnnotationMethodRestContext _localctx = new AnnotationMethodRestContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_annotationMethodRest);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1416);
			id();
			setState(1417);
			match(LPAREN);
			setState(1418);
			match(RPAREN);
			setState(1420);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DEFAULT) {
				{
				setState(1419);
				defaultValue();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public TerminalNode ASSERT() { return getToken(AspectJParser.ASSERT, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public TerminalNode COLON() { return getToken(AspectJParser.COLON, 0); }
		public TerminalNode IF() { return getToken(AspectJParser.IF, 0); }
		public ParExpressionContext parExpression() {
			return getRuleContext(ParExpressionContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(AspectJParser.ELSE, 0); }
		public TerminalNode FOR() { return getToken(AspectJParser.FOR, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public ForControlContext forControl() {
			return getRuleContext(ForControlContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public TerminalNode WHILE() { return getToken(AspectJParser.WHILE, 0); }
		public TerminalNode DO() { return getToken(AspectJParser.DO, 0); }
		public TerminalNode TRY() { return getToken(AspectJParser.TRY, 0); }
		public FinallyBlockContext finallyBlock() {
			return getRuleContext(FinallyBlockContext.class,0);
		}
		public List<CatchClauseContext> catchClause() {
			return getRuleContexts(CatchClauseContext.class);
		}
		public CatchClauseContext catchClause(int i) {
			return getRuleContext(CatchClauseContext.class,i);
		}
		public ResourceSpecificationContext resourceSpecification() {
			return getRuleContext(ResourceSpecificationContext.class,0);
		}
		public TerminalNode SWITCH() { return getToken(AspectJParser.SWITCH, 0); }
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<SwitchBlockStatementGroupContext> switchBlockStatementGroup() {
			return getRuleContexts(SwitchBlockStatementGroupContext.class);
		}
		public SwitchBlockStatementGroupContext switchBlockStatementGroup(int i) {
			return getRuleContext(SwitchBlockStatementGroupContext.class,i);
		}
		public List<SwitchLabelContext> switchLabel() {
			return getRuleContexts(SwitchLabelContext.class);
		}
		public SwitchLabelContext switchLabel(int i) {
			return getRuleContext(SwitchLabelContext.class,i);
		}
		public TerminalNode SYNCHRONIZED() { return getToken(AspectJParser.SYNCHRONIZED, 0); }
		public TerminalNode RETURN() { return getToken(AspectJParser.RETURN, 0); }
		public TerminalNode THROW() { return getToken(AspectJParser.THROW, 0); }
		public TerminalNode BREAK() { return getToken(AspectJParser.BREAK, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode CONTINUE() { return getToken(AspectJParser.CONTINUE, 0); }
		public StatementExpressionContext statementExpression() {
			return getRuleContext(StatementExpressionContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_statement);
		int _la;
		try {
			int _alt;
			setState(1527);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,148,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1422);
				block();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1423);
				match(ASSERT);
				setState(1424);
				expression(0);
				setState(1427);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COLON) {
					{
					setState(1425);
					match(COLON);
					setState(1426);
					expression(0);
					}
				}

				setState(1429);
				match(SEMI);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1431);
				match(IF);
				setState(1432);
				parExpression();
				setState(1433);
				statement();
				setState(1436);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,137,_ctx) ) {
				case 1:
					{
					setState(1434);
					match(ELSE);
					setState(1435);
					statement();
					}
					break;
				}
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1438);
				match(FOR);
				setState(1439);
				match(LPAREN);
				setState(1440);
				forControl();
				setState(1441);
				match(RPAREN);
				setState(1442);
				statement();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1444);
				match(WHILE);
				setState(1445);
				parExpression();
				setState(1446);
				statement();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1448);
				match(DO);
				setState(1449);
				statement();
				setState(1450);
				match(WHILE);
				setState(1451);
				parExpression();
				setState(1452);
				match(SEMI);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1454);
				match(TRY);
				setState(1455);
				block();
				setState(1465);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case CATCH:
					{
					setState(1457); 
					_errHandler.sync(this);
					_la = _input.LA(1);
					do {
						{
						{
						setState(1456);
						catchClause();
						}
						}
						setState(1459); 
						_errHandler.sync(this);
						_la = _input.LA(1);
					} while ( _la==CATCH );
					setState(1462);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==FINALLY) {
						{
						setState(1461);
						finallyBlock();
						}
					}

					}
					break;
				case FINALLY:
					{
					setState(1464);
					finallyBlock();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1467);
				match(TRY);
				setState(1468);
				resourceSpecification();
				setState(1469);
				block();
				setState(1473);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==CATCH) {
					{
					{
					setState(1470);
					catchClause();
					}
					}
					setState(1475);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1477);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==FINALLY) {
					{
					setState(1476);
					finallyBlock();
					}
				}

				}
				break;
			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(1479);
				match(SWITCH);
				setState(1480);
				parExpression();
				setState(1481);
				match(LBRACE);
				setState(1485);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,143,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1482);
						switchBlockStatementGroup();
						}
						} 
					}
					setState(1487);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,143,_ctx);
				}
				setState(1491);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==CASE || _la==DEFAULT) {
					{
					{
					setState(1488);
					switchLabel();
					}
					}
					setState(1493);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1494);
				match(RBRACE);
				}
				break;
			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(1496);
				match(SYNCHRONIZED);
				setState(1497);
				parExpression();
				setState(1498);
				block();
				}
				break;
			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(1500);
				match(RETURN);
				setState(1502);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
					{
					setState(1501);
					expression(0);
					}
				}

				setState(1504);
				match(SEMI);
				}
				break;
			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(1505);
				match(THROW);
				setState(1506);
				expression(0);
				setState(1507);
				match(SEMI);
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(1509);
				match(BREAK);
				setState(1511);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE))) != 0) || _la==Identifier) {
					{
					setState(1510);
					id();
					}
				}

				setState(1513);
				match(SEMI);
				}
				break;
			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(1514);
				match(CONTINUE);
				setState(1516);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE))) != 0) || _la==Identifier) {
					{
					setState(1515);
					id();
					}
				}

				setState(1518);
				match(SEMI);
				}
				break;
			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(1519);
				match(SEMI);
				}
				break;
			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(1520);
				statementExpression();
				setState(1521);
				match(SEMI);
				}
				break;
			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(1523);
				id();
				setState(1524);
				match(COLON);
				setState(1525);
				statement();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchClauseContext extends ParserRuleContext {
		public TerminalNode CATCH() { return getToken(AspectJParser.CATCH, 0); }
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public CatchTypeContext catchType() {
			return getRuleContext(CatchTypeContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public List<VariableModifierContext> variableModifier() {
			return getRuleContexts(VariableModifierContext.class);
		}
		public VariableModifierContext variableModifier(int i) {
			return getRuleContext(VariableModifierContext.class,i);
		}
		public CatchClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCatchClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCatchClause(this);
		}
	}

	public final CatchClauseContext catchClause() throws RecognitionException {
		CatchClauseContext _localctx = new CatchClauseContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_catchClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1529);
			match(CATCH);
			setState(1530);
			match(LPAREN);
			setState(1534);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT || _la==FINAL) {
				{
				{
				setState(1531);
				variableModifier();
				}
				}
				setState(1536);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1537);
			catchType();
			setState(1538);
			id();
			setState(1539);
			match(RPAREN);
			setState(1540);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public PrimaryContext primary() {
			return getRuleContext(PrimaryContext.class,0);
		}
		public TerminalNode NEW() { return getToken(AspectJParser.NEW, 0); }
		public CreatorContext creator() {
			return getRuleContext(CreatorContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode ADD() { return getToken(AspectJParser.ADD, 0); }
		public TerminalNode SUB() { return getToken(AspectJParser.SUB, 0); }
		public TerminalNode INC() { return getToken(AspectJParser.INC, 0); }
		public TerminalNode DEC() { return getToken(AspectJParser.DEC, 0); }
		public TerminalNode TILDE() { return getToken(AspectJParser.TILDE, 0); }
		public TerminalNode BANG() { return getToken(AspectJParser.BANG, 0); }
		public TerminalNode MUL() { return getToken(AspectJParser.MUL, 0); }
		public TerminalNode DIV() { return getToken(AspectJParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(AspectJParser.MOD, 0); }
		public List<TerminalNode> LT() { return getTokens(AspectJParser.LT); }
		public TerminalNode LT(int i) {
			return getToken(AspectJParser.LT, i);
		}
		public List<TerminalNode> GT() { return getTokens(AspectJParser.GT); }
		public TerminalNode GT(int i) {
			return getToken(AspectJParser.GT, i);
		}
		public TerminalNode LE() { return getToken(AspectJParser.LE, 0); }
		public TerminalNode GE() { return getToken(AspectJParser.GE, 0); }
		public TerminalNode EQUAL() { return getToken(AspectJParser.EQUAL, 0); }
		public TerminalNode NOTEQUAL() { return getToken(AspectJParser.NOTEQUAL, 0); }
		public TerminalNode BITAND() { return getToken(AspectJParser.BITAND, 0); }
		public TerminalNode CARET() { return getToken(AspectJParser.CARET, 0); }
		public TerminalNode BITOR() { return getToken(AspectJParser.BITOR, 0); }
		public TerminalNode AND() { return getToken(AspectJParser.AND, 0); }
		public TerminalNode OR() { return getToken(AspectJParser.OR, 0); }
		public TerminalNode QUESTION() { return getToken(AspectJParser.QUESTION, 0); }
		public TerminalNode COLON() { return getToken(AspectJParser.COLON, 0); }
		public TerminalNode ASSIGN() { return getToken(AspectJParser.ASSIGN, 0); }
		public TerminalNode ADD_ASSIGN() { return getToken(AspectJParser.ADD_ASSIGN, 0); }
		public TerminalNode SUB_ASSIGN() { return getToken(AspectJParser.SUB_ASSIGN, 0); }
		public TerminalNode MUL_ASSIGN() { return getToken(AspectJParser.MUL_ASSIGN, 0); }
		public TerminalNode DIV_ASSIGN() { return getToken(AspectJParser.DIV_ASSIGN, 0); }
		public TerminalNode AND_ASSIGN() { return getToken(AspectJParser.AND_ASSIGN, 0); }
		public TerminalNode OR_ASSIGN() { return getToken(AspectJParser.OR_ASSIGN, 0); }
		public TerminalNode XOR_ASSIGN() { return getToken(AspectJParser.XOR_ASSIGN, 0); }
		public TerminalNode RSHIFT_ASSIGN() { return getToken(AspectJParser.RSHIFT_ASSIGN, 0); }
		public TerminalNode URSHIFT_ASSIGN() { return getToken(AspectJParser.URSHIFT_ASSIGN, 0); }
		public TerminalNode LSHIFT_ASSIGN() { return getToken(AspectJParser.LSHIFT_ASSIGN, 0); }
		public TerminalNode MOD_ASSIGN() { return getToken(AspectJParser.MOD_ASSIGN, 0); }
		public TerminalNode DOT() { return getToken(AspectJParser.DOT, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode THIS() { return getToken(AspectJParser.THIS, 0); }
		public InnerCreatorContext innerCreator() {
			return getRuleContext(InnerCreatorContext.class,0);
		}
		public NonWildcardTypeArgumentsContext nonWildcardTypeArguments() {
			return getRuleContext(NonWildcardTypeArgumentsContext.class,0);
		}
		public TerminalNode SUPER() { return getToken(AspectJParser.SUPER, 0); }
		public SuperSuffixContext superSuffix() {
			return getRuleContext(SuperSuffixContext.class,0);
		}
		public ExplicitGenericInvocationContext explicitGenericInvocation() {
			return getRuleContext(ExplicitGenericInvocationContext.class,0);
		}
		public TerminalNode LBRACK() { return getToken(AspectJParser.LBRACK, 0); }
		public TerminalNode RBRACK() { return getToken(AspectJParser.RBRACK, 0); }
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public TerminalNode INSTANCEOF() { return getToken(AspectJParser.INSTANCEOF, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 130;
		enterRecursionRule(_localctx, 130, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1555);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,150,_ctx) ) {
			case 1:
				{
				setState(1543);
				primary();
				}
				break;
			case 2:
				{
				setState(1544);
				match(NEW);
				setState(1545);
				creator();
				}
				break;
			case 3:
				{
				setState(1546);
				match(LPAREN);
				setState(1547);
				type();
				setState(1548);
				match(RPAREN);
				setState(1549);
				expression(17);
				}
				break;
			case 4:
				{
				setState(1551);
				_la = _input.LA(1);
				if ( !(((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1552);
				expression(15);
				}
				break;
			case 5:
				{
				setState(1553);
				_la = _input.LA(1);
				if ( !(_la==BANG || _la==TILDE) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(1554);
				expression(14);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(1642);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,155,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(1640);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,154,_ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1557);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(1558);
						_la = _input.LA(1);
						if ( !(((((_la - 141)) & ~0x3f) == 0 && ((1L << (_la - 141)) & ((1L << (MUL - 141)) | (1L << (DIV - 141)) | (1L << (MOD - 141)))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1559);
						expression(14);
						}
						break;
					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1560);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(1561);
						_la = _input.LA(1);
						if ( !(_la==ADD || _la==SUB) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1562);
						expression(13);
						}
						break;
					case 3:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1563);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(1571);
						_errHandler.sync(this);
						switch ( getInterpreter().adaptivePredict(_input,151,_ctx) ) {
						case 1:
							{
							setState(1564);
							match(LT);
							setState(1565);
							match(LT);
							}
							break;
						case 2:
							{
							setState(1566);
							match(GT);
							setState(1567);
							match(GT);
							setState(1568);
							match(GT);
							}
							break;
						case 3:
							{
							setState(1569);
							match(GT);
							setState(1570);
							match(GT);
							}
							break;
						}
						setState(1573);
						expression(12);
						}
						break;
					case 4:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1574);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(1575);
						_la = _input.LA(1);
						if ( !(((((_la - 125)) & ~0x3f) == 0 && ((1L << (_la - 125)) & ((1L << (GT - 125)) | (1L << (LT - 125)) | (1L << (LE - 125)) | (1L << (GE - 125)))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1576);
						expression(11);
						}
						break;
					case 5:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1577);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(1578);
						_la = _input.LA(1);
						if ( !(_la==EQUAL || _la==NOTEQUAL) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1579);
						expression(9);
						}
						break;
					case 6:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1580);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(1581);
						match(BITAND);
						setState(1582);
						expression(8);
						}
						break;
					case 7:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1583);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(1584);
						match(CARET);
						setState(1585);
						expression(7);
						}
						break;
					case 8:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1586);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(1587);
						match(BITOR);
						setState(1588);
						expression(6);
						}
						break;
					case 9:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1589);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(1590);
						match(AND);
						setState(1591);
						expression(5);
						}
						break;
					case 10:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1592);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(1593);
						match(OR);
						setState(1594);
						expression(4);
						}
						break;
					case 11:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1595);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(1596);
						match(QUESTION);
						setState(1597);
						expression(0);
						setState(1598);
						match(COLON);
						setState(1599);
						expression(3);
						}
						break;
					case 12:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1601);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(1602);
						_la = _input.LA(1);
						if ( !(((((_la - 124)) & ~0x3f) == 0 && ((1L << (_la - 124)) & ((1L << (ASSIGN - 124)) | (1L << (ADD_ASSIGN - 124)) | (1L << (SUB_ASSIGN - 124)) | (1L << (MUL_ASSIGN - 124)) | (1L << (DIV_ASSIGN - 124)) | (1L << (AND_ASSIGN - 124)) | (1L << (OR_ASSIGN - 124)) | (1L << (XOR_ASSIGN - 124)) | (1L << (MOD_ASSIGN - 124)) | (1L << (LSHIFT_ASSIGN - 124)) | (1L << (RSHIFT_ASSIGN - 124)) | (1L << (URSHIFT_ASSIGN - 124)))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						setState(1603);
						expression(2);
						}
						break;
					case 13:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1604);
						if (!(precpred(_ctx, 25))) throw new FailedPredicateException(this, "precpred(_ctx, 25)");
						setState(1605);
						match(DOT);
						setState(1606);
						id();
						}
						break;
					case 14:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1607);
						if (!(precpred(_ctx, 24))) throw new FailedPredicateException(this, "precpred(_ctx, 24)");
						setState(1608);
						match(DOT);
						setState(1609);
						match(THIS);
						}
						break;
					case 15:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1610);
						if (!(precpred(_ctx, 23))) throw new FailedPredicateException(this, "precpred(_ctx, 23)");
						setState(1611);
						match(DOT);
						setState(1612);
						match(NEW);
						setState(1614);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la==LT) {
							{
							setState(1613);
							nonWildcardTypeArguments();
							}
						}

						setState(1616);
						innerCreator();
						}
						break;
					case 16:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1617);
						if (!(precpred(_ctx, 22))) throw new FailedPredicateException(this, "precpred(_ctx, 22)");
						setState(1618);
						match(DOT);
						setState(1619);
						match(SUPER);
						setState(1620);
						superSuffix();
						}
						break;
					case 17:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1621);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(1622);
						match(DOT);
						setState(1623);
						explicitGenericInvocation();
						}
						break;
					case 18:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1624);
						if (!(precpred(_ctx, 20))) throw new FailedPredicateException(this, "precpred(_ctx, 20)");
						setState(1625);
						match(LBRACK);
						setState(1626);
						expression(0);
						setState(1627);
						match(RBRACK);
						}
						break;
					case 19:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1629);
						if (!(precpred(_ctx, 19))) throw new FailedPredicateException(this, "precpred(_ctx, 19)");
						setState(1630);
						match(LPAREN);
						setState(1632);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
							{
							setState(1631);
							expressionList();
							}
						}

						setState(1634);
						match(RPAREN);
						}
						break;
					case 20:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1635);
						if (!(precpred(_ctx, 16))) throw new FailedPredicateException(this, "precpred(_ctx, 16)");
						setState(1636);
						_la = _input.LA(1);
						if ( !(_la==INC || _la==DEC) ) {
						_errHandler.recoverInline(this);
						}
						else {
							if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
						}
						break;
					case 21:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(1637);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(1638);
						match(INSTANCEOF);
						setState(1639);
						type();
						}
						break;
					}
					} 
				}
				setState(1644);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,155,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PrimaryContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public TerminalNode THIS() { return getToken(AspectJParser.THIS, 0); }
		public TerminalNode SUPER() { return getToken(AspectJParser.SUPER, 0); }
		public LiteralContext literal() {
			return getRuleContext(LiteralContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode DOT() { return getToken(AspectJParser.DOT, 0); }
		public TerminalNode CLASS() { return getToken(AspectJParser.CLASS, 0); }
		public TerminalNode VOID() { return getToken(AspectJParser.VOID, 0); }
		public NonWildcardTypeArgumentsContext nonWildcardTypeArguments() {
			return getRuleContext(NonWildcardTypeArgumentsContext.class,0);
		}
		public ExplicitGenericInvocationSuffixContext explicitGenericInvocationSuffix() {
			return getRuleContext(ExplicitGenericInvocationSuffixContext.class,0);
		}
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public PrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitPrimary(this);
		}
	}

	public final PrimaryContext primary() throws RecognitionException {
		PrimaryContext _localctx = new PrimaryContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_primary);
		try {
			setState(1666);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,157,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1645);
				match(LPAREN);
				setState(1646);
				expression(0);
				setState(1647);
				match(RPAREN);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1649);
				match(THIS);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1650);
				match(SUPER);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1651);
				literal();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1652);
				id();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1653);
				type();
				setState(1654);
				match(DOT);
				setState(1655);
				match(CLASS);
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1657);
				match(VOID);
				setState(1658);
				match(DOT);
				setState(1659);
				match(CLASS);
				}
				break;
			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(1660);
				nonWildcardTypeArguments();
				setState(1664);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case ARGS:
				case AFTER:
				case AROUND:
				case ASPECT:
				case BEFORE:
				case CALL:
				case CFLOW:
				case CFLOWBELOW:
				case DECLARE:
				case ERROR:
				case EXECUTION:
				case GET:
				case HANDLER:
				case INITIALIZATION:
				case ISSINGLETON:
				case PARENTS:
				case PERCFLOW:
				case PERCFLOWBELOW:
				case PERTARGET:
				case PERTHIS:
				case PERTYPEWITHIN:
				case POINTCUT:
				case PRECEDENCE:
				case PREINITIALIZATION:
				case PRIVILEGED:
				case RETURNING:
				case SET:
				case SOFT:
				case STATICINITIALIZATION:
				case TARGET:
				case THROWING:
				case WARNING:
				case WITHIN:
				case WITHINCODE:
				case SUPER:
				case Identifier:
					{
					setState(1661);
					explicitGenericInvocationSuffix();
					}
					break;
				case THIS:
					{
					setState(1662);
					match(THIS);
					setState(1663);
					arguments();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreatedNameContext extends ParserRuleContext {
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public List<TypeArgumentsOrDiamondContext> typeArgumentsOrDiamond() {
			return getRuleContexts(TypeArgumentsOrDiamondContext.class);
		}
		public TypeArgumentsOrDiamondContext typeArgumentsOrDiamond(int i) {
			return getRuleContext(TypeArgumentsOrDiamondContext.class,i);
		}
		public List<TerminalNode> DOT() { return getTokens(AspectJParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(AspectJParser.DOT, i);
		}
		public PrimitiveTypeContext primitiveType() {
			return getRuleContext(PrimitiveTypeContext.class,0);
		}
		public CreatedNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_createdName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCreatedName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCreatedName(this);
		}
	}

	public final CreatedNameContext createdName() throws RecognitionException {
		CreatedNameContext _localctx = new CreatedNameContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_createdName);
		int _la;
		try {
			setState(1683);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1668);
				id();
				setState(1670);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==LT) {
					{
					setState(1669);
					typeArgumentsOrDiamond();
					}
				}

				setState(1679);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==DOT) {
					{
					{
					setState(1672);
					match(DOT);
					setState(1673);
					id();
					setState(1675);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la==LT) {
						{
						setState(1674);
						typeArgumentsOrDiamond();
						}
					}

					}
					}
					setState(1681);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1682);
				primitiveType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InnerCreatorContext extends ParserRuleContext {
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public ClassCreatorRestContext classCreatorRest() {
			return getRuleContext(ClassCreatorRestContext.class,0);
		}
		public NonWildcardTypeArgumentsOrDiamondContext nonWildcardTypeArgumentsOrDiamond() {
			return getRuleContext(NonWildcardTypeArgumentsOrDiamondContext.class,0);
		}
		public InnerCreatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_innerCreator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInnerCreator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInnerCreator(this);
		}
	}

	public final InnerCreatorContext innerCreator() throws RecognitionException {
		InnerCreatorContext _localctx = new InnerCreatorContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_innerCreator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1685);
			id();
			setState(1687);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==LT) {
				{
				setState(1686);
				nonWildcardTypeArgumentsOrDiamond();
				}
			}

			setState(1689);
			classCreatorRest();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SuperSuffixContext extends ParserRuleContext {
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public TerminalNode DOT() { return getToken(AspectJParser.DOT, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public SuperSuffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_superSuffix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterSuperSuffix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitSuperSuffix(this);
		}
	}

	public final SuperSuffixContext superSuffix() throws RecognitionException {
		SuperSuffixContext _localctx = new SuperSuffixContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_superSuffix);
		try {
			setState(1697);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(1691);
				arguments();
				}
				break;
			case DOT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1692);
				match(DOT);
				setState(1693);
				id();
				setState(1695);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,163,_ctx) ) {
				case 1:
					{
					setState(1694);
					arguments();
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExplicitGenericInvocationSuffixContext extends ParserRuleContext {
		public TerminalNode SUPER() { return getToken(AspectJParser.SUPER, 0); }
		public SuperSuffixContext superSuffix() {
			return getRuleContext(SuperSuffixContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public ExplicitGenericInvocationSuffixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_explicitGenericInvocationSuffix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterExplicitGenericInvocationSuffix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitExplicitGenericInvocationSuffix(this);
		}
	}

	public final ExplicitGenericInvocationSuffixContext explicitGenericInvocationSuffix() throws RecognitionException {
		ExplicitGenericInvocationSuffixContext _localctx = new ExplicitGenericInvocationSuffixContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_explicitGenericInvocationSuffix);
		try {
			setState(1704);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SUPER:
				enterOuterAlt(_localctx, 1);
				{
				setState(1699);
				match(SUPER);
				setState(1700);
				superSuffix();
				}
				break;
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(1701);
				id();
				setState(1702);
				arguments();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompilationUnitContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(AspectJParser.EOF, 0); }
		public PackageDeclarationContext packageDeclaration() {
			return getRuleContext(PackageDeclarationContext.class,0);
		}
		public List<ImportDeclarationContext> importDeclaration() {
			return getRuleContexts(ImportDeclarationContext.class);
		}
		public ImportDeclarationContext importDeclaration(int i) {
			return getRuleContext(ImportDeclarationContext.class,i);
		}
		public List<TypeDeclarationContext> typeDeclaration() {
			return getRuleContexts(TypeDeclarationContext.class);
		}
		public TypeDeclarationContext typeDeclaration(int i) {
			return getRuleContext(TypeDeclarationContext.class,i);
		}
		public CompilationUnitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compilationUnit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCompilationUnit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCompilationUnit(this);
		}
	}

	public final CompilationUnitContext compilationUnit() throws RecognitionException {
		CompilationUnitContext _localctx = new CompilationUnitContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_compilationUnit);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1707);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,166,_ctx) ) {
			case 1:
				{
				setState(1706);
				packageDeclaration();
				}
				break;
			}
			setState(1712);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==IMPORT) {
				{
				{
				setState(1709);
				importDeclaration();
				}
				}
				setState(1714);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1718);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (CLASS - 58)) | (1L << (ENUM - 58)) | (1L << (FINAL - 58)) | (1L << (INTERFACE - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)) | (1L << (SEMI - 58)))) != 0)) {
				{
				{
				setState(1715);
				typeDeclaration();
				}
				}
				setState(1720);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1721);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PackageDeclarationContext extends ParserRuleContext {
		public TerminalNode PACKAGE() { return getToken(AspectJParser.PACKAGE, 0); }
		public QualifiedNameContext qualifiedName() {
			return getRuleContext(QualifiedNameContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public List<AnnotationContext> annotation() {
			return getRuleContexts(AnnotationContext.class);
		}
		public AnnotationContext annotation(int i) {
			return getRuleContext(AnnotationContext.class,i);
		}
		public PackageDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_packageDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterPackageDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitPackageDeclaration(this);
		}
	}

	public final PackageDeclarationContext packageDeclaration() throws RecognitionException {
		PackageDeclarationContext _localctx = new PackageDeclarationContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_packageDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1726);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT) {
				{
				{
				setState(1723);
				annotation();
				}
				}
				setState(1728);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1729);
			match(PACKAGE);
			setState(1730);
			qualifiedName();
			setState(1731);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportDeclarationContext extends ParserRuleContext {
		public TerminalNode IMPORT() { return getToken(AspectJParser.IMPORT, 0); }
		public QualifiedNameContext qualifiedName() {
			return getRuleContext(QualifiedNameContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public TerminalNode STATIC() { return getToken(AspectJParser.STATIC, 0); }
		public TerminalNode DOT() { return getToken(AspectJParser.DOT, 0); }
		public TerminalNode MUL() { return getToken(AspectJParser.MUL, 0); }
		public ImportDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterImportDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitImportDeclaration(this);
		}
	}

	public final ImportDeclarationContext importDeclaration() throws RecognitionException {
		ImportDeclarationContext _localctx = new ImportDeclarationContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_importDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1733);
			match(IMPORT);
			setState(1735);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==STATIC) {
				{
				setState(1734);
				match(STATIC);
				}
			}

			setState(1737);
			qualifiedName();
			setState(1740);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==DOT) {
				{
				setState(1738);
				match(DOT);
				setState(1739);
				match(MUL);
				}
			}

			setState(1742);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeDeclarationContext extends ParserRuleContext {
		public ClassDeclarationContext classDeclaration() {
			return getRuleContext(ClassDeclarationContext.class,0);
		}
		public List<ClassOrInterfaceModifierContext> classOrInterfaceModifier() {
			return getRuleContexts(ClassOrInterfaceModifierContext.class);
		}
		public ClassOrInterfaceModifierContext classOrInterfaceModifier(int i) {
			return getRuleContext(ClassOrInterfaceModifierContext.class,i);
		}
		public EnumDeclarationContext enumDeclaration() {
			return getRuleContext(EnumDeclarationContext.class,0);
		}
		public InterfaceDeclarationContext interfaceDeclaration() {
			return getRuleContext(InterfaceDeclarationContext.class,0);
		}
		public AnnotationTypeDeclarationContext annotationTypeDeclaration() {
			return getRuleContext(AnnotationTypeDeclarationContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public TypeDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeDeclaration(this);
		}
	}

	public final TypeDeclarationContext typeDeclaration() throws RecognitionException {
		TypeDeclarationContext _localctx = new TypeDeclarationContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_typeDeclaration);
		int _la;
		try {
			int _alt;
			setState(1773);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,176,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1747);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)))) != 0)) {
					{
					{
					setState(1744);
					classOrInterfaceModifier();
					}
					}
					setState(1749);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1750);
				classDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1754);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)))) != 0)) {
					{
					{
					setState(1751);
					classOrInterfaceModifier();
					}
					}
					setState(1756);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1757);
				enumDeclaration();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1761);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 58)) & ~0x3f) == 0 && ((1L << (_la - 58)) & ((1L << (AT - 58)) | (1L << (ABSTRACT - 58)) | (1L << (FINAL - 58)) | (1L << (PRIVATE - 58)) | (1L << (PROTECTED - 58)) | (1L << (PUBLIC - 58)) | (1L << (STATIC - 58)) | (1L << (STRICTFP - 58)))) != 0)) {
					{
					{
					setState(1758);
					classOrInterfaceModifier();
					}
					}
					setState(1763);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(1764);
				interfaceDeclaration();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1768);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,175,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1765);
						classOrInterfaceModifier();
						}
						} 
					}
					setState(1770);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,175,_ctx);
				}
				setState(1771);
				annotationTypeDeclaration();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1772);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModifierContext extends ParserRuleContext {
		public ClassOrInterfaceModifierContext classOrInterfaceModifier() {
			return getRuleContext(ClassOrInterfaceModifierContext.class,0);
		}
		public TerminalNode NATIVE() { return getToken(AspectJParser.NATIVE, 0); }
		public TerminalNode SYNCHRONIZED() { return getToken(AspectJParser.SYNCHRONIZED, 0); }
		public TerminalNode TRANSIENT() { return getToken(AspectJParser.TRANSIENT, 0); }
		public TerminalNode VOLATILE() { return getToken(AspectJParser.VOLATILE, 0); }
		public ModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitModifier(this);
		}
	}

	public final ModifierContext modifier() throws RecognitionException {
		ModifierContext _localctx = new ModifierContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_modifier);
		int _la;
		try {
			setState(1777);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AT:
			case ABSTRACT:
			case FINAL:
			case PRIVATE:
			case PROTECTED:
			case PUBLIC:
			case STATIC:
			case STRICTFP:
				enterOuterAlt(_localctx, 1);
				{
				setState(1775);
				classOrInterfaceModifier();
				}
				break;
			case NATIVE:
			case SYNCHRONIZED:
			case TRANSIENT:
			case VOLATILE:
				enterOuterAlt(_localctx, 2);
				{
				setState(1776);
				_la = _input.LA(1);
				if ( !(((((_la - 88)) & ~0x3f) == 0 && ((1L << (_la - 88)) & ((1L << (NATIVE - 88)) | (1L << (SYNCHRONIZED - 88)) | (1L << (TRANSIENT - 88)) | (1L << (VOLATILE - 88)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassOrInterfaceModifierContext extends ParserRuleContext {
		public AnnotationContext annotation() {
			return getRuleContext(AnnotationContext.class,0);
		}
		public TerminalNode PUBLIC() { return getToken(AspectJParser.PUBLIC, 0); }
		public TerminalNode PROTECTED() { return getToken(AspectJParser.PROTECTED, 0); }
		public TerminalNode PRIVATE() { return getToken(AspectJParser.PRIVATE, 0); }
		public TerminalNode STATIC() { return getToken(AspectJParser.STATIC, 0); }
		public TerminalNode ABSTRACT() { return getToken(AspectJParser.ABSTRACT, 0); }
		public TerminalNode FINAL() { return getToken(AspectJParser.FINAL, 0); }
		public TerminalNode STRICTFP() { return getToken(AspectJParser.STRICTFP, 0); }
		public ClassOrInterfaceModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classOrInterfaceModifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassOrInterfaceModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassOrInterfaceModifier(this);
		}
	}

	public final ClassOrInterfaceModifierContext classOrInterfaceModifier() throws RecognitionException {
		ClassOrInterfaceModifierContext _localctx = new ClassOrInterfaceModifierContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_classOrInterfaceModifier);
		int _la;
		try {
			setState(1781);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AT:
				enterOuterAlt(_localctx, 1);
				{
				setState(1779);
				annotation();
				}
				break;
			case ABSTRACT:
			case FINAL:
			case PRIVATE:
			case PROTECTED:
			case PUBLIC:
			case STATIC:
			case STRICTFP:
				enterOuterAlt(_localctx, 2);
				{
				setState(1780);
				_la = _input.LA(1);
				if ( !(((((_la - 59)) & ~0x3f) == 0 && ((1L << (_la - 59)) & ((1L << (ABSTRACT - 59)) | (1L << (FINAL - 59)) | (1L << (PRIVATE - 59)) | (1L << (PROTECTED - 59)) | (1L << (PUBLIC - 59)) | (1L << (STATIC - 59)) | (1L << (STRICTFP - 59)))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableModifierContext extends ParserRuleContext {
		public TerminalNode FINAL() { return getToken(AspectJParser.FINAL, 0); }
		public AnnotationContext annotation() {
			return getRuleContext(AnnotationContext.class,0);
		}
		public VariableModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableModifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterVariableModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitVariableModifier(this);
		}
	}

	public final VariableModifierContext variableModifier() throws RecognitionException {
		VariableModifierContext _localctx = new VariableModifierContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_variableModifier);
		try {
			setState(1785);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case FINAL:
				enterOuterAlt(_localctx, 1);
				{
				setState(1783);
				match(FINAL);
				}
				break;
			case AT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1784);
				annotation();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeParametersContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(AspectJParser.LT, 0); }
		public List<TypeParameterContext> typeParameter() {
			return getRuleContexts(TypeParameterContext.class);
		}
		public TypeParameterContext typeParameter(int i) {
			return getRuleContext(TypeParameterContext.class,i);
		}
		public TerminalNode GT() { return getToken(AspectJParser.GT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public TypeParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeParameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeParameters(this);
		}
	}

	public final TypeParametersContext typeParameters() throws RecognitionException {
		TypeParametersContext _localctx = new TypeParametersContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_typeParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1787);
			match(LT);
			setState(1788);
			typeParameter();
			setState(1793);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1789);
				match(COMMA);
				setState(1790);
				typeParameter();
				}
				}
				setState(1795);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1796);
			match(GT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeBoundContext extends ParserRuleContext {
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> BITAND() { return getTokens(AspectJParser.BITAND); }
		public TerminalNode BITAND(int i) {
			return getToken(AspectJParser.BITAND, i);
		}
		public TypeBoundContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeBound; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeBound(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeBound(this);
		}
	}

	public final TypeBoundContext typeBound() throws RecognitionException {
		TypeBoundContext _localctx = new TypeBoundContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_typeBound);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1798);
			type();
			setState(1803);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BITAND) {
				{
				{
				setState(1799);
				match(BITAND);
				setState(1800);
				type();
				}
				}
				setState(1805);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnumConstantsContext extends ParserRuleContext {
		public List<EnumConstantContext> enumConstant() {
			return getRuleContexts(EnumConstantContext.class);
		}
		public EnumConstantContext enumConstant(int i) {
			return getRuleContext(EnumConstantContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public EnumConstantsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumConstants; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterEnumConstants(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitEnumConstants(this);
		}
	}

	public final EnumConstantsContext enumConstants() throws RecognitionException {
		EnumConstantsContext _localctx = new EnumConstantsContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_enumConstants);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1806);
			enumConstant();
			setState(1811);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,182,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(1807);
					match(COMMA);
					setState(1808);
					enumConstant();
					}
					} 
				}
				setState(1813);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,182,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnumBodyDeclarationsContext extends ParserRuleContext {
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public List<ClassBodyDeclarationContext> classBodyDeclaration() {
			return getRuleContexts(ClassBodyDeclarationContext.class);
		}
		public ClassBodyDeclarationContext classBodyDeclaration(int i) {
			return getRuleContext(ClassBodyDeclarationContext.class,i);
		}
		public EnumBodyDeclarationsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enumBodyDeclarations; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterEnumBodyDeclarations(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitEnumBodyDeclarations(this);
		}
	}

	public final EnumBodyDeclarationsContext enumBodyDeclarations() throws RecognitionException {
		EnumBodyDeclarationsContext _localctx = new EnumBodyDeclarationsContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_enumBodyDeclarations);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1814);
			match(SEMI);
			setState(1818);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << ABSTRACT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (CLASS - 66)) | (1L << (DOUBLE - 66)) | (1L << (ENUM - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (INTERFACE - 66)) | (1L << (LONG - 66)) | (1L << (NATIVE - 66)) | (1L << (PRIVATE - 66)) | (1L << (PROTECTED - 66)) | (1L << (PUBLIC - 66)) | (1L << (SHORT - 66)) | (1L << (STATIC - 66)) | (1L << (STRICTFP - 66)) | (1L << (SYNCHRONIZED - 66)) | (1L << (TRANSIENT - 66)) | (1L << (VOID - 66)) | (1L << (VOLATILE - 66)) | (1L << (LBRACE - 66)) | (1L << (SEMI - 66)) | (1L << (LT - 66)))) != 0) || _la==Identifier) {
				{
				{
				setState(1815);
				classBodyDeclaration();
				}
				}
				setState(1820);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeListContext extends ParserRuleContext {
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public TypeListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeList(this);
		}
	}

	public final TypeListContext typeList() throws RecognitionException {
		TypeListContext _localctx = new TypeListContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_typeList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1821);
			type();
			setState(1826);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1822);
				match(COMMA);
				setState(1823);
				type();
				}
				}
				setState(1828);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassBodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<ClassBodyDeclarationContext> classBodyDeclaration() {
			return getRuleContexts(ClassBodyDeclarationContext.class);
		}
		public ClassBodyDeclarationContext classBodyDeclaration(int i) {
			return getRuleContext(ClassBodyDeclarationContext.class,i);
		}
		public ClassBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassBody(this);
		}
	}

	public final ClassBodyContext classBody() throws RecognitionException {
		ClassBodyContext _localctx = new ClassBodyContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_classBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1829);
			match(LBRACE);
			setState(1833);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << ABSTRACT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (CLASS - 66)) | (1L << (DOUBLE - 66)) | (1L << (ENUM - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (INTERFACE - 66)) | (1L << (LONG - 66)) | (1L << (NATIVE - 66)) | (1L << (PRIVATE - 66)) | (1L << (PROTECTED - 66)) | (1L << (PUBLIC - 66)) | (1L << (SHORT - 66)) | (1L << (STATIC - 66)) | (1L << (STRICTFP - 66)) | (1L << (SYNCHRONIZED - 66)) | (1L << (TRANSIENT - 66)) | (1L << (VOID - 66)) | (1L << (VOLATILE - 66)) | (1L << (LBRACE - 66)) | (1L << (SEMI - 66)) | (1L << (LT - 66)))) != 0) || _la==Identifier) {
				{
				{
				setState(1830);
				classBodyDeclaration();
				}
				}
				setState(1835);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1836);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterfaceBodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<InterfaceBodyDeclarationContext> interfaceBodyDeclaration() {
			return getRuleContexts(InterfaceBodyDeclarationContext.class);
		}
		public InterfaceBodyDeclarationContext interfaceBodyDeclaration(int i) {
			return getRuleContext(InterfaceBodyDeclarationContext.class,i);
		}
		public InterfaceBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interfaceBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInterfaceBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInterfaceBody(this);
		}
	}

	public final InterfaceBodyContext interfaceBody() throws RecognitionException {
		InterfaceBodyContext _localctx = new InterfaceBodyContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_interfaceBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1838);
			match(LBRACE);
			setState(1842);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << ABSTRACT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (CLASS - 66)) | (1L << (DOUBLE - 66)) | (1L << (ENUM - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (INTERFACE - 66)) | (1L << (LONG - 66)) | (1L << (NATIVE - 66)) | (1L << (PRIVATE - 66)) | (1L << (PROTECTED - 66)) | (1L << (PUBLIC - 66)) | (1L << (SHORT - 66)) | (1L << (STATIC - 66)) | (1L << (STRICTFP - 66)) | (1L << (SYNCHRONIZED - 66)) | (1L << (TRANSIENT - 66)) | (1L << (VOID - 66)) | (1L << (VOLATILE - 66)) | (1L << (SEMI - 66)) | (1L << (LT - 66)))) != 0) || _la==Identifier) {
				{
				{
				setState(1839);
				interfaceBodyDeclaration();
				}
				}
				setState(1844);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1845);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GenericMethodDeclarationContext extends ParserRuleContext {
		public TypeParametersContext typeParameters() {
			return getRuleContext(TypeParametersContext.class,0);
		}
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public GenericMethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_genericMethodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterGenericMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitGenericMethodDeclaration(this);
		}
	}

	public final GenericMethodDeclarationContext genericMethodDeclaration() throws RecognitionException {
		GenericMethodDeclarationContext _localctx = new GenericMethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_genericMethodDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1847);
			typeParameters();
			setState(1848);
			methodDeclaration();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GenericConstructorDeclarationContext extends ParserRuleContext {
		public TypeParametersContext typeParameters() {
			return getRuleContext(TypeParametersContext.class,0);
		}
		public ConstructorDeclarationContext constructorDeclaration() {
			return getRuleContext(ConstructorDeclarationContext.class,0);
		}
		public GenericConstructorDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_genericConstructorDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterGenericConstructorDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitGenericConstructorDeclaration(this);
		}
	}

	public final GenericConstructorDeclarationContext genericConstructorDeclaration() throws RecognitionException {
		GenericConstructorDeclarationContext _localctx = new GenericConstructorDeclarationContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_genericConstructorDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1850);
			typeParameters();
			setState(1851);
			constructorDeclaration();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldDeclarationContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VariableDeclaratorsContext variableDeclarators() {
			return getRuleContext(VariableDeclaratorsContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public FieldDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFieldDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFieldDeclaration(this);
		}
	}

	public final FieldDeclarationContext fieldDeclaration() throws RecognitionException {
		FieldDeclarationContext _localctx = new FieldDeclarationContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_fieldDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1853);
			type();
			setState(1854);
			variableDeclarators();
			setState(1855);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterfaceBodyDeclarationContext extends ParserRuleContext {
		public InterfaceMemberDeclarationContext interfaceMemberDeclaration() {
			return getRuleContext(InterfaceMemberDeclarationContext.class,0);
		}
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public InterfaceBodyDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interfaceBodyDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInterfaceBodyDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInterfaceBodyDeclaration(this);
		}
	}

	public final InterfaceBodyDeclarationContext interfaceBodyDeclaration() throws RecognitionException {
		InterfaceBodyDeclarationContext _localctx = new InterfaceBodyDeclarationContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_interfaceBodyDeclaration);
		try {
			int _alt;
			setState(1865);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case AT:
			case ABSTRACT:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CLASS:
			case DOUBLE:
			case ENUM:
			case FINAL:
			case FLOAT:
			case INT:
			case INTERFACE:
			case LONG:
			case NATIVE:
			case PRIVATE:
			case PROTECTED:
			case PUBLIC:
			case SHORT:
			case STATIC:
			case STRICTFP:
			case SYNCHRONIZED:
			case TRANSIENT:
			case VOID:
			case VOLATILE:
			case LT:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1860);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,187,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1857);
						modifier();
						}
						} 
					}
					setState(1862);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,187,_ctx);
				}
				setState(1863);
				interfaceMemberDeclaration();
				}
				break;
			case SEMI:
				enterOuterAlt(_localctx, 2);
				{
				setState(1864);
				match(SEMI);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterfaceMemberDeclarationContext extends ParserRuleContext {
		public ConstDeclarationContext constDeclaration() {
			return getRuleContext(ConstDeclarationContext.class,0);
		}
		public InterfaceMethodDeclarationContext interfaceMethodDeclaration() {
			return getRuleContext(InterfaceMethodDeclarationContext.class,0);
		}
		public GenericInterfaceMethodDeclarationContext genericInterfaceMethodDeclaration() {
			return getRuleContext(GenericInterfaceMethodDeclarationContext.class,0);
		}
		public InterfaceDeclarationContext interfaceDeclaration() {
			return getRuleContext(InterfaceDeclarationContext.class,0);
		}
		public AnnotationTypeDeclarationContext annotationTypeDeclaration() {
			return getRuleContext(AnnotationTypeDeclarationContext.class,0);
		}
		public ClassDeclarationContext classDeclaration() {
			return getRuleContext(ClassDeclarationContext.class,0);
		}
		public EnumDeclarationContext enumDeclaration() {
			return getRuleContext(EnumDeclarationContext.class,0);
		}
		public InterfaceMemberDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_interfaceMemberDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterInterfaceMemberDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitInterfaceMemberDeclaration(this);
		}
	}

	public final InterfaceMemberDeclarationContext interfaceMemberDeclaration() throws RecognitionException {
		InterfaceMemberDeclarationContext _localctx = new InterfaceMemberDeclarationContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_interfaceMemberDeclaration);
		try {
			setState(1874);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,189,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1867);
				constDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1868);
				interfaceMethodDeclaration();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1869);
				genericInterfaceMethodDeclaration();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1870);
				interfaceDeclaration();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1871);
				annotationTypeDeclaration();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1872);
				classDeclaration();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(1873);
				enumDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstDeclarationContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public List<ConstantDeclaratorContext> constantDeclarator() {
			return getRuleContexts(ConstantDeclaratorContext.class);
		}
		public ConstantDeclaratorContext constantDeclarator(int i) {
			return getRuleContext(ConstantDeclaratorContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public ConstDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstDeclaration(this);
		}
	}

	public final ConstDeclarationContext constDeclaration() throws RecognitionException {
		ConstDeclarationContext _localctx = new ConstDeclarationContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_constDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1876);
			type();
			setState(1877);
			constantDeclarator();
			setState(1882);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1878);
				match(COMMA);
				setState(1879);
				constantDeclarator();
				}
				}
				setState(1884);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1885);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GenericInterfaceMethodDeclarationContext extends ParserRuleContext {
		public TypeParametersContext typeParameters() {
			return getRuleContext(TypeParametersContext.class,0);
		}
		public InterfaceMethodDeclarationContext interfaceMethodDeclaration() {
			return getRuleContext(InterfaceMethodDeclarationContext.class,0);
		}
		public GenericInterfaceMethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_genericInterfaceMethodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterGenericInterfaceMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitGenericInterfaceMethodDeclaration(this);
		}
	}

	public final GenericInterfaceMethodDeclarationContext genericInterfaceMethodDeclaration() throws RecognitionException {
		GenericInterfaceMethodDeclarationContext _localctx = new GenericInterfaceMethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_genericInterfaceMethodDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1887);
			typeParameters();
			setState(1888);
			interfaceMethodDeclaration();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclaratorsContext extends ParserRuleContext {
		public List<VariableDeclaratorContext> variableDeclarator() {
			return getRuleContexts(VariableDeclaratorContext.class);
		}
		public VariableDeclaratorContext variableDeclarator(int i) {
			return getRuleContext(VariableDeclaratorContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public VariableDeclaratorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterVariableDeclarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitVariableDeclarators(this);
		}
	}

	public final VariableDeclaratorsContext variableDeclarators() throws RecognitionException {
		VariableDeclaratorsContext _localctx = new VariableDeclaratorsContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_variableDeclarators);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1890);
			variableDeclarator();
			setState(1895);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1891);
				match(COMMA);
				setState(1892);
				variableDeclarator();
				}
				}
				setState(1897);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclaratorContext extends ParserRuleContext {
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(AspectJParser.ASSIGN, 0); }
		public VariableInitializerContext variableInitializer() {
			return getRuleContext(VariableInitializerContext.class,0);
		}
		public VariableDeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterVariableDeclarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitVariableDeclarator(this);
		}
	}

	public final VariableDeclaratorContext variableDeclarator() throws RecognitionException {
		VariableDeclaratorContext _localctx = new VariableDeclaratorContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_variableDeclarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1898);
			variableDeclaratorId();
			setState(1901);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==ASSIGN) {
				{
				setState(1899);
				match(ASSIGN);
				setState(1900);
				variableInitializer();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableInitializerContext extends ParserRuleContext {
		public ArrayInitializerContext arrayInitializer() {
			return getRuleContext(ArrayInitializerContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VariableInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterVariableInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitVariableInitializer(this);
		}
	}

	public final VariableInitializerContext variableInitializer() throws RecognitionException {
		VariableInitializerContext _localctx = new VariableInitializerContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_variableInitializer);
		try {
			setState(1905);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LBRACE:
				enterOuterAlt(_localctx, 1);
				{
				setState(1903);
				arrayInitializer();
				}
				break;
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case NEW:
			case SHORT:
			case SUPER:
			case THIS:
			case VOID:
			case IntegerLiteral:
			case FloatingPointLiteral:
			case BooleanLiteral:
			case CharacterLiteral:
			case StringLiteral:
			case NullLiteral:
			case LPAREN:
			case LT:
			case BANG:
			case TILDE:
			case INC:
			case DEC:
			case ADD:
			case SUB:
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(1904);
				expression(0);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayInitializerContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<VariableInitializerContext> variableInitializer() {
			return getRuleContexts(VariableInitializerContext.class);
		}
		public VariableInitializerContext variableInitializer(int i) {
			return getRuleContext(VariableInitializerContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public ArrayInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterArrayInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitArrayInitializer(this);
		}
	}

	public final ArrayInitializerContext arrayInitializer() throws RecognitionException {
		ArrayInitializerContext _localctx = new ArrayInitializerContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_arrayInitializer);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(1907);
			match(LBRACE);
			setState(1919);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LBRACE - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
				{
				setState(1908);
				variableInitializer();
				setState(1913);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,194,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1909);
						match(COMMA);
						setState(1910);
						variableInitializer();
						}
						} 
					}
					setState(1915);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,194,_ctx);
				}
				setState(1917);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMMA) {
					{
					setState(1916);
					match(COMMA);
					}
				}

				}
			}

			setState(1921);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public ClassOrInterfaceTypeContext classOrInterfaceType() {
			return getRuleContext(ClassOrInterfaceTypeContext.class,0);
		}
		public List<TerminalNode> LBRACK() { return getTokens(AspectJParser.LBRACK); }
		public TerminalNode LBRACK(int i) {
			return getToken(AspectJParser.LBRACK, i);
		}
		public List<TerminalNode> RBRACK() { return getTokens(AspectJParser.RBRACK); }
		public TerminalNode RBRACK(int i) {
			return getToken(AspectJParser.RBRACK, i);
		}
		public PrimitiveTypeContext primitiveType() {
			return getRuleContext(PrimitiveTypeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_type);
		try {
			int _alt;
			setState(1939);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1923);
				classOrInterfaceType();
				setState(1928);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,197,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1924);
						match(LBRACK);
						setState(1925);
						match(RBRACK);
						}
						} 
					}
					setState(1930);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,197,_ctx);
				}
				}
				break;
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
				enterOuterAlt(_localctx, 2);
				{
				setState(1931);
				primitiveType();
				setState(1936);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,198,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1932);
						match(LBRACK);
						setState(1933);
						match(RBRACK);
						}
						} 
					}
					setState(1938);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,198,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimitiveTypeContext extends ParserRuleContext {
		public TerminalNode BOOLEAN() { return getToken(AspectJParser.BOOLEAN, 0); }
		public TerminalNode CHAR() { return getToken(AspectJParser.CHAR, 0); }
		public TerminalNode BYTE() { return getToken(AspectJParser.BYTE, 0); }
		public TerminalNode SHORT() { return getToken(AspectJParser.SHORT, 0); }
		public TerminalNode INT() { return getToken(AspectJParser.INT, 0); }
		public TerminalNode LONG() { return getToken(AspectJParser.LONG, 0); }
		public TerminalNode FLOAT() { return getToken(AspectJParser.FLOAT, 0); }
		public TerminalNode DOUBLE() { return getToken(AspectJParser.DOUBLE, 0); }
		public PrimitiveTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primitiveType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterPrimitiveType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitPrimitiveType(this);
		}
	}

	public final PrimitiveTypeContext primitiveType() throws RecognitionException {
		PrimitiveTypeContext _localctx = new PrimitiveTypeContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_primitiveType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1941);
			_la = _input.LA(1);
			if ( !(((((_la - 61)) & ~0x3f) == 0 && ((1L << (_la - 61)) & ((1L << (BOOLEAN - 61)) | (1L << (BYTE - 61)) | (1L << (CHAR - 61)) | (1L << (DOUBLE - 61)) | (1L << (FLOAT - 61)) | (1L << (INT - 61)) | (1L << (LONG - 61)) | (1L << (SHORT - 61)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeArgumentsContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(AspectJParser.LT, 0); }
		public List<TypeArgumentContext> typeArgument() {
			return getRuleContexts(TypeArgumentContext.class);
		}
		public TypeArgumentContext typeArgument(int i) {
			return getRuleContext(TypeArgumentContext.class,i);
		}
		public TerminalNode GT() { return getToken(AspectJParser.GT, 0); }
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public TypeArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeArguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeArguments(this);
		}
	}

	public final TypeArgumentsContext typeArguments() throws RecognitionException {
		TypeArgumentsContext _localctx = new TypeArgumentsContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_typeArguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1943);
			match(LT);
			setState(1944);
			typeArgument();
			setState(1949);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1945);
				match(COMMA);
				setState(1946);
				typeArgument();
				}
				}
				setState(1951);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1952);
			match(GT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeArgumentContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode QUESTION() { return getToken(AspectJParser.QUESTION, 0); }
		public TerminalNode EXTENDS() { return getToken(AspectJParser.EXTENDS, 0); }
		public TerminalNode SUPER() { return getToken(AspectJParser.SUPER, 0); }
		public TypeArgumentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeArgument; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeArgument(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeArgument(this);
		}
	}

	public final TypeArgumentContext typeArgument() throws RecognitionException {
		TypeArgumentContext _localctx = new TypeArgumentContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_typeArgument);
		int _la;
		try {
			setState(1960);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(1954);
				type();
				}
				break;
			case QUESTION:
				enterOuterAlt(_localctx, 2);
				{
				setState(1955);
				match(QUESTION);
				setState(1958);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==EXTENDS || _la==SUPER) {
					{
					setState(1956);
					_la = _input.LA(1);
					if ( !(_la==EXTENDS || _la==SUPER) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(1957);
					type();
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualifiedNameListContext extends ParserRuleContext {
		public List<QualifiedNameContext> qualifiedName() {
			return getRuleContexts(QualifiedNameContext.class);
		}
		public QualifiedNameContext qualifiedName(int i) {
			return getRuleContext(QualifiedNameContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public QualifiedNameListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualifiedNameList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterQualifiedNameList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitQualifiedNameList(this);
		}
	}

	public final QualifiedNameListContext qualifiedNameList() throws RecognitionException {
		QualifiedNameListContext _localctx = new QualifiedNameListContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_qualifiedNameList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1962);
			qualifiedName();
			setState(1967);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(1963);
				match(COMMA);
				setState(1964);
				qualifiedName();
				}
				}
				setState(1969);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParametersContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public FormalParameterListContext formalParameterList() {
			return getRuleContext(FormalParameterListContext.class,0);
		}
		public FormalParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFormalParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFormalParameters(this);
		}
	}

	public final FormalParametersContext formalParameters() throws RecognitionException {
		FormalParametersContext _localctx = new FormalParametersContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_formalParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1970);
			match(LPAREN);
			setState(1972);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (SHORT - 66)))) != 0) || _la==Identifier) {
				{
				setState(1971);
				formalParameterList();
				}
			}

			setState(1974);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParameterListContext extends ParserRuleContext {
		public List<FormalParameterContext> formalParameter() {
			return getRuleContexts(FormalParameterContext.class);
		}
		public FormalParameterContext formalParameter(int i) {
			return getRuleContext(FormalParameterContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public LastFormalParameterContext lastFormalParameter() {
			return getRuleContext(LastFormalParameterContext.class,0);
		}
		public FormalParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameterList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFormalParameterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFormalParameterList(this);
		}
	}

	public final FormalParameterListContext formalParameterList() throws RecognitionException {
		FormalParameterListContext _localctx = new FormalParameterListContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_formalParameterList);
		int _la;
		try {
			int _alt;
			setState(1989);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,207,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1976);
				formalParameter();
				setState(1981);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,205,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(1977);
						match(COMMA);
						setState(1978);
						formalParameter();
						}
						} 
					}
					setState(1983);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,205,_ctx);
				}
				setState(1986);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==COMMA) {
					{
					setState(1984);
					match(COMMA);
					setState(1985);
					lastFormalParameter();
					}
				}

				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1988);
				lastFormalParameter();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParameterContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public List<VariableModifierContext> variableModifier() {
			return getRuleContexts(VariableModifierContext.class);
		}
		public VariableModifierContext variableModifier(int i) {
			return getRuleContext(VariableModifierContext.class,i);
		}
		public FormalParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFormalParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFormalParameter(this);
		}
	}

	public final FormalParameterContext formalParameter() throws RecognitionException {
		FormalParameterContext _localctx = new FormalParameterContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_formalParameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1994);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT || _la==FINAL) {
				{
				{
				setState(1991);
				variableModifier();
				}
				}
				setState(1996);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(1997);
			type();
			setState(1998);
			variableDeclaratorId();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LastFormalParameterContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode ELLIPSIS() { return getToken(AspectJParser.ELLIPSIS, 0); }
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public List<VariableModifierContext> variableModifier() {
			return getRuleContexts(VariableModifierContext.class);
		}
		public VariableModifierContext variableModifier(int i) {
			return getRuleContext(VariableModifierContext.class,i);
		}
		public LastFormalParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lastFormalParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterLastFormalParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitLastFormalParameter(this);
		}
	}

	public final LastFormalParameterContext lastFormalParameter() throws RecognitionException {
		LastFormalParameterContext _localctx = new LastFormalParameterContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_lastFormalParameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2003);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT || _la==FINAL) {
				{
				{
				setState(2000);
				variableModifier();
				}
				}
				setState(2005);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2006);
			type();
			setState(2007);
			match(ELLIPSIS);
			setState(2008);
			variableDeclaratorId();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodBodyContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public MethodBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterMethodBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitMethodBody(this);
		}
	}

	public final MethodBodyContext methodBody() throws RecognitionException {
		MethodBodyContext _localctx = new MethodBodyContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_methodBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2010);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructorBodyContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ConstructorBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructorBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstructorBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstructorBody(this);
		}
	}

	public final ConstructorBodyContext constructorBody() throws RecognitionException {
		ConstructorBodyContext _localctx = new ConstructorBodyContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_constructorBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2012);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LiteralContext extends ParserRuleContext {
		public TerminalNode IntegerLiteral() { return getToken(AspectJParser.IntegerLiteral, 0); }
		public TerminalNode FloatingPointLiteral() { return getToken(AspectJParser.FloatingPointLiteral, 0); }
		public TerminalNode CharacterLiteral() { return getToken(AspectJParser.CharacterLiteral, 0); }
		public TerminalNode StringLiteral() { return getToken(AspectJParser.StringLiteral, 0); }
		public TerminalNode BooleanLiteral() { return getToken(AspectJParser.BooleanLiteral, 0); }
		public TerminalNode NullLiteral() { return getToken(AspectJParser.NullLiteral, 0); }
		public LiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_literal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitLiteral(this);
		}
	}

	public final LiteralContext literal() throws RecognitionException {
		LiteralContext _localctx = new LiteralContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_literal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2014);
			_la = _input.LA(1);
			if ( !(((((_la - 109)) & ~0x3f) == 0 && ((1L << (_la - 109)) & ((1L << (IntegerLiteral - 109)) | (1L << (FloatingPointLiteral - 109)) | (1L << (BooleanLiteral - 109)) | (1L << (CharacterLiteral - 109)) | (1L << (StringLiteral - 109)) | (1L << (NullLiteral - 109)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationNameContext extends ParserRuleContext {
		public QualifiedNameContext qualifiedName() {
			return getRuleContext(QualifiedNameContext.class,0);
		}
		public AnnotationNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationName(this);
		}
	}

	public final AnnotationNameContext annotationName() throws RecognitionException {
		AnnotationNameContext _localctx = new AnnotationNameContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_annotationName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2016);
			qualifiedName();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementValuePairsContext extends ParserRuleContext {
		public List<ElementValuePairContext> elementValuePair() {
			return getRuleContexts(ElementValuePairContext.class);
		}
		public ElementValuePairContext elementValuePair(int i) {
			return getRuleContext(ElementValuePairContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public ElementValuePairsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elementValuePairs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterElementValuePairs(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitElementValuePairs(this);
		}
	}

	public final ElementValuePairsContext elementValuePairs() throws RecognitionException {
		ElementValuePairsContext _localctx = new ElementValuePairsContext(_ctx, getState());
		enterRule(_localctx, 218, RULE_elementValuePairs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2018);
			elementValuePair();
			setState(2023);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(2019);
				match(COMMA);
				setState(2020);
				elementValuePair();
				}
				}
				setState(2025);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementValueContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AnnotationContext annotation() {
			return getRuleContext(AnnotationContext.class,0);
		}
		public ElementValueArrayInitializerContext elementValueArrayInitializer() {
			return getRuleContext(ElementValueArrayInitializerContext.class,0);
		}
		public ElementValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elementValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterElementValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitElementValue(this);
		}
	}

	public final ElementValueContext elementValue() throws RecognitionException {
		ElementValueContext _localctx = new ElementValueContext(_ctx, getState());
		enterRule(_localctx, 220, RULE_elementValue);
		try {
			setState(2029);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case NEW:
			case SHORT:
			case SUPER:
			case THIS:
			case VOID:
			case IntegerLiteral:
			case FloatingPointLiteral:
			case BooleanLiteral:
			case CharacterLiteral:
			case StringLiteral:
			case NullLiteral:
			case LPAREN:
			case LT:
			case BANG:
			case TILDE:
			case INC:
			case DEC:
			case ADD:
			case SUB:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(2026);
				expression(0);
				}
				break;
			case AT:
				enterOuterAlt(_localctx, 2);
				{
				setState(2027);
				annotation();
				}
				break;
			case LBRACE:
				enterOuterAlt(_localctx, 3);
				{
				setState(2028);
				elementValueArrayInitializer();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementValueArrayInitializerContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<ElementValueContext> elementValue() {
			return getRuleContexts(ElementValueContext.class);
		}
		public ElementValueContext elementValue(int i) {
			return getRuleContext(ElementValueContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public ElementValueArrayInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elementValueArrayInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterElementValueArrayInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitElementValueArrayInitializer(this);
		}
	}

	public final ElementValueArrayInitializerContext elementValueArrayInitializer() throws RecognitionException {
		ElementValueArrayInitializerContext _localctx = new ElementValueArrayInitializerContext(_ctx, getState());
		enterRule(_localctx, 222, RULE_elementValueArrayInitializer);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2031);
			match(LBRACE);
			setState(2040);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LBRACE - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
				{
				setState(2032);
				elementValue();
				setState(2037);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,212,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(2033);
						match(COMMA);
						setState(2034);
						elementValue();
						}
						} 
					}
					setState(2039);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,212,_ctx);
				}
				}
			}

			setState(2043);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==COMMA) {
				{
				setState(2042);
				match(COMMA);
				}
			}

			setState(2045);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationTypeBodyContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<AnnotationTypeElementDeclarationContext> annotationTypeElementDeclaration() {
			return getRuleContexts(AnnotationTypeElementDeclarationContext.class);
		}
		public AnnotationTypeElementDeclarationContext annotationTypeElementDeclaration(int i) {
			return getRuleContext(AnnotationTypeElementDeclarationContext.class,i);
		}
		public AnnotationTypeBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationTypeBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationTypeBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationTypeBody(this);
		}
	}

	public final AnnotationTypeBodyContext annotationTypeBody() throws RecognitionException {
		AnnotationTypeBodyContext _localctx = new AnnotationTypeBodyContext(_ctx, getState());
		enterRule(_localctx, 224, RULE_annotationTypeBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2047);
			match(LBRACE);
			setState(2051);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << ABSTRACT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (CLASS - 66)) | (1L << (DOUBLE - 66)) | (1L << (ENUM - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (INTERFACE - 66)) | (1L << (LONG - 66)) | (1L << (NATIVE - 66)) | (1L << (PRIVATE - 66)) | (1L << (PROTECTED - 66)) | (1L << (PUBLIC - 66)) | (1L << (SHORT - 66)) | (1L << (STATIC - 66)) | (1L << (STRICTFP - 66)) | (1L << (SYNCHRONIZED - 66)) | (1L << (TRANSIENT - 66)) | (1L << (VOLATILE - 66)) | (1L << (SEMI - 66)))) != 0) || _la==Identifier) {
				{
				{
				setState(2048);
				annotationTypeElementDeclaration();
				}
				}
				setState(2053);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2054);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationTypeElementDeclarationContext extends ParserRuleContext {
		public AnnotationTypeElementRestContext annotationTypeElementRest() {
			return getRuleContext(AnnotationTypeElementRestContext.class,0);
		}
		public List<ModifierContext> modifier() {
			return getRuleContexts(ModifierContext.class);
		}
		public ModifierContext modifier(int i) {
			return getRuleContext(ModifierContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public AnnotationTypeElementDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationTypeElementDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationTypeElementDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationTypeElementDeclaration(this);
		}
	}

	public final AnnotationTypeElementDeclarationContext annotationTypeElementDeclaration() throws RecognitionException {
		AnnotationTypeElementDeclarationContext _localctx = new AnnotationTypeElementDeclarationContext(_ctx, getState());
		enterRule(_localctx, 226, RULE_annotationTypeElementDeclaration);
		try {
			int _alt;
			setState(2064);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case AT:
			case ABSTRACT:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case CLASS:
			case DOUBLE:
			case ENUM:
			case FINAL:
			case FLOAT:
			case INT:
			case INTERFACE:
			case LONG:
			case NATIVE:
			case PRIVATE:
			case PROTECTED:
			case PUBLIC:
			case SHORT:
			case STATIC:
			case STRICTFP:
			case SYNCHRONIZED:
			case TRANSIENT:
			case VOLATILE:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(2059);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,216,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(2056);
						modifier();
						}
						} 
					}
					setState(2061);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,216,_ctx);
				}
				setState(2062);
				annotationTypeElementRest();
				}
				break;
			case SEMI:
				enterOuterAlt(_localctx, 2);
				{
				setState(2063);
				match(SEMI);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationTypeElementRestContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public AnnotationMethodOrConstantRestContext annotationMethodOrConstantRest() {
			return getRuleContext(AnnotationMethodOrConstantRestContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public ClassDeclarationContext classDeclaration() {
			return getRuleContext(ClassDeclarationContext.class,0);
		}
		public InterfaceDeclarationContext interfaceDeclaration() {
			return getRuleContext(InterfaceDeclarationContext.class,0);
		}
		public EnumDeclarationContext enumDeclaration() {
			return getRuleContext(EnumDeclarationContext.class,0);
		}
		public AnnotationTypeDeclarationContext annotationTypeDeclaration() {
			return getRuleContext(AnnotationTypeDeclarationContext.class,0);
		}
		public AnnotationTypeElementRestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationTypeElementRest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationTypeElementRest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationTypeElementRest(this);
		}
	}

	public final AnnotationTypeElementRestContext annotationTypeElementRest() throws RecognitionException {
		AnnotationTypeElementRestContext _localctx = new AnnotationTypeElementRestContext(_ctx, getState());
		enterRule(_localctx, 228, RULE_annotationTypeElementRest);
		try {
			setState(2086);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(2066);
				type();
				setState(2067);
				annotationMethodOrConstantRest();
				setState(2068);
				match(SEMI);
				}
				break;
			case CLASS:
				enterOuterAlt(_localctx, 2);
				{
				setState(2070);
				classDeclaration();
				setState(2072);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,218,_ctx) ) {
				case 1:
					{
					setState(2071);
					match(SEMI);
					}
					break;
				}
				}
				break;
			case INTERFACE:
				enterOuterAlt(_localctx, 3);
				{
				setState(2074);
				interfaceDeclaration();
				setState(2076);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,219,_ctx) ) {
				case 1:
					{
					setState(2075);
					match(SEMI);
					}
					break;
				}
				}
				break;
			case ENUM:
				enterOuterAlt(_localctx, 4);
				{
				setState(2078);
				enumDeclaration();
				setState(2080);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,220,_ctx) ) {
				case 1:
					{
					setState(2079);
					match(SEMI);
					}
					break;
				}
				}
				break;
			case AT:
				enterOuterAlt(_localctx, 5);
				{
				setState(2082);
				annotationTypeDeclaration();
				setState(2084);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,221,_ctx) ) {
				case 1:
					{
					setState(2083);
					match(SEMI);
					}
					break;
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationMethodOrConstantRestContext extends ParserRuleContext {
		public AnnotationMethodRestContext annotationMethodRest() {
			return getRuleContext(AnnotationMethodRestContext.class,0);
		}
		public AnnotationConstantRestContext annotationConstantRest() {
			return getRuleContext(AnnotationConstantRestContext.class,0);
		}
		public AnnotationMethodOrConstantRestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationMethodOrConstantRest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationMethodOrConstantRest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationMethodOrConstantRest(this);
		}
	}

	public final AnnotationMethodOrConstantRestContext annotationMethodOrConstantRest() throws RecognitionException {
		AnnotationMethodOrConstantRestContext _localctx = new AnnotationMethodOrConstantRestContext(_ctx, getState());
		enterRule(_localctx, 230, RULE_annotationMethodOrConstantRest);
		try {
			setState(2090);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,223,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2088);
				annotationMethodRest();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2089);
				annotationConstantRest();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnnotationConstantRestContext extends ParserRuleContext {
		public VariableDeclaratorsContext variableDeclarators() {
			return getRuleContext(VariableDeclaratorsContext.class,0);
		}
		public AnnotationConstantRestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_annotationConstantRest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterAnnotationConstantRest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitAnnotationConstantRest(this);
		}
	}

	public final AnnotationConstantRestContext annotationConstantRest() throws RecognitionException {
		AnnotationConstantRestContext _localctx = new AnnotationConstantRestContext(_ctx, getState());
		enterRule(_localctx, 232, RULE_annotationConstantRest);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2092);
			variableDeclarators();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefaultValueContext extends ParserRuleContext {
		public TerminalNode DEFAULT() { return getToken(AspectJParser.DEFAULT, 0); }
		public ElementValueContext elementValue() {
			return getRuleContext(ElementValueContext.class,0);
		}
		public DefaultValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defaultValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterDefaultValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitDefaultValue(this);
		}
	}

	public final DefaultValueContext defaultValue() throws RecognitionException {
		DefaultValueContext _localctx = new DefaultValueContext(_ctx, getState());
		enterRule(_localctx, 234, RULE_defaultValue);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2094);
			match(DEFAULT);
			setState(2095);
			elementValue();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(AspectJParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(AspectJParser.RBRACE, 0); }
		public List<BlockStatementContext> blockStatement() {
			return getRuleContexts(BlockStatementContext.class);
		}
		public BlockStatementContext blockStatement(int i) {
			return getRuleContext(BlockStatementContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitBlock(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 236, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2097);
			match(LBRACE);
			setState(2101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << ABSTRACT) | (1L << ASSERT) | (1L << BOOLEAN) | (1L << BREAK) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (CLASS - 66)) | (1L << (CONTINUE - 66)) | (1L << (DO - 66)) | (1L << (DOUBLE - 66)) | (1L << (ENUM - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (FOR - 66)) | (1L << (IF - 66)) | (1L << (INT - 66)) | (1L << (INTERFACE - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (PRIVATE - 66)) | (1L << (PROTECTED - 66)) | (1L << (PUBLIC - 66)) | (1L << (RETURN - 66)) | (1L << (SHORT - 66)) | (1L << (STATIC - 66)) | (1L << (STRICTFP - 66)) | (1L << (SUPER - 66)) | (1L << (SWITCH - 66)) | (1L << (SYNCHRONIZED - 66)) | (1L << (THIS - 66)) | (1L << (THROW - 66)) | (1L << (TRY - 66)) | (1L << (VOID - 66)) | (1L << (WHILE - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LBRACE - 66)) | (1L << (SEMI - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
				{
				{
				setState(2098);
				blockStatement();
				}
				}
				setState(2103);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2104);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockStatementContext extends ParserRuleContext {
		public LocalVariableDeclarationStatementContext localVariableDeclarationStatement() {
			return getRuleContext(LocalVariableDeclarationStatementContext.class,0);
		}
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public TypeDeclarationContext typeDeclaration() {
			return getRuleContext(TypeDeclarationContext.class,0);
		}
		public BlockStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blockStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterBlockStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitBlockStatement(this);
		}
	}

	public final BlockStatementContext blockStatement() throws RecognitionException {
		BlockStatementContext _localctx = new BlockStatementContext(_ctx, getState());
		enterRule(_localctx, 238, RULE_blockStatement);
		try {
			setState(2109);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,225,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2106);
				localVariableDeclarationStatement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2107);
				statement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2108);
				typeDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalVariableDeclarationStatementContext extends ParserRuleContext {
		public LocalVariableDeclarationContext localVariableDeclaration() {
			return getRuleContext(LocalVariableDeclarationContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public LocalVariableDeclarationStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localVariableDeclarationStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterLocalVariableDeclarationStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitLocalVariableDeclarationStatement(this);
		}
	}

	public final LocalVariableDeclarationStatementContext localVariableDeclarationStatement() throws RecognitionException {
		LocalVariableDeclarationStatementContext _localctx = new LocalVariableDeclarationStatementContext(_ctx, getState());
		enterRule(_localctx, 240, RULE_localVariableDeclarationStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2111);
			localVariableDeclaration();
			setState(2112);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LocalVariableDeclarationContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VariableDeclaratorsContext variableDeclarators() {
			return getRuleContext(VariableDeclaratorsContext.class,0);
		}
		public List<VariableModifierContext> variableModifier() {
			return getRuleContexts(VariableModifierContext.class);
		}
		public VariableModifierContext variableModifier(int i) {
			return getRuleContext(VariableModifierContext.class,i);
		}
		public LocalVariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_localVariableDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterLocalVariableDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitLocalVariableDeclaration(this);
		}
	}

	public final LocalVariableDeclarationContext localVariableDeclaration() throws RecognitionException {
		LocalVariableDeclarationContext _localctx = new LocalVariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 242, RULE_localVariableDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2117);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT || _la==FINAL) {
				{
				{
				setState(2114);
				variableModifier();
				}
				}
				setState(2119);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2120);
			type();
			setState(2121);
			variableDeclarators();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CatchTypeContext extends ParserRuleContext {
		public List<QualifiedNameContext> qualifiedName() {
			return getRuleContexts(QualifiedNameContext.class);
		}
		public QualifiedNameContext qualifiedName(int i) {
			return getRuleContext(QualifiedNameContext.class,i);
		}
		public List<TerminalNode> BITOR() { return getTokens(AspectJParser.BITOR); }
		public TerminalNode BITOR(int i) {
			return getToken(AspectJParser.BITOR, i);
		}
		public CatchTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_catchType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCatchType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCatchType(this);
		}
	}

	public final CatchTypeContext catchType() throws RecognitionException {
		CatchTypeContext _localctx = new CatchTypeContext(_ctx, getState());
		enterRule(_localctx, 244, RULE_catchType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2123);
			qualifiedName();
			setState(2128);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BITOR) {
				{
				{
				setState(2124);
				match(BITOR);
				setState(2125);
				qualifiedName();
				}
				}
				setState(2130);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FinallyBlockContext extends ParserRuleContext {
		public TerminalNode FINALLY() { return getToken(AspectJParser.FINALLY, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public FinallyBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_finallyBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterFinallyBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitFinallyBlock(this);
		}
	}

	public final FinallyBlockContext finallyBlock() throws RecognitionException {
		FinallyBlockContext _localctx = new FinallyBlockContext(_ctx, getState());
		enterRule(_localctx, 246, RULE_finallyBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2131);
			match(FINALLY);
			setState(2132);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ResourceSpecificationContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public ResourcesContext resources() {
			return getRuleContext(ResourcesContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public TerminalNode SEMI() { return getToken(AspectJParser.SEMI, 0); }
		public ResourceSpecificationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resourceSpecification; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterResourceSpecification(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitResourceSpecification(this);
		}
	}

	public final ResourceSpecificationContext resourceSpecification() throws RecognitionException {
		ResourceSpecificationContext _localctx = new ResourceSpecificationContext(_ctx, getState());
		enterRule(_localctx, 248, RULE_resourceSpecification);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2134);
			match(LPAREN);
			setState(2135);
			resources();
			setState(2137);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SEMI) {
				{
				setState(2136);
				match(SEMI);
				}
			}

			setState(2139);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ResourcesContext extends ParserRuleContext {
		public List<ResourceContext> resource() {
			return getRuleContexts(ResourceContext.class);
		}
		public ResourceContext resource(int i) {
			return getRuleContext(ResourceContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(AspectJParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(AspectJParser.SEMI, i);
		}
		public ResourcesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resources; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterResources(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitResources(this);
		}
	}

	public final ResourcesContext resources() throws RecognitionException {
		ResourcesContext _localctx = new ResourcesContext(_ctx, getState());
		enterRule(_localctx, 250, RULE_resources);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2141);
			resource();
			setState(2146);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,229,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(2142);
					match(SEMI);
					setState(2143);
					resource();
					}
					} 
				}
				setState(2148);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,229,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ResourceContext extends ParserRuleContext {
		public ClassOrInterfaceTypeContext classOrInterfaceType() {
			return getRuleContext(ClassOrInterfaceTypeContext.class,0);
		}
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public TerminalNode ASSIGN() { return getToken(AspectJParser.ASSIGN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<VariableModifierContext> variableModifier() {
			return getRuleContexts(VariableModifierContext.class);
		}
		public VariableModifierContext variableModifier(int i) {
			return getRuleContext(VariableModifierContext.class,i);
		}
		public ResourceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_resource; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterResource(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitResource(this);
		}
	}

	public final ResourceContext resource() throws RecognitionException {
		ResourceContext _localctx = new ResourceContext(_ctx, getState());
		enterRule(_localctx, 252, RULE_resource);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2152);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT || _la==FINAL) {
				{
				{
				setState(2149);
				variableModifier();
				}
				}
				setState(2154);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2155);
			classOrInterfaceType();
			setState(2156);
			variableDeclaratorId();
			setState(2157);
			match(ASSIGN);
			setState(2158);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchBlockStatementGroupContext extends ParserRuleContext {
		public List<SwitchLabelContext> switchLabel() {
			return getRuleContexts(SwitchLabelContext.class);
		}
		public SwitchLabelContext switchLabel(int i) {
			return getRuleContext(SwitchLabelContext.class,i);
		}
		public List<BlockStatementContext> blockStatement() {
			return getRuleContexts(BlockStatementContext.class);
		}
		public BlockStatementContext blockStatement(int i) {
			return getRuleContext(BlockStatementContext.class,i);
		}
		public SwitchBlockStatementGroupContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchBlockStatementGroup; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterSwitchBlockStatementGroup(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitSwitchBlockStatementGroup(this);
		}
	}

	public final SwitchBlockStatementGroupContext switchBlockStatementGroup() throws RecognitionException {
		SwitchBlockStatementGroupContext _localctx = new SwitchBlockStatementGroupContext(_ctx, getState());
		enterRule(_localctx, 254, RULE_switchBlockStatementGroup);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2161); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(2160);
				switchLabel();
				}
				}
				setState(2163); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CASE || _la==DEFAULT );
			setState(2166); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(2165);
				blockStatement();
				}
				}
				setState(2168); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << ABSTRACT) | (1L << ASSERT) | (1L << BOOLEAN) | (1L << BREAK) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (CLASS - 66)) | (1L << (CONTINUE - 66)) | (1L << (DO - 66)) | (1L << (DOUBLE - 66)) | (1L << (ENUM - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (FOR - 66)) | (1L << (IF - 66)) | (1L << (INT - 66)) | (1L << (INTERFACE - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (PRIVATE - 66)) | (1L << (PROTECTED - 66)) | (1L << (PUBLIC - 66)) | (1L << (RETURN - 66)) | (1L << (SHORT - 66)) | (1L << (STATIC - 66)) | (1L << (STRICTFP - 66)) | (1L << (SUPER - 66)) | (1L << (SWITCH - 66)) | (1L << (SYNCHRONIZED - 66)) | (1L << (THIS - 66)) | (1L << (THROW - 66)) | (1L << (TRY - 66)) | (1L << (VOID - 66)) | (1L << (WHILE - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LBRACE - 66)) | (1L << (SEMI - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchLabelContext extends ParserRuleContext {
		public TerminalNode CASE() { return getToken(AspectJParser.CASE, 0); }
		public ConstantExpressionContext constantExpression() {
			return getRuleContext(ConstantExpressionContext.class,0);
		}
		public TerminalNode COLON() { return getToken(AspectJParser.COLON, 0); }
		public EnumConstantNameContext enumConstantName() {
			return getRuleContext(EnumConstantNameContext.class,0);
		}
		public TerminalNode DEFAULT() { return getToken(AspectJParser.DEFAULT, 0); }
		public SwitchLabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchLabel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterSwitchLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitSwitchLabel(this);
		}
	}

	public final SwitchLabelContext switchLabel() throws RecognitionException {
		SwitchLabelContext _localctx = new SwitchLabelContext(_ctx, getState());
		enterRule(_localctx, 256, RULE_switchLabel);
		try {
			setState(2180);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,233,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2170);
				match(CASE);
				setState(2171);
				constantExpression();
				setState(2172);
				match(COLON);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2174);
				match(CASE);
				setState(2175);
				enumConstantName();
				setState(2176);
				match(COLON);
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(2178);
				match(DEFAULT);
				setState(2179);
				match(COLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForControlContext extends ParserRuleContext {
		public EnhancedForControlContext enhancedForControl() {
			return getRuleContext(EnhancedForControlContext.class,0);
		}
		public List<TerminalNode> SEMI() { return getTokens(AspectJParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(AspectJParser.SEMI, i);
		}
		public ForInitContext forInit() {
			return getRuleContext(ForInitContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForUpdateContext forUpdate() {
			return getRuleContext(ForUpdateContext.class,0);
		}
		public ForControlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forControl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterForControl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitForControl(this);
		}
	}

	public final ForControlContext forControl() throws RecognitionException {
		ForControlContext _localctx = new ForControlContext(_ctx, getState());
		enterRule(_localctx, 258, RULE_forControl);
		int _la;
		try {
			setState(2194);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,237,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2182);
				enhancedForControl();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2184);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << AT) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FINAL - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
					{
					setState(2183);
					forInit();
					}
				}

				setState(2186);
				match(SEMI);
				setState(2188);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
					{
					setState(2187);
					expression(0);
					}
				}

				setState(2190);
				match(SEMI);
				setState(2192);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
					{
					setState(2191);
					forUpdate();
					}
				}

				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForInitContext extends ParserRuleContext {
		public LocalVariableDeclarationContext localVariableDeclaration() {
			return getRuleContext(LocalVariableDeclarationContext.class,0);
		}
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public ForInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forInit; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterForInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitForInit(this);
		}
	}

	public final ForInitContext forInit() throws RecognitionException {
		ForInitContext _localctx = new ForInitContext(_ctx, getState());
		enterRule(_localctx, 260, RULE_forInit);
		try {
			setState(2198);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,238,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2196);
				localVariableDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2197);
				expressionList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EnhancedForControlContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VariableDeclaratorIdContext variableDeclaratorId() {
			return getRuleContext(VariableDeclaratorIdContext.class,0);
		}
		public TerminalNode COLON() { return getToken(AspectJParser.COLON, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public List<VariableModifierContext> variableModifier() {
			return getRuleContexts(VariableModifierContext.class);
		}
		public VariableModifierContext variableModifier(int i) {
			return getRuleContext(VariableModifierContext.class,i);
		}
		public EnhancedForControlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enhancedForControl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterEnhancedForControl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitEnhancedForControl(this);
		}
	}

	public final EnhancedForControlContext enhancedForControl() throws RecognitionException {
		EnhancedForControlContext _localctx = new EnhancedForControlContext(_ctx, getState());
		enterRule(_localctx, 262, RULE_enhancedForControl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2203);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AT || _la==FINAL) {
				{
				{
				setState(2200);
				variableModifier();
				}
				}
				setState(2205);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(2206);
			type();
			setState(2207);
			variableDeclaratorId();
			setState(2208);
			match(COLON);
			setState(2209);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForUpdateContext extends ParserRuleContext {
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public ForUpdateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forUpdate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterForUpdate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitForUpdate(this);
		}
	}

	public final ForUpdateContext forUpdate() throws RecognitionException {
		ForUpdateContext _localctx = new ForUpdateContext(_ctx, getState());
		enterRule(_localctx, 264, RULE_forUpdate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2211);
			expressionList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParExpressionContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public ParExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterParExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitParExpression(this);
		}
	}

	public final ParExpressionContext parExpression() throws RecognitionException {
		ParExpressionContext _localctx = new ParExpressionContext(_ctx, getState());
		enterRule(_localctx, 266, RULE_parExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2213);
			match(LPAREN);
			setState(2214);
			expression(0);
			setState(2215);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(AspectJParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(AspectJParser.COMMA, i);
		}
		public ExpressionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterExpressionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitExpressionList(this);
		}
	}

	public final ExpressionListContext expressionList() throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState());
		enterRule(_localctx, 268, RULE_expressionList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2217);
			expression(0);
			setState(2222);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(2218);
				match(COMMA);
				setState(2219);
				expression(0);
				}
				}
				setState(2224);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StatementExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statementExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterStatementExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitStatementExpression(this);
		}
	}

	public final StatementExpressionContext statementExpression() throws RecognitionException {
		StatementExpressionContext _localctx = new StatementExpressionContext(_ctx, getState());
		enterRule(_localctx, 270, RULE_statementExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2225);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConstantExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constantExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterConstantExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitConstantExpression(this);
		}
	}

	public final ConstantExpressionContext constantExpression() throws RecognitionException {
		ConstantExpressionContext _localctx = new ConstantExpressionContext(_ctx, getState());
		enterRule(_localctx, 272, RULE_constantExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2227);
			expression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreatorContext extends ParserRuleContext {
		public NonWildcardTypeArgumentsContext nonWildcardTypeArguments() {
			return getRuleContext(NonWildcardTypeArgumentsContext.class,0);
		}
		public CreatedNameContext createdName() {
			return getRuleContext(CreatedNameContext.class,0);
		}
		public ClassCreatorRestContext classCreatorRest() {
			return getRuleContext(ClassCreatorRestContext.class,0);
		}
		public ArrayCreatorRestContext arrayCreatorRest() {
			return getRuleContext(ArrayCreatorRestContext.class,0);
		}
		public CreatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_creator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterCreator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitCreator(this);
		}
	}

	public final CreatorContext creator() throws RecognitionException {
		CreatorContext _localctx = new CreatorContext(_ctx, getState());
		enterRule(_localctx, 274, RULE_creator);
		try {
			setState(2238);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case LT:
				enterOuterAlt(_localctx, 1);
				{
				setState(2229);
				nonWildcardTypeArguments();
				setState(2230);
				createdName();
				setState(2231);
				classCreatorRest();
				}
				break;
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case SHORT:
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(2233);
				createdName();
				setState(2236);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case LBRACK:
					{
					setState(2234);
					arrayCreatorRest();
					}
					break;
				case LPAREN:
					{
					setState(2235);
					classCreatorRest();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayCreatorRestContext extends ParserRuleContext {
		public List<TerminalNode> LBRACK() { return getTokens(AspectJParser.LBRACK); }
		public TerminalNode LBRACK(int i) {
			return getToken(AspectJParser.LBRACK, i);
		}
		public List<TerminalNode> RBRACK() { return getTokens(AspectJParser.RBRACK); }
		public TerminalNode RBRACK(int i) {
			return getToken(AspectJParser.RBRACK, i);
		}
		public ArrayInitializerContext arrayInitializer() {
			return getRuleContext(ArrayInitializerContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ArrayCreatorRestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayCreatorRest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterArrayCreatorRest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitArrayCreatorRest(this);
		}
	}

	public final ArrayCreatorRestContext arrayCreatorRest() throws RecognitionException {
		ArrayCreatorRestContext _localctx = new ArrayCreatorRestContext(_ctx, getState());
		enterRule(_localctx, 276, RULE_arrayCreatorRest);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(2240);
			match(LBRACK);
			setState(2268);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case RBRACK:
				{
				setState(2241);
				match(RBRACK);
				setState(2246);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==LBRACK) {
					{
					{
					setState(2242);
					match(LBRACK);
					setState(2243);
					match(RBRACK);
					}
					}
					setState(2248);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(2249);
				arrayInitializer();
				}
				break;
			case ARGS:
			case AFTER:
			case AROUND:
			case ASPECT:
			case BEFORE:
			case CALL:
			case CFLOW:
			case CFLOWBELOW:
			case DECLARE:
			case ERROR:
			case EXECUTION:
			case GET:
			case HANDLER:
			case INITIALIZATION:
			case ISSINGLETON:
			case PARENTS:
			case PERCFLOW:
			case PERCFLOWBELOW:
			case PERTARGET:
			case PERTHIS:
			case PERTYPEWITHIN:
			case POINTCUT:
			case PRECEDENCE:
			case PREINITIALIZATION:
			case PRIVILEGED:
			case RETURNING:
			case SET:
			case SOFT:
			case STATICINITIALIZATION:
			case TARGET:
			case THROWING:
			case WARNING:
			case WITHIN:
			case WITHINCODE:
			case BOOLEAN:
			case BYTE:
			case CHAR:
			case DOUBLE:
			case FLOAT:
			case INT:
			case LONG:
			case NEW:
			case SHORT:
			case SUPER:
			case THIS:
			case VOID:
			case IntegerLiteral:
			case FloatingPointLiteral:
			case BooleanLiteral:
			case CharacterLiteral:
			case StringLiteral:
			case NullLiteral:
			case LPAREN:
			case LT:
			case BANG:
			case TILDE:
			case INC:
			case DEC:
			case ADD:
			case SUB:
			case Identifier:
				{
				setState(2250);
				expression(0);
				setState(2251);
				match(RBRACK);
				setState(2258);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,244,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(2252);
						match(LBRACK);
						setState(2253);
						expression(0);
						setState(2254);
						match(RBRACK);
						}
						} 
					}
					setState(2260);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,244,_ctx);
				}
				setState(2265);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,245,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(2261);
						match(LBRACK);
						setState(2262);
						match(RBRACK);
						}
						} 
					}
					setState(2267);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,245,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassCreatorRestContext extends ParserRuleContext {
		public ArgumentsContext arguments() {
			return getRuleContext(ArgumentsContext.class,0);
		}
		public ClassBodyContext classBody() {
			return getRuleContext(ClassBodyContext.class,0);
		}
		public ClassCreatorRestContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classCreatorRest; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterClassCreatorRest(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitClassCreatorRest(this);
		}
	}

	public final ClassCreatorRestContext classCreatorRest() throws RecognitionException {
		ClassCreatorRestContext _localctx = new ClassCreatorRestContext(_ctx, getState());
		enterRule(_localctx, 278, RULE_classCreatorRest);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2270);
			arguments();
			setState(2272);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,247,_ctx) ) {
			case 1:
				{
				setState(2271);
				classBody();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExplicitGenericInvocationContext extends ParserRuleContext {
		public NonWildcardTypeArgumentsContext nonWildcardTypeArguments() {
			return getRuleContext(NonWildcardTypeArgumentsContext.class,0);
		}
		public ExplicitGenericInvocationSuffixContext explicitGenericInvocationSuffix() {
			return getRuleContext(ExplicitGenericInvocationSuffixContext.class,0);
		}
		public ExplicitGenericInvocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_explicitGenericInvocation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterExplicitGenericInvocation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitExplicitGenericInvocation(this);
		}
	}

	public final ExplicitGenericInvocationContext explicitGenericInvocation() throws RecognitionException {
		ExplicitGenericInvocationContext _localctx = new ExplicitGenericInvocationContext(_ctx, getState());
		enterRule(_localctx, 280, RULE_explicitGenericInvocation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2274);
			nonWildcardTypeArguments();
			setState(2275);
			explicitGenericInvocationSuffix();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NonWildcardTypeArgumentsContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(AspectJParser.LT, 0); }
		public TypeListContext typeList() {
			return getRuleContext(TypeListContext.class,0);
		}
		public TerminalNode GT() { return getToken(AspectJParser.GT, 0); }
		public NonWildcardTypeArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nonWildcardTypeArguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterNonWildcardTypeArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitNonWildcardTypeArguments(this);
		}
	}

	public final NonWildcardTypeArgumentsContext nonWildcardTypeArguments() throws RecognitionException {
		NonWildcardTypeArgumentsContext _localctx = new NonWildcardTypeArgumentsContext(_ctx, getState());
		enterRule(_localctx, 282, RULE_nonWildcardTypeArguments);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2277);
			match(LT);
			setState(2278);
			typeList();
			setState(2279);
			match(GT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeArgumentsOrDiamondContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(AspectJParser.LT, 0); }
		public TerminalNode GT() { return getToken(AspectJParser.GT, 0); }
		public TypeArgumentsContext typeArguments() {
			return getRuleContext(TypeArgumentsContext.class,0);
		}
		public TypeArgumentsOrDiamondContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeArgumentsOrDiamond; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterTypeArgumentsOrDiamond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitTypeArgumentsOrDiamond(this);
		}
	}

	public final TypeArgumentsOrDiamondContext typeArgumentsOrDiamond() throws RecognitionException {
		TypeArgumentsOrDiamondContext _localctx = new TypeArgumentsOrDiamondContext(_ctx, getState());
		enterRule(_localctx, 284, RULE_typeArgumentsOrDiamond);
		try {
			setState(2284);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,248,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2281);
				match(LT);
				setState(2282);
				match(GT);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2283);
				typeArguments();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NonWildcardTypeArgumentsOrDiamondContext extends ParserRuleContext {
		public TerminalNode LT() { return getToken(AspectJParser.LT, 0); }
		public TerminalNode GT() { return getToken(AspectJParser.GT, 0); }
		public NonWildcardTypeArgumentsContext nonWildcardTypeArguments() {
			return getRuleContext(NonWildcardTypeArgumentsContext.class,0);
		}
		public NonWildcardTypeArgumentsOrDiamondContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nonWildcardTypeArgumentsOrDiamond; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterNonWildcardTypeArgumentsOrDiamond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitNonWildcardTypeArgumentsOrDiamond(this);
		}
	}

	public final NonWildcardTypeArgumentsOrDiamondContext nonWildcardTypeArgumentsOrDiamond() throws RecognitionException {
		NonWildcardTypeArgumentsOrDiamondContext _localctx = new NonWildcardTypeArgumentsOrDiamondContext(_ctx, getState());
		enterRule(_localctx, 286, RULE_nonWildcardTypeArgumentsOrDiamond);
		try {
			setState(2289);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,249,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(2286);
				match(LT);
				setState(2287);
				match(GT);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(2288);
				nonWildcardTypeArguments();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgumentsContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(AspectJParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AspectJParser.RPAREN, 0); }
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public ArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).enterArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AspectJParserListener ) ((AspectJParserListener)listener).exitArguments(this);
		}
	}

	public final ArgumentsContext arguments() throws RecognitionException {
		ArgumentsContext _localctx = new ArgumentsContext(_ctx, getState());
		enterRule(_localctx, 288, RULE_arguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(2291);
			match(LPAREN);
			setState(2293);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ARGS) | (1L << AFTER) | (1L << AROUND) | (1L << ASPECT) | (1L << BEFORE) | (1L << CALL) | (1L << CFLOW) | (1L << CFLOWBELOW) | (1L << DECLARE) | (1L << ERROR) | (1L << EXECUTION) | (1L << GET) | (1L << HANDLER) | (1L << INITIALIZATION) | (1L << ISSINGLETON) | (1L << PARENTS) | (1L << PERCFLOW) | (1L << PERCFLOWBELOW) | (1L << PERTARGET) | (1L << PERTHIS) | (1L << PERTYPEWITHIN) | (1L << POINTCUT) | (1L << PRECEDENCE) | (1L << PREINITIALIZATION) | (1L << PRIVILEGED) | (1L << RETURNING) | (1L << SET) | (1L << SOFT) | (1L << STATICINITIALIZATION) | (1L << TARGET) | (1L << THROWING) | (1L << WARNING) | (1L << WITHIN) | (1L << WITHINCODE) | (1L << BOOLEAN) | (1L << BYTE))) != 0) || ((((_la - 66)) & ~0x3f) == 0 && ((1L << (_la - 66)) & ((1L << (CHAR - 66)) | (1L << (DOUBLE - 66)) | (1L << (FLOAT - 66)) | (1L << (INT - 66)) | (1L << (LONG - 66)) | (1L << (NEW - 66)) | (1L << (SHORT - 66)) | (1L << (SUPER - 66)) | (1L << (THIS - 66)) | (1L << (VOID - 66)) | (1L << (IntegerLiteral - 66)) | (1L << (FloatingPointLiteral - 66)) | (1L << (BooleanLiteral - 66)) | (1L << (CharacterLiteral - 66)) | (1L << (StringLiteral - 66)) | (1L << (NullLiteral - 66)) | (1L << (LPAREN - 66)) | (1L << (LT - 66)) | (1L << (BANG - 66)) | (1L << (TILDE - 66)))) != 0) || ((((_la - 137)) & ~0x3f) == 0 && ((1L << (_la - 137)) & ((1L << (INC - 137)) | (1L << (DEC - 137)) | (1L << (ADD - 137)) | (1L << (SUB - 137)) | (1L << (Identifier - 137)))) != 0)) {
				{
				setState(2292);
				expressionList();
				}
			}

			setState(2295);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 12:
			return pointcutExpression_sempred((PointcutExpressionContext)_localctx, predIndex);
		case 17:
			return typePattern_sempred((TypePatternContext)_localctx, predIndex);
		case 65:
			return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean pointcutExpression_sempred(PointcutExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 2);
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean typePattern_sempred(TypePatternContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 2);
		case 3:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 13);
		case 5:
			return precpred(_ctx, 12);
		case 6:
			return precpred(_ctx, 11);
		case 7:
			return precpred(_ctx, 10);
		case 8:
			return precpred(_ctx, 8);
		case 9:
			return precpred(_ctx, 7);
		case 10:
			return precpred(_ctx, 6);
		case 11:
			return precpred(_ctx, 5);
		case 12:
			return precpred(_ctx, 4);
		case 13:
			return precpred(_ctx, 3);
		case 14:
			return precpred(_ctx, 2);
		case 15:
			return precpred(_ctx, 1);
		case 16:
			return precpred(_ctx, 25);
		case 17:
			return precpred(_ctx, 24);
		case 18:
			return precpred(_ctx, 23);
		case 19:
			return precpred(_ctx, 22);
		case 20:
			return precpred(_ctx, 21);
		case 21:
			return precpred(_ctx, 20);
		case 22:
			return precpred(_ctx, 19);
		case 23:
			return precpred(_ctx, 16);
		case 24:
			return precpred(_ctx, 9);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\u00b3\u08fc\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\4[\t[\4\\\t\\\4]\t]\4^\t^\4_\t_\4"+
		"`\t`\4a\ta\4b\tb\4c\tc\4d\td\4e\te\4f\tf\4g\tg\4h\th\4i\ti\4j\tj\4k\t"+
		"k\4l\tl\4m\tm\4n\tn\4o\to\4p\tp\4q\tq\4r\tr\4s\ts\4t\tt\4u\tu\4v\tv\4"+
		"w\tw\4x\tx\4y\ty\4z\tz\4{\t{\4|\t|\4}\t}\4~\t~\4\177\t\177\4\u0080\t\u0080"+
		"\4\u0081\t\u0081\4\u0082\t\u0082\4\u0083\t\u0083\4\u0084\t\u0084\4\u0085"+
		"\t\u0085\4\u0086\t\u0086\4\u0087\t\u0087\4\u0088\t\u0088\4\u0089\t\u0089"+
		"\4\u008a\t\u008a\4\u008b\t\u008b\4\u008c\t\u008c\4\u008d\t\u008d\4\u008e"+
		"\t\u008e\4\u008f\t\u008f\4\u0090\t\u0090\4\u0091\t\u0091\4\u0092\t\u0092"+
		"\3\2\5\2\u0126\n\2\3\2\7\2\u0129\n\2\f\2\16\2\u012c\13\2\3\2\3\2\3\2\3"+
		"\2\5\2\u0132\n\2\3\2\3\2\5\2\u0136\n\2\3\2\5\2\u0139\n\2\3\2\3\2\3\3\3"+
		"\3\7\3\u013f\n\3\f\3\16\3\u0142\13\3\3\3\3\3\3\4\3\4\3\4\3\4\5\4\u014a"+
		"\n\4\3\5\3\5\5\5\u014e\n\5\3\5\3\5\7\5\u0152\n\5\f\5\16\5\u0155\13\5\3"+
		"\5\5\5\u0158\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u0164\n\6"+
		"\3\7\3\7\3\7\3\7\3\7\5\7\u016b\n\7\3\7\5\7\u016e\n\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7"+
		"\u01a8\n\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\u01f5\n\7\3\7\3\7\5\7\u01f9\n\7\3\b"+
		"\3\b\3\b\7\b\u01fe\n\b\f\b\16\b\u0201\13\b\3\b\3\b\3\b\3\t\3\t\3\t\7\t"+
		"\u0209\n\t\f\t\16\t\u020c\13\t\3\n\5\n\u020f\n\n\3\n\3\n\3\n\5\n\u0214"+
		"\n\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13"+
		"\u0223\n\13\3\13\5\13\u0226\n\13\3\13\3\13\3\13\3\13\3\13\5\13\u022d\n"+
		"\13\3\13\5\13\u0230\n\13\3\13\3\13\5\13\u0234\n\13\3\13\3\13\5\13\u0238"+
		"\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u0256\n\f\3\r\3\r"+
		"\7\r\u025a\n\r\f\r\16\r\u025d\13\r\3\r\3\r\3\r\3\r\3\r\3\r\7\r\u0265\n"+
		"\r\f\r\16\r\u0268\13\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u0271\n\r\3\16"+
		"\3\16\3\16\5\16\u0276\n\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u027e\n"+
		"\16\3\16\3\16\3\16\3\16\3\16\3\16\7\16\u0286\n\16\f\16\16\16\u0289\13"+
		"\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u02cd\n\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u0303\n\17\3\20"+
		"\3\20\3\20\5\20\u0308\n\20\3\20\3\20\3\20\3\21\7\21\u030e\n\21\f\21\16"+
		"\21\u0311\13\21\3\21\3\21\5\21\u0315\n\21\3\21\3\21\3\21\3\21\3\21\3\21"+
		"\5\21\u031d\n\21\3\21\3\21\3\21\7\21\u0322\n\21\f\21\16\21\u0325\13\21"+
		"\3\21\3\21\7\21\u0329\n\21\f\21\16\21\u032c\13\21\3\21\3\21\5\21\u0330"+
		"\n\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u0338\n\21\3\21\3\21\3\21\7\21"+
		"\u033d\n\21\f\21\16\21\u0340\13\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21"+
		"\u0348\n\21\3\21\3\21\3\21\7\21\u034d\n\21\f\21\16\21\u0350\13\21\3\21"+
		"\3\21\5\21\u0354\n\21\3\21\3\21\3\21\3\21\3\21\5\21\u035b\n\21\3\21\3"+
		"\21\5\21\u035f\n\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22"+
		"\3\22\3\22\5\22\u03b3\n\22\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u03bb\n"+
		"\23\3\23\3\23\3\23\5\23\u03c0\n\23\3\23\3\23\3\23\3\23\3\23\3\23\7\23"+
		"\u03c8\n\23\f\23\16\23\u03cb\13\23\3\24\3\24\5\24\u03cf\n\24\3\24\3\24"+
		"\7\24\u03d3\n\24\f\24\16\24\u03d6\13\24\3\25\3\25\3\25\3\25\3\25\6\25"+
		"\u03dd\n\25\r\25\16\25\u03de\3\25\5\25\u03e2\n\25\3\26\3\26\5\26\u03e6"+
		"\n\26\3\26\3\26\3\26\3\26\5\26\u03ec\n\26\3\26\5\26\u03ef\n\26\3\27\5"+
		"\27\u03f2\n\27\3\27\5\27\u03f5\n\27\3\27\3\27\3\27\3\27\5\27\u03fb\n\27"+
		"\3\27\3\27\3\30\5\30\u0400\n\30\3\30\3\30\7\30\u0404\n\30\f\30\16\30\u0407"+
		"\13\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33\7\33\u0410\n\33\f\33\16\33\u0413"+
		"\13\33\3\33\5\33\u0416\n\33\3\33\3\33\3\33\3\33\7\33\u041c\n\33\f\33\16"+
		"\33\u041f\13\33\3\33\5\33\u0422\n\33\5\33\u0424\n\33\3\34\3\34\5\34\u0428"+
		"\n\34\3\35\5\35\u042b\n\35\3\35\5\35\u042e\n\35\3\35\3\35\3\35\3\35\5"+
		"\35\u0434\n\35\3\35\3\35\3\35\5\35\u0439\n\35\3\36\5\36\u043c\n\36\3\36"+
		"\3\36\7\36\u0440\n\36\f\36\16\36\u0443\13\36\3\37\3\37\3 \3 \3 \7 \u044a"+
		"\n \f \16 \u044d\13 \3 \3 \3 \7 \u0452\n \f \16 \u0455\13 \3 \3 \3 \5"+
		" \u045a\n \3!\3!\3!\7!\u045f\n!\f!\16!\u0462\13!\3!\3!\3!\5!\u0467\n!"+
		"\3\"\3\"\3\"\3#\3#\3#\7#\u046f\n#\f#\16#\u0472\13#\3$\5$\u0475\n$\3$\5"+
		"$\u0478\n$\3$\3$\3$\5$\u047d\n$\3$\3$\3$\5$\u0482\n$\3%\5%\u0485\n%\3"+
		"%\3%\7%\u0489\n%\f%\16%\u048c\13%\3&\3&\3\'\5\'\u0491\n\'\3\'\3\'\3\'"+
		"\7\'\u0496\n\'\f\'\16\'\u0499\13\'\3(\3(\3(\3(\3(\5(\u04a0\n(\3)\3)\5"+
		")\u04a4\n)\3)\3)\3*\3*\5*\u04aa\n*\3+\3+\5+\u04ae\n+\3,\3,\3,\5,\u04b3"+
		"\n,\3,\3,\3,\7,\u04b8\n,\f,\16,\u04bb\13,\3,\3,\3,\7,\u04c0\n,\f,\16,"+
		"\u04c3\13,\5,\u04c5\n,\3-\3-\3-\7-\u04ca\n-\f-\16-\u04cd\13-\3-\3-\3-"+
		"\7-\u04d2\n-\f-\16-\u04d5\13-\5-\u04d7\n-\3.\3.\5.\u04db\n.\3/\3/\3/\7"+
		"/\u04e0\n/\f/\16/\u04e3\13/\3\60\3\60\5\60\u04e7\n\60\3\61\3\61\3\61\5"+
		"\61\u04ec\n\61\3\61\3\61\5\61\u04f0\n\61\3\61\3\61\5\61\u04f4\n\61\3\61"+
		"\3\61\3\62\3\62\3\62\5\62\u04fb\n\62\3\63\3\63\3\63\3\63\5\63\u0501\n"+
		"\63\3\63\3\63\5\63\u0505\n\63\3\63\5\63\u0508\n\63\3\63\5\63\u050b\n\63"+
		"\3\63\3\63\3\64\7\64\u0510\n\64\f\64\16\64\u0513\13\64\3\64\3\64\5\64"+
		"\u0517\n\64\3\64\5\64\u051a\n\64\3\65\3\65\3\65\5\65\u051f\n\65\3\65\3"+
		"\65\5\65\u0523\n\65\3\65\3\65\3\66\3\66\5\66\u0529\n\66\3\66\3\66\3\66"+
		"\3\66\7\66\u052f\n\66\f\66\16\66\u0532\13\66\3\66\3\66\5\66\u0536\n\66"+
		"\3\66\3\66\5\66\u053a\n\66\3\67\3\67\3\67\3\67\5\67\u0540\n\67\3\67\3"+
		"\67\38\38\38\78\u0547\n8\f8\168\u054a\138\38\38\38\39\39\59\u0551\n9\3"+
		"9\39\39\39\79\u0557\n9\f9\169\u055a\139\39\39\59\u055e\n9\39\39\3:\3:"+
		"\3:\7:\u0565\n:\f:\16:\u0568\13:\3;\3;\3<\3<\5<\u056e\n<\3<\3<\3<\5<\u0573"+
		"\n<\7<\u0575\n<\f<\16<\u0578\13<\3=\3=\3=\7=\u057d\n=\f=\16=\u0580\13"+
		"=\3>\3>\3>\3>\3?\3?\3?\3?\3?\3@\3@\3@\3@\5@\u058f\n@\3A\3A\3A\3A\3A\5"+
		"A\u0596\nA\3A\3A\3A\3A\3A\3A\3A\5A\u059f\nA\3A\3A\3A\3A\3A\3A\3A\3A\3"+
		"A\3A\3A\3A\3A\3A\3A\3A\3A\3A\3A\6A\u05b4\nA\rA\16A\u05b5\3A\5A\u05b9\n"+
		"A\3A\5A\u05bc\nA\3A\3A\3A\3A\7A\u05c2\nA\fA\16A\u05c5\13A\3A\5A\u05c8"+
		"\nA\3A\3A\3A\3A\7A\u05ce\nA\fA\16A\u05d1\13A\3A\7A\u05d4\nA\fA\16A\u05d7"+
		"\13A\3A\3A\3A\3A\3A\3A\3A\3A\5A\u05e1\nA\3A\3A\3A\3A\3A\3A\3A\5A\u05ea"+
		"\nA\3A\3A\3A\5A\u05ef\nA\3A\3A\3A\3A\3A\3A\3A\3A\3A\5A\u05fa\nA\3B\3B"+
		"\3B\7B\u05ff\nB\fB\16B\u0602\13B\3B\3B\3B\3B\3B\3C\3C\3C\3C\3C\3C\3C\3"+
		"C\3C\3C\3C\3C\3C\5C\u0616\nC\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3"+
		"C\5C\u0626\nC\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3"+
		"C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\5"+
		"C\u0651\nC\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\3C\5C\u0663\n"+
		"C\3C\3C\3C\3C\3C\3C\7C\u066b\nC\fC\16C\u066e\13C\3D\3D\3D\3D\3D\3D\3D"+
		"\3D\3D\3D\3D\3D\3D\3D\3D\3D\3D\3D\3D\5D\u0683\nD\5D\u0685\nD\3E\3E\5E"+
		"\u0689\nE\3E\3E\3E\5E\u068e\nE\7E\u0690\nE\fE\16E\u0693\13E\3E\5E\u0696"+
		"\nE\3F\3F\5F\u069a\nF\3F\3F\3G\3G\3G\3G\5G\u06a2\nG\5G\u06a4\nG\3H\3H"+
		"\3H\3H\3H\5H\u06ab\nH\3I\5I\u06ae\nI\3I\7I\u06b1\nI\fI\16I\u06b4\13I\3"+
		"I\7I\u06b7\nI\fI\16I\u06ba\13I\3I\3I\3J\7J\u06bf\nJ\fJ\16J\u06c2\13J\3"+
		"J\3J\3J\3J\3K\3K\5K\u06ca\nK\3K\3K\3K\5K\u06cf\nK\3K\3K\3L\7L\u06d4\n"+
		"L\fL\16L\u06d7\13L\3L\3L\7L\u06db\nL\fL\16L\u06de\13L\3L\3L\7L\u06e2\n"+
		"L\fL\16L\u06e5\13L\3L\3L\7L\u06e9\nL\fL\16L\u06ec\13L\3L\3L\5L\u06f0\n"+
		"L\3M\3M\5M\u06f4\nM\3N\3N\5N\u06f8\nN\3O\3O\5O\u06fc\nO\3P\3P\3P\3P\7"+
		"P\u0702\nP\fP\16P\u0705\13P\3P\3P\3Q\3Q\3Q\7Q\u070c\nQ\fQ\16Q\u070f\13"+
		"Q\3R\3R\3R\7R\u0714\nR\fR\16R\u0717\13R\3S\3S\7S\u071b\nS\fS\16S\u071e"+
		"\13S\3T\3T\3T\7T\u0723\nT\fT\16T\u0726\13T\3U\3U\7U\u072a\nU\fU\16U\u072d"+
		"\13U\3U\3U\3V\3V\7V\u0733\nV\fV\16V\u0736\13V\3V\3V\3W\3W\3W\3X\3X\3X"+
		"\3Y\3Y\3Y\3Y\3Z\7Z\u0745\nZ\fZ\16Z\u0748\13Z\3Z\3Z\5Z\u074c\nZ\3[\3[\3"+
		"[\3[\3[\3[\3[\5[\u0755\n[\3\\\3\\\3\\\3\\\7\\\u075b\n\\\f\\\16\\\u075e"+
		"\13\\\3\\\3\\\3]\3]\3]\3^\3^\3^\7^\u0768\n^\f^\16^\u076b\13^\3_\3_\3_"+
		"\5_\u0770\n_\3`\3`\5`\u0774\n`\3a\3a\3a\3a\7a\u077a\na\fa\16a\u077d\13"+
		"a\3a\5a\u0780\na\5a\u0782\na\3a\3a\3b\3b\3b\7b\u0789\nb\fb\16b\u078c\13"+
		"b\3b\3b\3b\7b\u0791\nb\fb\16b\u0794\13b\5b\u0796\nb\3c\3c\3d\3d\3d\3d"+
		"\7d\u079e\nd\fd\16d\u07a1\13d\3d\3d\3e\3e\3e\3e\5e\u07a9\ne\5e\u07ab\n"+
		"e\3f\3f\3f\7f\u07b0\nf\ff\16f\u07b3\13f\3g\3g\5g\u07b7\ng\3g\3g\3h\3h"+
		"\3h\7h\u07be\nh\fh\16h\u07c1\13h\3h\3h\5h\u07c5\nh\3h\5h\u07c8\nh\3i\7"+
		"i\u07cb\ni\fi\16i\u07ce\13i\3i\3i\3i\3j\7j\u07d4\nj\fj\16j\u07d7\13j\3"+
		"j\3j\3j\3j\3k\3k\3l\3l\3m\3m\3n\3n\3o\3o\3o\7o\u07e8\no\fo\16o\u07eb\13"+
		"o\3p\3p\3p\5p\u07f0\np\3q\3q\3q\3q\7q\u07f6\nq\fq\16q\u07f9\13q\5q\u07fb"+
		"\nq\3q\5q\u07fe\nq\3q\3q\3r\3r\7r\u0804\nr\fr\16r\u0807\13r\3r\3r\3s\7"+
		"s\u080c\ns\fs\16s\u080f\13s\3s\3s\5s\u0813\ns\3t\3t\3t\3t\3t\3t\5t\u081b"+
		"\nt\3t\3t\5t\u081f\nt\3t\3t\5t\u0823\nt\3t\3t\5t\u0827\nt\5t\u0829\nt"+
		"\3u\3u\5u\u082d\nu\3v\3v\3w\3w\3w\3x\3x\7x\u0836\nx\fx\16x\u0839\13x\3"+
		"x\3x\3y\3y\3y\5y\u0840\ny\3z\3z\3z\3{\7{\u0846\n{\f{\16{\u0849\13{\3{"+
		"\3{\3{\3|\3|\3|\7|\u0851\n|\f|\16|\u0854\13|\3}\3}\3}\3~\3~\3~\5~\u085c"+
		"\n~\3~\3~\3\177\3\177\3\177\7\177\u0863\n\177\f\177\16\177\u0866\13\177"+
		"\3\u0080\7\u0080\u0869\n\u0080\f\u0080\16\u0080\u086c\13\u0080\3\u0080"+
		"\3\u0080\3\u0080\3\u0080\3\u0080\3\u0081\6\u0081\u0874\n\u0081\r\u0081"+
		"\16\u0081\u0875\3\u0081\6\u0081\u0879\n\u0081\r\u0081\16\u0081\u087a\3"+
		"\u0082\3\u0082\3\u0082\3\u0082\3\u0082\3\u0082\3\u0082\3\u0082\3\u0082"+
		"\3\u0082\5\u0082\u0887\n\u0082\3\u0083\3\u0083\5\u0083\u088b\n\u0083\3"+
		"\u0083\3\u0083\5\u0083\u088f\n\u0083\3\u0083\3\u0083\5\u0083\u0893\n\u0083"+
		"\5\u0083\u0895\n\u0083\3\u0084\3\u0084\5\u0084\u0899\n\u0084\3\u0085\7"+
		"\u0085\u089c\n\u0085\f\u0085\16\u0085\u089f\13\u0085\3\u0085\3\u0085\3"+
		"\u0085\3\u0085\3\u0085\3\u0086\3\u0086\3\u0087\3\u0087\3\u0087\3\u0087"+
		"\3\u0088\3\u0088\3\u0088\7\u0088\u08af\n\u0088\f\u0088\16\u0088\u08b2"+
		"\13\u0088\3\u0089\3\u0089\3\u008a\3\u008a\3\u008b\3\u008b\3\u008b\3\u008b"+
		"\3\u008b\3\u008b\3\u008b\5\u008b\u08bf\n\u008b\5\u008b\u08c1\n\u008b\3"+
		"\u008c\3\u008c\3\u008c\3\u008c\7\u008c\u08c7\n\u008c\f\u008c\16\u008c"+
		"\u08ca\13\u008c\3\u008c\3\u008c\3\u008c\3\u008c\3\u008c\3\u008c\3\u008c"+
		"\7\u008c\u08d3\n\u008c\f\u008c\16\u008c\u08d6\13\u008c\3\u008c\3\u008c"+
		"\7\u008c\u08da\n\u008c\f\u008c\16\u008c\u08dd\13\u008c\5\u008c\u08df\n"+
		"\u008c\3\u008d\3\u008d\5\u008d\u08e3\n\u008d\3\u008e\3\u008e\3\u008e\3"+
		"\u008f\3\u008f\3\u008f\3\u008f\3\u0090\3\u0090\3\u0090\5\u0090\u08ef\n"+
		"\u0090\3\u0091\3\u0091\3\u0091\5\u0091\u08f4\n\u0091\3\u0092\3\u0092\5"+
		"\u0092\u08f8\n\u0092\3\u0092\3\u0092\3\u0092\2\5\32$\u0084\u0093\2\4\6"+
		"\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRT"+
		"VXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a\u008c\u008e"+
		"\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6"+
		"\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\u00b8\u00ba\u00bc\u00be"+
		"\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc\u00ce\u00d0\u00d2\u00d4\u00d6"+
		"\u00d8\u00da\u00dc\u00de\u00e0\u00e2\u00e4\u00e6\u00e8\u00ea\u00ec\u00ee"+
		"\u00f0\u00f2\u00f4\u00f6\u00f8\u00fa\u00fc\u00fe\u0100\u0102\u0104\u0106"+
		"\u0108\u010a\u010c\u010e\u0110\u0112\u0114\u0116\u0118\u011a\u011c\u011e"+
		"\u0120\u0122\2\25\6\2NN]_bbjj\4\2\3\3}}\6\2NN]_bbff\3\2]_\4\2\3\3\u008f"+
		"\u008f\3\2\7(\3\2\u008b\u008e\3\2\u0081\u0082\4\2\u008f\u0090\u0094\u0094"+
		"\3\2\u008d\u008e\4\2\177\u0080\u0086\u0087\4\2\u0085\u0085\u0088\u0088"+
		"\4\2~~\u0095\u009f\3\2\u008b\u008c\6\2ZZffjjmm\6\2==NN]_bc\n\2??AADDJ"+
		"JPPWWYYaa\4\2MMdd\3\2ot\2\u09e5\2\u0125\3\2\2\2\4\u013c\3\2\2\2\6\u0149"+
		"\3\2\2\2\b\u0157\3\2\2\2\n\u0163\3\2\2\2\f\u01f8\3\2\2\2\16\u01fa\3\2"+
		"\2\2\20\u0205\3\2\2\2\22\u020e\3\2\2\2\24\u0237\3\2\2\2\26\u0255\3\2\2"+
		"\2\30\u0270\3\2\2\2\32\u027d\3\2\2\2\34\u0302\3\2\2\2\36\u0307\3\2\2\2"+
		" \u035e\3\2\2\2\"\u03b2\3\2\2\2$\u03bf\3\2\2\2&\u03cc\3\2\2\2(\u03e1\3"+
		"\2\2\2*\u03ee\3\2\2\2,\u03f1\3\2\2\2.\u03ff\3\2\2\2\60\u0408\3\2\2\2\62"+
		"\u040a\3\2\2\2\64\u0423\3\2\2\2\66\u0427\3\2\2\28\u042a\3\2\2\2:\u043b"+
		"\3\2\2\2<\u0444\3\2\2\2>\u0459\3\2\2\2@\u0466\3\2\2\2B\u0468\3\2\2\2D"+
		"\u046b\3\2\2\2F\u0474\3\2\2\2H\u0484\3\2\2\2J\u048d\3\2\2\2L\u0490\3\2"+
		"\2\2N\u049f\3\2\2\2P\u04a1\3\2\2\2R\u04a9\3\2\2\2T\u04ad\3\2\2\2V\u04c4"+
		"\3\2\2\2X\u04d6\3\2\2\2Z\u04da\3\2\2\2\\\u04dc\3\2\2\2^\u04e6\3\2\2\2"+
		"`\u04e8\3\2\2\2b\u04f7\3\2\2\2d\u04fc\3\2\2\2f\u0511\3\2\2\2h\u051b\3"+
		"\2\2\2j\u0528\3\2\2\2l\u053b\3\2\2\2n\u0543\3\2\2\2p\u0550\3\2\2\2r\u0561"+
		"\3\2\2\2t\u0569\3\2\2\2v\u056b\3\2\2\2x\u0579\3\2\2\2z\u0581\3\2\2\2|"+
		"\u0585\3\2\2\2~\u058a\3\2\2\2\u0080\u05f9\3\2\2\2\u0082\u05fb\3\2\2\2"+
		"\u0084\u0615\3\2\2\2\u0086\u0684\3\2\2\2\u0088\u0695\3\2\2\2\u008a\u0697"+
		"\3\2\2\2\u008c\u06a3\3\2\2\2\u008e\u06aa\3\2\2\2\u0090\u06ad\3\2\2\2\u0092"+
		"\u06c0\3\2\2\2\u0094\u06c7\3\2\2\2\u0096\u06ef\3\2\2\2\u0098\u06f3\3\2"+
		"\2\2\u009a\u06f7\3\2\2\2\u009c\u06fb\3\2\2\2\u009e\u06fd\3\2\2\2\u00a0"+
		"\u0708\3\2\2\2\u00a2\u0710\3\2\2\2\u00a4\u0718\3\2\2\2\u00a6\u071f\3\2"+
		"\2\2\u00a8\u0727\3\2\2\2\u00aa\u0730\3\2\2\2\u00ac\u0739\3\2\2\2\u00ae"+
		"\u073c\3\2\2\2\u00b0\u073f\3\2\2\2\u00b2\u074b\3\2\2\2\u00b4\u0754\3\2"+
		"\2\2\u00b6\u0756\3\2\2\2\u00b8\u0761\3\2\2\2\u00ba\u0764\3\2\2\2\u00bc"+
		"\u076c\3\2\2\2\u00be\u0773\3\2\2\2\u00c0\u0775\3\2\2\2\u00c2\u0795\3\2"+
		"\2\2\u00c4\u0797\3\2\2\2\u00c6\u0799\3\2\2\2\u00c8\u07aa\3\2\2\2\u00ca"+
		"\u07ac\3\2\2\2\u00cc\u07b4\3\2\2\2\u00ce\u07c7\3\2\2\2\u00d0\u07cc\3\2"+
		"\2\2\u00d2\u07d5\3\2\2\2\u00d4\u07dc\3\2\2\2\u00d6\u07de\3\2\2\2\u00d8"+
		"\u07e0\3\2\2\2\u00da\u07e2\3\2\2\2\u00dc\u07e4\3\2\2\2\u00de\u07ef\3\2"+
		"\2\2\u00e0\u07f1\3\2\2\2\u00e2\u0801\3\2\2\2\u00e4\u0812\3\2\2\2\u00e6"+
		"\u0828\3\2\2\2\u00e8\u082c\3\2\2\2\u00ea\u082e\3\2\2\2\u00ec\u0830\3\2"+
		"\2\2\u00ee\u0833\3\2\2\2\u00f0\u083f\3\2\2\2\u00f2\u0841\3\2\2\2\u00f4"+
		"\u0847\3\2\2\2\u00f6\u084d\3\2\2\2\u00f8\u0855\3\2\2\2\u00fa\u0858\3\2"+
		"\2\2\u00fc\u085f\3\2\2\2\u00fe\u086a\3\2\2\2\u0100\u0873\3\2\2\2\u0102"+
		"\u0886\3\2\2\2\u0104\u0894\3\2\2\2\u0106\u0898\3\2\2\2\u0108\u089d\3\2"+
		"\2\2\u010a\u08a5\3\2\2\2\u010c\u08a7\3\2\2\2\u010e\u08ab\3\2\2\2\u0110"+
		"\u08b3\3\2\2\2\u0112\u08b5\3\2\2\2\u0114\u08c0\3\2\2\2\u0116\u08c2\3\2"+
		"\2\2\u0118\u08e0\3\2\2\2\u011a\u08e4\3\2\2\2\u011c\u08e7\3\2\2\2\u011e"+
		"\u08ee\3\2\2\2\u0120\u08f3\3\2\2\2\u0122\u08f5\3\2\2\2\u0124\u0126\7\37"+
		"\2\2\u0125\u0124\3\2\2\2\u0125\u0126\3\2\2\2\u0126\u012a\3\2\2\2\u0127"+
		"\u0129\5\u0098M\2\u0128\u0127\3\2\2\2\u0129\u012c\3\2\2\2\u012a\u0128"+
		"\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u012d\3\2\2\2\u012c\u012a\3\2\2\2\u012d"+
		"\u012e\7\n\2\2\u012e\u0131\5^\60\2\u012f\u0130\7M\2\2\u0130\u0132\5\u00c2"+
		"b\2\u0131\u012f\3\2\2\2\u0131\u0132\3\2\2\2\u0132\u0135\3\2\2\2\u0133"+
		"\u0134\7T\2\2\u0134\u0136\5\u00a6T\2\u0135\u0133\3\2\2\2\u0135\u0136\3"+
		"\2\2\2\u0136\u0138\3\2\2\2\u0137\u0139\5\26\f\2\u0138\u0137\3\2\2\2\u0138"+
		"\u0139\3\2\2\2\u0139\u013a\3\2\2\2\u013a\u013b\5\4\3\2\u013b\3\3\2\2\2"+
		"\u013c\u0140\7w\2\2\u013d\u013f\5\6\4\2\u013e\u013d\3\2\2\2\u013f\u0142"+
		"\3\2\2\2\u0140\u013e\3\2\2\2\u0140\u0141\3\2\2\2\u0141\u0143\3\2\2\2\u0142"+
		"\u0140\3\2\2\2\u0143\u0144\7x\2\2\u0144\5\3\2\2\2\u0145\u014a\5\b\5\2"+
		"\u0146\u014a\5\22\n\2\u0147\u014a\5 \21\2\u0148\u014a\5\"\22\2\u0149\u0145"+
		"\3\2\2\2\u0149\u0146\3\2\2\2\u0149\u0147\3\2\2\2\u0149\u0148\3\2\2\2\u014a"+
		"\7\3\2\2\2\u014b\u0158\7{\2\2\u014c\u014e\7b\2\2\u014d\u014c\3\2\2\2\u014d"+
		"\u014e\3\2\2\2\u014e\u014f\3\2\2\2\u014f\u0158\5\u00eex\2\u0150\u0152"+
		"\5\u0098M\2\u0151\u0150\3\2\2\2\u0152\u0155\3\2\2\2\u0153\u0151\3\2\2"+
		"\2\u0153\u0154\3\2\2\2\u0154\u0156\3\2\2\2\u0155\u0153\3\2\2\2\u0156\u0158"+
		"\5\n\6\2\u0157\u014b\3\2\2\2\u0157\u014d\3\2\2\2\u0157\u0153\3\2\2\2\u0158"+
		"\t\3\2\2\2\u0159\u0164\5j\66\2\u015a\u0164\5\u00acW\2\u015b\u0164\5\u00b0"+
		"Y\2\u015c\u0164\5l\67\2\u015d\u0164\5\u00aeX\2\u015e\u0164\5h\65\2\u015f"+
		"\u0164\5|?\2\u0160\u0164\5`\61\2\u0161\u0164\5d\63\2\u0162\u0164\5\30"+
		"\r\2\u0163\u0159\3\2\2\2\u0163\u015a\3\2\2\2\u0163\u015b\3\2\2\2\u0163"+
		"\u015c\3\2\2\2\u0163\u015d\3\2\2\2\u0163\u015e\3\2\2\2\u0163\u015f\3\2"+
		"\2\2\u0163\u0160\3\2\2\2\u0163\u0161\3\2\2\2\u0163\u0162\3\2\2\2\u0164"+
		"\13\3\2\2\2\u0165\u0166\7<\2\2\u0166\u016d\5\u00dan\2\u0167\u016a\7u\2"+
		"\2\u0168\u016b\5\u00dco\2\u0169\u016b\5\u00dep\2\u016a\u0168\3\2\2\2\u016a"+
		"\u0169\3\2\2\2\u016a\u016b\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u016e\7v"+
		"\2\2\u016d\u0167\3\2\2\2\u016d\u016e\3\2\2\2\u016e\u01f9\3\2\2\2\u016f"+
		"\u0170\7<\2\2\u0170\u0171\7)\2\2\u0171\u0172\7u\2\2\u0172\u0173\7\4\2"+
		"\2\u0173\u0174\5\32\16\2\u0174\u0175\7\4\2\2\u0175\u0176\7v\2\2\u0176"+
		"\u01f9\3\2\2\2\u0177\u0178\7<\2\2\u0178\u0179\7*\2\2\u0179\u017a\7u\2"+
		"\2\u017a\u017b\7\4\2\2\u017b\u017c\5\32\16\2\u017c\u017d\7\4\2\2\u017d"+
		"\u017e\7v\2\2\u017e\u01f9\3\2\2\2\u017f\u0180\7<\2\2\u0180\u0181\7*\2"+
		"\2\u0181\u0182\7u\2\2\u0182\u0183\7\34\2\2\u0183\u0184\7~\2\2\u0184\u0185"+
		"\7\4\2\2\u0185\u0186\5\32\16\2\u0186\u0187\7\4\2\2\u0187\u0188\7|\2\2"+
		"\u0188\u0189\7 \2\2\u0189\u018a\7~\2\2\u018a\u018b\7\4\2\2\u018b\u018c"+
		"\5^\60\2\u018c\u018d\7\4\2\2\u018d\u018e\7v\2\2\u018e\u01f9\3\2\2\2\u018f"+
		"\u0190\7<\2\2\u0190\u0191\7+\2\2\u0191\u0192\7u\2\2\u0192\u0193\7\4\2"+
		"\2\u0193\u0194\5\32\16\2\u0194\u0195\7\4\2\2\u0195\u0196\7v\2\2\u0196"+
		"\u01f9\3\2\2\2\u0197\u0198\7<\2\2\u0198\u0199\7,\2\2\u0199\u019a\7u\2"+
		"\2\u019a\u019b\7\4\2\2\u019b\u019c\5\32\16\2\u019c\u019d\7\4\2\2\u019d"+
		"\u019e\7v\2\2\u019e\u01f9\3\2\2\2\u019f\u01a0\7<\2\2\u01a0\u01a7\7-\2"+
		"\2\u01a1\u01a2\7u\2\2\u01a2\u01a3\7\4\2\2\u01a3\u01a4\5\26\f\2\u01a4\u01a5"+
		"\7\4\2\2\u01a5\u01a6\7v\2\2\u01a6\u01a8\3\2\2\2\u01a7\u01a1\3\2\2\2\u01a7"+
		"\u01a8\3\2\2\2\u01a8\u01f9\3\2\2\2\u01a9\u01aa\7<\2\2\u01aa\u01ab\7.\2"+
		"\2\u01ab\u01ac\7u\2\2\u01ac\u01ad\7\4\2\2\u01ad\u01ae\5\32\16\2\u01ae"+
		"\u01af\7\4\2\2\u01af\u01b0\7v\2\2\u01b0\u01f9\3\2\2\2\u01b1\u01b2\7<\2"+
		"\2\u01b2\u01b3\7\62\2\2\u01b3\u01b4\7u\2\2\u01b4\u01b5\7\4\2\2\u01b5\u01b6"+
		"\5\32\16\2\u01b6\u01b7\7\4\2\2\u01b7\u01b8\7v\2\2\u01b8\u01f9\3\2\2\2"+
		"\u01b9\u01ba\7<\2\2\u01ba\u01bb\7\60\2\2\u01bb\u01bc\7u\2\2\u01bc\u01bd"+
		"\7;\2\2\u01bd\u01be\7~\2\2\u01be\u01bf\7\4\2\2\u01bf\u01c0\5$\23\2\u01c0"+
		"\u01c1\7\4\2\2\u01c1\u01c2\7|\2\2\u01c2\u01c3\78\2\2\u01c3\u01c4\7~\2"+
		"\2\u01c4\u01c5\7w\2\2\u01c5\u01c6\5\20\t\2\u01c6\u01c7\7x\2\2\u01c7\u01c8"+
		"\7v\2\2\u01c8\u01f9\3\2\2\2\u01c9\u01ca\7<\2\2\u01ca\u01cb\7/\2\2\u01cb"+
		"\u01cc\7u\2\2\u01cc\u01cd\7\4\2\2\u01cd\u01ce\5$\23\2\u01ce\u01cf\7\4"+
		"\2\2\u01cf\u01d0\7v\2\2\u01d0\u01f9\3\2\2\2\u01d1\u01d2\7<\2\2\u01d2\u01d3"+
		"\7/\2\2\u01d3\u01d4\7u\2\2\u01d4\u01d5\7;\2\2\u01d5\u01d6\7~\2\2\u01d6"+
		"\u01d7\7\4\2\2\u01d7\u01d8\5$\23\2\u01d8\u01d9\7\4\2\2\u01d9\u01da\7|"+
		"\2\2\u01da\u01db\7\66\2\2\u01db\u01dc\7~\2\2\u01dc\u01dd\5\16\b\2\u01dd"+
		"\u01de\7v\2\2\u01de\u01f9\3\2\2\2\u01df\u01e0\7<\2\2\u01e0\u01e1\7\63"+
		"\2\2\u01e1\u01e2\7u\2\2\u01e2\u01e3\7\4\2\2\u01e3\u01e4\5D#\2\u01e4\u01e5"+
		"\7\4\2\2\u01e5\u01e6\7v\2\2\u01e6\u01f9\3\2\2\2\u01e7\u01e8\7<\2\2\u01e8"+
		"\u01e9\7\61\2\2\u01e9\u01ea\7u\2\2\u01ea\u01eb\7\4\2\2\u01eb\u01ec\5\32"+
		"\16\2\u01ec\u01ed\7\4\2\2\u01ed\u01ee\7v\2\2\u01ee\u01f9\3\2\2\2\u01ef"+
		"\u01f0\7<\2\2\u01f0\u01f1\7\64\2\2\u01f1\u01f2\7u\2\2\u01f2\u01f4\7\4"+
		"\2\2\u01f3\u01f5\5\32\16\2\u01f4\u01f3\3\2\2\2\u01f4\u01f5\3\2\2\2\u01f5"+
		"\u01f6\3\2\2\2\u01f6\u01f7\7\4\2\2\u01f7\u01f9\7v\2\2\u01f8\u0165\3\2"+
		"\2\2\u01f8\u016f\3\2\2\2\u01f8\u0177\3\2\2\2\u01f8\u017f\3\2\2\2\u01f8"+
		"\u018f\3\2\2\2\u01f8\u0197\3\2\2\2\u01f8\u019f\3\2\2\2\u01f8\u01a9\3\2"+
		"\2\2\u01f8\u01b1\3\2\2\2\u01f8\u01b9\3\2\2\2\u01f8\u01c9\3\2\2\2\u01f8"+
		"\u01d1\3\2\2\2\u01f8\u01df\3\2\2\2\u01f8\u01e7\3\2\2\2\u01f8\u01ef\3\2"+
		"\2\2\u01f9\r\3\2\2\2\u01fa\u01ff\5^\60\2\u01fb\u01fc\7}\2\2\u01fc\u01fe"+
		"\5^\60\2\u01fd\u01fb\3\2\2\2\u01fe\u0201\3\2\2\2\u01ff\u01fd\3\2\2\2\u01ff"+
		"\u0200\3\2\2\2\u0200\u0202\3\2\2\2\u0201\u01ff\3\2\2\2\u0202\u0203\7}"+
		"\2\2\u0203\u0204\7E\2\2\u0204\17\3\2\2\2\u0205\u020a\5\16\b\2\u0206\u0207"+
		"\7|\2\2\u0207\u0209\5\16\b\2\u0208\u0206\3\2\2\2\u0209\u020c\3\2\2\2\u020a"+
		"\u0208\3\2\2\2\u020a\u020b\3\2\2\2\u020b\21\3\2\2\2\u020c\u020a\3\2\2"+
		"\2\u020d\u020f\7c\2\2\u020e\u020d\3\2\2\2\u020e\u020f\3\2\2\2\u020f\u0210"+
		"\3\2\2\2\u0210\u0213\5\24\13\2\u0211\u0212\7i\2\2\u0212\u0214\5\u00a6"+
		"T\2\u0213\u0211\3\2\2\2\u0213\u0214\3\2\2\2\u0214\u0215\3\2\2\2\u0215"+
		"\u0216\7\u0084\2\2\u0216\u0217\5\32\16\2\u0217\u0218\5\u00d4k\2\u0218"+
		"\23\3\2\2\2\u0219\u021a\7\13\2\2\u021a\u0238\5\u00ccg\2\u021b\u021c\7"+
		"\b\2\2\u021c\u0238\5\u00ccg\2\u021d\u021e\7\b\2\2\u021e\u021f\5\u00cc"+
		"g\2\u021f\u0225\7 \2\2\u0220\u0222\7u\2\2\u0221\u0223\5\u00d0i\2\u0222"+
		"\u0221\3\2\2\2\u0222\u0223\3\2\2\2\u0223\u0224\3\2\2\2\u0224\u0226\7v"+
		"\2\2\u0225\u0220\3\2\2\2\u0225\u0226\3\2\2\2\u0226\u0238\3\2\2\2\u0227"+
		"\u0228\7\b\2\2\u0228\u0229\5\u00ccg\2\u0229\u022f\7%\2\2\u022a\u022c\7"+
		"u\2\2\u022b\u022d\5\u00d0i\2\u022c\u022b\3\2\2\2\u022c\u022d\3\2\2\2\u022d"+
		"\u022e\3\2\2\2\u022e\u0230\7v\2\2\u022f\u022a\3\2\2\2\u022f\u0230\3\2"+
		"\2\2\u0230\u0238\3\2\2\2\u0231\u0234\5\u00c2b\2\u0232\u0234\7l\2\2\u0233"+
		"\u0231\3\2\2\2\u0233\u0232\3\2\2\2\u0234\u0235\3\2\2\2\u0235\u0236\7\t"+
		"\2\2\u0236\u0238\5\u00ccg\2\u0237\u0219\3\2\2\2\u0237\u021b\3\2\2\2\u0237"+
		"\u021d\3\2\2\2\u0237\u0227\3\2\2\2\u0237\u0233\3\2\2\2\u0238\25\3\2\2"+
		"\2\u0239\u023a\7\31\2\2\u023a\u023b\7u\2\2\u023b\u023c\5\32\16\2\u023c"+
		"\u023d\7v\2\2\u023d\u0256\3\2\2\2\u023e\u023f\7\32\2\2\u023f\u0240\7u"+
		"\2\2\u0240\u0241\5\32\16\2\u0241\u0242\7v\2\2\u0242\u0256\3\2\2\2\u0243"+
		"\u0244\7\27\2\2\u0244\u0245\7u\2\2\u0245\u0246\5\32\16\2\u0246\u0247\7"+
		"v\2\2\u0247\u0256\3\2\2\2\u0248\u0249\7\30\2\2\u0249\u024a\7u\2\2\u024a"+
		"\u024b\5\32\16\2\u024b\u024c\7v\2\2\u024c\u0256\3\2\2\2\u024d\u024e\7"+
		"\33\2\2\u024e\u024f\7u\2\2\u024f\u0250\5$\23\2\u0250\u0251\7v\2\2\u0251"+
		"\u0256\3\2\2\2\u0252\u0253\7\25\2\2\u0253\u0254\7u\2\2\u0254\u0256\7v"+
		"\2\2\u0255\u0239\3\2\2\2\u0255\u023e\3\2\2\2\u0255\u0243\3\2\2\2\u0255"+
		"\u0248\3\2\2\2\u0255\u024d\3\2\2\2\u0255\u0252\3\2\2\2\u0256\27\3\2\2"+
		"\2\u0257\u025b\7=\2\2\u0258\u025a\5\u0098M\2\u0259\u0258\3\2\2\2\u025a"+
		"\u025d\3\2\2\2\u025b\u0259\3\2\2\2\u025b\u025c\3\2\2\2\u025c\u025e\3\2"+
		"\2\2\u025d\u025b\3\2\2\2\u025e\u025f\7\34\2\2\u025f\u0260\5^\60\2\u0260"+
		"\u0261\5\u00ccg\2\u0261\u0262\7{\2\2\u0262\u0271\3\2\2\2\u0263\u0265\5"+
		"\u0098M\2\u0264\u0263\3\2\2\2\u0265\u0268\3\2\2\2\u0266\u0264\3\2\2\2"+
		"\u0266\u0267\3\2\2\2\u0267\u0269\3\2\2\2\u0268\u0266\3\2\2\2\u0269\u026a"+
		"\7\34\2\2\u026a\u026b\5^\60\2\u026b\u026c\5\u00ccg\2\u026c\u026d\7\u0084"+
		"\2\2\u026d\u026e\5\32\16\2\u026e\u026f\7{\2\2\u026f\u0271\3\2\2\2\u0270"+
		"\u0257\3\2\2\2\u0270\u0266\3\2\2\2\u0271\31\3\2\2\2\u0272\u0275\b\16\1"+
		"\2\u0273\u0276\5\34\17\2\u0274\u0276\5\36\20\2\u0275\u0273\3\2\2\2\u0275"+
		"\u0274\3\2\2\2\u0276\u027e\3\2\2\2\u0277\u0278\7\u0081\2\2\u0278\u027e"+
		"\5\32\16\6\u0279\u027a\7u\2\2\u027a\u027b\5\32\16\2\u027b\u027c\7v\2\2"+
		"\u027c\u027e\3\2\2\2\u027d\u0272\3\2\2\2\u027d\u0277\3\2\2\2\u027d\u0279"+
		"\3\2\2\2\u027e\u0287\3\2\2\2\u027f\u0280\f\4\2\2\u0280\u0281\7\u0089\2"+
		"\2\u0281\u0286\5\32\16\5\u0282\u0283\f\3\2\2\u0283\u0284\7\u008a\2\2\u0284"+
		"\u0286\5\32\16\4\u0285\u027f\3\2\2\2\u0285\u0282\3\2\2\2\u0286\u0289\3"+
		"\2\2\2\u0287\u0285\3\2\2\2\u0287\u0288\3\2\2\2\u0288\33\3\2\2\2\u0289"+
		"\u0287\3\2\2\2\u028a\u028b\7\f\2\2\u028b\u028c\7u\2\2\u028c\u028d\5\66"+
		"\34\2\u028d\u028e\7v\2\2\u028e\u0303\3\2\2\2\u028f\u0290\7\21\2\2\u0290"+
		"\u0291\7u\2\2\u0291\u0292\5\66\34\2\u0292\u0293\7v\2\2\u0293\u0303\3\2"+
		"\2\2\u0294\u0295\7\24\2\2\u0295\u0296\7u\2\2\u0296\u0297\5F$\2\u0297\u0298"+
		"\7v\2\2\u0298\u0303\3\2\2\2\u0299\u029a\7\36\2\2\u029a\u029b\7u\2\2\u029b"+
		"\u029c\5F$\2\u029c\u029d\7v\2\2\u029d\u0303\3\2\2\2\u029e\u029f\7#\2\2"+
		"\u029f\u02a0\7u\2\2\u02a0\u02a1\5*\26\2\u02a1\u02a2\7v\2\2\u02a2\u0303"+
		"\3\2\2\2\u02a3\u02a4\7\22\2\2\u02a4\u02a5\7u\2\2\u02a5\u02a6\5,\27\2\u02a6"+
		"\u02a7\7v\2\2\u02a7\u0303\3\2\2\2\u02a8\u02a9\7!\2\2\u02a9\u02aa\7u\2"+
		"\2\u02aa\u02ab\5,\27\2\u02ab\u02ac\7v\2\2\u02ac\u0303\3\2\2\2\u02ad\u02ae"+
		"\7\23\2\2\u02ae\u02af\7u\2\2\u02af\u02b0\5*\26\2\u02b0\u02b1\7v\2\2\u02b1"+
		"\u0303\3\2\2\2\u02b2\u02b3\7\5\2\2\u02b3\u02b4\7u\2\2\u02b4\u0303\7v\2"+
		"\2\u02b5\u02b6\7\'\2\2\u02b6\u02b7\7u\2\2\u02b7\u02b8\5*\26\2\u02b8\u02b9"+
		"\7v\2\2\u02b9\u0303\3\2\2\2\u02ba\u02bb\7(\2\2\u02bb\u02bc\7u\2\2\u02bc"+
		"\u02bd\5\66\34\2\u02bd\u02be\7v\2\2\u02be\u0303\3\2\2\2\u02bf\u02c0\7"+
		"\r\2\2\u02c0\u02c1\7u\2\2\u02c1\u02c2\5\32\16\2\u02c2\u02c3\7v\2\2\u02c3"+
		"\u0303\3\2\2\2\u02c4\u02c5\7\16\2\2\u02c5\u02c6\7u\2\2\u02c6\u02c7\5\32"+
		"\16\2\u02c7\u02c8\7v\2\2\u02c8\u0303\3\2\2\2\u02c9\u02ca\7R\2\2\u02ca"+
		"\u02cc\7u\2\2\u02cb\u02cd\5\u0084C\2\u02cc\u02cb\3\2\2\2\u02cc\u02cd\3"+
		"\2\2\2\u02cd\u02ce\3\2\2\2\u02ce\u0303\7v\2\2\u02cf\u02d0\7g\2\2\u02d0"+
		"\u02d1\7u\2\2\u02d1\u02d2\5R*\2\u02d2\u02d3\7v\2\2\u02d3\u0303\3\2\2\2"+
		"\u02d4\u02d5\7$\2\2\u02d5\u02d6\7u\2\2\u02d6\u02d7\5R*\2\u02d7\u02d8\7"+
		"v\2\2\u02d8\u0303\3\2\2\2\u02d9\u02da\7\7\2\2\u02da\u02db\7u\2\2\u02db"+
		"\u02dc\5\\/\2\u02dc\u02dd\7v\2\2\u02dd\u0303\3\2\2\2\u02de\u02df\7<\2"+
		"\2\u02df\u02e0\7g\2\2\u02e0\u02e1\7u\2\2\u02e1\u02e2\5T+\2\u02e2\u02e3"+
		"\7v\2\2\u02e3\u0303\3\2\2\2\u02e4\u02e5\7<\2\2\u02e5\u02e6\7$\2\2\u02e6"+
		"\u02e7\7u\2\2\u02e7\u02e8\5T+\2\u02e8\u02e9\7v\2\2\u02e9\u0303\3\2\2\2"+
		"\u02ea\u02eb\7<\2\2\u02eb\u02ec\7\7\2\2\u02ec\u02ed\7u\2\2\u02ed\u02ee"+
		"\5V,\2\u02ee\u02ef\7v\2\2\u02ef\u0303\3\2\2\2\u02f0\u02f1\7<\2\2\u02f1"+
		"\u02f2\7\'\2\2\u02f2\u02f3\7u\2\2\u02f3\u02f4\5T+\2\u02f4\u02f5\7v\2\2"+
		"\u02f5\u0303\3\2\2\2\u02f6\u02f7\7<\2\2\u02f7\u02f8\7(\2\2\u02f8\u02f9"+
		"\7u\2\2\u02f9\u02fa\5T+\2\u02fa\u02fb\7v\2\2\u02fb\u0303\3\2\2\2\u02fc"+
		"\u02fd\7<\2\2\u02fd\u02fe\7\6\2\2\u02fe\u02ff\7u\2\2\u02ff\u0300\5T+\2"+
		"\u0300\u0301\7v\2\2\u0301\u0303\3\2\2\2\u0302\u028a\3\2\2\2\u0302\u028f"+
		"\3\2\2\2\u0302\u0294\3\2\2\2\u0302\u0299\3\2\2\2\u0302\u029e\3\2\2\2\u0302"+
		"\u02a3\3\2\2\2\u0302\u02a8\3\2\2\2\u0302\u02ad\3\2\2\2\u0302\u02b2\3\2"+
		"\2\2\u0302\u02b5\3\2\2\2\u0302\u02ba\3\2\2\2\u0302\u02bf\3\2\2\2\u0302"+
		"\u02c4\3\2\2\2\u0302\u02c9\3\2\2\2\u0302\u02cf\3\2\2\2\u0302\u02d4\3\2"+
		"\2\2\u0302\u02d9\3\2\2\2\u0302\u02de\3\2\2\2\u0302\u02e4\3\2\2\2\u0302"+
		"\u02ea\3\2\2\2\u0302\u02f0\3\2\2\2\u0302\u02f6\3\2\2\2\u0302\u02fc\3\2"+
		"\2\2\u0303\35\3\2\2\2\u0304\u0305\5$\23\2\u0305\u0306\7}\2\2\u0306\u0308"+
		"\3\2\2\2\u0307\u0304\3\2\2\2\u0307\u0308\3\2\2\2\u0308\u0309\3\2\2\2\u0309"+
		"\u030a\5^\60\2\u030a\u030b\5P)\2\u030b\37\3\2\2\2\u030c\u030e\5\u0098"+
		"M\2\u030d\u030c\3\2\2\2\u030e\u0311\3\2\2\2\u030f\u030d\3\2\2\2\u030f"+
		"\u0310\3\2\2\2\u0310\u0314\3\2\2\2\u0311\u030f\3\2\2\2\u0312\u0315\5\u00c2"+
		"b\2\u0313\u0315\7l\2\2\u0314\u0312\3\2\2\2\u0314\u0313\3\2\2\2\u0315\u0316"+
		"\3\2\2\2\u0316\u0317\5\u00c2b\2\u0317\u0318\7}\2\2\u0318\u0319\5^\60\2"+
		"\u0319\u031c\5\u00ccg\2\u031a\u031b\7i\2\2\u031b\u031d\5\u00a6T\2\u031c"+
		"\u031a\3\2\2\2\u031c\u031d\3\2\2\2\u031d\u031e\3\2\2\2\u031e\u031f\5\u00d4"+
		"k\2\u031f\u035f\3\2\2\2\u0320\u0322\5\u0098M\2\u0321\u0320\3\2\2\2\u0322"+
		"\u0325\3\2\2\2\u0323\u0321\3\2\2\2\u0323\u0324\3\2\2\2\u0324\u0326\3\2"+
		"\2\2\u0325\u0323\3\2\2\2\u0326\u032a\7=\2\2\u0327\u0329\5\u0098M\2\u0328"+
		"\u0327\3\2\2\2\u0329\u032c\3\2\2\2\u032a\u0328\3\2\2\2\u032a\u032b\3\2"+
		"\2\2\u032b\u032f\3\2\2\2\u032c\u032a\3\2\2\2\u032d\u0330\5\u00c2b\2\u032e"+
		"\u0330\7l\2\2\u032f\u032d\3\2\2\2\u032f\u032e\3\2\2\2\u0330\u0331\3\2"+
		"\2\2\u0331\u0332\5\u00c2b\2\u0332\u0333\7}\2\2\u0333\u0334\5^\60\2\u0334"+
		"\u0337\5\u00ccg\2\u0335\u0336\7i\2\2\u0336\u0338\5\u00a6T\2\u0337\u0335"+
		"\3\2\2\2\u0337\u0338\3\2\2\2\u0338\u0339\3\2\2\2\u0339\u033a\7{\2\2\u033a"+
		"\u035f\3\2\2\2\u033b\u033d\5\u0098M\2\u033c\u033b\3\2\2\2\u033d\u0340"+
		"\3\2\2\2\u033e\u033c\3\2\2\2\u033e\u033f\3\2\2\2\u033f\u0341\3\2\2\2\u0340"+
		"\u033e\3\2\2\2\u0341\u0342\5\u00c2b\2\u0342\u0343\7}\2\2\u0343\u0344\7"+
		"[\2\2\u0344\u0347\5\u00ccg\2\u0345\u0346\7i\2\2\u0346\u0348\5\u00a6T\2"+
		"\u0347\u0345\3\2\2\2\u0347\u0348\3\2\2\2\u0348\u0349\3\2\2\2\u0349\u034a"+
		"\5\u00d4k\2\u034a\u035f\3\2\2\2\u034b\u034d\5\u0098M\2\u034c\u034b\3\2"+
		"\2\2\u034d\u0350\3\2\2\2\u034e\u034c\3\2\2\2\u034e\u034f\3\2\2\2\u034f"+
		"\u0353\3\2\2\2\u0350\u034e\3\2\2\2\u0351\u0354\5\u00c2b\2\u0352\u0354"+
		"\7l\2\2\u0353\u0351\3\2\2\2\u0353\u0352\3\2\2\2\u0354\u0355\3\2\2\2\u0355"+
		"\u0356\5\u00c2b\2\u0356\u0357\7}\2\2\u0357\u035a\5^\60\2\u0358\u0359\7"+
		"~\2\2\u0359\u035b\5\u0084C\2\u035a\u0358\3\2\2\2\u035a\u035b\3\2\2\2\u035b"+
		"\u035c\3\2\2\2\u035c\u035d\7{\2\2\u035d\u035f\3\2\2\2\u035e\u030f\3\2"+
		"\2\2\u035e\u0323\3\2\2\2\u035e\u033e\3\2\2\2\u035e\u034e\3\2\2\2\u035f"+
		"!\3\2\2\2\u0360\u0361\7\17\2\2\u0361\u0362\7\26\2\2\u0362\u0363\7\u0084"+
		"\2\2\u0363\u0364\5$\23\2\u0364\u0365\7M\2\2\u0365\u0366\5\u00c2b\2\u0366"+
		"\u0367\7{\2\2\u0367\u03b3\3\2\2\2\u0368\u0369\7\17\2\2\u0369\u036a\7\26"+
		"\2\2\u036a\u036b\7\u0084\2\2\u036b\u036c\5$\23\2\u036c\u036d\7T\2\2\u036d"+
		"\u036e\5\u00a6T\2\u036e\u036f\7{\2\2\u036f\u03b3\3\2\2\2\u0370\u0371\7"+
		"\17\2\2\u0371\u0372\7&\2\2\u0372\u0373\7\u0084\2\2\u0373\u0374\5\32\16"+
		"\2\u0374\u0375\7\u0084\2\2\u0375\u0376\7s\2\2\u0376\u0377\7{\2\2\u0377"+
		"\u03b3\3\2\2\2\u0378\u0379\7\17\2\2\u0379\u037a\7\20\2\2\u037a\u037b\7"+
		"\u0084\2\2\u037b\u037c\5\32\16\2\u037c\u037d\7\u0084\2\2\u037d\u037e\7"+
		"s\2\2\u037e\u037f\7{\2\2\u037f\u03b3\3\2\2\2\u0380\u0381\7\17\2\2\u0381"+
		"\u0382\7\"\2\2\u0382\u0383\7\u0084\2\2\u0383\u0384\5\u00c2b\2\u0384\u0385"+
		"\7\u0084\2\2\u0385\u0386\5\32\16\2\u0386\u0387\7{\2\2\u0387\u03b3\3\2"+
		"\2\2\u0388\u0389\7\17\2\2\u0389\u038a\7\35\2\2\u038a\u038b\7\u0084\2\2"+
		"\u038b\u038c\5D#\2\u038c\u038d\7{\2\2\u038d\u03b3\3\2\2\2\u038e\u038f"+
		"\7\17\2\2\u038f\u0390\7<\2\2\u0390\u0391\79\2\2\u0391\u0392\7\u0084\2"+
		"\2\u0392\u0393\5$\23\2\u0393\u0394\7\u0084\2\2\u0394\u0395\5\f\7\2\u0395"+
		"\u0396\7{\2\2\u0396\u03b3\3\2\2\2\u0397\u0398\7\17\2\2\u0398\u0399\7<"+
		"\2\2\u0399\u039a\7:\2\2\u039a\u039b\7\u0084\2\2\u039b\u039c\58\35\2\u039c"+
		"\u039d\7\u0084\2\2\u039d\u039e\5\f\7\2\u039e\u039f\7{\2\2\u039f\u03b3"+
		"\3\2\2\2\u03a0\u03a1\7\17\2\2\u03a1\u03a2\7<\2\2\u03a2\u03a3\7\65\2\2"+
		"\u03a3\u03a4\7\u0084\2\2\u03a4\u03a5\5F$\2\u03a5\u03a6\7\u0084\2\2\u03a6"+
		"\u03a7\5\f\7\2\u03a7\u03a8\7{\2\2\u03a8\u03b3\3\2\2\2\u03a9\u03aa\7\17"+
		"\2\2\u03aa\u03ab\7<\2\2\u03ab\u03ac\7\67\2\2\u03ac\u03ad\7\u0084\2\2\u03ad"+
		"\u03ae\5,\27\2\u03ae\u03af\7\u0084\2\2\u03af\u03b0\5\f\7\2\u03b0\u03b1"+
		"\7{\2\2\u03b1\u03b3\3\2\2\2\u03b2\u0360\3\2\2\2\u03b2\u0368\3\2\2\2\u03b2"+
		"\u0370\3\2\2\2\u03b2\u0378\3\2\2\2\u03b2\u0380\3\2\2\2\u03b2\u0388\3\2"+
		"\2\2\u03b2\u038e\3\2\2\2\u03b2\u0397\3\2\2\2\u03b2\u03a0\3\2\2\2\u03b2"+
		"\u03a9\3\2\2\2\u03b3#\3\2\2\2\u03b4\u03b5\b\23\1\2\u03b5\u03c0\5&\24\2"+
		"\u03b6\u03b7\7\u0081\2\2\u03b7\u03c0\5$\23\6\u03b8\u03ba\7u\2\2\u03b9"+
		"\u03bb\5L\'\2\u03ba\u03b9\3\2\2\2\u03ba\u03bb\3\2\2\2\u03bb\u03bc\3\2"+
		"\2\2\u03bc\u03bd\5$\23\2\u03bd\u03be\7v\2\2\u03be\u03c0\3\2\2\2\u03bf"+
		"\u03b4\3\2\2\2\u03bf\u03b6\3\2\2\2\u03bf\u03b8\3\2\2\2\u03c0\u03c9\3\2"+
		"\2\2\u03c1\u03c2\f\4\2\2\u03c2\u03c3\7\u0089\2\2\u03c3\u03c8\5$\23\5\u03c4"+
		"\u03c5\f\3\2\2\u03c5\u03c6\7\u008a\2\2\u03c6\u03c8\5$\23\4\u03c7\u03c1"+
		"\3\2\2\2\u03c7\u03c4\3\2\2\2\u03c8\u03cb\3\2\2\2\u03c9\u03c7\3\2\2\2\u03c9"+
		"\u03ca\3\2\2\2\u03ca%\3\2\2\2\u03cb\u03c9\3\2\2\2\u03cc\u03ce\5(\25\2"+
		"\u03cd\u03cf\7\u008d\2\2\u03ce\u03cd\3\2\2\2\u03ce\u03cf\3\2\2\2\u03cf"+
		"\u03d4\3\2\2\2\u03d0\u03d1\7y\2\2\u03d1\u03d3\7z\2\2\u03d2\u03d0\3\2\2"+
		"\2\u03d3\u03d6\3\2\2\2\u03d4\u03d2\3\2\2\2\u03d4\u03d5\3\2\2\2\u03d5\'"+
		"\3\2\2\2\u03d6\u03d4\3\2\2\2\u03d7\u03dd\5\u00c2b\2\u03d8\u03dd\5^\60"+
		"\2\u03d9\u03dd\7\u008f\2\2\u03da\u03dd\7}\2\2\u03db\u03dd\7\3\2\2\u03dc"+
		"\u03d7\3\2\2\2\u03dc\u03d8\3\2\2\2\u03dc\u03d9\3\2\2\2\u03dc\u03da\3\2"+
		"\2\2\u03dc\u03db\3\2\2\2\u03dd\u03de\3\2\2\2\u03de\u03dc\3\2\2\2\u03de"+
		"\u03df\3\2\2\2\u03df\u03e2\3\2\2\2\u03e0\u03e2\7l\2\2\u03e1\u03dc\3\2"+
		"\2\2\u03e1\u03e0\3\2\2\2\u03e2)\3\2\2\2\u03e3\u03e5\7u\2\2\u03e4\u03e6"+
		"\5L\'\2\u03e5\u03e4\3\2\2\2\u03e5\u03e6\3\2\2\2\u03e6\u03e7\3\2\2\2\u03e7"+
		"\u03e8\5$\23\2\u03e8\u03e9\7v\2\2\u03e9\u03ef\3\2\2\2\u03ea\u03ec\5L\'"+
		"\2\u03eb\u03ea\3\2\2\2\u03eb\u03ec\3\2\2\2\u03ec\u03ed\3\2\2\2\u03ed\u03ef"+
		"\5$\23\2\u03ee\u03e3\3\2\2\2\u03ee\u03eb\3\2\2\2\u03ef+\3\2\2\2\u03f0"+
		"\u03f2\5L\'\2\u03f1\u03f0\3\2\2\2\u03f1\u03f2\3\2\2\2\u03f2\u03f4\3\2"+
		"\2\2\u03f3\u03f5\5.\30\2\u03f4\u03f3\3\2\2\2\u03f4\u03f5\3\2\2\2\u03f5"+
		"\u03f6\3\2\2\2\u03f6\u03fa\5$\23\2\u03f7\u03f8\5$\23\2\u03f8\u03f9\5\62"+
		"\32\2\u03f9\u03fb\3\2\2\2\u03fa\u03f7\3\2\2\2\u03fa\u03fb\3\2\2\2\u03fb"+
		"\u03fc\3\2\2\2\u03fc\u03fd\5\64\33\2\u03fd-\3\2\2\2\u03fe\u0400\7\u0081"+
		"\2\2\u03ff\u03fe\3\2\2\2\u03ff\u0400\3\2\2\2\u0400\u0401\3\2\2\2\u0401"+
		"\u0405\5\60\31\2\u0402\u0404\5.\30\2\u0403\u0402\3\2\2\2\u0404\u0407\3"+
		"\2\2\2\u0405\u0403\3\2\2\2\u0405\u0406\3\2\2\2\u0406/\3\2\2\2\u0407\u0405"+
		"\3\2\2\2\u0408\u0409\t\2\2\2\u0409\61\3\2\2\2\u040a\u040b\t\3\2\2\u040b"+
		"\63\3\2\2\2\u040c\u0411\5^\60\2\u040d\u040e\7\u008f\2\2\u040e\u0410\5"+
		"^\60\2\u040f\u040d\3\2\2\2\u0410\u0413\3\2\2\2\u0411\u040f\3\2\2\2\u0411"+
		"\u0412\3\2\2\2\u0412\u0415\3\2\2\2\u0413\u0411\3\2\2\2\u0414\u0416\7\u008f"+
		"\2\2\u0415\u0414\3\2\2\2\u0415\u0416\3\2\2\2\u0416\u0424\3\2\2\2\u0417"+
		"\u041d\7\u008f\2\2\u0418\u0419\5^\60\2\u0419\u041a\7\u008f\2\2\u041a\u041c"+
		"\3\2\2\2\u041b\u0418\3\2\2\2\u041c\u041f\3\2\2\2\u041d\u041b\3\2\2\2\u041d"+
		"\u041e\3\2\2\2\u041e\u0421\3\2\2\2\u041f\u041d\3\2\2\2\u0420\u0422\5^"+
		"\60\2\u0421\u0420\3\2\2\2\u0421\u0422\3\2\2\2\u0422\u0424\3\2\2\2\u0423"+
		"\u040c\3\2\2\2\u0423\u0417\3\2\2\2\u0424\65\3\2\2\2\u0425\u0428\58\35"+
		"\2\u0426\u0428\5F$\2\u0427\u0425\3\2\2\2\u0427\u0426\3\2\2\2\u0428\67"+
		"\3\2\2\2\u0429\u042b\5L\'\2\u042a\u0429\3\2\2\2\u042a\u042b\3\2\2\2\u042b"+
		"\u042d\3\2\2\2\u042c\u042e\5:\36\2\u042d\u042c\3\2\2\2\u042d\u042e\3\2"+
		"\2\2\u042e\u042f\3\2\2\2\u042f\u0433\5$\23\2\u0430\u0431\5$\23\2\u0431"+
		"\u0432\5\62\32\2\u0432\u0434\3\2\2\2\u0433\u0430\3\2\2\2\u0433\u0434\3"+
		"\2\2\2\u0434\u0435\3\2\2\2\u0435\u0436\5\64\33\2\u0436\u0438\5P)\2\u0437"+
		"\u0439\5B\"\2\u0438\u0437\3\2\2\2\u0438\u0439\3\2\2\2\u04399\3\2\2\2\u043a"+
		"\u043c\7\u0081\2\2\u043b\u043a\3\2\2\2\u043b\u043c\3\2\2\2\u043c\u043d"+
		"\3\2\2\2\u043d\u0441\5<\37\2\u043e\u0440\5:\36\2\u043f\u043e\3\2\2\2\u0440"+
		"\u0443\3\2\2\2\u0441\u043f\3\2\2\2\u0441\u0442\3\2\2\2\u0442;\3\2\2\2"+
		"\u0443\u0441\3\2\2\2\u0444\u0445\t\4\2\2\u0445=\3\2\2\2\u0446\u044b\7"+
		"\3\2\2\u0447\u0448\7|\2\2\u0448\u044a\5@!\2\u0449\u0447\3\2\2\2\u044a"+
		"\u044d\3\2\2\2\u044b\u0449\3\2\2\2\u044b\u044c\3\2\2\2\u044c\u045a\3\2"+
		"\2\2\u044d\u044b\3\2\2\2\u044e\u0453\5*\26\2\u044f\u0450\7|\2\2\u0450"+
		"\u0452\5> \2\u0451\u044f\3\2\2\2\u0452\u0455\3\2\2\2\u0453\u0451\3\2\2"+
		"\2\u0453\u0454\3\2\2\2\u0454\u045a\3\2\2\2\u0455\u0453\3\2\2\2\u0456\u0457"+
		"\5$\23\2\u0457\u0458\7\u00a1\2\2\u0458\u045a\3\2\2\2\u0459\u0446\3\2\2"+
		"\2\u0459\u044e\3\2\2\2\u0459\u0456\3\2\2\2\u045a?\3\2\2\2\u045b\u0460"+
		"\5*\26\2\u045c\u045d\7|\2\2\u045d\u045f\5@!\2\u045e\u045c\3\2\2\2\u045f"+
		"\u0462\3\2\2\2\u0460\u045e\3\2\2\2\u0460\u0461\3\2\2\2\u0461\u0467\3\2"+
		"\2\2\u0462\u0460\3\2\2\2\u0463\u0464\5$\23\2\u0464\u0465\7\u00a1\2\2\u0465"+
		"\u0467\3\2\2\2\u0466\u045b\3\2\2\2\u0466\u0463\3\2\2\2\u0467A\3\2\2\2"+
		"\u0468\u0469\7i\2\2\u0469\u046a\5D#\2\u046aC\3\2\2\2\u046b\u0470\5$\23"+
		"\2\u046c\u046d\7|\2\2\u046d\u046f\5$\23\2\u046e\u046c\3\2\2\2\u046f\u0472"+
		"\3\2\2\2\u0470\u046e\3\2\2\2\u0470\u0471\3\2\2\2\u0471E\3\2\2\2\u0472"+
		"\u0470\3\2\2\2\u0473\u0475\5L\'\2\u0474\u0473\3\2\2\2\u0474\u0475\3\2"+
		"\2\2\u0475\u0477\3\2\2\2\u0476\u0478\5H%\2\u0477\u0476\3\2\2\2\u0477\u0478"+
		"\3\2\2\2\u0478\u047c\3\2\2\2\u0479\u047a\5$\23\2\u047a\u047b\5\62\32\2"+
		"\u047b\u047d\3\2\2\2\u047c\u0479\3\2\2\2\u047c\u047d\3\2\2\2\u047d\u047e"+
		"\3\2\2\2\u047e\u047f\7[\2\2\u047f\u0481\5P)\2\u0480\u0482\5B\"\2\u0481"+
		"\u0480\3\2\2\2\u0481\u0482\3\2\2\2\u0482G\3\2\2\2\u0483\u0485\7\u0081"+
		"\2\2\u0484\u0483\3\2\2\2\u0484\u0485\3\2\2\2\u0485\u0486\3\2\2\2\u0486"+
		"\u048a\5J&\2\u0487\u0489\5H%\2\u0488\u0487\3\2\2\2\u0489\u048c\3\2\2\2"+
		"\u048a\u0488\3\2\2\2\u048a\u048b\3\2\2\2\u048bI\3\2\2\2\u048c\u048a\3"+
		"\2\2\2\u048d\u048e\t\5\2\2\u048eK\3\2\2\2\u048f\u0491\7\u0081\2\2\u0490"+
		"\u048f\3\2\2\2\u0490\u0491\3\2\2\2\u0491\u0492\3\2\2\2\u0492\u0493\7<"+
		"\2\2\u0493\u0497\5N(\2\u0494\u0496\5L\'\2\u0495\u0494\3\2\2\2\u0496\u0499"+
		"\3\2\2\2\u0497\u0495\3\2\2\2\u0497\u0498\3\2\2\2\u0498M\3\2\2\2\u0499"+
		"\u0497\3\2\2\2\u049a\u04a0\5x=\2\u049b\u049c\7u\2\2\u049c\u049d\5$\23"+
		"\2\u049d\u049e\7v\2\2\u049e\u04a0\3\2\2\2\u049f\u049a\3\2\2\2\u049f\u049b"+
		"\3\2\2\2\u04a0O\3\2\2\2\u04a1\u04a3\7u\2\2\u04a2\u04a4\5> \2\u04a3\u04a2"+
		"\3\2\2\2\u04a3\u04a4\3\2\2\2\u04a4\u04a5\3\2\2\2\u04a5\u04a6\7v\2\2\u04a6"+
		"Q\3\2\2\2\u04a7\u04aa\5\u00c2b\2\u04a8\u04aa\5r:\2\u04a9\u04a7\3\2\2\2"+
		"\u04a9\u04a8\3\2\2\2\u04aaS\3\2\2\2\u04ab\u04ae\5x=\2\u04ac\u04ae\5^\60"+
		"\2\u04ad\u04ab\3\2\2\2\u04ad\u04ac\3\2\2\2\u04aeU\3\2\2\2\u04af\u04b2"+
		"\7\3\2\2\u04b0\u04b1\7|\2\2\u04b1\u04b3\5X-\2\u04b2\u04b0\3\2\2\2\u04b2"+
		"\u04b3\3\2\2\2\u04b3\u04c5\3\2\2\2\u04b4\u04b9\5T+\2\u04b5\u04b6\7|\2"+
		"\2\u04b6\u04b8\5V,\2\u04b7\u04b5\3\2\2\2\u04b8\u04bb\3\2\2\2\u04b9\u04b7"+
		"\3\2\2\2\u04b9\u04ba\3\2\2\2\u04ba\u04c5\3\2\2\2\u04bb\u04b9\3\2\2\2\u04bc"+
		"\u04c1\7\u008f\2\2\u04bd\u04be\7|\2\2\u04be\u04c0\5V,\2\u04bf\u04bd\3"+
		"\2\2\2\u04c0\u04c3\3\2\2\2\u04c1\u04bf\3\2\2\2\u04c1\u04c2\3\2\2\2\u04c2"+
		"\u04c5\3\2\2\2\u04c3\u04c1\3\2\2\2\u04c4\u04af\3\2\2\2\u04c4\u04b4\3\2"+
		"\2\2\u04c4\u04bc\3\2\2\2\u04c5W\3\2\2\2\u04c6\u04cb\5T+\2\u04c7\u04c8"+
		"\7|\2\2\u04c8\u04ca\5X-\2\u04c9\u04c7\3\2\2\2\u04ca\u04cd\3\2\2\2\u04cb"+
		"\u04c9\3\2\2\2\u04cb\u04cc\3\2\2\2\u04cc\u04d7\3\2\2\2\u04cd\u04cb\3\2"+
		"\2\2\u04ce\u04d3\7\u008f\2\2\u04cf\u04d0\7|\2\2\u04d0\u04d2\5X-\2\u04d1"+
		"\u04cf\3\2\2\2\u04d2\u04d5\3\2\2\2\u04d3\u04d1\3\2\2\2\u04d3\u04d4\3\2"+
		"\2\2\u04d4\u04d7\3\2\2\2\u04d5\u04d3\3\2\2\2\u04d6\u04c6\3\2\2\2\u04d6"+
		"\u04ce\3\2\2\2\u04d7Y\3\2\2\2\u04d8\u04db\5R*\2\u04d9\u04db\t\6\2\2\u04da"+
		"\u04d8\3\2\2\2\u04da\u04d9\3\2\2\2\u04db[\3\2\2\2\u04dc\u04e1\5Z.\2\u04dd"+
		"\u04de\7|\2\2\u04de\u04e0\5Z.\2\u04df\u04dd\3\2\2\2\u04e0\u04e3\3\2\2"+
		"\2\u04e1\u04df\3\2\2\2\u04e1\u04e2\3\2\2\2\u04e2]\3\2\2\2\u04e3\u04e1"+
		"\3\2\2\2\u04e4\u04e7\t\7\2\2\u04e5\u04e7\7\u00a0\2\2\u04e6\u04e4\3\2\2"+
		"\2\u04e6\u04e5\3\2\2\2\u04e7_\3\2\2\2\u04e8\u04e9\7E\2\2\u04e9\u04eb\5"+
		"^\60\2\u04ea\u04ec\5\u009eP\2\u04eb\u04ea\3\2\2\2\u04eb\u04ec\3\2\2\2"+
		"\u04ec\u04ef\3\2\2\2\u04ed\u04ee\7M\2\2\u04ee\u04f0\5\u00c2b\2\u04ef\u04ed"+
		"\3\2\2\2\u04ef\u04f0\3\2\2\2\u04f0\u04f3\3\2\2\2\u04f1\u04f2\7T\2\2\u04f2"+
		"\u04f4\5\u00a6T\2\u04f3\u04f1\3\2\2\2\u04f3\u04f4\3\2\2\2\u04f4\u04f5"+
		"\3\2\2\2\u04f5\u04f6\5\u00a8U\2\u04f6a\3\2\2\2\u04f7\u04fa\5^\60\2\u04f8"+
		"\u04f9\7M\2\2\u04f9\u04fb\5\u00a0Q\2\u04fa\u04f8\3\2\2\2\u04fa\u04fb\3"+
		"\2\2\2\u04fbc\3\2\2\2\u04fc\u04fd\7L\2\2\u04fd\u0500\5^\60\2\u04fe\u04ff"+
		"\7T\2\2\u04ff\u0501\5\u00a6T\2\u0500\u04fe\3\2\2\2\u0500\u0501\3\2\2\2"+
		"\u0501\u0502\3\2\2\2\u0502\u0504\7w\2\2\u0503\u0505\5\u00a2R\2\u0504\u0503"+
		"\3\2\2\2\u0504\u0505\3\2\2\2\u0505\u0507\3\2\2\2\u0506\u0508\7|\2\2\u0507"+
		"\u0506\3\2\2\2\u0507\u0508\3\2\2\2\u0508\u050a\3\2\2\2\u0509\u050b\5\u00a4"+
		"S\2\u050a\u0509\3\2\2\2\u050a\u050b\3\2\2\2\u050b\u050c\3\2\2\2\u050c"+
		"\u050d\7x\2\2\u050de\3\2\2\2\u050e\u0510\5\f\7\2\u050f\u050e\3\2\2\2\u0510"+
		"\u0513\3\2\2\2\u0511\u050f\3\2\2\2\u0511\u0512\3\2\2\2\u0512\u0514\3\2"+
		"\2\2\u0513\u0511\3\2\2\2\u0514\u0516\5^\60\2\u0515\u0517\5\u0122\u0092"+
		"\2\u0516\u0515\3\2\2\2\u0516\u0517\3\2\2\2\u0517\u0519\3\2\2\2\u0518\u051a"+
		"\5\u00a8U\2\u0519\u0518\3\2\2\2\u0519\u051a\3\2\2\2\u051ag\3\2\2\2\u051b"+
		"\u051c\7X\2\2\u051c\u051e\5^\60\2\u051d\u051f\5\u009eP\2\u051e\u051d\3"+
		"\2\2\2\u051e\u051f\3\2\2\2\u051f\u0522\3\2\2\2\u0520\u0521\7M\2\2\u0521"+
		"\u0523\5\u00a6T\2\u0522\u0520\3\2\2\2\u0522\u0523\3\2\2\2\u0523\u0524"+
		"\3\2\2\2\u0524\u0525\5\u00aaV\2\u0525i\3\2\2\2\u0526\u0529\5\u00c2b\2"+
		"\u0527\u0529\7l\2\2\u0528\u0526\3\2\2\2\u0528\u0527\3\2\2\2\u0529\u052a"+
		"\3\2\2\2\u052a\u052b\5^\60\2\u052b\u0530\5\u00ccg\2\u052c\u052d\7y\2\2"+
		"\u052d\u052f\7z\2\2\u052e\u052c\3\2\2\2\u052f\u0532\3\2\2\2\u0530\u052e"+
		"\3\2\2\2\u0530\u0531\3\2\2\2\u0531\u0535\3\2\2\2\u0532\u0530\3\2\2\2\u0533"+
		"\u0534\7i\2\2\u0534\u0536\5\u00caf\2\u0535\u0533\3\2\2\2\u0535\u0536\3"+
		"\2\2\2\u0536\u0539\3\2\2\2\u0537\u053a\5\u00d4k\2\u0538\u053a\7{\2\2\u0539"+
		"\u0537\3\2\2\2\u0539\u0538\3\2\2\2\u053ak\3\2\2\2\u053b\u053c\5^\60\2"+
		"\u053c\u053f\5\u00ccg\2\u053d\u053e\7i\2\2\u053e\u0540\5\u00caf\2\u053f"+
		"\u053d\3\2\2\2\u053f\u0540\3\2\2\2\u0540\u0541\3\2\2\2\u0541\u0542\5\u00d6"+
		"l\2\u0542m\3\2\2\2\u0543\u0548\5^\60\2\u0544\u0545\7y\2\2\u0545\u0547"+
		"\7z\2\2\u0546\u0544\3\2\2\2\u0547\u054a\3\2\2\2\u0548\u0546\3\2\2\2\u0548"+
		"\u0549\3\2\2\2\u0549\u054b\3\2\2\2\u054a\u0548\3\2\2\2\u054b\u054c\7~"+
		"\2\2\u054c\u054d\5\u00be`\2\u054do\3\2\2\2\u054e\u0551\5\u00c2b\2\u054f"+
		"\u0551\7l\2\2\u0550\u054e\3\2\2\2\u0550\u054f\3\2\2\2\u0551\u0552\3\2"+
		"\2\2\u0552\u0553\5^\60\2\u0553\u0558\5\u00ccg\2\u0554\u0555\7y\2\2\u0555"+
		"\u0557\7z\2\2\u0556\u0554\3\2\2\2\u0557\u055a\3\2\2\2\u0558\u0556\3\2"+
		"\2\2\u0558\u0559\3\2\2\2\u0559\u055d\3\2\2\2\u055a\u0558\3\2\2\2\u055b"+
		"\u055c\7i\2\2\u055c\u055e\5\u00caf\2\u055d\u055b\3\2\2\2\u055d\u055e\3"+
		"\2\2\2\u055e\u055f\3\2\2\2\u055f\u0560\7{\2\2\u0560q\3\2\2\2\u0561\u0566"+
		"\5^\60\2\u0562\u0563\7y\2\2\u0563\u0565\7z\2\2\u0564\u0562\3\2\2\2\u0565"+
		"\u0568\3\2\2\2\u0566\u0564\3\2\2\2\u0566\u0567\3\2\2\2\u0567s\3\2\2\2"+
		"\u0568\u0566\3\2\2\2\u0569\u056a\5^\60\2\u056au\3\2\2\2\u056b\u056d\5"+
		"^\60\2\u056c\u056e\5\u00c6d\2\u056d\u056c\3\2\2\2\u056d\u056e\3\2\2\2"+
		"\u056e\u0576\3\2\2\2\u056f\u0570\7}\2\2\u0570\u0572\5^\60\2\u0571\u0573"+
		"\5\u00c6d\2\u0572\u0571\3\2\2\2\u0572\u0573\3\2\2\2\u0573\u0575\3\2\2"+
		"\2\u0574\u056f\3\2\2\2\u0575\u0578\3\2\2\2\u0576\u0574\3\2\2\2\u0576\u0577"+
		"\3\2\2\2\u0577w\3\2\2\2\u0578\u0576\3\2\2\2\u0579\u057e\5^\60\2\u057a"+
		"\u057b\7}\2\2\u057b\u057d\5^\60\2\u057c\u057a\3\2\2\2\u057d\u0580\3\2"+
		"\2\2\u057e\u057c\3\2\2\2\u057e\u057f\3\2\2\2\u057fy\3\2\2\2\u0580\u057e"+
		"\3\2\2\2\u0581\u0582\5^\60\2\u0582\u0583\7~\2\2\u0583\u0584\5\u00dep\2"+
		"\u0584{\3\2\2\2\u0585\u0586\7<\2\2\u0586\u0587\7X\2\2\u0587\u0588\5^\60"+
		"\2\u0588\u0589\5\u00e2r\2\u0589}\3\2\2\2\u058a\u058b\5^\60\2\u058b\u058c"+
		"\7u\2\2\u058c\u058e\7v\2\2\u058d\u058f\5\u00ecw\2\u058e\u058d\3\2\2\2"+
		"\u058e\u058f\3\2\2\2\u058f\177\3\2\2\2\u0590\u05fa\5\u00eex\2\u0591\u0592"+
		"\7>\2\2\u0592\u0595\5\u0084C\2\u0593\u0594\7\u0084\2\2\u0594\u0596\5\u0084"+
		"C\2\u0595\u0593\3\2\2\2\u0595\u0596\3\2\2\2\u0596\u0597\3\2\2\2\u0597"+
		"\u0598\7{\2\2\u0598\u05fa\3\2\2\2\u0599\u059a\7R\2\2\u059a\u059b\5\u010c"+
		"\u0087\2\u059b\u059e\5\u0080A\2\u059c\u059d\7K\2\2\u059d\u059f\5\u0080"+
		"A\2\u059e\u059c\3\2\2\2\u059e\u059f\3\2\2\2\u059f\u05fa\3\2\2\2\u05a0"+
		"\u05a1\7Q\2\2\u05a1\u05a2\7u\2\2\u05a2\u05a3\5\u0104\u0083\2\u05a3\u05a4"+
		"\7v\2\2\u05a4\u05a5\5\u0080A\2\u05a5\u05fa\3\2\2\2\u05a6\u05a7\7n\2\2"+
		"\u05a7\u05a8\5\u010c\u0087\2\u05a8\u05a9\5\u0080A\2\u05a9\u05fa\3\2\2"+
		"\2\u05aa\u05ab\7I\2\2\u05ab\u05ac\5\u0080A\2\u05ac\u05ad\7n\2\2\u05ad"+
		"\u05ae\5\u010c\u0087\2\u05ae\u05af\7{\2\2\u05af\u05fa\3\2\2\2\u05b0\u05b1"+
		"\7k\2\2\u05b1\u05bb\5\u00eex\2\u05b2\u05b4\5\u0082B\2\u05b3\u05b2\3\2"+
		"\2\2\u05b4\u05b5\3\2\2\2\u05b5\u05b3\3\2\2\2\u05b5\u05b6\3\2\2\2\u05b6"+
		"\u05b8\3\2\2\2\u05b7\u05b9\5\u00f8}\2\u05b8\u05b7\3\2\2\2\u05b8\u05b9"+
		"\3\2\2\2\u05b9\u05bc\3\2\2\2\u05ba\u05bc\5\u00f8}\2\u05bb\u05b3\3\2\2"+
		"\2\u05bb\u05ba\3\2\2\2\u05bc\u05fa\3\2\2\2\u05bd\u05be\7k\2\2\u05be\u05bf"+
		"\5\u00fa~\2\u05bf\u05c3\5\u00eex\2\u05c0\u05c2\5\u0082B\2\u05c1\u05c0"+
		"\3\2\2\2\u05c2\u05c5\3\2\2\2\u05c3\u05c1\3\2\2\2\u05c3\u05c4\3\2\2\2\u05c4"+
		"\u05c7\3\2\2\2\u05c5\u05c3\3\2\2\2\u05c6\u05c8\5\u00f8}\2\u05c7\u05c6"+
		"\3\2\2\2\u05c7\u05c8\3\2\2\2\u05c8\u05fa\3\2\2\2\u05c9\u05ca\7e\2\2\u05ca"+
		"\u05cb\5\u010c\u0087\2\u05cb\u05cf\7w\2\2\u05cc\u05ce\5\u0100\u0081\2"+
		"\u05cd\u05cc\3\2\2\2\u05ce\u05d1\3\2\2\2\u05cf\u05cd\3\2\2\2\u05cf\u05d0"+
		"\3\2\2\2\u05d0\u05d5\3\2\2\2\u05d1\u05cf\3\2\2\2\u05d2\u05d4\5\u0102\u0082"+
		"\2\u05d3\u05d2\3\2\2\2\u05d4\u05d7\3\2\2\2\u05d5\u05d3\3\2\2\2\u05d5\u05d6"+
		"\3\2\2\2\u05d6\u05d8\3\2\2\2\u05d7\u05d5\3\2\2\2\u05d8\u05d9\7x\2\2\u05d9"+
		"\u05fa\3\2\2\2\u05da\u05db\7f\2\2\u05db\u05dc\5\u010c\u0087\2\u05dc\u05dd"+
		"\5\u00eex\2\u05dd\u05fa\3\2\2\2\u05de\u05e0\7`\2\2\u05df\u05e1\5\u0084"+
		"C\2\u05e0\u05df\3\2\2\2\u05e0\u05e1\3\2\2\2\u05e1\u05e2\3\2\2\2\u05e2"+
		"\u05fa\7{\2\2\u05e3\u05e4\7h\2\2\u05e4\u05e5\5\u0084C\2\u05e5\u05e6\7"+
		"{\2\2\u05e6\u05fa\3\2\2\2\u05e7\u05e9\7@\2\2\u05e8\u05ea\5^\60\2\u05e9"+
		"\u05e8\3\2\2\2\u05e9\u05ea\3\2\2\2\u05ea\u05eb\3\2\2\2\u05eb\u05fa\7{"+
		"\2\2\u05ec\u05ee\7G\2\2\u05ed\u05ef\5^\60\2\u05ee\u05ed\3\2\2\2\u05ee"+
		"\u05ef\3\2\2\2\u05ef\u05f0\3\2\2\2\u05f0\u05fa\7{\2\2\u05f1\u05fa\7{\2"+
		"\2\u05f2\u05f3\5\u0110\u0089\2\u05f3\u05f4\7{\2\2\u05f4\u05fa\3\2\2\2"+
		"\u05f5\u05f6\5^\60\2\u05f6\u05f7\7\u0084\2\2\u05f7\u05f8\5\u0080A\2\u05f8"+
		"\u05fa\3\2\2\2\u05f9\u0590\3\2\2\2\u05f9\u0591\3\2\2\2\u05f9\u0599\3\2"+
		"\2\2\u05f9\u05a0\3\2\2\2\u05f9\u05a6\3\2\2\2\u05f9\u05aa\3\2\2\2\u05f9"+
		"\u05b0\3\2\2\2\u05f9\u05bd\3\2\2\2\u05f9\u05c9\3\2\2\2\u05f9\u05da\3\2"+
		"\2\2\u05f9\u05de\3\2\2\2\u05f9\u05e3\3\2\2\2\u05f9\u05e7\3\2\2\2\u05f9"+
		"\u05ec\3\2\2\2\u05f9\u05f1\3\2\2\2\u05f9\u05f2\3\2\2\2\u05f9\u05f5\3\2"+
		"\2\2\u05fa\u0081\3\2\2\2\u05fb\u05fc\7C\2\2\u05fc\u0600\7u\2\2\u05fd\u05ff"+
		"\5\u009cO\2\u05fe\u05fd\3\2\2\2\u05ff\u0602\3\2\2\2\u0600\u05fe\3\2\2"+
		"\2\u0600\u0601\3\2\2\2\u0601\u0603\3\2\2\2\u0602\u0600\3\2\2\2\u0603\u0604"+
		"\5\u00f6|\2\u0604\u0605\5^\60\2\u0605\u0606\7v\2\2\u0606\u0607\5\u00ee"+
		"x\2\u0607\u0083\3\2\2\2\u0608\u0609\bC\1\2\u0609\u0616\5\u0086D\2\u060a"+
		"\u060b\7[\2\2\u060b\u0616\5\u0114\u008b\2\u060c\u060d\7u\2\2\u060d\u060e"+
		"\5\u00c2b\2\u060e\u060f\7v\2\2\u060f\u0610\5\u0084C\23\u0610\u0616\3\2"+
		"\2\2\u0611\u0612\t\b\2\2\u0612\u0616\5\u0084C\21\u0613\u0614\t\t\2\2\u0614"+
		"\u0616\5\u0084C\20\u0615\u0608\3\2\2\2\u0615\u060a\3\2\2\2\u0615\u060c"+
		"\3\2\2\2\u0615\u0611\3\2\2\2\u0615\u0613\3\2\2\2\u0616\u066c\3\2\2\2\u0617"+
		"\u0618\f\17\2\2\u0618\u0619\t\n\2\2\u0619\u066b\5\u0084C\20\u061a\u061b"+
		"\f\16\2\2\u061b\u061c\t\13\2\2\u061c\u066b\5\u0084C\17\u061d\u0625\f\r"+
		"\2\2\u061e\u061f\7\u0080\2\2\u061f\u0626\7\u0080\2\2\u0620\u0621\7\177"+
		"\2\2\u0621\u0622\7\177\2\2\u0622\u0626\7\177\2\2\u0623\u0624\7\177\2\2"+
		"\u0624\u0626\7\177\2\2\u0625\u061e\3\2\2\2\u0625\u0620\3\2\2\2\u0625\u0623"+
		"\3\2\2\2\u0626\u0627\3\2\2\2\u0627\u066b\5\u0084C\16\u0628\u0629\f\f\2"+
		"\2\u0629\u062a\t\f\2\2\u062a\u066b\5\u0084C\r\u062b\u062c\f\n\2\2\u062c"+
		"\u062d\t\r\2\2\u062d\u066b\5\u0084C\13\u062e\u062f\f\t\2\2\u062f\u0630"+
		"\7\u0091\2\2\u0630\u066b\5\u0084C\n\u0631\u0632\f\b\2\2\u0632\u0633\7"+
		"\u0093\2\2\u0633\u066b\5\u0084C\t\u0634\u0635\f\7\2\2\u0635\u0636\7\u0092"+
		"\2\2\u0636\u066b\5\u0084C\b\u0637\u0638\f\6\2\2\u0638\u0639\7\u0089\2"+
		"\2\u0639\u066b\5\u0084C\7\u063a\u063b\f\5\2\2\u063b\u063c\7\u008a\2\2"+
		"\u063c\u066b\5\u0084C\6\u063d\u063e\f\4\2\2\u063e\u063f\7\u0083\2\2\u063f"+
		"\u0640\5\u0084C\2\u0640\u0641\7\u0084\2\2\u0641\u0642\5\u0084C\5\u0642"+
		"\u066b\3\2\2\2\u0643\u0644\f\3\2\2\u0644\u0645\t\16\2\2\u0645\u066b\5"+
		"\u0084C\4\u0646\u0647\f\33\2\2\u0647\u0648\7}\2\2\u0648\u066b\5^\60\2"+
		"\u0649\u064a\f\32\2\2\u064a\u064b\7}\2\2\u064b\u066b\7g\2\2\u064c\u064d"+
		"\f\31\2\2\u064d\u064e\7}\2\2\u064e\u0650\7[\2\2\u064f\u0651\5\u011c\u008f"+
		"\2\u0650\u064f\3\2\2\2\u0650\u0651\3\2\2\2\u0651\u0652\3\2\2\2\u0652\u066b"+
		"\5\u008aF\2\u0653\u0654\f\30\2\2\u0654\u0655\7}\2\2\u0655\u0656\7d\2\2"+
		"\u0656\u066b\5\u008cG\2\u0657\u0658\f\27\2\2\u0658\u0659\7}\2\2\u0659"+
		"\u066b\5\u011a\u008e\2\u065a\u065b\f\26\2\2\u065b\u065c\7y\2\2\u065c\u065d"+
		"\5\u0084C\2\u065d\u065e\7z\2\2\u065e\u066b\3\2\2\2\u065f\u0660\f\25\2"+
		"\2\u0660\u0662\7u\2\2\u0661\u0663\5\u010e\u0088\2\u0662\u0661\3\2\2\2"+
		"\u0662\u0663\3\2\2\2\u0663\u0664\3\2\2\2\u0664\u066b\7v\2\2\u0665\u0666"+
		"\f\22\2\2\u0666\u066b\t\17\2\2\u0667\u0668\f\13\2\2\u0668\u0669\7V\2\2"+
		"\u0669\u066b\5\u00c2b\2\u066a\u0617\3\2\2\2\u066a\u061a\3\2\2\2\u066a"+
		"\u061d\3\2\2\2\u066a\u0628\3\2\2\2\u066a\u062b\3\2\2\2\u066a\u062e\3\2"+
		"\2\2\u066a\u0631\3\2\2\2\u066a\u0634\3\2\2\2\u066a\u0637\3\2\2\2\u066a"+
		"\u063a\3\2\2\2\u066a\u063d\3\2\2\2\u066a\u0643\3\2\2\2\u066a\u0646\3\2"+
		"\2\2\u066a\u0649\3\2\2\2\u066a\u064c\3\2\2\2\u066a\u0653\3\2\2\2\u066a"+
		"\u0657\3\2\2\2\u066a\u065a\3\2\2\2\u066a\u065f\3\2\2\2\u066a\u0665\3\2"+
		"\2\2\u066a\u0667\3\2\2\2\u066b\u066e\3\2\2\2\u066c\u066a\3\2\2\2\u066c"+
		"\u066d\3\2\2\2\u066d\u0085\3\2\2\2\u066e\u066c\3\2\2\2\u066f\u0670\7u"+
		"\2\2\u0670\u0671\5\u0084C\2\u0671\u0672\7v\2\2\u0672\u0685\3\2\2\2\u0673"+
		"\u0685\7g\2\2\u0674\u0685\7d\2\2\u0675\u0685\5\u00d8m\2\u0676\u0685\5"+
		"^\60\2\u0677\u0678\5\u00c2b\2\u0678\u0679\7}\2\2\u0679\u067a\7E\2\2\u067a"+
		"\u0685\3\2\2\2\u067b\u067c\7l\2\2\u067c\u067d\7}\2\2\u067d\u0685\7E\2"+
		"\2\u067e\u0682\5\u011c\u008f\2\u067f\u0683\5\u008eH\2\u0680\u0681\7g\2"+
		"\2\u0681\u0683\5\u0122\u0092\2\u0682\u067f\3\2\2\2\u0682\u0680\3\2\2\2"+
		"\u0683\u0685\3\2\2\2\u0684\u066f\3\2\2\2\u0684\u0673\3\2\2\2\u0684\u0674"+
		"\3\2\2\2\u0684\u0675\3\2\2\2\u0684\u0676\3\2\2\2\u0684\u0677\3\2\2\2\u0684"+
		"\u067b\3\2\2\2\u0684\u067e\3\2\2\2\u0685\u0087\3\2\2\2\u0686\u0688\5^"+
		"\60\2\u0687\u0689\5\u011e\u0090\2\u0688\u0687\3\2\2\2\u0688\u0689\3\2"+
		"\2\2\u0689\u0691\3\2\2\2\u068a\u068b\7}\2\2\u068b\u068d\5^\60\2\u068c"+
		"\u068e\5\u011e\u0090\2\u068d\u068c\3\2\2\2\u068d\u068e\3\2\2\2\u068e\u0690"+
		"\3\2\2\2\u068f\u068a\3\2\2\2\u0690\u0693\3\2\2\2\u0691\u068f\3\2\2\2\u0691"+
		"\u0692\3\2\2\2\u0692\u0696\3\2\2\2\u0693\u0691\3\2\2\2\u0694\u0696\5\u00c4"+
		"c\2\u0695\u0686\3\2\2\2\u0695\u0694\3\2\2\2\u0696\u0089\3\2\2\2\u0697"+
		"\u0699\5^\60\2\u0698\u069a\5\u0120\u0091\2\u0699\u0698\3\2\2\2\u0699\u069a"+
		"\3\2\2\2\u069a\u069b\3\2\2\2\u069b\u069c\5\u0118\u008d\2\u069c\u008b\3"+
		"\2\2\2\u069d\u06a4\5\u0122\u0092\2\u069e\u069f\7}\2\2\u069f\u06a1\5^\60"+
		"\2\u06a0\u06a2\5\u0122\u0092\2\u06a1\u06a0\3\2\2\2\u06a1\u06a2\3\2\2\2"+
		"\u06a2\u06a4\3\2\2\2\u06a3\u069d\3\2\2\2\u06a3\u069e\3\2\2\2\u06a4\u008d"+
		"\3\2\2\2\u06a5\u06a6\7d\2\2\u06a6\u06ab\5\u008cG\2\u06a7\u06a8\5^\60\2"+
		"\u06a8\u06a9\5\u0122\u0092\2\u06a9\u06ab\3\2\2\2\u06aa\u06a5\3\2\2\2\u06aa"+
		"\u06a7\3\2\2\2\u06ab\u008f\3\2\2\2\u06ac\u06ae\5\u0092J\2\u06ad\u06ac"+
		"\3\2\2\2\u06ad\u06ae\3\2\2\2\u06ae\u06b2\3\2\2\2\u06af\u06b1\5\u0094K"+
		"\2\u06b0\u06af\3\2\2\2\u06b1\u06b4\3\2\2\2\u06b2\u06b0\3\2\2\2\u06b2\u06b3"+
		"\3\2\2\2\u06b3\u06b8\3\2\2\2\u06b4\u06b2\3\2\2\2\u06b5\u06b7\5\u0096L"+
		"\2\u06b6\u06b5\3\2\2\2\u06b7\u06ba\3\2\2\2\u06b8\u06b6\3\2\2\2\u06b8\u06b9"+
		"\3\2\2\2\u06b9\u06bb\3\2\2\2\u06ba\u06b8\3\2\2\2\u06bb\u06bc\7\2\2\3\u06bc"+
		"\u0091\3\2\2\2\u06bd\u06bf\5\f\7\2\u06be\u06bd\3\2\2\2\u06bf\u06c2\3\2"+
		"\2\2\u06c0\u06be\3\2\2\2\u06c0\u06c1\3\2\2\2\u06c1\u06c3\3\2\2\2\u06c2"+
		"\u06c0\3\2\2\2\u06c3\u06c4\7\\\2\2\u06c4\u06c5\5x=\2\u06c5\u06c6\7{\2"+
		"\2\u06c6\u0093\3\2\2\2\u06c7\u06c9\7U\2\2\u06c8\u06ca\7b\2\2\u06c9\u06c8"+
		"\3\2\2\2\u06c9\u06ca\3\2\2\2\u06ca\u06cb\3\2\2\2\u06cb\u06ce\5x=\2\u06cc"+
		"\u06cd\7}\2\2\u06cd\u06cf\7\u008f\2\2\u06ce\u06cc\3\2\2\2\u06ce\u06cf"+
		"\3\2\2\2\u06cf\u06d0\3\2\2\2\u06d0\u06d1\7{\2\2\u06d1\u0095\3\2\2\2\u06d2"+
		"\u06d4\5\u009aN\2\u06d3\u06d2\3\2\2\2\u06d4\u06d7\3\2\2\2\u06d5\u06d3"+
		"\3\2\2\2\u06d5\u06d6\3\2\2\2\u06d6\u06d8\3\2\2\2\u06d7\u06d5\3\2\2\2\u06d8"+
		"\u06f0\5`\61\2\u06d9\u06db\5\u009aN\2\u06da\u06d9\3\2\2\2\u06db\u06de"+
		"\3\2\2\2\u06dc\u06da\3\2\2\2\u06dc\u06dd\3\2\2\2\u06dd\u06df\3\2\2\2\u06de"+
		"\u06dc\3\2\2\2\u06df\u06f0\5d\63\2\u06e0\u06e2\5\u009aN\2\u06e1\u06e0"+
		"\3\2\2\2\u06e2\u06e5\3\2\2\2\u06e3\u06e1\3\2\2\2\u06e3\u06e4\3\2\2\2\u06e4"+
		"\u06e6\3\2\2\2\u06e5\u06e3\3\2\2\2\u06e6\u06f0\5h\65\2\u06e7\u06e9\5\u009a"+
		"N\2\u06e8\u06e7\3\2\2\2\u06e9\u06ec\3\2\2\2\u06ea\u06e8\3\2\2\2\u06ea"+
		"\u06eb\3\2\2\2\u06eb\u06ed\3\2\2\2\u06ec\u06ea\3\2\2\2\u06ed\u06f0\5|"+
		"?\2\u06ee\u06f0\7{\2\2\u06ef\u06d5\3\2\2\2\u06ef\u06dc\3\2\2\2\u06ef\u06e3"+
		"\3\2\2\2\u06ef\u06ea\3\2\2\2\u06ef\u06ee\3\2\2\2\u06f0\u0097\3\2\2\2\u06f1"+
		"\u06f4\5\u009aN\2\u06f2\u06f4\t\20\2\2\u06f3\u06f1\3\2\2\2\u06f3\u06f2"+
		"\3\2\2\2\u06f4\u0099\3\2\2\2\u06f5\u06f8\5\f\7\2\u06f6\u06f8\t\21\2\2"+
		"\u06f7\u06f5\3\2\2\2\u06f7\u06f6\3\2\2\2\u06f8\u009b\3\2\2\2\u06f9\u06fc"+
		"\7N\2\2\u06fa\u06fc\5\f\7\2\u06fb\u06f9\3\2\2\2\u06fb\u06fa\3\2\2\2\u06fc"+
		"\u009d\3\2\2\2\u06fd\u06fe\7\u0080\2\2\u06fe\u0703\5b\62\2\u06ff\u0700"+
		"\7|\2\2\u0700\u0702\5b\62\2\u0701\u06ff\3\2\2\2\u0702\u0705\3\2\2\2\u0703"+
		"\u0701\3\2\2\2\u0703\u0704\3\2\2\2\u0704\u0706\3\2\2\2\u0705\u0703\3\2"+
		"\2\2\u0706\u0707\7\177\2\2\u0707\u009f\3\2\2\2\u0708\u070d\5\u00c2b\2"+
		"\u0709\u070a\7\u0091\2\2\u070a\u070c\5\u00c2b\2\u070b\u0709\3\2\2\2\u070c"+
		"\u070f\3\2\2\2\u070d\u070b\3\2\2\2\u070d\u070e\3\2\2\2\u070e\u00a1\3\2"+
		"\2\2\u070f\u070d\3\2\2\2\u0710\u0715\5f\64\2\u0711\u0712\7|\2\2\u0712"+
		"\u0714\5f\64\2\u0713\u0711\3\2\2\2\u0714\u0717\3\2\2\2\u0715\u0713\3\2"+
		"\2\2\u0715\u0716\3\2\2\2\u0716\u00a3\3\2\2\2\u0717\u0715\3\2\2\2\u0718"+
		"\u071c\7{\2\2\u0719\u071b\5\b\5\2\u071a\u0719\3\2\2\2\u071b\u071e\3\2"+
		"\2\2\u071c\u071a\3\2\2\2\u071c\u071d\3\2\2\2\u071d\u00a5\3\2\2\2\u071e"+
		"\u071c\3\2\2\2\u071f\u0724\5\u00c2b\2\u0720\u0721\7|\2\2\u0721\u0723\5"+
		"\u00c2b\2\u0722\u0720\3\2\2\2\u0723\u0726\3\2\2\2\u0724\u0722\3\2\2\2"+
		"\u0724\u0725\3\2\2\2\u0725\u00a7\3\2\2\2\u0726\u0724\3\2\2\2\u0727\u072b"+
		"\7w\2\2\u0728\u072a\5\b\5\2\u0729\u0728\3\2\2\2\u072a\u072d\3\2\2\2\u072b"+
		"\u0729\3\2\2\2\u072b\u072c\3\2\2\2\u072c\u072e\3\2\2\2\u072d\u072b\3\2"+
		"\2\2\u072e\u072f\7x\2\2\u072f\u00a9\3\2\2\2\u0730\u0734\7w\2\2\u0731\u0733"+
		"\5\u00b2Z\2\u0732\u0731\3\2\2\2\u0733\u0736\3\2\2\2\u0734\u0732\3\2\2"+
		"\2\u0734\u0735\3\2\2\2\u0735\u0737\3\2\2\2\u0736\u0734\3\2\2\2\u0737\u0738"+
		"\7x\2\2\u0738\u00ab\3\2\2\2\u0739\u073a\5\u009eP\2\u073a\u073b\5j\66\2"+
		"\u073b\u00ad\3\2\2\2\u073c\u073d\5\u009eP\2\u073d\u073e\5l\67\2\u073e"+
		"\u00af\3\2\2\2\u073f\u0740\5\u00c2b\2\u0740\u0741\5\u00ba^\2\u0741\u0742"+
		"\7{\2\2\u0742\u00b1\3\2\2\2\u0743\u0745\5\u0098M\2\u0744\u0743\3\2\2\2"+
		"\u0745\u0748\3\2\2\2\u0746\u0744\3\2\2\2\u0746\u0747\3\2\2\2\u0747\u0749"+
		"\3\2\2\2\u0748\u0746\3\2\2\2\u0749\u074c\5\u00b4[\2\u074a\u074c\7{\2\2"+
		"\u074b\u0746\3\2\2\2\u074b\u074a\3\2\2\2\u074c\u00b3\3\2\2\2\u074d\u0755"+
		"\5\u00b6\\\2\u074e\u0755\5p9\2\u074f\u0755\5\u00b8]\2\u0750\u0755\5h\65"+
		"\2\u0751\u0755\5|?\2\u0752\u0755\5`\61\2\u0753\u0755\5d\63\2\u0754\u074d"+
		"\3\2\2\2\u0754\u074e\3\2\2\2\u0754\u074f\3\2\2\2\u0754\u0750\3\2\2\2\u0754"+
		"\u0751\3\2\2\2\u0754\u0752\3\2\2\2\u0754\u0753\3\2\2\2\u0755\u00b5\3\2"+
		"\2\2\u0756\u0757\5\u00c2b\2\u0757\u075c\5n8\2\u0758\u0759\7|\2\2\u0759"+
		"\u075b\5n8\2\u075a\u0758\3\2\2\2\u075b\u075e\3\2\2\2\u075c\u075a\3\2\2"+
		"\2\u075c\u075d\3\2\2\2\u075d\u075f\3\2\2\2\u075e\u075c\3\2\2\2\u075f\u0760"+
		"\7{\2\2\u0760\u00b7\3\2\2\2\u0761\u0762\5\u009eP\2\u0762\u0763\5p9\2\u0763"+
		"\u00b9\3\2\2\2\u0764\u0769\5\u00bc_\2\u0765\u0766\7|\2\2\u0766\u0768\5"+
		"\u00bc_\2\u0767\u0765\3\2\2\2\u0768\u076b\3\2\2\2\u0769\u0767\3\2\2\2"+
		"\u0769\u076a\3\2\2\2\u076a\u00bb\3\2\2\2\u076b\u0769\3\2\2\2\u076c\u076f"+
		"\5r:\2\u076d\u076e\7~\2\2\u076e\u0770\5\u00be`\2\u076f\u076d\3\2\2\2\u076f"+
		"\u0770\3\2\2\2\u0770\u00bd\3\2\2\2\u0771\u0774\5\u00c0a\2\u0772\u0774"+
		"\5\u0084C\2\u0773\u0771\3\2\2\2\u0773\u0772\3\2\2\2\u0774\u00bf\3\2\2"+
		"\2\u0775\u0781\7w\2\2\u0776\u077b\5\u00be`\2\u0777\u0778\7|\2\2\u0778"+
		"\u077a\5\u00be`\2\u0779\u0777\3\2\2\2\u077a\u077d\3\2\2\2\u077b\u0779"+
		"\3\2\2\2\u077b\u077c\3\2\2\2\u077c\u077f\3\2\2\2\u077d\u077b\3\2\2\2\u077e"+
		"\u0780\7|\2\2\u077f\u077e\3\2\2\2\u077f\u0780\3\2\2\2\u0780\u0782\3\2"+
		"\2\2\u0781\u0776\3\2\2\2\u0781\u0782\3\2\2\2\u0782\u0783\3\2\2\2\u0783"+
		"\u0784\7x\2\2\u0784\u00c1\3\2\2\2\u0785\u078a\5v<\2\u0786\u0787\7y\2\2"+
		"\u0787\u0789\7z\2\2\u0788\u0786\3\2\2\2\u0789\u078c\3\2\2\2\u078a\u0788"+
		"\3\2\2\2\u078a\u078b\3\2\2\2\u078b\u0796\3\2\2\2\u078c\u078a\3\2\2\2\u078d"+
		"\u0792\5\u00c4c\2\u078e\u078f\7y\2\2\u078f\u0791\7z\2\2\u0790\u078e\3"+
		"\2\2\2\u0791\u0794\3\2\2\2\u0792\u0790\3\2\2\2\u0792\u0793\3\2\2\2\u0793"+
		"\u0796\3\2\2\2\u0794\u0792\3\2\2\2\u0795\u0785\3\2\2\2\u0795\u078d\3\2"+
		"\2\2\u0796\u00c3\3\2\2\2\u0797\u0798\t\22\2\2\u0798\u00c5\3\2\2\2\u0799"+
		"\u079a\7\u0080\2\2\u079a\u079f\5\u00c8e\2\u079b\u079c\7|\2\2\u079c\u079e"+
		"\5\u00c8e\2\u079d\u079b\3\2\2\2\u079e\u07a1\3\2\2\2\u079f\u079d\3\2\2"+
		"\2\u079f\u07a0\3\2\2\2\u07a0\u07a2\3\2\2\2\u07a1\u079f\3\2\2\2\u07a2\u07a3"+
		"\7\177\2\2\u07a3\u00c7\3\2\2\2\u07a4\u07ab\5\u00c2b\2\u07a5\u07a8\7\u0083"+
		"\2\2\u07a6\u07a7\t\23\2\2\u07a7\u07a9\5\u00c2b\2\u07a8\u07a6\3\2\2\2\u07a8"+
		"\u07a9\3\2\2\2\u07a9\u07ab\3\2\2\2\u07aa\u07a4\3\2\2\2\u07aa\u07a5\3\2"+
		"\2\2\u07ab\u00c9\3\2\2\2\u07ac\u07b1\5x=\2\u07ad\u07ae\7|\2\2\u07ae\u07b0"+
		"\5x=\2\u07af\u07ad\3\2\2\2\u07b0\u07b3\3\2\2\2\u07b1\u07af\3\2\2\2\u07b1"+
		"\u07b2\3\2\2\2\u07b2\u00cb\3\2\2\2\u07b3\u07b1\3\2\2\2\u07b4\u07b6\7u"+
		"\2\2\u07b5\u07b7\5\u00ceh\2\u07b6\u07b5\3\2\2\2\u07b6\u07b7\3\2\2\2\u07b7"+
		"\u07b8\3\2\2\2\u07b8\u07b9\7v\2\2\u07b9\u00cd\3\2\2\2\u07ba\u07bf\5\u00d0"+
		"i\2\u07bb\u07bc\7|\2\2\u07bc\u07be\5\u00d0i\2\u07bd\u07bb\3\2\2\2\u07be"+
		"\u07c1\3\2\2\2\u07bf\u07bd\3\2\2\2\u07bf\u07c0\3\2\2\2\u07c0\u07c4\3\2"+
		"\2\2\u07c1\u07bf\3\2\2\2\u07c2\u07c3\7|\2\2\u07c3\u07c5\5\u00d2j\2\u07c4"+
		"\u07c2\3\2\2\2\u07c4\u07c5\3\2\2\2\u07c5\u07c8\3\2\2\2\u07c6\u07c8\5\u00d2"+
		"j\2\u07c7\u07ba\3\2\2\2\u07c7\u07c6\3\2\2\2\u07c8\u00cf\3\2\2\2\u07c9"+
		"\u07cb\5\u009cO\2\u07ca\u07c9\3\2\2\2\u07cb\u07ce\3\2\2\2\u07cc\u07ca"+
		"\3\2\2\2\u07cc\u07cd\3\2\2\2\u07cd\u07cf\3\2\2\2\u07ce\u07cc\3\2\2\2\u07cf"+
		"\u07d0\5\u00c2b\2\u07d0\u07d1\5r:\2\u07d1\u00d1\3\2\2\2\u07d2\u07d4\5"+
		"\u009cO\2\u07d3\u07d2\3\2\2\2\u07d4\u07d7\3\2\2\2\u07d5\u07d3\3\2\2\2"+
		"\u07d5\u07d6\3\2\2\2\u07d6\u07d8\3\2\2\2\u07d7\u07d5\3\2\2\2\u07d8\u07d9"+
		"\5\u00c2b\2\u07d9\u07da\7\u00a1\2\2\u07da\u07db\5r:\2\u07db\u00d3\3\2"+
		"\2\2\u07dc\u07dd\5\u00eex\2\u07dd\u00d5\3\2\2\2\u07de\u07df\5\u00eex\2"+
		"\u07df\u00d7\3\2\2\2\u07e0\u07e1\t\24\2\2\u07e1\u00d9\3\2\2\2\u07e2\u07e3"+
		"\5x=\2\u07e3\u00db\3\2\2\2\u07e4\u07e9\5z>\2\u07e5\u07e6\7|\2\2\u07e6"+
		"\u07e8\5z>\2\u07e7\u07e5\3\2\2\2\u07e8\u07eb\3\2\2\2\u07e9\u07e7\3\2\2"+
		"\2\u07e9\u07ea\3\2\2\2\u07ea\u00dd\3\2\2\2\u07eb\u07e9\3\2\2\2\u07ec\u07f0"+
		"\5\u0084C\2\u07ed\u07f0\5\f\7\2\u07ee\u07f0\5\u00e0q\2\u07ef\u07ec\3\2"+
		"\2\2\u07ef\u07ed\3\2\2\2\u07ef\u07ee\3\2\2\2\u07f0\u00df\3\2\2\2\u07f1"+
		"\u07fa\7w\2\2\u07f2\u07f7\5\u00dep\2\u07f3\u07f4\7|\2\2\u07f4\u07f6\5"+
		"\u00dep\2\u07f5\u07f3\3\2\2\2\u07f6\u07f9\3\2\2\2\u07f7\u07f5\3\2\2\2"+
		"\u07f7\u07f8\3\2\2\2\u07f8\u07fb\3\2\2\2\u07f9\u07f7\3\2\2\2\u07fa\u07f2"+
		"\3\2\2\2\u07fa\u07fb\3\2\2\2\u07fb\u07fd\3\2\2\2\u07fc\u07fe\7|\2\2\u07fd"+
		"\u07fc\3\2\2\2\u07fd\u07fe\3\2\2\2\u07fe\u07ff\3\2\2\2\u07ff\u0800\7x"+
		"\2\2\u0800\u00e1\3\2\2\2\u0801\u0805\7w\2\2\u0802\u0804\5\u00e4s\2\u0803"+
		"\u0802\3\2\2\2\u0804\u0807\3\2\2\2\u0805\u0803\3\2\2\2\u0805\u0806\3\2"+
		"\2\2\u0806\u0808\3\2\2\2\u0807\u0805\3\2\2\2\u0808\u0809\7x\2\2\u0809"+
		"\u00e3\3\2\2\2\u080a\u080c\5\u0098M\2\u080b\u080a\3\2\2\2\u080c\u080f"+
		"\3\2\2\2\u080d\u080b\3\2\2\2\u080d\u080e\3\2\2\2\u080e\u0810\3\2\2\2\u080f"+
		"\u080d\3\2\2\2\u0810\u0813\5\u00e6t\2\u0811\u0813\7{\2\2\u0812\u080d\3"+
		"\2\2\2\u0812\u0811\3\2\2\2\u0813\u00e5\3\2\2\2\u0814\u0815\5\u00c2b\2"+
		"\u0815\u0816\5\u00e8u\2\u0816\u0817\7{\2\2\u0817\u0829\3\2\2\2\u0818\u081a"+
		"\5`\61\2\u0819\u081b\7{\2\2\u081a\u0819\3\2\2\2\u081a\u081b\3\2\2\2\u081b"+
		"\u0829\3\2\2\2\u081c\u081e\5h\65\2\u081d\u081f\7{\2\2\u081e\u081d\3\2"+
		"\2\2\u081e\u081f\3\2\2\2\u081f\u0829\3\2\2\2\u0820\u0822\5d\63\2\u0821"+
		"\u0823\7{\2\2\u0822\u0821\3\2\2\2\u0822\u0823\3\2\2\2\u0823\u0829\3\2"+
		"\2\2\u0824\u0826\5|?\2\u0825\u0827\7{\2\2\u0826\u0825\3\2\2\2\u0826\u0827"+
		"\3\2\2\2\u0827\u0829\3\2\2\2\u0828\u0814\3\2\2\2\u0828\u0818\3\2\2\2\u0828"+
		"\u081c\3\2\2\2\u0828\u0820\3\2\2\2\u0828\u0824\3\2\2\2\u0829\u00e7\3\2"+
		"\2\2\u082a\u082d\5~@\2\u082b\u082d\5\u00eav\2\u082c\u082a\3\2\2\2\u082c"+
		"\u082b\3\2\2\2\u082d\u00e9\3\2\2\2\u082e\u082f\5\u00ba^\2\u082f\u00eb"+
		"\3\2\2\2\u0830\u0831\7H\2\2\u0831\u0832\5\u00dep\2\u0832\u00ed\3\2\2\2"+
		"\u0833\u0837\7w\2\2\u0834\u0836\5\u00f0y\2\u0835\u0834\3\2\2\2\u0836\u0839"+
		"\3\2\2\2\u0837\u0835\3\2\2\2\u0837\u0838\3\2\2\2\u0838\u083a\3\2\2\2\u0839"+
		"\u0837\3\2\2\2\u083a\u083b\7x\2\2\u083b\u00ef\3\2\2\2\u083c\u0840\5\u00f2"+
		"z\2\u083d\u0840\5\u0080A\2\u083e\u0840\5\u0096L\2\u083f\u083c\3\2\2\2"+
		"\u083f\u083d\3\2\2\2\u083f\u083e\3\2\2\2\u0840\u00f1\3\2\2\2\u0841\u0842"+
		"\5\u00f4{\2\u0842\u0843\7{\2\2\u0843\u00f3\3\2\2\2\u0844\u0846\5\u009c"+
		"O\2\u0845\u0844\3\2\2\2\u0846\u0849\3\2\2\2\u0847\u0845\3\2\2\2\u0847"+
		"\u0848\3\2\2\2\u0848\u084a\3\2\2\2\u0849\u0847\3\2\2\2\u084a\u084b\5\u00c2"+
		"b\2\u084b\u084c\5\u00ba^\2\u084c\u00f5\3\2\2\2\u084d\u0852\5x=\2\u084e"+
		"\u084f\7\u0092\2\2\u084f\u0851\5x=\2\u0850\u084e\3\2\2\2\u0851\u0854\3"+
		"\2\2\2\u0852\u0850\3\2\2\2\u0852\u0853\3\2\2\2\u0853\u00f7\3\2\2\2\u0854"+
		"\u0852\3\2\2\2\u0855\u0856\7O\2\2\u0856\u0857\5\u00eex\2\u0857\u00f9\3"+
		"\2\2\2\u0858\u0859\7u\2\2\u0859\u085b\5\u00fc\177\2\u085a\u085c\7{\2\2"+
		"\u085b\u085a\3\2\2\2\u085b\u085c\3\2\2\2\u085c\u085d\3\2\2\2\u085d\u085e"+
		"\7v\2\2\u085e\u00fb\3\2\2\2\u085f\u0864\5\u00fe\u0080\2\u0860\u0861\7"+
		"{\2\2\u0861\u0863\5\u00fe\u0080\2\u0862\u0860\3\2\2\2\u0863\u0866\3\2"+
		"\2\2\u0864\u0862\3\2\2\2\u0864\u0865\3\2\2\2\u0865\u00fd\3\2\2\2\u0866"+
		"\u0864\3\2\2\2\u0867\u0869\5\u009cO\2\u0868\u0867\3\2\2\2\u0869\u086c"+
		"\3\2\2\2\u086a\u0868\3\2\2\2\u086a\u086b\3\2\2\2\u086b\u086d\3\2\2\2\u086c"+
		"\u086a\3\2\2\2\u086d\u086e\5v<\2\u086e\u086f\5r:\2\u086f\u0870\7~\2\2"+
		"\u0870\u0871\5\u0084C\2\u0871\u00ff\3\2\2\2\u0872\u0874\5\u0102\u0082"+
		"\2\u0873\u0872\3\2\2\2\u0874\u0875\3\2\2\2\u0875\u0873\3\2\2\2\u0875\u0876"+
		"\3\2\2\2\u0876\u0878\3\2\2\2\u0877\u0879\5\u00f0y\2\u0878\u0877\3\2\2"+
		"\2\u0879\u087a\3\2\2\2\u087a\u0878\3\2\2\2\u087a\u087b\3\2\2\2\u087b\u0101"+
		"\3\2\2\2\u087c\u087d\7B\2\2\u087d\u087e\5\u0112\u008a\2\u087e\u087f\7"+
		"\u0084\2\2\u087f\u0887\3\2\2\2\u0880\u0881\7B\2\2\u0881\u0882\5t;\2\u0882"+
		"\u0883\7\u0084\2\2\u0883\u0887\3\2\2\2\u0884\u0885\7H\2\2\u0885\u0887"+
		"\7\u0084\2\2\u0886\u087c\3\2\2\2\u0886\u0880\3\2\2\2\u0886\u0884\3\2\2"+
		"\2\u0887\u0103\3\2\2\2\u0888\u0895\5\u0108\u0085\2\u0889\u088b\5\u0106"+
		"\u0084\2\u088a\u0889\3\2\2\2\u088a\u088b\3\2\2\2\u088b\u088c\3\2\2\2\u088c"+
		"\u088e\7{\2\2\u088d\u088f\5\u0084C\2\u088e\u088d\3\2\2\2\u088e\u088f\3"+
		"\2\2\2\u088f\u0890\3\2\2\2\u0890\u0892\7{\2\2\u0891\u0893\5\u010a\u0086"+
		"\2\u0892\u0891\3\2\2\2\u0892\u0893\3\2\2\2\u0893\u0895\3\2\2\2\u0894\u0888"+
		"\3\2\2\2\u0894\u088a\3\2\2\2\u0895\u0105\3\2\2\2\u0896\u0899\5\u00f4{"+
		"\2\u0897\u0899\5\u010e\u0088\2\u0898\u0896\3\2\2\2\u0898\u0897\3\2\2\2"+
		"\u0899\u0107\3\2\2\2\u089a\u089c\5\u009cO\2\u089b\u089a\3\2\2\2\u089c"+
		"\u089f\3\2\2\2\u089d\u089b\3\2\2\2\u089d\u089e\3\2\2\2\u089e\u08a0\3\2"+
		"\2\2\u089f\u089d\3\2\2\2\u08a0\u08a1\5\u00c2b\2\u08a1\u08a2\5r:\2\u08a2"+
		"\u08a3\7\u0084\2\2\u08a3\u08a4\5\u0084C\2\u08a4\u0109\3\2\2\2\u08a5\u08a6"+
		"\5\u010e\u0088\2\u08a6\u010b\3\2\2\2\u08a7\u08a8\7u\2\2\u08a8\u08a9\5"+
		"\u0084C\2\u08a9\u08aa\7v\2\2\u08aa\u010d\3\2\2\2\u08ab\u08b0\5\u0084C"+
		"\2\u08ac\u08ad\7|\2\2\u08ad\u08af\5\u0084C\2\u08ae\u08ac\3\2\2\2\u08af"+
		"\u08b2\3\2\2\2\u08b0\u08ae\3\2\2\2\u08b0\u08b1\3\2\2\2\u08b1\u010f\3\2"+
		"\2\2\u08b2\u08b0\3\2\2\2\u08b3\u08b4\5\u0084C\2\u08b4\u0111\3\2\2\2\u08b5"+
		"\u08b6\5\u0084C\2\u08b6\u0113\3\2\2\2\u08b7\u08b8\5\u011c\u008f\2\u08b8"+
		"\u08b9\5\u0088E\2\u08b9\u08ba\5\u0118\u008d\2\u08ba\u08c1\3\2\2\2\u08bb"+
		"\u08be\5\u0088E\2\u08bc\u08bf\5\u0116\u008c\2\u08bd\u08bf\5\u0118\u008d"+
		"\2\u08be\u08bc\3\2\2\2\u08be\u08bd\3\2\2\2\u08bf\u08c1\3\2\2\2\u08c0\u08b7"+
		"\3\2\2\2\u08c0\u08bb\3\2\2\2\u08c1\u0115\3\2\2\2\u08c2\u08de\7y\2\2\u08c3"+
		"\u08c8\7z\2\2\u08c4\u08c5\7y\2\2\u08c5\u08c7\7z\2\2\u08c6\u08c4\3\2\2"+
		"\2\u08c7\u08ca\3\2\2\2\u08c8\u08c6\3\2\2\2\u08c8\u08c9\3\2\2\2\u08c9\u08cb"+
		"\3\2\2\2\u08ca\u08c8\3\2\2\2\u08cb\u08df\5\u00c0a\2\u08cc\u08cd\5\u0084"+
		"C\2\u08cd\u08d4\7z\2\2\u08ce\u08cf\7y\2\2\u08cf\u08d0\5\u0084C\2\u08d0"+
		"\u08d1\7z\2\2\u08d1\u08d3\3\2\2\2\u08d2\u08ce\3\2\2\2\u08d3\u08d6\3\2"+
		"\2\2\u08d4\u08d2\3\2\2\2\u08d4\u08d5\3\2\2\2\u08d5\u08db\3\2\2\2\u08d6"+
		"\u08d4\3\2\2\2\u08d7\u08d8\7y\2\2\u08d8\u08da\7z\2\2\u08d9\u08d7\3\2\2"+
		"\2\u08da\u08dd\3\2\2\2\u08db\u08d9\3\2\2\2\u08db\u08dc\3\2\2\2\u08dc\u08df"+
		"\3\2\2\2\u08dd\u08db\3\2\2\2\u08de\u08c3\3\2\2\2\u08de\u08cc\3\2\2\2\u08df"+
		"\u0117\3\2\2\2\u08e0\u08e2\5\u0122\u0092\2\u08e1\u08e3\5\u00a8U\2\u08e2"+
		"\u08e1\3\2\2\2\u08e2\u08e3\3\2\2\2\u08e3\u0119\3\2\2\2\u08e4\u08e5\5\u011c"+
		"\u008f\2\u08e5\u08e6\5\u008eH\2\u08e6\u011b\3\2\2\2\u08e7\u08e8\7\u0080"+
		"\2\2\u08e8\u08e9\5\u00a6T\2\u08e9\u08ea\7\177\2\2\u08ea\u011d\3\2\2\2"+
		"\u08eb\u08ec\7\u0080\2\2\u08ec\u08ef\7\177\2\2\u08ed\u08ef\5\u00c6d\2"+
		"\u08ee\u08eb\3\2\2\2\u08ee\u08ed\3\2\2\2\u08ef\u011f\3\2\2\2\u08f0\u08f1"+
		"\7\u0080\2\2\u08f1\u08f4\7\177\2\2\u08f2\u08f4\5\u011c\u008f\2\u08f3\u08f0"+
		"\3\2\2\2\u08f3\u08f2\3\2\2\2\u08f4\u0121\3\2\2\2\u08f5\u08f7\7u\2\2\u08f6"+
		"\u08f8\5\u010e\u0088\2\u08f7\u08f6\3\2\2\2\u08f7\u08f8\3\2\2\2\u08f8\u08f9"+
		"\3\2\2\2\u08f9\u08fa\7v\2\2\u08fa\u0123\3\2\2\2\u00fd\u0125\u012a\u0131"+
		"\u0135\u0138\u0140\u0149\u014d\u0153\u0157\u0163\u016a\u016d\u01a7\u01f4"+
		"\u01f8\u01ff\u020a\u020e\u0213\u0222\u0225\u022c\u022f\u0233\u0237\u0255"+
		"\u025b\u0266\u0270\u0275\u027d\u0285\u0287\u02cc\u0302\u0307\u030f\u0314"+
		"\u031c\u0323\u032a\u032f\u0337\u033e\u0347\u034e\u0353\u035a\u035e\u03b2"+
		"\u03ba\u03bf\u03c7\u03c9\u03ce\u03d4\u03dc\u03de\u03e1\u03e5\u03eb\u03ee"+
		"\u03f1\u03f4\u03fa\u03ff\u0405\u0411\u0415\u041d\u0421\u0423\u0427\u042a"+
		"\u042d\u0433\u0438\u043b\u0441\u044b\u0453\u0459\u0460\u0466\u0470\u0474"+
		"\u0477\u047c\u0481\u0484\u048a\u0490\u0497\u049f\u04a3\u04a9\u04ad\u04b2"+
		"\u04b9\u04c1\u04c4\u04cb\u04d3\u04d6\u04da\u04e1\u04e6\u04eb\u04ef\u04f3"+
		"\u04fa\u0500\u0504\u0507\u050a\u0511\u0516\u0519\u051e\u0522\u0528\u0530"+
		"\u0535\u0539\u053f\u0548\u0550\u0558\u055d\u0566\u056d\u0572\u0576\u057e"+
		"\u058e\u0595\u059e\u05b5\u05b8\u05bb\u05c3\u05c7\u05cf\u05d5\u05e0\u05e9"+
		"\u05ee\u05f9\u0600\u0615\u0625\u0650\u0662\u066a\u066c\u0682\u0684\u0688"+
		"\u068d\u0691\u0695\u0699\u06a1\u06a3\u06aa\u06ad\u06b2\u06b8\u06c0\u06c9"+
		"\u06ce\u06d5\u06dc\u06e3\u06ea\u06ef\u06f3\u06f7\u06fb\u0703\u070d\u0715"+
		"\u071c\u0724\u072b\u0734\u0746\u074b\u0754\u075c\u0769\u076f\u0773\u077b"+
		"\u077f\u0781\u078a\u0792\u0795\u079f\u07a8\u07aa\u07b1\u07b6\u07bf\u07c4"+
		"\u07c7\u07cc\u07d5\u07e9\u07ef\u07f7\u07fa\u07fd\u0805\u080d\u0812\u081a"+
		"\u081e\u0822\u0826\u0828\u082c\u0837\u083f\u0847\u0852\u085b\u0864\u086a"+
		"\u0875\u087a\u0886\u088a\u088e\u0892\u0894\u0898\u089d\u08b0\u08be\u08c0"+
		"\u08c8\u08d4\u08db\u08de\u08e2\u08ee\u08f3\u08f7";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}