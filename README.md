# AspectJ Language Server
This project has the purpose to create a Language Server, as specified by the [Language Server Protocol](https://microsoft.github.io/language-server-protocol/), for the AspectJ language.
The project was carried out as part of the TSP course (Tecniche Speciali di Programmazione) held at the University of Milan.

The server provides three functionalities:
- autocompletion
- validation
- weaving info calculation

The client-server communication, which is compliant to the Language Server Protocol, is managed through the [LSP4J](https://github.com/eclipse/lsp4j) library.
Moreover, the three functionalities mentioned before are implemented employing the [aspectjtools](https://javadoc.io/doc/org.aspectj/aspectjtools/latest/index.html) library e the [ANTLR4 parser generator](https://www.antlr.org/).

## Install
```bash
mvn clean install
```

## Run
If you want you can run the server with
```bash
$ java -cp launcher/target/launcher.jar ServerLauncher
```
however, usually you will use a client application like the [VS Code AspectJ Extension](https://gitlab.com/tsp_project/aspectj_extension), which will be responsible to launch the server.

## Functionalities
The first two functionalities of this project were developed exploiting an AspectJ parser generated with ANTLR4.
For more information about the grammars and the parser's generation, please read the [grammars' README](language-server/src/main/java/it/unimi/di/parser/grammar/README.md).

### Autocompletion
The implementation of this functionality exploits the internals of the ANTLR4's parser, in particular the [Augmented Transition Network](https://www.antlr.org/api/Java/org/antlr/v4/runtime/atn/ATNState.html) ([ANTLR4 runtime API](https://www.antlr.org/api/Java/)), which represents an automata that can change state through epsilon transitions or when a certain token is received.
The idea is to process all the tokens from the beginnning of the file until the caret position.
At that point, the system looks at which transitions are available at the current state of the ATN: the tokens used by those transitions are the tokens that the parser would expect and therefore they are also the tokens that should be suggested to the user.
This idea is explained in [this article](https://tomassetti.me/autocompletion-editor-antlr/) and the relative code (in Kotlin) is available at this [repo's commit](https://github.com/ftomassetti/kanvas/tree/3b917087914b3c906c72ef58b40bc555a211c9fd).
It took some effort to adapt the code from Kotlin to Java and also to adapt it to the AspectJ language.

Finally, the autocompletion system is also capable to suggest some basic code snippets, like the `aspect` or `pointcut` snippets.

#### Example
![Autcompletion example](.gifs/autocompletion.gif)

### Validation
This functionality was implemented defining an appropriate [ANTLRErrorListener](https://www.antlr.org/api/Java/org/antlr/v4/runtime/BaseErrorListener.html) which collects syntax errors during parsing.
Any syntax error collected during parsing is then sent to the client through the diagnostic publishing system provided by LSP4J.

#### Example
![Validation example](.gifs/validation.gif)

### Weaveing Info Calculation
This feature was implememented exploiting the APIs provided by the [aspectjtools package](https://javadoc.io/doc/org.aspectj/aspectjtools/latest/index.html).

When the client wants to calculate weaving information, it sends a specific request to the server called `calculateWeaveInfo`.
At that point, the server saves all the open files locally in a temporary directory.
These files are then compiled throught `ajc`, which is accessed programmatically thanks to the `aspectjtools` package.
Specifically, compilation is launched with the `-showWeaveInfo` option enabled and all the weaving information messages that comes out are collected by the language server.
After this, every time the client sends an hover request regarding one of the code lines affected by weaving operations, the server will answer with proper weaving information.
However, if some modification happens in the code, hover information is disabled and the mechanism has to be relaunched.

This approach, i.e. exploiting directly `ajc` to collect weaving information, has the advantage to be fully consistent to the results `ajc` would return to the user during compile time.

Be aware that the calculation will take into account only those files which are currently open in the editor's window.

At the moment this mechanism is limited to the collection of weaving information about advice only.

#### Example

- File [`SimpleClass.java`](.example/SimpleClass.java):
```java
public class SimpleClass{

  public void hello(){
    System.out.println("Hello!");
  }
  
  public void bye(){
    System.out.println("Bye!");
  }
}
```

- File [`LoggerAspect.aj`](.example/LoggerAspect.aj):
```aspectj
aspect LoggerAspect{

  public void log(String message){
    System.out.println("[LOG] "+message);
  }

  pointcut hello() :
    execution(* *.hello(..)) ;

  pointcut bye() :
    execution(* *.bye(..)) ;    

  before() : hello() {
    log("Entering hello method!");
  }

  after() : hello() {
    log("Exiting hello method!");
  }
  
  void around() : bye() {
  	log("Entering bye method!");
  	proceed();
  	log("Exiting bye method!");
  }
}
```

![Weaving Info Calculation example](.gifs/weave_info_calculation.gif)

#### Possible improvements
Currently, weaving information is extracted directly from messages' strings of `ajc`.
A cleaner way to do that could be to define a proper aspect containing an advice which intercepts calls to `WeaveMessage.constructWeavingMessage()`, showed below
```java
public static WeaveMessage constructWeavingMessage(WeaveMessageKind kind, String[] inserts, String affectedtypename, String aspectname) {
    ...
}
```
which is responsible to build the weaving messages.
In this way information could be directly obtained from the `inserts` array, rather than extracting it from the, already built, final message.
However some problems emerged when I tried to integrate AspectJ in this project.

## Other resources
- Implementing a Language Server…How Hard Can It Be?? [[Part 1](https://medium.com/ballerina-techblog/implementing-a-language-server-how-hard-can-it-be-part-1-introduction-c915d2437076), [Part 2](https://medium.com/ballerina-techblog/implementing-a-language-server-how-hard-can-it-be-part-2-fa65a741aa23), [Part 3](https://medium.com/ballerina-techblog/implementing-a-language-server-how-hard-can-it-be-part-3-7269962498ac)]

